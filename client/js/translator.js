
// https://github.com/thedaviddelta/lingva-translate

const translator = {};

translator.lingvaInstances = [
    "https://lingva.ml",
    "https://translate.igna.wtf",
    "https://translate.plausibility.cloud",
    "https://lingva.lunar.icu",
    "https://translate.projectsegfau.lt",
    "https://translate.dr460nf1r3.org",
    "https://lingva.garudalinux.org",
    "https://translate.jae.fi",
];

translator.returnModel = 
{
    translation : String,
    sourceLang  : String,
    instance    : String
}

translator.translate = async function(params = {})
{
    const source = params.source || "auto";
    const target = params.target;
    const string = params.string;
    const instance = params.instance || translator.lingvaInstances[0];


    return new Promise((resolve, reject) => {
        const q = encodeURIComponent(string.trim());
        const url = instance + `/api/v1/${source}/${target}/${q}`
        //const url ="https://asdadasd";
        console.log("Using instance " + instance);
        fetch(url)
            .then((res) => {
                if (res.ok) {
                    return res.json()
                }
                return null;
            }).then((data) => {
                if (!data) return;
                const result = Object.assign(translator.returnModel, {
                    translation: data.translation,
                    sourceLang: data.detectedSource,
                    instance: instance,
                });
				console.log(result)
                return resolve(result);
            }).catch((e) => {
                console.log("Translation error:")
                console.log(e);
                const instanceIndex = translator.lingvaInstances.findIndex((it) => it === instance);
                if (instanceIndex >= 0 && !!translator.lingvaInstances[instanceIndex+1])
                {
                    console.info("Trying next instance");
                    params.instance = translator.lingvaInstances[instanceIndex+1];
                    return translator.translate(params).then((res) => resolve(res))
                }

                return reject(new Error("Translation error"));
            })
            
    })//.then(() => console.log("Translated"));
};
