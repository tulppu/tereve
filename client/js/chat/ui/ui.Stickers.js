
"use strict";


document.addEventListener("DOMContentLoaded", () => stickers.init(), false);


function stickerToBlob(image)
{
    return new Promise((resolve, reject) => {
        const canvas = document.createElement("canvas");

        const context = canvas.getContext("2d"); 
        context.globalAlpha = 1;

        canvas.width = image.naturalWidth;
        canvas.height = image.naturalHeight;

        context.drawImage(image, 0, 0);

        canvas.toBlob((blob) => resolve(blob));
    });
}

const stickers = {};

stickers.categories = {};
stickers.categories.ALL = "all";
stickers.categories.FAVORITES = "favorites";

Object.freeze(stickers.categories);


stickers.css = `

#stickerList {
    display: flex;
    flex-wrap: wrap;
    overflow auto;
    width: max(55vw, min(95vw, 600px));
    height: 90vh;
    overflow: auto;
    margin: 3px auto auto;
    background: white;
    border: 1px solid black;
    align-content: baseline;
    justify-content: space-around;
}

#stickerList img.sticker {
    width: 100%;
    height: 100%;
    object-fit: contain;
}
#stickerList .stickerWrapper:hover img.sticker  {
    filter: drop-shadow(1px 1px #917e7e) brightness(105%);
}

#stickerList span.stickerWrapper {
    display: inline-block;
    margin: min(3px, 1vw);
    width: min(100px, 35vw);
    height: 100px;
    position: relative;
}
#stickerList .sticker-star {
    position: absolute;
    right: 1px;
    top: 1px;
    filter: drop-shadow(1px 2px white) opacity(0.4);
	border-radius: 20px;
	padding: 5px;
	z-index: 500;
	border: 1px solid rgb(0,0,0,0.0);
}

#stickerList .stickerWrapper:hover .sticker-star {
    filter: drop-shadow(1px 1px #e2c4c4) opacity(0.8) !important;
    box-shadow: 2px 2px 2px #d8cfcf57;
	background: rgb(255, 255, 255, 0.4);
}

#stickerList .stickerWrapper:hover .sticker-star:hover {
/*#stickerList .sticker-star:hover {*/
    filter: drop-shadow(1px 1px #e2c4c4) opacity(1) !important;
	background: rgb(255, 255, 255, 0.9) !important;
	border: 1px solid rgb(0,0,0,0.2);
}

.sticker-star {
    width: 16px;
    height: 16px;
    z-index: 5000;
}

.sticker-star.unfavorited::before {
    content: url("img/icons/non-starred-symbolic.svg");
    width: 16px;
    height: 16px;
    z-index: 5000;

}

.sticker-star.favorited::before {
    content: url("img/icons/starred-symbolic.svg");
    width: 16px;
    height: 16px;
    z-index: 5000;
}


select#stickerCategorySelection option,
select#stickerCategorySelection  {
    background: white;
    color: black;
}

select#stickerCategorySelection option {
    padding: 2px;
}

div#stickersModal #inputWrapper {
    background: white;
    z-index: 1000;
    top: unset !important;
    top: 1% !important;
    filter: opacity(0.95);
}

#stickerList {
    margin-top: auto!important;
    padding: 25px;
	position: relative;
}


select#stickerCategorySelection {
    padding: 0 8px 0 8px;
    text-align: center;
}


#stickersModal {
    position: absolute;
    left: 0px;
    top: 0px;
    display: flex;
    align-items: center;
    justify-content: end;
    flex-direction: column;
    width: 100vw;
    height: 100vh;
    background: rgba(0, 0, 0, 0.1);
}

#stickersModal.hidden {
    display: none;
}

#stickersHeader {
    margin-top: auto;
    padding: 8px;
	text-align: center;
	position: fixed;
	z-index: 5000;
	top: 2%;
}

#inputWrapper {
    position: relative;
}

#stickerFilter {
    text-align: center;
	outline: none;
	margin-left: 2px;
}

#stickersHeader .hidden {
    display: none;
}

#stickersHeader #reset {
    position: absolute;
    right: 7px;
    bottom: 2px;
    cursor: pointer;
    font-weight: bold;
    color: #bb585857;
}

#stickersHeader #closeStickers {
	position: fixed;
	display: none;
}

@media only screen and (max-width: 700px) {
	#stickersHeader #closeStickers {
		display: block;
		right: 3%;
		top: calc(1% + 15px);
		font-weight: bold;
		font-size: 20px;
		color: #8f6363;
		background: #00000012;
		padding: 0 5px 0 5px;
		border-radius: 4rem;
		cursor: pointer;
		z-index: 6000;;
	}
	#stickersHeader #closeStickers :hover {
		filter: brightness(105%);
	}
}


`;

stickers.iconStar = new Image(16, 16);

stickers.favorites = {};
stickers.favorites.map = new Map();

stickers.favorites.add = function(sticker) {
    stickers.favorites.map.set(sticker.key, sticker);
    stickers.favorites.save();
}

stickers.favorites.remove = function(sticker) {
    stickers.favorites.map.delete(sticker.key);
    stickers.favorites.save();
}

stickers.favorites.has = function(sticker) {
    if (!sticker || typeof sticker.key === "undefined") {
        throw new Error("Bad sticker!");
    }
    return !!stickers.favorites.map.has(sticker.key);
}

stickers.favorites.load = function() {
    if (!localStorage.favorite_stickers) {
        return;
    }
    try {
        stickers.favorites.map =  new Map(JSON.parse(localStorage.favorite_stickers));
    } catch (e) {
        console.error(e);
    }
}

stickers.favorites.save = function() {
    const favorites = stickers.favorites.map;

    if (!favorites.size) {
        delete localStorage.favorite_stickers;
        return;
    }

    try {
        const json = JSON.stringify(Array.from(stickers.favorites.map.entries()));
        localStorage.favorite_stickers = json;
    } catch (e) {
        console.error(e);
    }
}

stickers.getSelectedCategory = function() {
    return stickers.modal.header.select.value;
}

stickers.getSortedListItemIndex = function(stickerKey, list) {

    if (!list.length) return -3;

    let left = 0;
    let right = list.length - 1;

    if (stickerKey < list[left].key) return -2;
    if (stickerKey > list[right].key) return -2;

    while (left <= right) {
        const middle = Math.floor(left + ((right - left) / 2));
        if (list[middle].key === stickerKey) {
            return middle;
        }
        
        if (stickerKey.localeCompare(list[middle].key) === -1) {
            right = middle-1;
        } else {
            left = middle+1;
        }

    }
    return -1;
}

stickers.getSortedListItem = function(stickerKey, list) {
    const index = stickers.getSortedListItemIndex(stickerKey, list);
    if (index >= 0) {
        return list.at(index);
    }
    return null;
}



stickers.isViewed = (() => stickers.modal.classList.contains("hidden") === false);
stickers.locallyStored = (() => configs.getConfig("store_stickers")); 

stickers.init = async function() {

    window.addEventListener("hashchange", (e) => {
        if (stickers.isViewed()) {
            stickers.close();
            e.preventDefault();
       }
    });

    stickers.favorites.load();

    configs.getConfigElement("store_stickers").addEventListener("change", async(e) => {

        const isDblClick = e.target.lastClicked + 500 > Date.now();

        if (!e.target.checked && isDblClick) {
            const clearReq = (await _chatGlobal.db.getStore("stickers")).clear();
            clearReq.onsuccess = (() => {
                chatFeeds.addItem({
                    message: "Cleared stickers store",
                })
        
            })
        }
        e.target.lastClicked = Date.now()
    });

	stickers.displayedCategory = localStorage.stickerCategory || stickers.displayedCategory;

    const modal = document.createElement("div");
    modal.id = "stickersModal";
    modal.classList.add("hidden");

    modal.header = document.createElement("header");
    modal.header.id = "stickersHeader";


    modal.header.inputWrapper = document.createElement("div");
    modal.header.inputWrapper.id = "inputWrapper"
    modal.header.appendChild(modal.header.inputWrapper);
    
	modal.header.select = document.createElement("select");
    modal.header.select.id = "stickerCategorySelection";

    modal.header.filter = document.createElement("input");
    modal.header.filter.id = "stickerFilter";
    modal.header.filter.type = "text";
    modal.header.filter.placeholder = "Filter";

    modal.header.resetFilter = document.createElement("span");
    modal.header.resetFilter.id = "reset"
    modal.header.resetFilter.innerText = "✖"
    modal.header.resetFilter.classList.add("hidden");
    
	modal.header.close = document.createElement("span");
	modal.header.close.id = "closeStickers";
	modal.header.close.innerText= "✖";
	modal.header.close.addEventListener("click", () => stickers.close());

    modal.header.inputWrapper.appendChild(modal.header.select);
    modal.header.inputWrapper.appendChild(modal.header.filter);
    modal.header.inputWrapper.appendChild(modal.header.resetFilter);

    modal.header.appendChild(modal.header.close);

    modal.header.resetFilter.addEventListener("click", () => {
        modal.header.filter.value = "";
        modal.header.filter.exec();
        modal.header.filter.focus();
    })


    modal.list = document.createElement("div");
    modal.list.id = "stickerList";
	modal.list.appendChild(modal.header);

    // modal.pages = document.createElement("div");
    // modal.pages.id = "sticersPages"
    // modal.appendChild(modal.pages);


    modal.list.addEventListener("click", (e) => {
        if (!e.target.classList.contains("toggle-favorite-sticker")) {
            return;
        }
        e.preventDefault();
    })


    $(modal.list).on("click", ".sticker-star", async(e) => {
        const target = e.target;
        if (!target.classList.contains("sticker-star")) {
            return;
        }
        const sticker = target.parentNode.querySelector(".sticker").sticker;

        if (target.classList.contains("favorited")) {
            stickers.favorites.remove(sticker);
            target.classList.add("nonfavorited");
            target.classList.remove("favorited");
        } else {
            stickers.favorites.add(sticker);

            target.classList.add("favorited");
            target.classList.remove("nonfavorited");
        }
        //e.preventDefault();
        e.stopImmediatePropagation();
    });

    modal.appendChild(modal.list);

    document.body.append(modal);
    stickers.modal = modal;

    const stickerModel = document.createElement("img");

    stickerModel.width = 100;
    stickerModel.height = 100;
    
    stickerModel.classList.add("sticker");
    stickerModel.loading = "lazy";

	const stickersStyle = document.createElement("style");
	stickersStyle.id = "stickersStyle";
	document.head.append(stickersStyle);

	stickersStyle.innerText = stickers.css;
    stickers.getStickerItem = function(key, stickersMap)
    {
        if (!stickersMap) return null;
        return stickers.getSortedListItem(key, stickersMap.items);
    }

    stickers.buildSticker = async function(it, stickersMap)
    {
        const storage = stickersMap.storages[it.storage]
                        // For testing
                        || _chatGlobal.buffers.find((ti) => ti.options.stickers?.storage)?.options.stickers?.storage;
        if (!storage) throw new Error("No sticker storage");

		const stickerId = it.key.replaceAll("-", "_");
        const src = storage.www_path + it.filename;

        if (it.built && stickersMap.allStickers.has(stickerId)) {
            return stickersMap.allStickers.get(stickerId);
        }
		
		let storedStickerSrc = null;

		if (stickers.locallyStored())
		{
			storedStickerSrc = await _chatGlobal.db.getFromStore("stickers", stickerId);
		}
        const sticker = stickerModel.cloneNode(true);
		sticker.alt = sticker.title = it.key;
        sticker.dataset.category = it.category;
        sticker.dataset.name = it.category.split("-").slice(-1).pop()
        
        sticker.sticker = it;

		sticker.width = storedStickerSrc?.width 	|| 100;
		sticker.height = storedStickerSrc?.height 	|| 100;

        if (!stickersMap.allStickers) {
            stickersMap.allStickers = new Map();
        }
		stickersMap.allStickers.set(stickerId, sticker);
        it.built = true;

        if (storedStickerSrc == null && stickers.locallyStored()) {
            // Store sticker loaded from web to localStorage.
			sticker.addEventListener("load", async (e) => {
				const loadedImg = e.target;
				try {
					const blobbed = await stickerToBlob(loadedImg);
					if (blobbed) {
						await _chatGlobal.db.addToStore("stickers", {
							id		: stickerId,
							blob	: blobbed,
							width	: loadedImg.naturalWidth,
							height  : loadedImg.naturalHeight,
						});
						console.log("Stored sticker");
					} else {
						console.info("Failed to blob sticker");
					}
				} catch (err) {
					console.info("Error while storing sticker");
					console.error(err)
				}
			}, { once: true} );
		}

		if (storedStickerSrc?.blob) {
            const uri = URL.createObjectURL(storedStickerSrc.blob);
            sticker.src = uri;
		} else {
            sticker.src = src;
		}

        return sticker;
    }

    stickers.loadBufferStickers = async function(buffer, category, callback)
    {
        const stickersMap = buffer.options?.stickers
        if (!stickersMap) return;

        if (!stickersMap.images) {
            stickersMap.images = {};
        }

        if (!stickersMap.allStickers) {
            stickersMap.allStickers = new Map();
        }


        if (category && stickersMap.images[category] && category !== "favorites") {
            return callback(stickersMap.images[category]);
        }

        let items = [];

        switch (category) {
            case stickers.categories.ALL: 
                items = stickersMap.items.map(value => ({ value, sort: Math.random() }))
                    .sort((a, b) => a.sort - b.sort)
                    .map(({ value }) => value)
                break;

            case stickers.categories.FAVORITES: 
                items = [...Array.from(stickers.favorites.map.entries()).map((it) => it[1])]
                break;

            default:
                items = stickersMap.items.filter((it) => it.category === category).sort((it) => !stickers.favorites.has(it));
                //items = items.sort((it) => !stickers.favorites.has(it))

                break;
        }

        let batch = [];
        for (let i = 0; i < items.length; i++) {
            const it = items[i];

            const sticker = await stickers.buildSticker(it, stickersMap);

            category = (category) ? category : it.category;

            if (category !== "favorites") {
                if (!stickersMap.images[category]) {
                    stickersMap.images[category] = [];
                }
                stickersMap.images[category].push(sticker);
            }
    
            if (!stickers.isViewed()) {
                return;
            }
            // In case the category changed during the load.
            if (category !== stickers.getSelectedCategory()) {
                return
            }

            batch.push(sticker);
            if (batch.length % 128 === 0) {
                callback(batch);
                batch = [];
                // Give time for UI to work.
                await waiterFunc();
            }
        }
        return callback(batch);
    }

    function addSelectionCategory(name, currentCategory) {
        const categoryOption = document.createElement("option");
        categoryOption.value = name;
        categoryOption.innerText = name;

        if (currentCategory && currentCategory === name) {
            categoryOption.setAttribute("selected", "selected");
        }

        modal.header.select.appendChild(categoryOption);
    }

    stickers.view = async function(category = stickers.displayedCategory)
    {
        const buffer = _chatGlobal.activeBuffer;
        const stickersMap = (buffer.s.options && buffer.options.stickers) ? buffer.options.stickers : null;

        if (!stickersMap) {
           // console.info("No stickers in current buffer.")
           chatFeeds.addItem({
                message: "Ei tarroja nykyisessä huoneessa.",
            });

        }

		if (category !== stickers.displayedCategory)
		{
			stickers.removeImages();
			stickers.modal.list.scrollTo(0, 0);			
		}
		stickers.displayedCategory = localStorage.displayedCategory = category;

        //if (!category && category !== "all") category = stickersMap.categories[0];

        modal.removeCategoryOptions();
        
        addSelectionCategory(stickers.categories.ALL, category);
        addSelectionCategory(stickers.categories.FAVORITES, category);

        stickersMap.categories.forEach((it) => {
            addSelectionCategory(it, category);
        })

        modal.classList.remove("hidden");
        if (!stickersMap) return;

        stickers.loadBufferStickers(buffer, category, function gotStickerBatch(stickerBatch) {

			const stickerWrapper = document.createElement("span");
			stickerWrapper.classList.add("stickerWrapper");

			for (let i = 0; i < stickerBatch.length; i++)
			{
				const it = stickerBatch[i];
				if (it.parentNode) continue
				const wrapper = stickerWrapper.cloneNode(false);
				wrapper.appendChild(it);

                const starWrapper = document.createElement("span");
                starWrapper.classList.add("sticker-star")

                const s = it.sticker;

                if (stickers.favorites.has(s)) {
                    starWrapper.classList.add("favorited");
                } else {
                    starWrapper.classList.add("unfavorited")
                }

                wrapper.appendChild(starWrapper)

				modal.list.appendChild(wrapper);
				filterSticker(it)
			}
        });

        modal.header.filter.focus();
    }

    stickers.removeImages = function()
    {
        const stickerWrappers = modal.list.querySelectorAll("span.stickerWrapper");
        stickerWrappers.forEach((it) => {
			const img = it.firstElementChild;
			it.removeChild(img);
			it.parentNode.removeChild(it)
		});
        modal.list.scrollTo(0, 0);
    }

    modal.removeCategoryOptions = function()
    {
        const categoryOptions = modal.header.select.querySelectorAll("option");
        categoryOptions.forEach((it) => it.parentNode.removeChild(it));
    }

    stickers.close = function()
    {
        modal.classList.add("hidden");
        modal.removeCategoryOptions()
    }

    modal.addEventListener("click", (e) => {
        const target = e.target;

        if (e.defaultPrevented) {
            return;
        }

        if (stickers.modal.list.matches(":hover") == false) {
			console.log(stickers.modal);
            stickers.close();
            return;
        }

        if (target.classList.contains("sticker")) {
            input.form.txtarea.focus();
            input.form.txtarea.dispatchEvent(new Event("focus", { bubbles: null, cancelable: true }))
            setTimeout(() =>{
                input.form.txtarea.focus();
				// @ts-ignore 
                document.execCommand('insertText', false /*no UI*/, "[st]" + target.alt + "[/st]"); 
                stickers.close();
            });
        }
    });

    function filterSticker(img)
    {
        const q = modal.header.filter.value.toLowerCase();
        if (!img.alt.toLowerCase().includes(q)) img.parentNode.classList.add("hidden")
        else  img.parentNode.classList.remove("hidden")
    }

    modal.header.filter.exec = function(_e) {
        
        if (modal.header.filter.value) {
            modal.header.resetFilter.classList.remove("hidden");
        } else {
            modal.header.resetFilter.classList.add("hidden");
        }

        const images = modal.list.querySelectorAll("img.sticker");
        for (var i = 0; i < images.length; i++) {
            const img = images[i];
            filterSticker(img);
            if (i % 254 == 0) {
                //await waiter();
            }
        }
    }
    modal.header.filter.addEventListener("keyup", modal.header.filter.exec);

    modal.header.select.addEventListener("change", async(e) => {
        //stickers.removeImages();
        modal.header.filter.value = '';
        stickers.view(e.target.value);
        localStorage.stickerCategory = e.target.value;
    });

    document.querySelector("#chat-page-posts").addEventListener("click", (e) => {
        if (e.target.classList.contains("sticker")) {
            const category = e.target.alt.replace("[st]", "").split("-")[0];
            if (!category) return;
            stickers.view(category);
        }
    });

	// Assume go back button press.
	window.addEventListener('popstate', function (e) {
	//window.onhashchange = function(e) {
		if (stickers.isViewed()) {
			stickers.close();
			e.preventDefault();
			e.stopPropagation();
		}
	});

}


document.addEventListener("keydown", (e) => {
    if (e.ctrlKey && e.key === "s") {
        e.preventDefault();

        if (stickers.isViewed())
        {
            stickers.close();
        } else {
            stickers.view();
        }

    }

    if (e.key === "Escape" && stickers.isViewed()) {
        stickers.close();
    }
})

