"use strict";


// Buffer list shows on side next to chat.
const bufferList = {};


bufferList.buffersContainer = document.querySelector("#buffers-container");

bufferList.getSelectedItem = function() {
    return bufferList.buffersContainer.querySelector(`#buffers-container .active-buffer`);
}

bufferList.getItemByBuffer = function(buffer) {
    return bufferList.buffersContainer.querySelector(`#buffers-container #buffer-item-${buffer.key}`);
}

bufferList.getBufferByItem = function(item) {
	return item.buffer;
}

bufferList.removeItemByBuffer = function(buffer) {
	const item = bufferList.getItemByBuffer(buffer);
	item.parentNode.removeChild(item);
}

bufferList.removeItem = function(item) {
    item.parentNode.removeChild(item);
}

bufferList.createBufferListItem = function(buffer)
{
	const itemId = `buffer-item-${buffer.key}`;

	const html = `
	<div id='${itemId}' class='buffer-item' draggable=true>
		<div class='buffer-link'>
			<a href='#${postBuilder.escapeHtml(buffer.getUrlHash())}' class="buffer-name-holder">
				<span class='buffer-prefix'></span>
				<span class='buffer-name'></span>
			</a>
			<span class="buffer-unreads"></span>
			<a class="buffer-close" title='x'>x</a>
		</div>
		<span class="buffer-description"></span>
	</div>`;

	const listType = buffer instanceof ChatChannelBuffer ? "buffers-publics" : "buffers-privates";
	const listHolder = bufferList.buffersContainer.querySelector("#" + listType);

	const item = buildDocumentFromHtmlString(html);
	item.dataset.buffer = buffer.key;

	listHolder.appendChild(item);

	item.description 		= item.querySelector(".buffer-description");
	item.unreads 			= item.querySelector(".buffer-unreads");
	item.nameHolder 		= item.querySelector(".buffer-name-holder");

	item.buffer = buffer;
	item.buffer.data = {};

	Object.seal(item);

	const prefixedBufferName = buffer.getName();

	const prefixEnd = prefixedBufferName.lastIndexOf("#") + 1;
	const prefix 	= prefixedBufferName.substr(0, prefixEnd);
	const name	 	= prefixedBufferName.substr(prefixEnd);

	item.nameHolder.querySelector(".buffer-prefix").innerText 	= prefix;
	item.nameHolder.querySelector(".buffer-name").innerText 	= name;

	return item;
}

bufferList.drawBufferListItem = function(buffer)
{
	const itemId = `buffer-item-${buffer.key}`;
	let item = bufferList.buffersContainer.querySelector("#" + itemId);

	if (!item) {
		item = bufferList.createBufferListItem(buffer);
	}

	const description = `
		${(buffer.keys_verified) ? `✓&nbsp` : '' }
		${(buffer instanceof ChatPrivateBuffer && buffer.friend_fingerprint)
			? buffer.friend_fingerprint
			: ''
		}
		${buffer.topic || buffer.getFirstMessage() || ''}
		`.trim();

	if (description && description.length) {
		item.description.innerHTML = postBuilder.formatChatMessage(description);
		item.description.setAttribute("title", description)
	}
	item.unreads.innerText = buffer.unreadCount || '';

	item.classList.remove("active-buffer");
	item.classList.remove("buffer-highlighter");

	if (buffer.isActive()) 	item.classList.add("active-buffer");
	if (buffer.highlighted) item.classList.add("buffer-highlighter");
}

bufferList.moveItemToTop = function(item)
{
	const holder = item.parentNode;
	const currentFirst = holder.firstElementChild;

	currentFirst.before(item);
	//holder.insertBefore(currentFirst, item);
}

bufferList.moveBufferToTop = function(buffer) {
	const item = bufferList.getItemByBuffer(buffer);
	return bufferList.moveItemToTop(item);
}

BaseBuffer.prototype.renderBufferMenuElement = function () {
	return bufferList.drawBufferListItem(this);
};


bufferList.buffersContainer.addEventListener("click", (e) => {

	const isTargetBufferItem = e.target.closest(".buffer-item")

	if (!isTargetBufferItem) {
		return true;
	}

	const item = e.target.closest(".buffer-item");
	const buffer = bufferList.getBufferByItem(item);

	const closeBuffer = e.target.classList.contains("buffer-close");

	if (closeBuffer) {
		if (buffer.isActive()) {
			const nextBufferItem = item.nextElementSibling || item.previousElementSibling;
			if (nextBufferItem) {
				const newBuffer = bufferList.getBufferByItem(nextBufferItem);
				newBuffer.draw();
			}
		}
		buffer.destroy();
	} else {
		buffer.draw();
	}
});

Object.freeze();

document.body.addEventListener("keydown", (e) => {
	if (e.altKey) {
		const currSelectedItem = bufferList.getSelectedItem();
		let nextItem = null;

		if (!currSelectedItem) return;

		const parentContainer = currSelectedItem.closest("#buffers-container");
		const currContainer = currSelectedItem.parentNode;


		if (e.key === "ArrowUp")  {
			nextItem = currSelectedItem.previousElementSibling // prev from current container
				|| currContainer.previousElementSibling?.lastElementChild
				|| parentContainer.lastElementChild.lastElementChild; // currently at first item of first container, jump to last of last.
		} else if (e.key === "ArrowDown") {
			nextItem = currSelectedItem.nextElementSibling
				|| currContainer.nextElementSibling?.firstElementChild
				|| parentContainer.firstElementChild.firstElementChild; // currently at last item of last container, jump to first of first.
		}

		if (nextItem) {
			const nextBuf = bufferList.getBufferByItem(nextItem);
			if (nextBuf) {
				nextBuf.draw();
			}
		}
	}
});

$("#chat-container").on("chatCreated", (e, _chat) => {

	// Push buffer to top of list after new post
	_chat.eventEmitter.addListener("postDrawn", (buf) => {
		const item = bufferList.getItemByBuffer(buf);
		if (!item) {
			console.info(`Buffer ${buf.getName()} added post while list item doesn't exists`);
			return;
		}
		bufferList.moveItemToTop(item);
	});


	_chat.eventEmitter.emitEvent("bufferAdded", (buffer) => {
		bufferList.drawBufferListItem(buffer);
	});


	_chat.eventEmitter.addListener("bufferLoaded", (buf) => {

		const item = bufferList.getItemByBuffer(buf) || bufferList.createBufferListItem(buf)
		const lastBump = buf.lastBump;
		const bufferContainer = item.parentNode;

		let prevItem = bufferContainer.firstElementChild;
		while (prevItem?.nextElementSibling) {
			if (prevItem.buffer.lastBump <= lastBump) {
				bufferContainer.insertBefore(item, prevItem);
				break;
			}
			prevItem = prevItem.nextElementSibling;
		}
		
		if (!item.parentElement) {
			bufferContainer.appendChild(item);
		}

	});
});