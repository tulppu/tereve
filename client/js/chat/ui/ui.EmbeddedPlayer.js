
const PLAYER_WIDTH = 430;
const PLAYER_HEIGHT = 250;

const embedsEnabled = () => configs.getConfig("media-embeds");

const getPlayerWidth = () => Math.max(window.innerWidth / 3, Math.min(400, window.innerWidth - 15));
const getPlayerHeight = () => Math.min(getPlayerWidth() * 0.75, window.innerHeight * 0.65);

class EmbedResolverProvider {
	constructor(width = "400px", height = "150px") {
		this.width = width;
		this.height = height;
	}

	isSupportedLink(link) {
		return false;
	}

	isSupported() {
		return false
	}

	getPlayer(href) {
		return new Promise();
	}
}

class YoutubeProvider extends EmbedResolverProvider {

	constructor() {
		super()
		this.re = /https\:\/\/(?:www\.)?youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})(?:\S+)?/g;
		this.reTime = /(?:\?|&)t=(\d+)/

		if (!embedsEnabled()) return;

		postBuilder.reFormatters.unshift({
			regex: this.re,
			out: (match, contents, offset, input_string) => {
				const timeStart = this.getStartTime(match);
				const t = (timeStart) ? `?t=${timeStart}` : '';
				return `${match} <a data-id=${contents} href='https://youtu.be/${contents+t}' class="embed youtube">[Upota]</a>`;
			}
		});
	}

	isSupportedLink(link) {
		//return false;
	}

	getStartTime(url) {
		const tMatch = this.reTime.exec(url);
		return (tMatch) ? tMatch[1] : 0;
	}

	isSupported(url) {
		return ((url.startsWith("https://www.youtube.com")
				|| url.startsWith("https://youtu.be"))
			&& url.match(this.re).length
			);
	}

	getPlayer(url) {
		return new Promise((resolve, reject) => {

			const results = this.re.exec(url);

			if (!results || !results[1]) {
				return reject(new Error("Failed to parse ID"));
			}

			const t = this.getStartTime(url);

			const html = `<iframe
							width="${getPlayerWidth()}" height="${getPlayerHeight()}"
							src="https://www.youtube.com/embed/${results[1]}?start=${t}&autoplay=1"
							frameborder="0"
							allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
						</iframe>`

			return resolve(html);
		});
	}
}



class VimeoProvider extends EmbedResolverProvider {

	constructor() {
		super()
		this.re = /https:\/\/vimeo.com\/(\d+)(?:\s|$)/gm;

		if (!embedsEnabled()) return;

		postBuilder.reFormatters.unshift({
			regex: this.re,
			out: (match, contents, offset, input_string) => {
				//const timeStart = this.getStartTime(match);
				///const t = (timeStart) ? `?t=${timeStart}` : '';
				//return `${match} <a data-id=${contents} href='https://youtu.be/${contents+t}' class="embed youtube">[Upota]</a>`;
				return `${match} <a data-id=${contents} href='https://vimeo.com/${contents}' class="embed vimeo">[Upota]</a>`;

				return ``
			}
		});
	}

	getStartTime(url) {
		const tMatch = this.reTime.exec(url);
		return (tMatch) ? tMatch[1] : 0;
	}

	isSupported(url) {
		return url.startsWith("https://vimeo.com")
	}

	getPlayer(url) {
		return new Promise((resolve, reject) => {

			const results = this.re.exec(url);

			if (!results || !results[1]) {
				return reject(new Error("Failed to parse ID"));
			}

			const html = `<iframe src="https://player.vimeo.com/video/${results[1]}?autoplay=1"
				width="${getPlayerWidth()}" height="${getPlayerHeight()}"
				frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>`;

			return resolve(html);
		});
	}
}


class SoundcloudProvider extends EmbedResolverProvider {


	constructor() {
		super();

		this.re = /(https:\/\/soundcloud.com\S+)/g;
		if (!embedsEnabled()) return;

		postBuilder.reFormatters.unshift({
			regex: this.re,
			out: (match, contents, offset, input_string) => {
				return ` ${match} <a href='${match}' class="embed soundcloud raw">[Upota]</a> `;
			}
		});

	}


	isSupported(url) {
		return url && url.startsWith("https://soundcloud.com/");
	}

	getPlayer(href) {
		return new Promise((resolve, reject) => {
			fetch('https://soundcloud.com/oembed?url=' + href).then(async (res) => {

				const response = await res.json();
				const results = response.html.match(/tracks%2F(\d+)/m);

				if (!results || results.length !== 2) return reject();
				const id = results[1].substr();

				return resolve(`<iframe width="${PLAYER_WIDTH * 2}" height="${PLAYER_HEIGHT - 25} scrolling="no" frameborder="no" allow="autoplay"
					src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/${id}&color=%23ff5500&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true">
				</iframe>`);
			});
		});
	}
}

class RawVideoProvider extends EmbedResolverProvider {

	
	constructor() {
		super();

		this.re = /((http[s]{0,1}\:\/\/\S{4,})\.(?:webm|mp4))(?:$|\s)/g;
		if (!embedsEnabled()) return;

		postBuilder.reFormatters.unshift({
			regex: this.re,
			out: (match, contents, offset, input_string) => {
				return ` ${match} <a href='${match}' class="embed raw">[Upota]</a> `;
			}
		});

	}

	isSupported(url) {
		return url.endsWith(".mp4") || url.endsWith(".webm");
	}
	

	getPlayer(url) {
		return new Promise((resolve, reject) => {
			const player = `
			<video style='max-width: 30vw; max-height: 60vh; object-fit: fill' controls autoplay loop>
				<source src='${url}' type='video/webm'>
				<source src='${url}' type='video/mp4'>
				Unsupported video.
			</video>`.trim()

			return resolve(player)
		});
	}
}

class RawPictureProvider extends EmbedResolverProvider {

	
	constructor() {
		super();

		this.re = /((http[s]{0,1}\:\/\/\S{4,})\.(?:jpg|jpeg|jpe|png|gif|webp|apng))(?:$|\s)/g;
		if (!embedsEnabled()) return;

		postBuilder.reFormatters.unshift({
			regex: this.re,
			out: (match, contents, offset, input_string) => {
				return ` ${match} <a href='${match}' class="embed raw">[Upota]</a> `;
			}
		});

	}

	isSupported(url) {
		return !!url.match(this.re)
		//return url.endsWith(".mp4") || url.endsWith(".webm");
	}
	

	getPlayer(url) {
		return new Promise((resolve, reject) => {

			const player = `<img src='${url}' style='max-width: 30vw; max-height: 60vh; object-fit: scale-down'>`.trim()

			return resolve(player)
		});
	}
}

class RawAudioProvider extends EmbedResolverProvider {

	
	constructor() {
		super();

		this.re = /((http[s]{0,1}\:\/\/\S{4,})\.(?:mp3|ogg|ogv|flac|wav|opus))(?:$|\s)/g;
		if (!embedsEnabled()) return;

		postBuilder.reFormatters.unshift({
			regex: this.re,
			out: (match, contents, offset, input_string) => {
				return ` ${match} <a href='${match}' class="embed raw">[Upota]</a> `;
			}
		});

	}

	isSupported(url) {
		return !!url.match(this.re)
	}
	

	getPlayer(url) {
		return new Promise((resolve, reject) => {

			const player = `
			<audio  controls autoplay loop>
				<source src='${url}' type='audio/ogg'>
				<source src='${url}' type='audio/mpeg'>
				<source src='${url}' type='video/wav'>
				Unsupported audio.
			</video>`.trim()

			return resolve(player)
		});
	}
}


class Embedder {

	constructor() {
		this.resolvers = [
			new SoundcloudProvider(),
			new YoutubeProvider(),
			new VimeoProvider(),
			new RawVideoProvider(),
			new RawPictureProvider(),
			new RawAudioProvider(),
		];

		this.$playerHolder = $(
			`<div id='media-player-holder'
				draggable='false'
				style=' position: absolute;
				z-index: 500;
				display: none;
				float: right;'
			>
				<header id='player-header'>
					<span id='player-url'></span>
					<a id='close-player'>✖</a>
				</header>
				<div id='player'></div>
			</div>`.trim());

		this.$player = this.$playerHolder.find("#player");

		const self = this;
		this.$playerHolder.find("#close-player").on("click", () => self.close());

		$("#chat-body-container").append(this.$playerHolder);

		window.addEventListener("resize", () => {
			this.$playerHolder.find("iframe")
				.attr("width", getPlayerWidth())
				.attr("height", getPlayerHeight());
		});
	
	}

	close() {
		this.$player.get(0).innerHTML = '';
		this.$playerHolder.hide();
	}

	getPlayerProvider(url) {
		return this.resolvers.find((it) => it.isSupported(url));
	}

	isSupported(url) {
		return !!this.getPlayerProvider(url);
	}

	setTitle(url, name, originalFileName=null) {
		if (!name) name = url;
		$("#player-url").html(`<a href='${postBuilder.escapeHtml(url)}'
			download='${postBuilder.escapeHtml(originalFileName || name)}'>${postBuilder.escapeHtml(name)}</a>`);
	}

	async play(url, name = null) {

		if (!name) {
			try {
				if (url.startsWith("https://youtu")) throw new Error();
				name = url;
				const filenameStart = title.lastIndexOf("/");
				if (!name) {
					name = title.substr(filenameStart+1)
				}
			} catch (e) {
				name = url;
			}
		}

		let self = this;

		const provider = self.getPlayerProvider(url);
		if (!provider) {
			console.info("No provider found for " + url);
			return;
		}

		const player = await provider.getPlayer((url));


		this.$playerHolder.show()
		this.$player.get(0).innerHTML = player;
		this.setTitle(url, name);
		this.setMediaVolume();

		const media = this.$player.get(0).querySelector("iframe,img,video,audio");

		this.$playerHolder.get(0).dataset.type = media.nodeName;
		//if (media) media.id = "player-media";
		this.media = media;
	}

	playMedia(media, name=null) {
		this.close();
		this.$playerHolder.show()
		this.$player.get(0).appendChild(media);
		if (media.paused) media.play();

		if (!name) {
			name = media.src ||  media.querySelector("source").src
		}
		const nameLineBreak = name.indexOf("\n");
		if (nameLineBreak > 0) name = name.substr(0, nameLineBreak);

		this.setTitle(media.src, name);

		//if (media) media.id = "player-media";
		this.media = media;
		this.$playerHolder.get(0).dataset.type = media.nodeName;
	}

	isPlaying() {
        return !!this.$player.is(":visible")
    }


	setMediaVolume() {
		const media = this.$player.get(0).querySelector("video,audio");
		if (!media) return;

		media.addEventListener("volumechange", (e) => {
			const newVol = e.target.volume;
			localStorage.setItem("volume", newVol);
		});

		const savedVolume = localStorage.getItem("volume");
		if (typeof savedVolume !== "undefined") media.volume = savedVolume;
	}

}


let mediaEmbedder = null;

$("#chat-container").on("chatCreated", (e, chat) => {

	/*
	function _process_post_for_embeds(post) {
		//console.log(post);
		const links = post.querySelectorAll(".chat-post-message > a.link");
		//console.log(links);
		//for (let l in links) {
		links.forEach((l) => {
			//console.log(l);
			//l.innerText = "??";
		});
	}


	chat.eventEmitter.addListener("postDrawn", (buf, addedPost, data) => {
		_process_post_for_embeds(addedPost[0]);	
		//console.log(addedPost[0]);
	});
	chat.eventEmitter.addListener("addingFragment", (buf, frag) => {
		console.log(buf, frag);
		for (let p in frag.childNodes) {
			//console.log(p);
		}
		_process_post_for_embeds(frag);	
	});*/


	mediaEmbedder = new Embedder();
	document.getElementById("chat-page-posts").addEventListener("click", (event) => {
		
		const target = event.target;
		const href = target.href;

		if (!target.classList.contains("embed")) return;

		event.preventDefault();

		let fileName, title = null;

		const inAttachmentFooter = !!target.closest("footer.chat-attachment-footer-container")
		if (inAttachmentFooter) {
			fileName = target.closest("article").querySelector("a.attachment-link").innerText;
			const attachmentHolder = target.closest("article").querySelector("a.chat-post-attachment")
			title = attachmentHolder.dataset.title || attachmentHolder.title;
		}


		mediaEmbedder.play(href, title || fileName);
	});


	const player = document.getElementById("media-player-holder")

	let origPos = null;
	let playerOnDrag = false;

	let chatPageMessagesSizes = null;

	let windowWidth, windowHeight, playerWidth, playerHeight;

	function setPositions(e) {
		origPos = {}

		chatPageMessagesSizes = document.getElementById("chat-page-posts").getBoundingClientRect();

		origPos.right = parseInt(getComputedStyle(player).getPropertyValue("right"));
		origPos.top = parseInt(getComputedStyle(player).getPropertyValue("top"));

		origPos.x = e.clientX;
		origPos.y = e.clientY;

		playerOnDrag = true;

		windowWidth = window.innerWidth;
		windowHeight = window.innerHeight;
		playerWidth = player.clientWidth;
		playerHeight = player.clientHeight;
	}

	//var i = 0;
	function playerMover(e) {
		if (!playerOnDrag || !origPos) return;
		//i++;
		//if (i % 2 !== 0) return;

		const minRight = (windowWidth - chatPageMessagesSizes.right) * -1;
		const maxRight = chatPageMessagesSizes.right - playerWidth;

		const right = Math.min(
			maxRight,
			Math.max(
				origPos.right + origPos.x - e.clientX,
				minRight
			),
		);
		
		const top = Math.max(
			0,
			Math.min(
				origPos.top + e.clientY - origPos.y,
				windowHeight - playerHeight
			)
		);

		player.style.right = `${right}px`;
		player.style.top = `${top}px`;
	}

	player.addEventListener("mousedown", function(e) {

		if (e.which !== 1) return;
		e.preventDefault();
		setPositions(e);
		playerOnDrag = true;
		player.classList.add("dragged");

		document.addEventListener("mousemove", playerMover);

		document.addEventListener("mouseup", function(e) {
			e.preventDefault();
			document.removeEventListener("mousemove", playerMover);
			player.classList.remove("dragged");
			playerOnDrag = false;
			return false;
		}, {once: true});
		return false;
	});



	
	document.addEventListener("auxclick", (e) => {

		const isMediaViewerClick = mediaViewer.media.contains(e.target)
		if (!isMediaViewerClick) return;

		e.preventDefault();

		const media = mediaViewer.media;

		mediaEmbedder.playMedia(media, media.dataset.title || media.title);


		
		media.removeAttribute("width");
		media.removeAttribute("heigth");

		media.style.width = ""
		media.style.height = ""
		media.style.position = "inherit";
		media.style.zIndex = 0;
		media.style.maxHeight = "60vh";
		media.style.maxWidth = "unset";
		media.style.objectFit = "fill";

		media.play();

		media.id = "player-media";

		if (mediaViewer.media) mediaViewer.media = null;
	})


	$("#player").on("wheel", "img,video,iframe", (event) => {


		event.preventDefault();

		//const pienennä = event.originalEvent.deltaY > 0;
		const suurenna = event.originalEvent.deltaY < 0;

		const media = event.target//.querySelector("img,video");

		let w = media.width;
		let h = media.height;


		if (!media.dataset.resized) {
			media.dataset.resized = true;

			w = media.width = media.clientWidth;
			h = media.height = media.clientHeight;

			media.style.objectFit = "fill";
			media.style.maxWidth = "unset";
			media.style.maxHeight = "unset";
		}

		let newW = (w / 100) * ((!suurenna) ? 95 : 105);
		let newH = (h / 100) * ((!suurenna) ? 95 : 105);

		const pHolder = event.target.closest("#media-player-holder");
		let r = parseInt(getComputedStyle(pHolder).getPropertyValue("right"));

		if (suurenna) {
			pHolder.style.right = `${r - ((newW - w) / 2)}px`;
		} else {
			pHolder.style.right = `${r + ((w - newW) / 2)}px`;
		}

		media.width = newW;
		media.height = newH;
	});

	/*
	const buildAttachmentFooterOrig = postBuilder.buildAttachmentFooter;
	const embedNode = document.createElement("a");
	embedNode.classList.add("embed");
	embedNode.innerText = "[Upota]";
	const spacerNode = document.createElement("span");
	spacerNode.classList.add("spacer");

	postBuilder.buildAttachmentFooter = (attachment, buffer) => {

		const footer = buildAttachmentFooterOrig(attachment, buffer);
		const mime = attachment.meta.mime;

		if (mime.startsWith("video/") || mime.startsWith("image") || mime.startsWith("audio")) {
		//if (mediaEmbedder.isSupported(attachment.url)) {
			embedNode.href = attachment.url;
			footer.appendChild(spacerNode);
			footer.appendChild(embedNode);
		} else {
			if (embedNode.parentNode) embedNode.parentNode.removeChild(embedNode);
			if (spacerNode.parentNode) spacerNode.parentNode.removeChild(spacerNode);
		}

		return footer;
	};
	*/
});
