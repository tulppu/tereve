


const chatFeeds = {};
chatFeeds.wrapper = document.querySelector("#chat-feed");
chatFeeds.style = null;

chatFeeds.init = function() {

	chatFeeds.style = document.createElement("style");
	chatFeeds.style.innerHTML = `
	#chat-feed:empty {
		display: none;
	}
	
	#chat-feed {
		position: sticky;
		overflow: auto;
		top: 3px;
		z-index: 300000;
		word-break: break-word;
		float: right;
		display: flex;
		flex-direction: column;
		align-items: flex-end;
		overflow: hidden;
		overflow-y: auto;
	}
	

	.chat-feed-container {
		max-width: max(15vw, min(85vw, 300px));
		background: rgb(0,0, 0, 0.20);
		border: 1px solid rgb(0, 0, 0, 0.15);
		padding: 5px !important;
		padding-bottom: 7px !important;
		margin-top: 3p;
		margin-bottom: 3px;
		cursor: pointer;
		display: inline-flex;
		font-size: 13px;
		animation: feedFadein 0.1s;
		max-height: min(300px, 60vh);
		overflow: auto;
	}
	
	.chat-feed-container:hover {
		background: rgb(0,0, 0, 0.40);
	}
	
	.chat-feed-message {
		width: 100%;
		font-size: 12px;
	}
	
	.chat-feed-container .feed-origin {
		font-size: 12px;
	}
	
	.chat-feed-container .feed-message {
		text-shadow: 0 0 1px rgba(0,0,0,0.3);
	}
	
	.chat-feed-container .feed-message {
		word-break: break-all;
	}
	
	.chat-feed-container:hover .chat-feed-remove {
		visibility: visible;
	}
	.feed-fadeout {
		animation: feedFadeout 1.2s;
	}
	@keyframes feedFadeout{
		0% {
			opacity: 1;
		}
		100% {
			opacity: 0;
		}
	}
	
	@keyframes feedFadein {
		0% {
			opacity: 0;
		}
		100% {
			opacity: 1;
		}
	}
	`.trim();
	document.head.appendChild(chatFeeds.style);

	chatFeeds.wrapper.addEventListener("click", (e) => {
		if (chatFeeds.wrapper.contains(e.target)) {
			const item = e.target.closest(".chat-feed-container");
			if (!item) return;
			item.parentNode.removeChild(item);
		}
	})

}
chatFeeds.init();

chatFeeds.mobileLog = function(message) {
	if (!isMobile()) return;
	return chatFeeds.addItem({
		message: message,
		fadeoutAfter: 1.5,
	})
}

chatFeeds.errorLog = function(message) {
	return chatFeeds.addItem({
		message: message,
		fadeoutAfter: 1.5,
	})
}

chatFeeds.addItem = function(params)
{
	const source = params.source;
	const txt = params.message;
	const fadeoutAfter = params.fadeoutAfter;

	const feedContainer = buildDocumentFromHtmlString(`
	<div class='chat-feed-container'>
		<span class='chat-feed-item' title='${(new Date).toLocaleTimeString()}'>
			<span class='feed-source' style='font-weight: bold'></span>
			<span class='feed-message' title=''></span>
		</span>
	</div>
	`);

	if (source) {
		feedContainer.querySelector(".feed-source").innerText = source;
	}
	feedContainer.querySelector(".feed-message").innerText = txt;

	if (params.class) {
		feedContainer.classList.add(params.class);
	}
	if (params.classes) {
		params.classes.forEach((c) => feedContainer.class.add(c));
	}
	if (params.type) {
		feedContainer.dataset.type = params.type;
	}
	if (params.icon) {

		const icon = new Image(params.icon.width || 15 , params.icon.height || 15);
		icon.src = params.icon.src;
		icon.style.verticalAlign = "sub";
		const feedItem = feedContainer.querySelector(".chat-feed-item");
		const firstEl = feedItem.firstElementChild;

		feedItem.insertBefore(icon, firstEl);
	}

	chatFeeds.wrapper.appendChild(feedContainer);

	if (fadeoutAfter) {
		setTimeout(() => {
			if (!configs.getConfig("dev-mode")) {
				feedContainer.addEventListener('animationend', () => {
					feedContainer.remove()
				});
			}
			feedContainer.classList.add("feed-fadeout");
		}, fadeoutAfter * 1000)
	}

	return feedContainer;
}