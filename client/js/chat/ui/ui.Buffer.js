"use strict";

const POST_ADDED_FIRST_CYCLE 	= (isMobile()) ? 12 : 63;
const POST_ADDED_PER_CYCLE 		= (isMobile()) ? 64 : 512;
const POSTS_ADDED_IN_TOTAL 		= (isMobile()) ? 64 : 512;


BaseBuffer.prototype.getBufferListItemEl = function () {
	return $("#buffer-item-" + this.key);
};

BaseBuffer.prototype.getMessageEl = function(messageId) {
	return this.getHolderElement().querySelector("#" + messageId);
};

BaseBuffer.prototype.drawPost = function (post) {

	const postNode = postBuilder.buildPostNode(post, this);

	this.getHolder().appendChild(postNode);

	this.chat.eventEmitter.emitEvent("postDrawn", [
		this,
		[ postNode ],
		post,
	]);

	const holder = this.getHolderElement();
	//holder.data.last = postNode;
	this.setAsLastIfNeeded(postNode);


	const postIndex = this.posts.findIndex((it) => it === post);
	if (postIndex >= 0) this.posts.splice(postIndex, 1);
	
	return postNode;
}

BaseBuffer.prototype.drawPosts = function (posts, prepend=true) {

	//this.getHolderElement().setAttribute("data-status", "RENDERING");
	this.setStatus("DRAWING");

	return new Promise((resolve, reject) => {
		const self = this;
		const postsHolder = self.getHolder();
		const postsCount = posts.length;

		console.dev.time("posts total")


		const fragment = document.createDocumentFragment();
		
		//let nodeList = null;

		const buildTimes = new Array();
		const devMode = configs.getConfig("dev-mode");

		const builtPosts = new Array();
		const postNodes = new Array(); // Nodes here can't be accessed after fragment in placed.

		let bStart,  bEnd = null;

		if (devMode) {
			bStart = performance.now();
		}

		for (let i = 0; i < postsCount; i++) {
			//let postNode = null;
		
			const postNode = postBuilder.buildPostNode(posts[i], self);

			if (postNode) {
				builtPosts.push(postNode);
				postNodes.push(postNode);
			} else {
				console.error("Failed to build " + posts[i]);
			}
		}


		if (devMode) {
			bEnd = performance.now();
			buildTimes.push(bEnd - bStart);
	
			const bSum = buildTimes.reduce((partialSum, a) => partialSum + a, 0);
			console.dev.log("Building " + builtPosts.length + " posts took " + bSum + "ms. (avg " + bSum / builtPosts.length + "ms)");
		}

		console.dev.time("fragment");
		for (let i = 0; i < postNodes.length; i++) {
			try {
				fragment.appendChild(postNodes[i]);
			} catch (e) {
				console.error(e);
			}
		}
		console.dev.timeEnd("fragment");

		console.dev.time("eventAddingFragment");
		self.chat.eventEmitter.emitEvent("addingFragment", [
			self,
			fragment
		]);
		console.dev.timeEnd("eventAddingFragment");

		console.dev.time("append");
		if (prepend) {
			postsHolder.insertBefore(fragment, postsHolder.firstChild)
		} else {
			postsHolder.append(fragment)
		}
		console.dev.timeEnd("append")
		
		console.dev.timeEnd("posts total");

		self.chat.eventEmitter.emitEvent("postsDrawn", [
			self,
			builtPosts
		]);

		self.setAsLastIfNeeded(postsHolder.lastElementChild);

		//this.getHolderElement().setAttribute("data-status", "READY");
		this.setStatus("LOADED");

		return resolve(builtPosts);
	});
}

const waiter = (timer=0) => {
	return new Promise((resolve) => window.requestAnimationFrame(() => setTimeout(() =>  resolve(), timer)));
}

BaseBuffer.prototype.setAsLastIfNeeded = function(post) {
	const holder = this.getHolderElement();
	const currLast = holder.data.last;

	if (!currLast || parseInt(currLast.dataset.postNum < post.dataset.postNum)) {
		holder.data.last = post;
	}
}

// TODO: move outside from buffer.draw
const displayFuncs = {
	smoothedDisplay: async (self) => {
		//scroller.scrollToEnd({buffer: self, smooth: false});

		//const posts = $holder.get(0).querySelectorAll("article");
		const holder = self.getHolderElement();
		const posts = holder.childNodes;

		const postsTotal = posts.length;

		const DISPLAYED_FIRST_CYCLE = Math.min(postsTotal, 64);
		const DISPLAYED_PER_CYCLE = Math.min(postsTotal, 254);

		let postsDisplayed = 0;
		//scroller.scrollToEnd({buffer: self, smooth: false});

		for (let i = postsTotal-1; i >= 0; i--) {
			const post = posts[i];
			post.classList.remove("hidden");

			if (postsDisplayed === DISPLAYED_FIRST_CYCLE) {
				scroller.scrollToEnd({buffer: self, smooth: false});
				await waiter();
				//scroller.scrollToEnd({buffer: self, smooth: false});

			}

			if (i > postsDisplayed && postsDisplayed % DISPLAYED_PER_CYCLE === 0) {
				scroller.scrollToEnd({buffer: self, smooth: false});
				await waiter();
			}
			postsDisplayed++;

		}
		scroller.scrollToEnd({buffer: self, smooth: false});

	},

	smoothedDraw: async (self) => {

		if (!self.posts || !self.posts.length) return [];

		const firstDisplayedPostOfCycle = self.posts.at(-1);

		const holder = self.getHolderElement()
		const lastPost = holder.data.last;
		const prepend =  firstDisplayedPostOfCycle.post_num > parseInt(lastPost?.dataset.postNum || 0);

		const addedPost = [];


		const addPosts = async(posts) => {
			return new Promise( (resolve, reject) => {
				self.drawPosts(posts, prepend).then(async(drawnPosts) => {
					console.dev.time("reqFrame")
					//scroller.scrollToEnd({buffer: self, smooth: false});
					//window.requestAnimationFrame(() => {
						console.dev.timeEnd("reqFrame");
						scroller.scrollToEnd({buffer: self, smooth: false});
						//holder.data.last = drawnPosts.at(-1);
						return resolve(drawnPosts);
					//});
				});
			});
		};

		// posts are handled from end of array to beginning and removed from self.posts array after they are added to dom
		while (self.posts.length && addedPost.length < POSTS_ADDED_IN_TOTAL) {

			// Was switched to another while still loading this.
			if (!self.isActive()) {
				return;
			}

			const isFirstCycle =  !lastPost && addedPost.length === 0;

			const maxToAdd = (isFirstCycle) ? POST_ADDED_FIRST_CYCLE :  POST_ADDED_PER_CYCLE;

			const postCountToAdd = Math.min(maxToAdd, self.posts.length);
			let postsToAdd = self.posts.slice(-1 * postCountToAdd);

			if (self instanceof ChatE2eEncryptedBuffer) {
				await Promise.all(postsToAdd.map((it) => self.openMessage(it)));
			}

			const newPosts = await addPosts(postsToAdd, self);
			addedPost.push(...newPosts);

			self.posts.splice(-1 * postCountToAdd);


			//await waiter();

			if (isFirstCycle && newPosts.length) {
				//holder.data.last = newPosts.at(-1);
				self.setAsLastIfNeeded(newPosts.at(-1));
				scroller.scrollToEnd({buffer: self, smooth: false});
				// in next cycle ui can get little sluggy, give time for other stuff to finish
				await waiter();
			}
		}
		return addedPost;
	},
};


BaseBuffer.prototype.draw = function () {

	if (this.isDrawing) return

	const bufHolderEl = this.getHolderElement();

	const finalise = (self) => {
		if (this.users) self.chat.uiFuncs.renderBufferUsers(this);
		input.form.txtarea.setAttribute("maxlength", self.configs.maxMessageLenght);
		if (!treatedAsMobile()) $(self.chat.inputEl).focus();

		window.location.hash = self.getUrlHash();
		document.title = self.getName();
		
		this.isDrawing = false
	};

	this.highlighted = false;

	if ((this.isActive() && this.holderElement?.childElementCount)) {
		scroller.scrollToEnd({buffer: this, smooth: false});
		return;
	}

	this.chat.getActiveBufferItem().getHolderElement().classList.add("hidden");

	//bufHolderEl.classList.add("buffer-drawing")
	this.isDrawing = true;
	bufHolderEl.classList.remove("hidden");

	if (!this.isActive() &&
		this.chat.getActiveBufferItem() &&
		this.chat.getActiveBufferItem().hasHolderElement()
	) {
		const buf = this.chat.getActiveBufferItem();
		bufHolderEl.classList.add("hidden");
		scroller.scrollToEnd({buffer: buf, smooth: false});

		//setTimeout(() => {
			buf.getHolderElement().querySelectorAll("article").forEach((it) => it.classList.add("hidden"));
			//scroller.scrollToEnd({buffer: buf});
		//})
	}
	

	this.chat.setActiveBufferItem(this);
	this.chat.getActiveBufferItem().getHolderElement().classList.remove("hidden");

	this.chat.uiFuncs.renderBufferHeader(this);
	this.chat.uiFuncs.renderBufferItems();

	scroller.scrollToEnd({buffer: this, smooth: false});

	const renderMessages = async (self) => {
		await waiter();
		await displayFuncs.smoothedDisplay(self);

		//if (this.holderElement.la)
		//if (this.holderElement?.childElementCount > POSTS_ADDED_IN_TOTAL) {
		//	return [];
		//}

		if (this.holderElement?.childElementCount) {
			return [];
		}
		
		const newPosts =  await displayFuncs.smoothedDraw(self);
		return newPosts;
	};

	this.getHolderElement().setAttribute("data-status", "RENDERING");

	const self = this;

	console.dev.time("bufferDraw");

	const loadingOverlay = document.createElement("div");
	loadingOverlay.id= "loadingOverlay";
	loadingOverlay.style.position 		= "absolute";
	loadingOverlay.style.left 			= 0;
	loadingOverlay.style.top 			= 0;
	loadingOverlay.style.background		= "rgb(0, 0, 0, 0.2)";
	loadingOverlay.style.zIndex			= 10000;
	loadingOverlay.style.width			= "100%";
	loadingOverlay.style.height			= "100%";
	loadingOverlay.style.cursor			= "wait";
	//const bodyContainer = document.getElementById("chat-body-container");
	//bodyContainer.append(loadingOverlay);

	document.getElementById("chat-page-posts").appendChild(loadingOverlay);


	renderMessages(self).then((newPosts) => {
		if (newPosts && newPosts.length) {
			self.setAsLastIfNeeded(newPosts[newPosts.length - 1])
		}
		self.chat.eventEmitter.emitEvent("bufferDrawn", [self, newPosts]);
		this.getHolderElement().setAttribute("data-status", "READY");
	}).catch((e) => console.error(e))
	.finally(() =>  {
		//self.lastDrawnPostNum = this.holderElement.lastElementChild.dataset.postNum;
		//self.holderElement.data.last = this.holderElement.lastElementChild;

		loadingOverlay.parentElement.removeChild(loadingOverlay);
		console.dev.timeEnd("bufferDraw");

		finalise(self);
	});
};



ChatChannelBuffer.prototype.renderBufferHeader = function () {

	let $bufInfoContainer = $("#active-buffer-info-container");
	$bufInfoContainer.empty();

	let title = `<span id='name'>${postBuilder.escapeHtml(this.name)}</span>`;
	if (this.topic && this.topic.length) {
		title += `<span contenteditable id='topic'>${postBuilder.formatChatMessage(this.topic.trim()) }</span>`;
	}

	$bufInfoContainer.append(`
		<span id='buffer-users-count' style='float: right'>
			<b id='count'>${this.users.length}</b> paikalla.
		</span>
		<div id='buffer-name-and-topic'>${title}</div>
	`);
};
ChatPrivateBuffer.prototype.renderBufferHeader = async function () {
	let $bufInfoContainer = $("#active-buffer-info-container");
	$bufInfoContainer.empty();

	let self = this;

	let html = '';

	if (this.contact.keysOk) {
		html +=
			`<div id='user-keys-verified'>
				<span id='keys-indicator' style='color: green'>✓</span>
				<span id='verified-contact-info'>
					Keys verified <b>${new Date(self.contact.added_at).toLocaleString()}</b> for alias
					<b id='user-alias'>
					${postBuilder.escapeHtml((self.alias && self.alias.length) ? self.alias : 'Unaliased')}</b>.
				</span>
			</div>`;
	}

	html += `<div id='conversation-id' title='Room checksum'>${await self.getFingerprint()}</div>`;

	const id = self.contact.id;
	const contact = await Contact.load(self.chat, id);

	if (!contact) {
		html += `<button id='save-contact'>Save contact</button>`;
	} else {
		html += `<button id='remove-contact'>Remove contact</button>`;
	}
	html += `<button id='challenge'>Challenge</button>`;

	html += `<button id='display-keys-button'>Keys</button>`;

	$bufInfoContainer.html(html);
};


$("#chat-container").one("chatCreated", (e, chat) => {
	chat.uiFuncs.renderBufferHeader = (() => {
		return chat.getActiveBufferItem().renderBufferHeader();
	});

	$("#chat-top-header").on("click", "#user-alias", (e) => {
		updateAlias(chat.getActiveBufferItem().friend_id);
	});
});
