const mediaViewer = {};

mediaViewer.wrapperElement = document.createElement("div");
mediaViewer.wrapperElement.id = "media-viewer";
mediaViewer.wrapperElement.classList.add("hidden");

mediaViewer.media = null; // video or img
mediaViewer.xhr = null;

mediaViewer.init = function()
{
    document.body.appendChild(mediaViewer.wrapperElement);
}

mediaViewer.isDisplayed = function()
{
    return mediaViewer.media !== null;
}

mediaViewer.getMediaSource = async function(params) {
    if (!params.encrypted) {
        return params.url;
    }
    return await downloadAttachment(params);
}

mediaViewer.isHovered = (() => {
    return !!(mediaViewer.media && mediaViewer.media.matches(":hover"));
})



mediaViewer.getPlayerFunc = function(e)
{
    const playerType = configs.getConfig("media-viewer-type");

    if (e.altKey || playerType === "centered" ||isMobile())
    {
        return mediaViewer.displayMediaCentered;
    } else {
        return mediaViewer.displayMediaNext;
    }
}


mediaViewer.displayMedia = function(params)
{
    return new Promise(async(resolve, reject) => {

        if (mediaViewer.media) {
            return reject();
        }

        mediaViewer.wrapperElement.classList.remove("hidden");

        const playerFunc = params.playerFunc || mediaViewer.getPlayerFunc(params.event);
        mediaViewer.clearPlayer();


        if (params.mime.startsWith("image")) {
            mediaViewer.media = new Image();
        } else if (params.mime.startsWith("video")) {

            mediaViewer.media = document.createElement("video");

            mediaViewer.media.loop          = true;
            mediaViewer.media.volume        = localStorage.getItem("volume") || 0.35;
            mediaViewer.media.controls      = true;
            mediaViewer.media.autoplay      = true;
            mediaViewer.media.controlslist  = "nofullscreen disablePictureInPicture";
            mediaViewer.media.disablePictureInPicture = true;

            const thumb = params.event.target;

            thumb.addEventListener("wheel", (e) => {
                e.preventDefault();        
                const lower = (e.deltaY || e.originalEvent.deltaY) > 0;
                mediaViewer.media.volume = (lower) 
                    ? (Math.max(mediaViewer.media.volume - 0.025, 0))
                    : (Math.min(mediaViewer.media.volume + 0.025, 100));
            });

            mediaViewer.media.onvolumechange = ((e) => {
                localStorage.setItem("volume", e.target.volume);
            });
            
        }

       // const addBackgroundColor = !params.hasAnimation && params.size >= 1024 * 1024;
        //if (addBackgroundColor || true) mediaViewer.media.classList.add("media-loading");


        
        mediaViewer.media.addEventListener("loadedmetadata", function metaLoaded() {
            resolve();
        },{
            once: true
        });
        mediaViewer.media.addEventListener("load", function mediaLoaded(e)  {
            e.target.classList.remove("media-loading");
            resolve();
        }, {
            once: true
        });
        mediaViewer.media.addEventListener("error", (e) => reject(e));
        mediaViewer.media.addEventListener("abort", (e) => reject(e));

        mediaViewer.media.src       = await mediaViewer.getMediaSource(params);
        mediaViewer.media.width     = params.width;
        mediaViewer.media.height    = params.height;

        mediaViewer.media.style.zIndex = 150000;


        mediaViewer.wrapperElement.appendChild(mediaViewer.media);

        try {
            return await playerFunc(params);
            //return reolve(res);
        } catch (e) {
            reject(e);
        }
    });

}

mediaViewer.adjustMediaNext = async function(params)
{
    const vHeight = viewPort.height();
    const vWidth = viewPort.width();

    const thumb = params.event.target;

    const thumbRect = thumb.getBoundingClientRect();

    const spaceInRight = vWidth - thumbRect.right - 25;
    const spaceInLeft = thumbRect.left - 25;
    
    const maxXSpace = Math.max(spaceInLeft, spaceInRight);
    //const xDirection = (spaceInRight > spaceInLeft) ? 'right' : 'left';
    

    mediaViewer.media.style.position = "fixed";
    mediaViewer.media.style.objectFit = "scale-down";

    const scaleRate = Math.min(1, maxXSpace / params.width, vHeight / params.height);

    mediaViewer.media.width = params.width * scaleRate;
    mediaViewer.media.height = params.height * scaleRate;

    const left = (spaceInRight > spaceInLeft) ? thumbRect.right : (thumbRect.left - mediaViewer.media.width);

    const middleLine = vHeight / 2;
    //const topBaseLine = thumbRect.bottom - thumb.offsetHeight / 2;
    const topBaseLine = params.event.clientY;

    const distanceToMiddleLine = (topBaseLine <= middleLine) ? (middleLine - topBaseLine) : (topBaseLine - middleLine);

    const verticalSpaceRatio = distanceToMiddleLine / middleLine;

    const alignment = (mediaViewer.media.height * 0.5 * verticalSpaceRatio) * ((topBaseLine < middleLine) ? 1 : -1)
    const top = (topBaseLine) - (mediaViewer.media.height * 0.5) + alignment;

    mediaViewer.media.style.left    = left + "px";
    mediaViewer.media.style.top     = (top) + "px";
}

mediaViewer.displayMediaNext = async function(params)
{
    await mediaViewer.adjustMediaNext(params);
    const thumb = params.event.target;
    const messages = document.getElementById("chat-page-posts");

    const unknownSizes = params.rotation || params.buffer instanceof ChatE2eEncryptedBuffer;

    if (unknownSizes) {

        mediaViewer.wrapperElement.classList.add("hidden");

        const mediaLoaded = ((e) => {
            const m = e.target;
            params.width    = m.naturalWidth  || m.videoWidth;
            params.height   = m.naturalHeight || m.videoHeight;
            mediaViewer.adjustMediaNext(params);

            mediaViewer.wrapperElement.classList.remove("hidden");
        });
        mediaViewer.media.addEventListener("loadedmetadata",    mediaLoaded, {once: true} )
        mediaViewer.media.addEventListener("load",              mediaLoaded, {once: true})
    }
    
    function scrolled(e) {
        mediaViewer.adjustMediaNext(params);
    }

    function mousemoved(e) {
        params.event = e;
        mediaViewer.adjustMediaNext(params);
    }

    function mouseLeft(e) {

		if (configs.getConfig("dev-mode")) return;

        if (mediaViewer.isHovered() && configs.getConfig("hoverable-videos") && mediaViewer.media.nodeName === "VIDEO") {
            mediaViewer.media.addEventListener("mouseleave", mouseLeft);
            return;
        }

        mediaViewer.clearPlayer();
        thumb.removeEventListener("mouseleave", mouseLeft);
        messages.removeEventListener("scroll", scrolled);
        thumb.removeEventListener("mousemove", mousemoved);

        params = null;
    }

    thumb.addEventListener("mouseleave", mouseLeft);
    thumb.addEventListener("mousemove", mousemoved);

    messages.addEventListener("scroll", scrolled);
}

/*
mediaViewer.centeredSTyle =  document.createElement("style");
mediaViewer.centeredSTyle.innerText = `
    #media-viewer > img {
        max-height: 100vh !important;
        object-fit: scale-down;
    }
`;*/

mediaViewer.displayMediaCentered = async function(params)
{

    mediaViewer.wrapperElement.style.left               = "0px";
    mediaViewer.wrapperElement.style.top                = "0px";
    mediaViewer.wrapperElement.style.width              = "100vw";
    mediaViewer.wrapperElement.style.height             = "100vh";
    mediaViewer.wrapperElement.style.position           = "absolute";
    mediaViewer.wrapperElement.style.display            = "flex";
    mediaViewer.wrapperElement.style.justifyContent     = "center"
    //mediaViewer.wrapperElement.style.alignItems         = "center"
    mediaViewer.wrapperElement.style.background         = "rgb(0,0,0,0.05)";
    mediaViewer.wrapperElement.style.overflowY          = "auto";

    //mediaViewer.media.style.objectFit                   = "scale-down";
    //mediaViewer.media.style.maxWidth                    = "95vw";
   // mediaViewer.media.style.maxHeight                   = "100vh";
    //mediaViewer.media.style.overflowY                   = "auto";
    mediaViewer.media.style.margin                      = "auto";

    function setMediaDimension(limitHeight=false) 
    {
        const dimensionLimits = [
            (Math.max(window.outerWidth - 100), window.outerWidth * 0.95) / params.width, 
            1
        ];
        
        if (limitHeight) {
            dimensionLimits.push(window.innerHeight / params.height)
            mediaViewer.media.classList.add("limitedHeight");
        } else {
            mediaViewer.media.classList.remove("limitedHeight");
        }

        const downScaleRate = Math.min(...dimensionLimits);

        mediaViewer.media.width     = params.width * downScaleRate;
        mediaViewer.media.height    = params.height * downScaleRate;

        if (mediaViewer.media.height < mediaViewer.media.naturalHeight) {
            mediaViewer.media.style.cursor = "pointer";
        }
    }

    setMediaDimension(true);
    mediaViewer.media.addEventListener("load", function mediaLoaded(e) {
        if (e.target?.parentNode && e.target === mediaViewer.media) {
            setMediaDimension(mediaViewer.media);
        }
    }, {
        once: true
    });

    mediaViewer.wrapperElement.addEventListener("click", function handlePlayerClick(e) {

        if (e.target !== mediaViewer.media) {            
            mediaViewer.wrapperElement.removeEventListener("click", handlePlayerClick);
            mediaViewer.clearPlayer();
            
            return;
        }
        const isLimited = mediaViewer.media.classList.contains("limitedHeight");
        setMediaDimension(isLimited == false);
    });
}

mediaViewer.clearPlayer = function()
{
    //mediaViewer.centeredSTyle.parentElement?.removeChild(mediaViewer.centeredSTyle);


    mediaViewer.wrapperElement.style.left               = "0";
    mediaViewer.wrapperElement.style.top                = "0";
    mediaViewer.wrapperElement.style.width              = "unset";
    mediaViewer.wrapperElement.style.height             = "unset";
    mediaViewer.wrapperElement.style.position           = "unset";
    mediaViewer.wrapperElement.style.display            = "unset";
    mediaViewer.wrapperElement.style.justifyContent     = "unset"
    mediaViewer.wrapperElement.style.background         = "unset";

    if (!mediaViewer.media) {
        return;
    }

    if (mediaViewer.media && mediaViewer.media.nodeName === "VIDEO") { 
        mediaViewer.media.pause();
        mediaViewer.media.removeAttribute("src");
        mediaViewer.media.load();
    } else {
        mediaViewer.media.src = null;
    }
    if (mediaViewer.media.parentNode) {
        mediaViewer.media.parentNode.removeChild(mediaViewer.media);
    }

    mediaViewer.media.dispatchEvent(new Event("destroy"));

    mediaViewer.media = null;
}




function validateSignature(chipperi, _signer, signature) {
    return new Promise((resolve, reject) => {
        return verifyData(_signer, signature, chipperi)
            .then((signatureMatches) => resolve(signatureMatches));
    });
}

function decryptFile(kikkeli) {
    return new Promise(async(resolve, reject) => {
        _chatGlobal.crypto.profile.decryptSharedMessage(
            await buf.contact.getSharedAES(buf.chat),
            kikkeli,
            iv,
        ).then((attachment) => {
            const dec = new TextDecoder();
            const decoded = dec.decode(attachment);

            return resolve(decoded);
        }).catch((e) => reject(e));
    });
}


mediaViewer.openDecyprtion = async function(params, file)
{
    const httpResult = mediaViewer.xhr.response;
    const buf = params.buffer;

    const signer = (params.poster === buf.chat.me.id)
        ? buf.chat.crypto.profile.signer.publicKey
        : buf.contact.keys.signer.key;

    if (!signer) return baseReject(new Error("Failed to fetch signer."));


    const attachmentChiper = b64ToArrayBuffer(httpResult);

    const signatureValid = await validateSignature(attachmentChiper, signer, hex2buf(params.signature));
    if (!signatureValid) {
        throw new Error("Invalid signature");
    }

    const decryptedBase64 = await decryptFile(attachmentChiper);
    return baseResolve(decryptedBase64);
}

mediaViewer.downloadAttachment = async function(params, progressCallback) {

    const isEncrypted = params.encrypted;
    const mime = params.mime;

    return new Promise((baseResolve, baseReject) => {

        if (mediaViewer.xhr) {
            mediaViewer.xhr.abort();
            mediaViewer.xhr = null;
        }

        mediaViewer.xhr = new XMLHttpRequest();
        mediaViewer.xhr.open("GET", params.url, true);

        if (isEncrypted) {
            mediaViewer.xhr.overrideMimeType('text/plain; charset=x-user-defined');
        } else {
            mediaViewer.xhr.responseType = "arraybuffer";
        }

        mediaViewer.xhr.onerror = () => baseReject(new Error("HTTP error"));

        if (progressCallback) {
            mediaViewer.xhr.onprogress = progressCallback;
        }


        mediaViewer.xhr.addEventListener("load", (async () => {

            if (mediaViewer.xhr.status !== 200 && mediaViewer.xhr.status !== 206) {
                if (mediaViewer.xhr.status === 404) return baseReject(new Error("File not found"));
                else return baseReject(new Error("Failed to download file"));
            }

            if (!isEncrypted) {
                const baseStart = `data:${mime};base64,`;
                const base64 = arrayByfferToBase64(mediaViewer.xhr.response);

                return baseResolve(baseStart + base64);
            }


            const downloadedFile = attachmentDownloadXhr.response;

            const signer = (params.poster === buf.chat.me.id)
                ? buf.chat.crypto.profile.signer.publicKey
                : buf.contact.keys.signer.key;

            if (!signer) return baseReject(new Error("Failed to fetch signer."));
            const attachmentChiper  = b64ToArrayBuffer(downloadedFile);
         
            try {
                const signatureValid = await validateSignature(attachmentChiper, signer, hex2buf(params.signature));
                if (!signatureValid) {
                    return baseReject(new Error("Invalid signature"));
                } 
                const decryptedBase64 = await decryptFile(attachmentChiper);
                return baseResolve(decryptedBase64);
            } catch (e) {
                return baseReject(e);
            }
        }));

        mediaViewer.xhr.send();

    });
}


mediaViewer.init();
