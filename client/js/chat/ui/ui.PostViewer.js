const postsViewer = {}


postsViewer.viewersHolder = document.querySelector("div#chat-post-viewers");
postsViewer.currentlyHovered = null;

postsViewer.getViewerElementGreaterThan = function(monesko) {
	const res = [];
	const viewers = postsViewer.viewersHolder.querySelectorAll("div.message-viewer-wrapper")

	for (var i = 0; i < viewers.length; i++) {
		const node = viewers[i];
		if (node.dataset.mones <= monesko) continue;
		res.push(node);
	}
	return res;
}

postsViewer.getHoveredtViewer = function()
{
	const last = postsViewer.viewersHolder.querySelector("div.message-viewer-wrapper:hover");
	return last;
}

postsViewer.isDisplayed = function()
{
	return postsViewer.viewersHolder.childElementCount !== 0;
}

postsViewer.isHovered = function()
{
	return postsViewer.viewersHolder.matches(":hover");
}

postsViewer.view = function(posts, e, parentId)
{
	const sourceNode = e.target;

	const postsViewerElement = document.createElement("div")
	postsViewerElement.style.position = "fixed";
	postsViewerElement.classList.add("message-viewer-wrapper");

	postsViewerElement.dataset.mones = postsViewer.viewersHolder.childElementCount + 1;;
	postsViewerElement.dataset.parent = parentId;

	const postsFragment = document.createDocumentFragment();
	for (let i = 0; i < posts.length; i++) {
		const post = posts[i].cloneNode(true);
		post.classList.remove("hidden");
		postsFragment.appendChild(post);

		if (post.classList.contains("lazy")) {
			lazyLoader.loadPost(post);
		}
	}

	postsViewerElement.appendChild(postsFragment);

	postsViewerElement.style.display = "block";
	postsViewerElement.style.visibility = "hidden";

	postsViewerElement.addEventListener("mouseenter", (e) => postsViewer.currentlyHovered = e.target);
	postsViewerElement.addEventListener("mouseleave", () => postsViewer.currentlyHovered = null);

	postsViewer.viewersHolder.appendChild(postsViewerElement);


	postsViewer.setPosition(postsViewerElement, e, sourceNode)

	return postsViewerElement;
}


postsViewer.destroyNode = function(node)
{
	const e = new CustomEvent("destroy");
	node.dispatchEvent(e);
	node.parentElement.removeChild(node);
}

postsViewer.destroyNodes = function(nodesToRemove)
{
	nodesToRemove.forEach((node) => postsViewer.destroyNode(node));
}

postsViewer.setPosition = async function(postsViewerElement, e, sourceNode) {

	const dHeight = viewPort.height();
	const dWidth = viewPort.width();

	const pos = sourceNode.getBoundingClientRect();

	const toRight = pos.left < dWidth - pos.left;

	const maxWidth = (toRight) 
						? dWidth - pos.left - sourceNode.offsetWidth
						: pos.left;

	postsViewerElement.style.maxWidth = `${maxWidth}px`;
	postsViewerElement.style.maxHeight = `${dHeight * 0.75}px`;

	// wait width and height to be set so position can be adjusted
	await (async () => { return new Promise((resolve) => window.requestAnimationFrame(() => resolve()));})();

	const left = (toRight)
					? (pos.left + sourceNode.offsetWidth)
					: (pos.left - postsViewerElement.offsetWidth);


	const viewerHeight = postsViewerElement.offsetHeight;

	const spaceUnder = dHeight - pos.top;
	const spaceAbove = pos.top;

	const hasRequiredSpaceAbove = spaceAbove > viewerHeight;

	// Always prefer space above if viewer is not already open.
	const directToAbove = (hasRequiredSpaceAbove) || spaceAbove > spaceUnder;

	let top = (directToAbove)
		? (pos.top - viewerHeight + 16 + sourceNode.offsetHeight)
		: (pos.top - 8);

	let overInTop = (pos.top - viewerHeight) * -1;
	if (directToAbove && overInTop > 0) {
		top += overInTop;
	}

	let overInBot = top + viewerHeight - dHeight;
	if (!directToAbove && overInBot > 0) {
		top -= overInBot * 1.25;
	}

	postsViewerElement.style.top 		= `${top}px`;
	postsViewerElement.style.left 		=  `${left}px`;
	postsViewerElement.style.maxWidth 	= `${maxWidth}px`;
	postsViewerElement.style.maxHeight 	= `${dHeight * 0.75}px`;
	postsViewerElement.style.zIndex 	= 1000 + postsViewerElement.dataset.mones;
	postsViewerElement.style.visibility = "visible";

	if (sourceNode.parentNode && sourceNode.parentNode.classList.contains("replies")) {
		if (directToAbove) {
			postsViewerElement.style.left = `${pos.left}px`;
			postsViewerElement.style.top = `${pos.top - postsViewerElement.offsetHeight}px`;
		} else {
			const linkHeight = sourceNode.offsetHeight;

			postsViewerElement.style.left = `${pos.left}px`;
			postsViewerElement.style.top = `${pos.top + linkHeight}px`;
		}
	}

	async function mouseLeaveCallback(e, timeout=null) {

		if (timeout) {
			//if (e.target)
			return setTimeout(() => mouseLeaveCallback(e, null));
		}

		//if (postsViewer.isHovered()) return;
		if (configs.getConfig("dev-mode")) return;
		//if ($("#media-viewer").is(":hover")) return;
		if (mediaViewer.isDisplayed()) return;

		const hoveredViewer = postsViewer.getHoveredtViewer();

		const mones = (hoveredViewer) ? hoveredViewer.dataset.mones : 0;
		const nodesToRemove = postsViewer.getViewerElementGreaterThan(mones);
		postsViewer.destroyNodes(nodesToRemove);

		if (e?.target) {
			e.target.removeEventListener("mouseleave", mouseLeaveCallback);
		}
	}

	if (hasMouse()) 
	{
		postsViewerElement.addEventListener("destroy", () => {
			postsViewerElement.removeEventListener("mouseleave", mouseLeaveCallback);
		}, {
			once: true,
		});

		if (configs.getConfig("hoverable-replies")) {
			postsViewerElement.addEventListener("mouseleave",  (() => mouseLeaveCallback(e, 300)));
			sourceNode.addEventListener("mouseleave", mouseLeaveCallback, {once: true})
		} else {
			sourceNode.addEventListener("mouseleave", () => {
				postsViewer.destroyNodes(postsViewer.viewersHolder.childNodes);
			}, {once: true});
		}
	}

	return postsViewerElement;
};

document.body.addEventListener("click", (e) => {
	setTimeout(() => {

		
		if (hasMouse() && (postsViewer.isDisplayed() && !mediaViewer.isDisplayed()))
		{
				const hoveredViewer = postsViewer.getHoveredtViewer();
				const nodesToRemove = postsViewer.getViewerElementGreaterThan((hoveredViewer) ? hoveredViewer.dataset.mones : 0);

				postsViewer.destroyNodes(nodesToRemove);
		}

		if (!hasMouse()) {
			if (!e.target.classList.contains("chat-reply-backlink")) {

				const hoveredViewer = postsViewer.getHoveredtViewer();
				const nodesToRemove = postsViewer.getViewerElementGreaterThan((hoveredViewer) ? hoveredViewer.dataset.mones : 0);

				postsViewer.destroyNodes(nodesToRemove);
			}
		}

	}, 10);

})
