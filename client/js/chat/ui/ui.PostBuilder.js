
const postBuilder = {};

postBuilder.SMILEYS = ['ROLLEYES', 'AWKWARD', 'TONGUE', 'ROFL', 'DANCE', 'FUCKTHEAI', 'SECRET', 'HUNTER', 'HI', 'AR15', 'FIGHT', 'CRZY', 'DUNNO', 'SEARCH', 'BLUSH', 'FOCUS', 'SHOCK', 'TIRED', 'OK', 'HOCH', 'BLACKEYE', 'UPSET', 'PSYCHEDELICS', 'SMILEY', 'STOP', 'BOUNCE', 'PRAISE', 'OLD', 'AAAARGH', 'YES', 'DEVIL', 'YAHOO', 'GLASSES', 'YEEES!', 'BAAA', 'DRINK', 'TEASE', 'DEADHORSE', 'POPCORN', 'CRAZY', 'AGREED', 'DIZZY', 'TRAINING', 'NAUGHTY', 'BYE', 'JOB', 'THANX', 'LAUGH', 'SUP', 'COOL', 'WINK', 'BUMP', 'OFFTOPIC', 'GAMER', 'LAZY', 'SMOKE', 'JOKINGLY', 'HELP', 'CRY', 'WIZARD', 'SMILE', 'HOWDY', 'HUH', 'TIP', 'SHOCKED', 'THANK', 'SCRATCH', 'HUG', 'ROCK', 'WALL', 'THUMBSUP', 'AI', 'SORRY', 'FROWN', 'PUKE', 'WALL2', 'BUSY', 'HEADPHONES', 'HOCH2', 'DRUNK', 'SCAN', 'BANANA']

postBuilder.escapeHtml = function(text) {
    if (!text) {
        return '';
    }

    return text.replace(/\%/g, "&amp;")
                .replace(/\</g, "&lt;")
                .replace(/\>/g, "&gt;")
                .replace(/\"/g, "&quot;")
                .replace(/\'/g, "&#039;");
}

postBuilder.reFormatters = Object.values({
    //buffer: null,
    replyLink: {
        regex: /&gt;&gt;(\d+)/gm,
        //out: "<a class='chat-reply-backlink' data-replies-to=$1>&gt;&gt;$1</a>",
        out: (match, contents, offset, input_string) => {
            const postNum = parseInt(match.substr(8), 10); // ;gt
            const buffer = postBuilder.reFormatters.buffer;
            const backLink = postBuilder.buildBacklink(postNum, buffer);
            return backLink.outerHTML;
        },
    },
    /*
    greenText: {
        regex:  /(^\&gt;.*)/gm,
        out: "<span class='greentext'>$1 </span>",
    },*/
    greenText: {
        regex:  /^[\s]?(&gt;.+)/gm,
        out: "<span class='greentext'>$1 </span>",
    },
    urlExpression: {
        regex:  /(?:\s|^)((http[s]{0,1}\:\/\/\S{4,}))\s{0}/gi,
        //regex:  /((http[s]{0,1}\:\/\/\S{4,}))\s{0}/gi,
        //out:    "<a href=\"$1\" class='link' rel='noopener noreferrer' target=\"_blank \">$1</a> ",
        out: (match, contents, offset, input_string) => {
            const post = postBuilder.reFormatters.post;

            const linkName = post?.options?.links?.find((it) => it.url === contents)?.title || match.trim();
            
            return ` <a href='${contents}' title='${linkName}' class='link' rel='noopener noreferrer' target="_blank">${linkName.substr(0, 128)}</a> `;
        },
    },
    magnet: {
        regex:  /(^|\n)(magnet\:\?xt=\S+)/g,
        out:    "<a href='$2' class='link magnet'>$2</a>",
    },
    /*
    spoiler: {
        regex:  /\[spoiler\]([\s|\S]*?)(\[\/spoiler\]|$)/g,
        out:    "<span class='spoiler'>$1</span>"
    },*/
    spoilerAsterix: {
        regex:  /\*\*(.*?)\*\*/g,
        out:    "<span class='spoiler'>$1</span>",
    },
    redText:{
        regex:  /==(.*?)==/g,
        out:    "<span class='redtext'>$1</span>",
    },
    bold: {
        regex:  /\[b\]([\s|\S]*?)(\[\/b\]|$)/g,
        out:    "<span class='bolted'>$1</span>"
    },
    bold2: {
        regex:  /\'\'\'(.*?)\'\'\'/g,
        out:    "<span class='bolted'>$1</span>"
    },
    italic: {
        regex:  /\[i\]([\s|\S]*?)(\[\/i\]|$)/g,
        out:    "<span class='italic'>$1</span>"
    },
    nonono: {
        regex: /\[nonono\]([\s|\S]*?)(\[\/nonono\]|$)/m,
        //out: "<a class='chat-reply-backlink' data-replies-to=$1>&gt;&gt;$1</a>",
        out: (match, contents, offset, input_string) => {
            return `<div class='animation nonono'>${contents}</div>`;
        },
    },
});

postBuilder.reFormatters.registerRegTag = function(tag, name, callback=null, extraClass='') {

    postBuilder.reFormatters.push({
        regex: tag,
        process: (input_string, re, match) => {

            const openingTag = match.at(0);
            const closingTag = "[/" + name + "]";

            const matchStartPos =  match.index;
            
            // first position after opening tag eg. [b]<-here
            const bodyStartPos = matchStartPos + openingTag.length;

            // position where closing tag eg. [/b] (or next [b]) is found, or the end of string
            const potentialEndPositions = new Array();

            const nextTagClose          = input_string.indexOf(closingTag, bodyStartPos);
            const nextSameTagStartPos   = input_string.substr(bodyStartPos).search(re);

            potentialEndPositions.push(input_string.length);

            if (nextTagClose >= 0) potentialEndPositions.push(nextTagClose);

            if (nextSameTagStartPos >= 0) {
                potentialEndPositions.push(bodyStartPos + nextSameTagStartPos);
            }

            const bodyEndPos = Math.min(...potentialEndPositions);

            const head  = input_string.substr(0, matchStartPos);
            const body  = input_string.substring(bodyStartPos, bodyEndPos);
            let tail    = input_string.substr(bodyEndPos);
            
           
            // same as bodyEndPos == nextTagClose
            //if (foundClosingTag) {
            if (tail.startsWith(closingTag)) {
                tail = tail.substring(closingTag.length);
            }

            if (
                head.lastIndexOf("[code]") > head.lastIndexOf("[/code]")
            ) {
                return false;
            }

            if (callback) {
                return callback({
                    head: head,
                    body: body,
                    tail: tail,
                    name: name,
                    extraClass: extraClass,
                    match: match,
                    input_string: input_string,
                });
            }
            return head + `<span class='${name} ${extraClass}'>${body}</span>` + tail;
        }
    });
}

postBuilder.registerTag = function(tag, callback=null, extraClass='') {
    postBuilder.tags.push({
        regex: tag,
        process: (input_string, matchStartPos)  => {

            const openingTag = "[" + tag  + "]";
            //const openingTag = match.at(0);
            const closingTag = "[/" + tag+ "]";

            //const matchStartPos =  match.index;
            //const matchStartPos =  input_string.indexOf(openingTag);
            
            // first position after opening tag eg. [b]<-here
            const bodyStartPos = matchStartPos + openingTag.length;

            // position where closing tag eg. [/b] (or next [b]) is found, or the end of string
            const potentialEndPositions = new Array();

            const nextTagClose          = input_string.indexOf(closingTag, bodyStartPos);
            //const nextSameTagStartPos   = input_string.substr(bodyStartPos).search(re);
            //const nextSameTagStartPos   = input_string.substr(bodyStartPos).indexOf(openingTag);

            potentialEndPositions.push(input_string.length);

            if (nextTagClose >= 0) potentialEndPositions.push(nextTagClose);

            //if (nextSameTagStartPos >= 0) {
            //    potentialEndPositions.push(bodyStartPos + nextSameTagStartPos);
            //}

            const bodyEndPos = Math.min(...potentialEndPositions);

            const head  = input_string.substr(0, matchStartPos);
            const body  = input_string.substring(bodyStartPos, bodyEndPos);
            let tail    = input_string.substr(bodyEndPos);
            
           
            // same as bodyEndPos == nextTagClose
            //if (foundClosingTag) {
            if (tail.startsWith(closingTag)) {
                tail = tail.substring(closingTag.length);
            }

            if (
                head.lastIndexOf("[code]") > head.lastIndexOf("[/code]")
            ) {
                return false;
            }

            if (callback) {
                return callback({
                    tag: tag,
                    head: head,
                    body: body,
                    tail: tail,
                    extraClass: extraClass,
                    //match: match,
                    input_string: input_string,
                });
            }
            return head + `<span class='${name} ${extraClass}'>${body}</span>` + tail;
        }
    });
}

postBuilder.reFormatters.registerRegTag(/\[spin\]/, "spin", null, "animation");
postBuilder.reFormatters.registerRegTag(/\[lspin\]/, "lspin", null, "animation");
postBuilder.reFormatters.registerRegTag(/\[roll\]/, "roll", null, "animation")
postBuilder.reFormatters.registerRegTag(/\[ree\]/, "ree", null,);
postBuilder.reFormatters.registerRegTag(/\[hflip\]/, "hflip", null,);
postBuilder.reFormatters.registerRegTag(/\[vflip\]/, "vflip", null,);
postBuilder.reFormatters.registerRegTag(/\[hop\]/, "hop", null);
postBuilder.reFormatters.registerRegTag(/\[walk\]/, "walk", null);
postBuilder.reFormatters.registerRegTag(/\[s\]/, "s", null);
postBuilder.reFormatters.registerRegTag(/\[u\]/, "u", null);
postBuilder.reFormatters.registerRegTag(/\[spoiler\]/, "spoiler", null);

/*
postBuilder.reFormatters.registerRegTag(/\[flag\]/, "flag", (params) => {
    const head = params.head;
    const body = params.body;
    const tail = params.tail;
    const match = params.match;

    return head + `<img class='kotflog' src='https://kotchan.fun/icons/countries2.${match}png'>` + tail;
});*/


postBuilder.reFormatters.registerRegTag(/\[color=(#?[a-zA-Z|\d]{1,8})]/, "color", (params) => {

    const head = params.head;
    const body = params.body;
    const tail = params.tail;
    const match = params.match;

    return head + `<span class='colored' style='color:${match.at(-1)} !important'>${body}</span>` + tail;
});
postBuilder.reFormatters.registerRegTag(/\[scale=(-?[.|\d]{1,15})]/, "scale", (params) => {

    const head = params.head;
    const body = params.body;
    const tail = params.tail;
    const match = params.match;

    return head + `<span class='scaled' style='transform: scale(${match.at(-1)}); transform-origin: 0px 0px 0px; display: inline-block;'>${body}</span>` + tail;
});
postBuilder.reFormatters.registerRegTag(/\[rotate=([+-]{0,1}[\d]{1,6})]/, "rotate", (params) => {

    const head = params.head;
    const body = params.body;
    const tail = params.tail;
    const match = params.match;

    return head + `<span class='animation' style='transform: rotate(${match.at(-1)}deg); display: inline-block'>${body}</span>` + tail;
});
postBuilder.reFormatters.registerRegTag(/\[st\]([\S]{1,8})-/, "st", (params) => {

    const buf = postBuilder.reFormatters.buffer;
    const stickersMap = buf.chat.getStickers(buf);
    
    if (!stickersMap) return false

    const match = params.match;
    const category = match.at(1);

    const head = params.head;
    const body = params.body;
    const tail = params.tail;

    const stickerKey = category + "-" + body;

    const stickerItem = stickers.getSortedListItem(stickerKey, stickersMap.items);

    if (!stickerItem) {
        return false;
    }

    const MAX_H = 135;
    const scaleRate = Math.min(1, MAX_H / stickerItem.height);

    const width = stickerItem.width * scaleRate;
    const height = stickerItem.height * scaleRate;

    return head 
        + `<span class='sticker-wrapper' data-sticker="${stickerItem.key}" width="${width}" height="${height}"`
        +   `style="display: inline-block; width: ${width}px; height: ${height}px">`
        + `</span>`
    + tail;
});
postBuilder.reFormatters.registerRegTag(/\[code\]/, "code")





postBuilder.formatChatMessage = function(message, buffer, post = null) {
    //message = postBuilder.escapeHtml(message);

    message = message.trim().replaceAll(">", "&gt;")
                        .replaceAll("<", "&lt;")
                        .replace(/\"/g, "&quot;")
                        .replace(/\'/g, "&#039;");

    postBuilder.reFormatters.buffer = buffer;
    postBuilder.reFormatters.post = post;
                    
    const formattersCount = postBuilder.reFormatters.length;
    for (var i = 0; i < formattersCount; i++) {
        const formatter = postBuilder.reFormatters[i];

        if (!formatter.regex) throw new Error(`Regex '${formatter.out}' not defined.`);
        if (!formatter.out && !formatter.process) throw new Error(`Regex '${formatter.regex}' not defined.`);

        if (formatter.disabled) continue;

        if (formatter.process) {
            let limiter = formatter.limit || -1;
            
            formatter.regex.exec("");
            formatter.regex.lastIndex = 0;

            if (formatter.regex instanceof RegExp) {
                let match;
                while ((match = formatter.regex.exec(message))?.length && limiter !== 0) {
                    if (!match.length) break;

                    const res = formatter.process(message, formatter.regex, match);
                    if (res === false || res === null) break;
                    message = res;
                    limiter--;
                    formatter.regex.exec("");
                    formatter.regex.lastIndex = 0;
                }
                formatter.regex.exec("");
                formatter.regex.lastIndex = 0;
            }

        } else {
            message = message.replace(formatter.regex, formatter.out);
        }
    }

    postBuilder.reFormatters.buffer = null;
    postBuilder.reFormatters.post = null;


    for (let i = 0; i < postBuilder.SMILEYS.length; i++)
    {
        message = message.replaceAll("*" + postBuilder.SMILEYS[i] + "*", "<img src='https://kotchan.fun/icons/smiles/" + postBuilder.SMILEYS[i] + ".gif'>")
    }

    return message;
}


postBuilder.htmlModels = {};
postBuilder.htmlModels.article = `
<article class='chat-post-wrapper'>
    <header class='chat-post-header'>
        <a class='chat-post-sender'></a>
        <span class='poster-icons-holder'>
            <!--<img class='poster-icons hidden' width=15 height=15>!-->
        </span>

        <span class='chat-message-timestamp' title=''></span>
        <a class="message-toggle-expand hidden">+</a>
        <a class='chat-post-num'></a>
        <div class="replies"></div>

        <span class='chat-post-delete default-right' title='Remove post' style=''>x</span>
        <span class='chat-post-toggle default-right'>-</span>
        <span title='Kuiskaa' class='chat-post-whisper default-right'>
            <img class='whisper-icon' width=15 height=15 src='img/icons/whisper.png'>
        </span>
        &nbsp;

    </header>

    <div class='chat-post-body'>
        <div class='chat-post-content'>
        </div>
    </div>
</article>`.trim();

postBuilder.htmlModels.postMessage =
`<div class='chat-post-message'></div>`.trim();

postBuilder.htmlModels.attachmentHolder = 
`<a class='chat-post-attachment' target='_blank' el='noopener noreferrer'></a>`;

postBuilder.htmlModels.imgThumb = 
`<img class='thumb-lazy attachment-thumb' data-src=''>`.trim();
/*postBuilder.htmlModels.imgThumb = 
`<picture class='thumb-lazy attachment-thumb' data-src=''>
    <source class='static'>
    <source class='animated'>
    <source class='tiny'>
</picture>`.trim();*/


postBuilder.htmlModels.attachmentFooter = 
`<footer class='chat-attachment-footer-container'>
    <span class='attachment-size'></span>
    <span class='spacer'></span>
    <span class='attachment-dimensions'></span>
    <span class='spacer'></span>
    <span class='attachment-duration'></span>
    <span class='spacer'></span>
    <span class='attachment-type'></span>
    <span class='spacer'></span>
    <a target='_blank' class='attachment-link'></a>
    <span class='attachment-note' style='display:none'>
        <span class='spacer'></span>
        <span class='note-message'></span>
    </span>
</footer>`.trim()

postBuilder.htmlModels.artCover = `
    <div class='art-cover-holder'>
        <img alt='' class='thumb-lazy attachment-thumb attachment-audio audio-cover-art'>
        <div class='audio-ext'></div>
    </div>`.trim();

postBuilder.articleModels = new Map();


function removeTextNodes(node) {
    node.childNodes.forEach((it) => {
        if (it.nodeName === "#text") {
            it.parentNode.removeChild(it);
        }
    })
}

function buildDocument(htmlSource) {

    //const fragment = document.createRange().createContextualFragment(htmlSource);
    //return fragment.firstChild;

    const template = document.createElement("template");
    template.innerHTML = htmlSource;
    return template.content.firstChild;
}
postBuilder.models = {
    article                             : buildDocument(postBuilder.htmlModels.article),
    postMessage                         : buildDocument(postBuilder.htmlModels.postMessage),
    attachmentHolder                    : buildDocument(postBuilder.htmlModels.attachmentHolder),
    imgThumb                            : buildDocument(postBuilder.htmlModels.imgThumb),
    artCover                            : buildDocument(postBuilder.htmlModels.artCover),
    attachmentFooter                    : buildDocument(postBuilder.htmlModels.attachmentFooter),
};

const flagIconModel = document.createElement("img");

flagIconModel.classList.add("poster-icon");
flagIconModel.width = 20;
flagIconModel.height = 20;
flagIconModel.loading = "lazy";

postBuilder.models.flagIcon = flagIconModel

const _a = postBuilder.models.article;
const _article = Object.assign(postBuilder.models.article, {
    header                              : _a.querySelector("header.chat-post-header"),
    postNum                             : _a.querySelector(".chat-post-num"),
    timeStamp                           : _a.querySelector(".chat-message-timestamp"),
    replies                             : _a.querySelector(".replies"),
    postSender                          : _a.querySelector(".chat-post-sender"),
    posterIconHolder                    : _a.querySelector(".poster-icons-holder"),
    postContent                         : _a.querySelector(".chat-post-content"),
    postBody                            : _a.querySelector(".chat-post-body"),
    contentWrapper                      : _a.querySelector(".content-wrapper"),
    //clearer                             : _a.querySelector(".clearer"),
});
removeTextNodes(_article.header);


const _attachmentFooter = postBuilder.models.attachmentFooter;
_attachmentFooter.size                  = _attachmentFooter.querySelector(".attachment-size");
_attachmentFooter.dimensions            = _attachmentFooter.querySelector(".attachment-dimensions");
_attachmentFooter.duration              = _attachmentFooter.querySelector(".attachment-duration");
_attachmentFooter.type                  = _attachmentFooter.querySelector(".attachment-type");
_attachmentFooter.name                  = _attachmentFooter.querySelector(".attachment-link");
_attachmentFooter.embed                 = _attachmentFooter.querySelector(".embed");
removeTextNodes(_attachmentFooter);

const _artCover = postBuilder.models.artCover;
_artCover.img = _artCover.querySelector("img");
_artCover.ext = _artCover.querySelector(".audio-ext");


function setDatasetIfHas(name, value, element) {
    if (!value) {
        delete element.dataset[name]
        return;
    }

    try {
        element["dataset"][name] = value;
    } catch (e) {
        console.info(name, value);
        console.error(e);
    }
}

secureHEXString = (hString) => {
    if (!hString) return '';
    return hString.replace(/\W/g, '');
};

postBuilder.iconMap = new Map();


postBuilder.buildPostAttachment = function(attachment, postData, buffer) {

    const attachmentHolder = postBuilder.models.attachmentHolder//.cloneNode(false);
    const imgThumb = postBuilder.models.imgThumb//.cloneNode(true)
    const artCover = postBuilder.models.artCover;

    if (attachmentHolder && attachmentHolder.parentNode) attachmentHolder.parentNode.removeChild(attachmentHolder);
    if (imgThumb && imgThumb.parentNode) imgThumb.parentNode.removeChild(imgThumb);
    if (artCover && artCover.parentNode) artCover.parentNode.removeChild(artCover);

    const meta = attachment.meta;

    if (!meta) {
        return;
    }

    const mime = meta.mime;

    attachmentHolder.dataset.mime = mime;
    attachmentHolder.href = attachment.url;

    const isEncrypted = postData.options && postData.options.encrypted

    const iv = secureHEXString(postData.options.attachment_data_iv);
    const signature = secureHEXString(postData.options.signature_attachment);   

    setDatasetIfHas("size", attachment.size, attachmentHolder);
    setDatasetIfHas("encrypted", isEncrypted, attachmentHolder);
    setDatasetIfHas("signature", signature, attachmentHolder);
    setDatasetIfHas("iv", iv, attachmentHolder);

    if (meta.title) {
        attachmentHolder.title = attachmentHolder.dataset.title = meta.title;
    } else {
        delete attachmentHolder.dataset.title;
        attachmentHolder.removeAttribute("title");
    }


    const isImage = mime.startsWith("image/");
    const isVideo = mime.startsWith("video/");
    const isPdf   = mime === "applicaiton/pdf";

    const canBeViewed = meta.thumb_url_default || meta.thumb_url_static

    if (canBeViewed && meta) {

        
        setDatasetIfHas("alpha", meta.has_alpha, imgThumb);
        setDatasetIfHas("rotation", meta.rotation, imgThumb);
       // setDatasetIfHas("animated", meta.animated_thumb, imgThumb);
        setDatasetIfHas("width", meta.width, imgThumb);
        setDatasetIfHas("height", meta.height, imgThumb);
        

        const thumbUrlStatic = meta.thumb_url_static || meta.thumb_url_default;

        delete imgThumb.dataset.thumbStatic;
        if (thumbUrlStatic) {
            //const staticExtStart = thumbUrlStatic.lastIndexOf(".");
            imgThumb.dataset.thumbStatic = thumbUrlStatic.split("/").at(-1);
        }
        delete imgThumb.dataset.thumbAnimated;
        if (meta.thumb_url_animated) {
            imgThumb.dataset.thumbAnimated = meta.thumb_url_animated.split("/").at(-1);
        }

        const cssMaxWidht = getComputedStyle(document.body)
            .getPropertyValue("--thumb-max-width").trim().replace("px", "");

        const cssMaxHeigth = getComputedStyle(document.body)
            .getPropertyValue("--thumb-max-heigth").trim().replace("px", "");

        const minWidth = 75;
        const minHeight = 20;

        const scaleRate = Math.max(
            Math.min(
                cssMaxWidht / meta.thumbWidth,
                cssMaxHeigth / meta.thumbHeight,
                1
            ),
            Math.max(
                 minWidth / meta.thumbWidth,
                 Math.max(minHeight / meta.thumbHeight, 1.01),
            )
        );

        imgThumb.alt    = meta.original_filename;
        imgThumb.width  = meta.thumbWidth * scaleRate;
        imgThumb.height = meta.thumbHeight * scaleRate;


        if (meta.thumb_url_static) {
            imgThumb.dataset.srcRoot = meta.thumb_url_static.replace(
                (meta.thumb_url_static).split("/").at(-1),
                ""
            );
        }
        /*
        if (isImage && meta.thumb_url_animated && getConfig("animated-gif-thumbs")) {
            imgThumb.dataset.src = meta.thumb_url_animated;
        }
        if (isVideo && meta.thumb_url_animated && getConfig("animated-video-thumbs")) {
            imgThumb.dataset.src = meta.thumb_url_animated;
        }*/

        imgThumb.classList.remove("has-animation");
        imgThumb.classList.remove("attachment-image");
        imgThumb.classList.remove("attachment-video")
        imgThumb.classList.remove("viewable-media")
        imgThumb.classList.remove("has-alhpa")
        imgThumb.classList.remove("attachment-pdf");
        
        if (meta.thumb_url_static) {
            imgThumb.classList.add("has-animation");
        }
        if (meta.has_alpha) {
            imgThumb.classList.add("has-alhpa");
        }
        if (isImage) {
            imgThumb.classList.add("attachment-image");
        }
        if (isVideo) {
            imgThumb.classList.add("attachment-video")
        }
        if (isPdf) {
            imgThumb.classList.add("attachment-pdf")
        }
        // exclude pdfs
        if (isVideo || isImage)  imgThumb.classList.add("viewable-media");

        //if (!imgThumb.parentNode) {
            attachmentHolder.appendChild(imgThumb);
        //}
    }

    //let artCover = null;
    if (mime.startsWith("audio/")) {
        //const artCover = postBuilder.models.artCover;
        //postBuilder.modelElements.artCover.img
        const cssMaxWidht = parseInt(getComputedStyle(document.body)
            .getPropertyValue("--thumb-max-width").trim().replace("px", ""));

        const cssMaxHeigth = parseInt(getComputedStyle(document.body)
            .getPropertyValue("--thumb-max-heigth").trim().replace("px", ""));

        const scaleRate = Math.min(
                cssMaxWidht / meta.thumbWidth,
                cssMaxHeigth / meta.thumbHeight,
                1
        );

        artCover.img.dataset.src = meta.coverart || "/img/icons/video-play.png";
        artCover.img.width = meta.thumbWidth * scaleRate || 100;
        artCover.img.height = meta.thumbHeight * scaleRate || 100;

        if (scaleRate !== 1 && meta.coverart) {
            artCover.img.dataset.scale = Math.max(
                    meta.thumbWidth / artCover.img.width,
                    meta.thumbHeight / artCover.img.height
                ).toFixed(2);
        } else {
            delete artCover.img.dataset.scale;
        }
    
       // artCover.img.alt = this.escapeHtml(meta.original_filename);
        artCover.ext.textContent = meta.ext;

        attachmentHolder.appendChild(artCover);
    }

    return attachmentHolder;
}

postBuilder.buildAttachmentFooter = function(attachment, buffer) {
    
    const attachmentFooter = postBuilder.models.attachmentFooter;
    //delete attachmentFooter.dataset;

    const datasetKeys = Object.keys(attachmentFooter.dataset);
    for (var i = 0; i < datasetKeys.length; i++) {
        delete attachmentFooter.dataset[datasetKeys[i]];
    }

    const meta = attachment.meta;


    const hasDimensions = meta.width && meta.height;

    const prevSpacer = attachmentFooter.dimensions.previousElementSibling;
    if (hasDimensions) {
        attachmentFooter.dimensions.textContent                             = meta.width + "x" + meta.height
        prevSpacer.style.display                                            = 'inline'
    }  else {
        attachmentFooter.dimensions.textContent                             = '';
        prevSpacer.style.display                                            = 'none'
    }

    attachmentFooter.size.textContent                                       = humanFileSize(attachment.size);
    attachmentFooter.type.textContent                                       = meta.mime;

    if (meta.duration) {
        attachmentFooter.duration.previousElementSibling.style.display      = 'inline'
        attachmentFooter.duration.textContent                               = meta.duration + "s";
    } else {
        attachmentFooter.duration.previousElementSibling.style.display      = 'none'
        attachmentFooter.duration.textContent                               = '';
    }

    const fullFilename = attachment.meta.original_filename;
    const filenameLen = fullFilename.length;
    const filenameUnfull = (filenameLen > 68)
        ? (`${fullFilename.substr(0, 49)}..${fullFilename.substr(filenameLen-12, filenameLen-16)}`)
        : fullFilename;


    attachmentFooter.name.textContent                                       = filenameUnfull;
    attachmentFooter.name.download                                          = fullFilename;
    attachmentFooter.name.href                                              = attachment.url;

    const mediaInfo = attachment.meta.mediaInfo;

    if (mediaInfo) {
        const keys = Object.keys(mediaInfo);

        for (var y = 0; y < keys.length; y++) {
            setDatasetIfHas(keys[y], mediaInfo[keys[y]], attachmentFooter);
        }   

        setDatasetIfHas("rotation",     mediaInfo.rotation,     attachmentFooter);
    }
    if (attachment.meta.duration) {
        setDatasetIfHas("duration",     attachment.meta.duration,    attachmentFooter);
    }
    if (attachment.meta.width && attachment.meta.height) {
        const w = attachment.meta.width;
        const h = attachment.meta.height;
        setDatasetIfHas("resolution", `${w}x${h}`, attachmentFooter);
    }

    setDatasetIfHas("size",         attachment.size,        attachmentFooter);


    return attachmentFooter;
}

/*
const chatPageMessages = document.getElementById("chat-page-posts");
const fixedPostWidth = chatPageMessages.clientWidth;

setTimeout(() => {
    postBuilder.models.article.style.waidth = chatPageMessages.clientWidth + "px";
}, 50);
*/

//postBuilder.models.article.style.width = "1500px";


postBuilder.backLink = document.createElement("a");
postBuilder.backLink.classList.add("chat-reply-backlink");

postBuilder.buildBacklink = function(postNum, buffer = null) {
    if (!buffer) buffer = _chatGlobal.activeBuffer;

    const backLink = postBuilder.backLink.cloneNode(false);
    backLink.dataset.repliesTo = postNum;
    backLink.textContent = ">>" + postNum;

    if (buffer && myPosts.hasPost(postNum, buffer.id)) {
        backLink.classList.add("to-me");
    }
    
    const postUserId = buffer?.getPostByNum(postNum)?.user_id || buffer.getHolderElement().querySelector("#p" + postNum)?.dataset?.userId;
    if (postUserId) {
        backLink.dataset.userId = postUserId;
    }

    return backLink;
}

postBuilder.buildPostNode = function(postData, buffer, _article = null) {


    const article = _article || postBuilder.models.article;

    article.id = "p" + postData.post_num;
    article.dataset.postNum = parseInt(postData.post_num);
    article.dataset.userId = postData.user_id;

	let postDate = null;
    let dateLocalised = '';

    try {
        postDate = new Date(postData.date);
        dateLocalised = (isMobile()) ? (postDate.toTimeString().split(' ')[0]) : postDate.toLocaleString();
    } catch (e) {
		console.error(e);
	}
    
    article.postNum.textContent                     = postData.post_num;
    article.timeStamp.textContent                   = dateLocalised;

    article.timeStamp.textContent					= dateLocalised;
    article.timeStamp.dataset.unix				    = parseInt(postDate / 1000);

    article.postSender.textContent                  = postData.user
    article.postSender.title                        = postData.user
    article.postSender.dataset.id                   = postData.user_id

    const repliesToMe = postData.replied_posts
         && postData.replied_posts.find(
             (post_num) => myPosts.hasPost(post_num, postData.buffer_id)
    )

    if (repliesToMe) article.classList.add("replies-to-me");
    else article.classList.remove("replies-to-me");


    while (article.replies.lastElementChild) {
        article.replies.removeChild(article.replies.lastElementChild);
    }

    if (postData.replied_by) {
        //postData.replied_by.forEach((postNum) => {
        for (var i = 0; i < postData.replied_by.length; i++) {
            const backLink = postBuilder.buildBacklink(parseInt(postData.replied_by[i]), buffer);
            article.replies.appendChild(backLink);
        //})
        }
    }
    let attachment = null;
    let attachmentFooter = null;

    if (postData.attachment) {
        attachment = postBuilder.buildPostAttachment(postData.attachment, postData);
        article.postContent.appendChild(attachment);

        attachmentFooter = postBuilder.buildAttachmentFooter(postData.attachment);
        article.postBody.insertBefore(attachmentFooter, article.postContent);
    }

    //return article.cloneNode(true);

    
    const postMessage = postBuilder.models.postMessage;
    if (postData.message) {
        postMessage.id                             = "msg-" + postData.id;
        postMessage.innerHTML                      = postBuilder.formatChatMessage(postData.message, buffer, postData);

        article.postContent.appendChild(postMessage);
    }


    const iconHodler = article.posterIconHolder
    while (iconHodler.lastElementChild) {
        iconHodler.removeChild(iconHodler.lastElementChild);
    }
    iconHodler.title = '';


    const hasIcons = postData.options && postData.options.icons && postData.options.icons.length;

    if (postData.geo && !hasIcons)
    {
        if (!postData.options || typeof postData.options !== "object")
        {
            postData.options = {};
        }


        const country = postData.geo.country
        if (!country) return;

        const flagIcon = {
            src     : postData.geo.flagSrc || 'img/flags/' + country.toLowerCase() + ".png",
            name    : postData.geo.country,
            title   : postData.geo.country + ", " + postData.geo.region,
        };
        

        postData.options.icons = new Array();
        postData.options.icons.push(flagIcon);
    }

    
    if (postData.options?.icons?.length) {
        for (var x = 0; x < postData.options.icons.length; x++)
        {
            const icon = postData.options.icons[x];
            let iconImg = postBuilder.iconMap.get(icon.name);

            if (!iconImg) {
                iconImg = postBuilder.models.flagIcon.cloneNode(false);
                iconImg.src     = icon.src;
                if (icon.name) {
                    iconImg.alt     = icon.name;
                }
                iconImg.title   = icon.title;
                iconImg.width   = icon.width  || 20;
                iconImg.height  = icon.height || 20;

                postBuilder.iconMap.set(icon.name, iconImg);
            }
            iconHodler.appendChild(iconImg);
        }

    }

    const hasLazyloadable = article.postContent.querySelector(".sticker-wrapper, .code, img.attachment-thumb");
    const returnedNode =  article.cloneNode(true);

    if (hasLazyloadable) {
        returnedNode.classList.add("lazy");
    }


    // Clean model for next use.
    if (attachment && attachment.parentNode) {
        attachment.parentNode.removeChild(attachment);
    }

    if (attachmentFooter && attachmentFooter.parentNode) {
        attachmentFooter.parentNode.removeChild(attachmentFooter);
    }

    if (postData.replied_by) {
        article.replies.textContent = '';
    }

    if (postMessage && postMessage.parentNode) {
        postMessage.parentNode.removeChild(postMessage);
    }



    return returnedNode;
}


postBuilder.buildPostNode2 = function(postData, buffer) {
    const html = postBuilder.buildHtmlStringForPost(postData, buffer);
    const fragment = document.createRange().createContextualFragment(html);

    return fragment.firstChild;
}
