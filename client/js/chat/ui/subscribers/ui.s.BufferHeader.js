$("#chat-container").on("chatCreated", (e, _chat) => {


	function challenged(contact, secretString, signature) {
		const buf = _chat.buffers.find((it) => it.contact === contact);

		if (buf) {
			buf.addInfoMessage("Got valid challenge response");
		}
	}

	$("#active-buffer-info-container").on("click", "#save-contact", async() => {

		const buf = _chat.getActiveBufferItem();
		const id = buf.friend_id;
		const defaultAlias = buf.alias || '';
		const alias = prompt("Alias for contact", defaultAlias);

		if (!alias || !alias.length) return;
		
		const contact = await Contact.addNew(buf.chat, id, alias, []);
		
		await contact.loadKeys();
		await contact.save(buf.chat);

		buf.contact = contact;

		buf.renderBufferMenuElement();
		buf.renderBufferHeader();

		buf.chat.crypto.sendChallengeRequest(contact, challenged);
	});

	$("#active-buffer-info-container").on("click", "#remove-contact", async() => {
		const buf = _chat.getActiveBufferItem();
		const contact = buf.contact;

		await contact.remove(buf.chat);
		$("#remove-contact").remove();
	});


	$("#active-buffer-info-container").on("click", "#challenge", async() => {

		const buf = _chat.getActiveBufferItem();
		const contact = buf.contact;
		
		buf.chat.crypto.sendChallengeRequest(contact, challenged);
	});

	$("#active-buffer-info-container").on("click", "#display-keys-button", () => {

		const buf = _chat.getActiveBufferItem();
		const contact = buf.contact;

		const getHtml = (name, fingerprint, pem) => {
			return `
			<div style='display: flex; flex-direction: column;'>
				<header style='
					font-weight: bold;
					font-size: smaller;
					display: flex;
					justify-content: space-evenly;
					align-items: center;
					'>
					<span style='inline-block'>${name}</span>
					<span style='max-width: 80%; text-align: center;'>${fingerprint}</span>
				</header>
				<span style='text-align: center; margin-top: 5px'>${pem}</span>
			</div>`;
		};

		const signer = getHtml('Signer', buf.contact.keys.signer.fingerprint, buf.contact.keys.signer.pem) + "<hr>";
		const ecdh = getHtml('ECDH', buf.contact.keys.ecdh.fingerprint, buf.contact.keys.ecdh.pem) + "<hr>";
		const rsa = getHtml('RSA', buf.contact.keys.rsa.fingerprint, buf.contact.keys.rsa.pem);


		buf.addInfoMessage(signer + ecdh + rsa);
		//buf.addInfoMessage(ecdh);
		//buf.addInfoMessage(rsa);

	});

});