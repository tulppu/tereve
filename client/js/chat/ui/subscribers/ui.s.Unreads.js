$("#chat-container").on("chatCreated", (e, _chat) => {


	function updateMenuElementUnreads(buffer) {
		return buffer.getBufferListItemEl().find(".buffer-unreads").html(buffer.unreadCount || '');
	}

	_chat.eventEmitter.addListener("postAdded", (buf, postData) => {

		if (buf.isActive() && document.hasFocus()) {
			return;
		}

		const repliesToMe = postData.replied_posts?.find(
			(post_num) => myPosts.hasPost(post_num, postData.buffer_id)
		);

		if (repliesToMe) {
			buf.highlighted = true;
		}


		buf.unreadCount++;
		updateMenuElementUnreads(buf);
	});

	_chat.eventEmitter.addListener("bufferSwitched", (newActiveBuffer) => {
		if (!newActiveBuffer.unreadCount) return;
		newActiveBuffer.unreadCount = 0;
		updateMenuElementUnreads(newActiveBuffer);
	});


	window.addEventListener("click", () => {
		const buf = _chat.getActiveBufferItem();
		if (buf) {
			buf.unreadCount = 0;
			buf.highlighted = false;
			updateMenuElementUnreads(buf);
		}
	});
	
});

