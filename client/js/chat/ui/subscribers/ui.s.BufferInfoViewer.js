
"use strict;"


const channelInfoViewer = {};

channelInfoViewer.requestGoing = false;
channelInfoViewer._chat = null;

channelInfoViewer.usrCountEl = null;

channelInfoViewer.infoContainer = null;
channelInfoViewer.wrapperElementId = "channelInfoViewer";

channelInfoViewer.isDisplayed = (() => channelInfoViewer.infoContainer !== null);
channelInfoViewer.isHovered = () => channelInfoViewer.isDisplayed() && channelInfoViewer.infoContainer.matches(":hover")

channelInfoViewer.initAbotert = (() => {
    channelInfoViewer.aborter = new AbortController()
    channelInfoViewer.abortSignal = channelInfoViewer.aborter.signal
});



channelInfoViewer.requestInfo = function() {

    if (channelInfoViewer.isDisplayed() || channelInfoViewer.requestGoing) return;

    channelInfoViewer.initAbotert();

    const buf = channelInfoViewer._chat.activeBuffer;
    channelInfoViewer.requestGoing = true;


    channelInfoViewer.usrCountEl.style.cursor = "wait";

    return buf.chat.fetchAction("buffer.channel.info",{
        signal: channelInfoViewer.abortSignal,
        qsData: {
            buffer_name: buf.name
        },
    }).then(async(res) => {
        
        if (channelInfoViewer.usrCountEl) {
            channelInfoViewer.usrCountEl.style.cursor = "auto";
        }
       // if (channelInfoViewer.isDisplayed()) return;

        if (!channelInfoViewer.usrCountEl?.matches(":hover")) {
            return;
        }

        const bufferInfo = await res.json();
        channelInfoViewer.displayInfo(bufferInfo);

    }).finally(() => channelInfoViewer.requestGoing = false)
}


channelInfoViewer.displayInfo = function(channelInfo) {

    channelInfoViewer.infoContainer = document.createElement("div");
    channelInfoViewer.infoContainer.id = channelInfoViewer.wrapperElementId;
    channelInfoViewer.infoContainer.style.position = "absolute";
    channelInfoViewer.infoContainer.style.top = channelInfoViewer.usrCountEl.offsetHeight + "px";
    channelInfoViewer.infoContainer.style.right = 0;
    channelInfoViewer.infoContainer.style.background = "rgb(0,0,0)";
    channelInfoViewer.infoContainer.style.padding = "2px";
    channelInfoViewer.infoContainer.style.maxHeight = "100vh";
    channelInfoViewer.infoContainer.style.overflowY = "auto";
    channelInfoViewer.infoContainer.style.zIndex = 300;

    channelInfo.users.forEach((usrInfo) => {
       const usrWrapper = document.createElement("div");
       usrWrapper.style.flex = 1;
       usrWrapper.style.whiteSpace = "nowrap";

       if (usrInfo.iconSrc) {
            const icon = document.createElement("img");
            icon.src = usrInfo.iconSrc;
            icon.classList.add("usr-info-icon")

            usrWrapper.appendChild(icon);
       }
       if (usrInfo.name) {
            const usrName = document.createElement("span");
            usrName.innerText = usrInfo.name;
            usrName.classList.add("usr-info-name");
            usrName.style.verticalAlign = "super";
            usrName.style.maxWidth = "60vw";
            usrName.style.overflow = "hidden";
            usrName.style.textOverflow = "ellipsis";

            usrName.title = usrInfo.id;

            usrWrapper.append(usrName);
       }
       if (usrInfo.pmAble && usrInfo.id) {
            //usrWrapper.style.cursor = "pointer";
            //usrWrapper.dataset.id = usrInfo.id;
       }

       if (usrInfo.counry) {
            usrWrapper.title = usrInfo.counry;
       }

       channelInfoViewer.infoContainer.appendChild(usrWrapper);
    });

    channelInfoViewer.usrCountEl.style.position = "relative";
    channelInfoViewer.usrCountEl.appendChild(channelInfoViewer.infoContainer);
}

channelInfoViewer.close = function()
{

    if (channelInfoViewer.usrCountEl) {
        channelInfoViewer.usrCountEl.style.cursor = "auto";
    }

    const els = document.querySelectorAll("#" + channelInfoViewer.wrapperElementId);
    els.forEach((it) => it.parentNode.removeChild(it));

    if (channelInfoViewer.requestGoing && !channelInfoViewer.isDisplayed()) {
        channelInfoViewer.aborter.abort();
        channelInfoViewer.requestGoing = false;
    }

    channelInfoViewer.infoContainer?.parentNode?.removeChild(channelInfoViewer.infoContainer);

    channelInfoViewer.usrCountEl = null;
    channelInfoViewer.infoContainer = null;

}

//document.getElementById("chat-container").addEventListener("chatCreated", (e, _chat) => {
$("#chat-container").on("chatCreated", (e, _chat) => {

    channelInfoViewer._chat = _chat;

    document.getElementById("chat-top-header").addEventListener("mouseenter", (e) => {

        channelInfoViewer.usrCountEl = e.target.querySelector("#buffer-users-count");
        if (!channelInfoViewer.usrCountEl || channelInfoViewer.requestGoing || channelInfoViewer.isDisplayed()) return;


        channelInfoViewer.usrCountEl.removeEventListener("mouseenter", channelInfoViewer.requestInfo, {});
        channelInfoViewer.usrCountEl.addEventListener("mouseenter", channelInfoViewer.requestInfo, {
            once: true
        });

        channelInfoViewer.usrCountEl.addEventListener("mouseleave", function closeHandler(e) {

            if (channelInfoViewer.isHovered()) return;

            if (configs.getConfig("dev-mode")) return;
            e.target.removeEventListener("mouseleave", closeHandler);
            channelInfoViewer.close();

        });


    });
});
