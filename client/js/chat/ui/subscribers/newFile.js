'use strict';
input.handleFileSelect = function (file) {

	if (!treatedAsMobile()) input.form.txtarea.focus();

	input.attachment.removeThumb();

	const activeBuffer = _chatGlobal.getActiveBufferItem();

	if (activeBuffer?.configs.maxFileSize && file.size > activeBuffer?.configs.maxFileSize * 1024 * 1024) {
		alert(`Maxium filesize for this channel is ${activeBuffer.configs.maxFileSize} MB`);
		input.attachment.reset();
		return;
	}

	input.form.thumbAlt.innerHTML = '';

	const thumbAltFilename = document.createElement("span");
	//thumbAltFilename.classList.add("upload-filename");
	thumbAltFilename.id = "upload-filename";
	thumbAltFilename.innerText = file.name.substring(0, 64);
	thumbAltFilename.title = file.name;
	thumbAltFilename.style.cursor = "pointer";

	input.form.thumbAlt.append(thumbAltFilename);
	input.form.thumbAlt.filename = thumbAltFilename;

	input.form.thumbAlt.append(
		document.createRange().createContextualFragment(
			`(<span id='upload-type'>${file.type}</span>, <span id='upload-size'>${humanFileSize(file.size)}</span>)`
		)
	);
	input.form.clear.show();
	input.form.thumbAlt.show();

	thumbAltFilename.addEventListener("click", async () => {
		const _file = input.form.file.files[0];
		const newName = prompt("", _file.name.substring(_file.name.lastIndexOf(".")));
		const blob = await readFileToBlob(_file);
		//const blob = new Blob([new Uint8Array(await readFileToBlob(_file))], {type: _file.type });
		const newFile = new File([blob], newName, {
			type: _file.type
		});
		const container = new DataTransfer();
		container.items.add(newFile);

		input.form.file.files = container.files;
		input.form.file.dispatchEvent(new Event("change"));
		//setTimeout(() => {
		//	input.form.file.dispatchEvent(new Event("change"));
		//});
	});

	if (!file.type.startsWith("image") && !file.type.startsWith("video")) {
		input.form.thumbAlt.show();
		return;
	}

	if (file.size >= (1024 * 1024 * 40)) {
		input.form.thumbAlt.show();
		return;
	}

	input.attachment.thumbFile(file).then((thumb) => {
		if (thumb) {
			thumb.setAttribute("alt", file.name);
			input.form.thumb.appendChild(thumb);
			input.form.thumb.classList.remove("hidden");
		} else {
			input.form.thumbAlt.show();
		}
	});
};
