



$("#chat-container").on("chatCreated", (e, _chat) => {

	const promtTemplate = document.createElement('template');
	const promtHtml =
	`<div id="promt-overlay" style="position: absolute; width: 100%; height: 100%; z-index: 100; display: none; justify-content: center; align-items: center; color: black">
		<div id='promt'  style="padding: 20px; background: #0000004f; color: black">
			<textarea id="promt-text" style="min-width: 200px; min-height: 150px; color: black"></textarea>
			<div id='promt-bottom'>
				<label for="promt-encrypt">Salaa</label>
				<input id="promt-encrypt" name="promt-encrypt" type="checkbox" value="Salaa" checked>
				<input id="promt-submit" type="submit" value="Lähetä">
			</div>
		</div>
	</div>`.trim();


	promtTemplate.innerHTML = promtHtml;
	const promtElement = promtTemplate.content.childNodes[0];
	document.querySelector("#chat-page-posts").appendChild(promtElement);

	const elPromtSubmit = promtElement.querySelector("#promt-submit");
	const elPromtText =  promtElement.querySelector("#promt-text");

	elPromtText.addEventListener("keydown", (e) => {
		if (e.key === "Enter" && !e.shiftKey) {
			elPromtSubmit.click();
		}
	});

	function isEncryptioEnabled() {
		return document.querySelector("#promt-encrypt").checked;
	}

	function whisperPromt() {
		return new Promise((resolve, rejejct) => {
			promtElement.style.display = "flex";
			elPromtText.focus();

			elPromtSubmit.addEventListener("click", () => {
				const textInputValue = elPromtText.value;
				promtElement.style.display = "none";
				return resolve(textInputValue);
			}, {once: true});
		
			promtElement.addEventListener("click", (e) => {
				if (e.target.id === "promt-overlay") {
					promtElement.style.display = "none";
					return resolve(null);
				}
			});
		})

	}

	$("#chat-page-posts").on("click", ".chat-post-whisper", async (e) => {

		//const receiver = $(e.target).closest(".chat-post-wrapper").find(".chat-post-sender").data("userid");
		const receiver = e.target.closest(".chat-post-wrapper").querySelector(".chat-post-sender");
		const postNum = e.target.closest(".chat-post-wrapper").dataset.postNum;

		const message = await whisperPromt();
		if (!message || !message.length) {
			return;
		}

		if (!isEncryptioEnabled()) {
			(_chat.getActiveBufferItem()).whisper(message, postNum, false);
			return;
		}

		if (message.length > 190) {
			alert("Viesti liian pitkä salattavaksi.");
			return;
		}

		if (!receiver && !receiver.dataset.id) {
			alert("Vastaanottajaa ei löytynyt.")
			return;
		}

		const keys = await getUserKeys(receiver.dataset.id);
		const rsaKey = await loadRSAFromPEM(keys.rsa);

		const encoder = new TextEncoder();
		const chiper =  await encryptUsingRSA(rsaKey, encoder.encode(message));

		(_chat.getActiveBufferItem()).whisper(buf2hex(chiper), postNum, true).then((err) => {
			if (err) return;

			elPromtText.value = "";


		})
	});

	_chat.eventEmitter.addListener("s.user.whisper", async(event, chat) => {
		if (configs.getConfig("ignore-whispers")) {
			return;
		}

		if (event.is_encrypted) {
			const enc = new TextDecoder();
			event.message = await _chat.crypto.profile.decrypt(hex2buf(event.message));
			event.message = enc.decode(event.message);
		}

		const sourceBuffer = chat.buffers.find((it) => it.id === event.source_buffer || it.name === event.source_buffer);

		if (!event.message) return;

		const message = event.message.replace(/([\n|\s]{2,})/gm, "\n").trim();

		chatFeeds.addItem({
			source: sourceBuffer.getName().substr(0, 16),
			message: message,
			onclick: null,
			icon: {
				src: "img/icons/whisper.png",
				width: 15,
				height: 15,
			}
		})

		return;

	});

	/*
	$("#chat-feed").on("click", ".chat-whisper-remove", (e) => {
		$(e.target).parent().remove();
	});*/

});
