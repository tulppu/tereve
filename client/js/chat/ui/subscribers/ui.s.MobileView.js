$("#chat-container").on("chatCreated", (e, _chat) => {

	const mobileBufSwitcher = document.getElementById("mobile-view-buffer-switcher");

	mobileBufSwitcher.addEventListener("mousedown", (e) => {
		if (e.target.nodeName === "OPTION")
		{
			return;
		}
		while (mobileBufSwitcher.firstElementChild) {
			mobileBufSwitcher.removeChild(mobileBufSwitcher.firstElementChild);
		}

		const listedBufferitems = document.querySelectorAll(".buffer-item");
		for (var i = 0; i < listedBufferitems.length; i++)
		{
			const bufItem = listedBufferitems[i];		

			const key = bufItem.dataset.buffer;
			const name = bufItem.querySelector(".buffer-name-holder").innerText.trim();
			const unread = bufItem.querySelector(".buffer-unreads").innerText.trim();
			const isActive = bufItem.classList.contains("active-buffer");	

			const option = document.createElement("option");
			option.value = key;
			option.innerText = name; 
			if (isActive) option.setAttribute("selected", "selected");

			mobileBufSwitcher.appendChild(option);
			continue;
		}
	});

	mobileBufSwitcher.addEventListener("change", (e) => {
		setTimeout(() => {
			const key = e.target.value; 
			const newBuf = _chat.buffers.find((it) => it.key == key);

			if (newBuf) {
				newBuf.draw();
			}
		});
	});

	const mobileFormToggle = document.getElementById("mobile-form-toggle");
	const formShowedClass = "show-input-form-mobile";

	document.body.addEventListener("click", (e) => {

		if (!input.form.self.classList.contains(formShowedClass)) {
			return;
		}

		if (
			e.target.closest(".buffer-holder")
			&& !e.target.closest("img.attachment-thumb")
			/*
				mediaViewer.wrapperElement.matches(":hover")
			||	e.target.contains(input.form.self)
			||	e.target.contains(mobileFormToggle)
			|| 	input.form.isHovered()
			*/
		) {
			input.form.self.classList.remove(formShowedClass);

		}
		//input.form.self.classList.remove(formShowedClass);

	});



	mobileFormToggle.addEventListener("click", (e) => {
		input.form.self.classList.toggle(formShowedClass);
	
		if (input.form.self.classList.contains(formShowedClass)) {
			setTimeout(() => { input.form.txtarea.focus() });
		}		
	});

	document.addEventListener("click", (e) => {
		if (e.target.classList.contains("chat-post-num")) {
			input.form.self.classList.add(formShowedClass);			
		}
	});


	$("#chat-settings").find("table").clone(true, true).appendTo("#mobile-view-settings");
	configs.applyConfigs(document.querySelector("#mobile-view-settings"));


	input.form.txtarea.addEventListener("focus", () => {
		input.form.self.classList.add(formShowedClass);
	});

	document.getElementById("mobile-view-settings-toggle").addEventListener("click", (() => {
		setTimeout(() => $("#mobile-view-settings").removeClass("hidden"));
	}));
	document.getElementById("mobile-view-stickers-toggle").addEventListener("click", (() => {
		setTimeout(() => stickers.view());
	}));


	$("body").on("click", () => {
		if (!($("table:hover").length)) {
			$("#mobile-view-settings").addClass("hidden");
		}
	})




});
