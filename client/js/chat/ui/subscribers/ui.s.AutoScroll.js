const scroller = {};

scroller.messageHolder = document.getElementById("chat-page-posts");
scroller._scrolledToEnd = true;


scroller.getIsScrolledToEnd = function () {
	return scroller._scrolledToEnd;
}

scroller.setIsScrolledToEnd = ((well_is) => scroller._scrolledToEnd = well_is);



scroller.scrollToEnd = function (params = {}) {

	const buffer = params.buffer || _chatGlobal.getActiveBufferItem();
	
	if (!buffer) throw new Error("Buffer not set");
	if (!buffer.getHolder()) return;

	const holder = buffer.getHolder().parentElement;

	const smoothScrollDisabled = getComputedStyle(document.documentElement)
		.getPropertyValue("--smooth-scroll-disabled").trim() === "1"

	const smooth = params.smooth && !smoothScrollDisabled;

	if (!smooth) {
		holder.classList.remove("smoothed-scroll")
	}

	if (configs.getConfig("reverse-ui-order")) {
		holder.scrollTo({
			top: 0,
			behavior: (smooth) ? 'smooth' : 'auto'
		});
	} else {
		holder.scrollTo({
			top: holder.scrollHeight,
			behavior: (smooth) ? 'smooth' : 'auto'
		});
		scroller.setIsScrolledToEnd(true);
	}
	scroller.setIsScrolledToEnd(true);
}

// helds post list scrolled at bottom
scroller.autoScrollToEnd = function (params = {}) {
	if (!scroller.getIsScrolledToEnd()) return;
	scroller.scrollToEnd(params);
}

scroller.messages = document.querySelector("#chat-page-posts");

scroller.calculateIsAtEnd = function()
{
	const messages = scroller.messages;

	if (configs.getConfig("reverse-ui-order")) {
		return messages.scrollTop  === 0;
	} else {
		return messages.scrollHeight  <= messages.scrollTop + messages.offsetHeight + 30;
	}
}


$("#chat-container").on("chatCreated", (e, _chat) => {


	scroller.messages.addEventListener("scroll", (e) => {
		scroller.setIsScrolledToEnd(scroller.calculateIsAtEnd())
	});

	_chat.eventEmitter.addListener("postDrawn", (buf, addedPosts, data) => {

		if (!buf.isActive()) {
			return;
		}

		if (mediaViewer.isDisplayed()) {
			return;
		}

		let hoveredScrollBlockingEl = document.querySelector("img.attachment-thumb:hover,a.chat-reply-backlink:hover, a.chat-reply-backlink:hover");
		if (mediaTooltip.isDisplayed()) {
			hoveredScrollBlockingEl = mediaTooltip.getElement();
		}

		const posts = addedPosts[0];

		if (hoveredScrollBlockingEl) {

			if (configs.getConfig("reverse-ui-order")) {
				if (scroller.getIsScrolledToEnd()) {
					const posts = addedPosts[0];
					posts.classList.add("hidden");
				}
			}

			hoveredScrollBlockingEl.addEventListener("mouseleave", () => {
				posts.classList.remove("hidden");

				if (postsViewer.isHovered()) {
					return;
				}

				scroller.autoScrollToEnd({
					buffer: buf,
					smooth: true
				});

			}, {once: true})
			return;
		}

		if (postsViewer.isDisplayed()) {
			return;
		}

		scroller.autoScrollToEnd({
			buffer: buf,
			smooth: document.hasFocus() === true,
		});

	});
});