const messageExpander = {};

messageExpander.toggleExpandButton = function(postContent, postArticle) {


	if (!postContent) return;
	const msg = postContent.querySelector(".chat-post-message");

	const cssMaxHeigth = getComputedStyle(postContent)
		.getPropertyValue("max-height").trim().replace("px", "");

	const isOverFlowing = 
				postContent.scrollHeight > cssMaxHeigth 
				//||	postContent.scrollHeight > postContent.clientHeight
				|| 	msg?.scrollHeight > cssMaxHeigth


	if (isOverFlowing) {
		const article = postContent.closest("article");
		/*console.log(article.dataset.postNum);
		if (article.dataset.postNum == 7670689) {
			console.log(cssMaxHeigth, postContent.scrollHeight, msg?.scrollHeight);
			console.log(postContent.clientHeight);
			console.log(article);
		}*/
		article.classList.add("overflown");

		const toggle = article.querySelector(".message-toggle-expand");
		toggle.classList.remove("hidden");
	}
}

messageExpander.displayButtons = function(postNodes) {
	postNodes.forEach((node) => {
		const postContent = node.querySelector("div.chat-post-content");
		messageExpander.toggleExpandButton(postContent, node);
	});
}


$("#chat-container").on("chatCreated", (e, _chat) => {

	/*
	_chat.eventEmitter.addListener("bufferDrawn", (buf, postNodes) => {
		window.requestAnimationFrame(() => {
			setTimeout(() => messageExpander.displayButtons(postNodes), 100);
		});
	});

	_chat.eventEmitter.addListener("postDrawn", (buf, postNodes, postData) => {
		messageExpander.toggleExpandButton(postNodes[0].querySelector("div.chat-post-content"));
	});
	*/

	document.getElementById("chat-page-posts").addEventListener("click", (e) => {

		if (!e.target.classList.contains("message-toggle-expand")) return;

		const postWrapper = e.target.closest("article.chat-post-wrapper");
		const postContent = postWrapper.querySelector("div.chat-post-content");

		if (postContent.classList.contains("message-expanded")) {
			postContent.classList.remove("message-expanded")
			e.target.innerText = "+"
		} else {
			postContent.classList.add("message-expanded")
			e.target.innerText = "-"
		}
		e.preventDefault();
		postContent.scrollTo(0, 0);


		if (!configs.getConfig("reverse-ui-order")) {
			return false;
		}

		window.requestAnimationFrame(() => {
			const wrapperReacts = postWrapper.getBoundingClientRect();
			const header = document.getElementById("chat-top-header");

			if (wrapperReacts.y < input.form.self.clientHeight + header.clientHeight) {
				postWrapper.scrollIntoView();
			}
		});

		return false;
	})

});
