$("#chat-container").on("chatCreated", (e, _chat) => {


	function launch(e, attachmentHolder, displayFunc) {

		//const $target = $mediaElement.closest(".chat-post-attachment");
		e.preventDefault();

		const postBody = attachmentHolder.closest(".chat-post-body");

		const imgThumb 				= postBody.querySelector("img.attachment-thumb");
		const attachmentLink 		= postBody.querySelector("a.chat-post-attachment");
		//const postArticle 			= postBody.querySelector("article.chat-post-wrapper");
		const attachmentFooter 		= postBody.querySelector("footer.chat-attachment-footer-container");
		const postSender 			= attachmentFooter.querySelector("a.chat-post-sender");
		const attachmentName 		= attachmentFooter.querySelector(".attachment-link");

		const params = {
			event: 			e,
			size: 			attachmentLink.dataset.size,
			encrypted: 		attachmentLink.dataset.encrypted,
			mime: 			attachmentLink.dataset.mime,
			url: 			attachmentLink.href,
			iv: 			attachmentLink.dataset.iv,

			poster: 		(postSender) ? postSender.dataset.id : null,

			signature: 		imgThumb.parentNode.dataset.signature,
			hasAlhpa: 		imgThumb.dataset.alpha,
			hasAnimation: 	imgThumb.dataset.animated,
			rotation: 		imgThumb.dataset.rotation,
			width: 			imgThumb.dataset.width,
			height: 		imgThumb.dataset.height,

			name: 			(attachmentName) ? attachmentName.title : null,
			title: 			attachmentLink.dataset.title || attachmentLink.title,

			buffer:			_chatGlobal.activeBuffer,

			displayFunc:	displayFunc,
		};

		attachmentHolder.style.cursor = "progress";

		return mediaViewer.displayMedia(params).then(() => {
			attachmentHolder.style.cursor = "pointer";
		}).catch((err) => {
			attachmentHolder.style.cursor = "not-alloed";
			//console.info("Failed to load media");
			console.error(err);
			if (!err) return;
			attachmentHolder.setAttribute("title", err.message);
		});

	}

	const chatBodyContainer = document.getElementById("chat-body-container");

	chatBodyContainer.addEventListener("mouseover", (e) => {

		if (!e.target.classList.contains("viewable-media")) {
			return;
		}
		const viewerType = configs.getConfig("media-viewer-type")


		if (e.altKey) {
			e.preventDefault();
			return;
		}

		if (viewerType === "centered" || postsViewer.isHovered()) {
			e.target.style.cursor = "zoom-in";
			return;
		}

		if (viewerType !== "next-to") return;
		if (postsViewer.isDisplayed()) return;

		e.target.style.cursor = "wait";

		const attachmentHolder = e.target.closest("a.chat-post-attachment");
		launch(e, attachmentHolder, mediaViewer.displayMediaNext).then(() => {
			e.target.style.cursor = "pointer";

		})


		return false;
	});
	chatBodyContainer.addEventListener("click", function clicked(e) {

		if (e.ctrlKey) return;

		const attachmentHolder = e.target.closest("a.chat-post-attachment");
		if (!attachmentHolder) {
			return;
		}

		const isViewable = attachmentHolder.querySelector(".viewable-media");
		if (!isViewable) return;

		const viewerType = configs.getConfig("media-viewer-type");

		let displayFunc = null;

		if (viewerType === "next-to") {
				
			if (e.altKey) {
				displayFunc = mediaViewer.displayMediaCentered
				e.preventDefault();
			}

			if (postsViewer.isHovered()) {
				displayFunc = mediaViewer.displayMediaNext;
			}
		} else {
			displayFunc = mediaViewer.displayMediaCentered;
		}

		if (!displayFunc) {
			return;
		}
		e.preventDefault();

		launch(e, attachmentHolder, displayFunc);

		return false;
	});



	function wheelVolumeSetter(e) {
		e.preventDefault();
		//if (!mediaObject) return;

		const lower = (e.deltaY || e.originalEvent.deltaY) > 0;
		const newVolume = (lower) 
			? (Math.max(mediaViewer.media.volume - 0.025, 0))
			: (Math.min(mediaViewer.media.volume + 0.025, 100));
	}

	// media-viewer // chat-post-viewers
	$("#chat-post-viewers").on("wheel","img.attachment-video", wheelVolumeSetter);
	$("#chat-page-posts").on("wheel","img.attachment-video", wheelVolumeSetter);
	$("#media-viewer").on("wheel","video", wheelVolumeSetter);

});