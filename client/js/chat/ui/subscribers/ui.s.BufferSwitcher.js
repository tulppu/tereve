

$("#chat-container").on("chatCreated", (e, _chat) => {
	const chat = _chat;


	function showRoomMaker() {

		const roomCreator = buildDocumentFromHtmlString(`
			<div id='new-room-holder' class='modal-wrapper'>
				<form id='room-create-form'
					style='	min-width: 15vw;
							min-height: 10vh;
							padding: 20px;
							display: flex;
							flex-direction: column;
							background: rgba(50, 0, 0, 0.10);
					'>
					<input name='name' type='text' placeholder='Nimi' required>
					<input name='topic' type='text' placeholder='Aihe'>
					<input name='password' type='password' placeholder='Salasana'>
					<br>
					<div style='margin-top: 10px; text-align: center'>
						<input id='join' type='submit' value='Liity'>
						<input id='create' type='submit' value='Luo'>
					</div>
				</form>
			</div>
		`);
		document.body.append(roomCreator);
		roomCreator.querySelector("input[name='name']").focus();

		return new Promise((resolve, reject) => {
			roomCreator.querySelector("form#room-create-form").addEventListener("submit", (e) => {
				const formData = new FormData(e.target);
				const res = {
					name: formData.get("name"),
					topic: formData.get("topic"),
					password: formData.get("password"),

				};
				roomCreator.parentNode.removeChild(roomCreator);
				return resolve(res);
			});

			roomCreator.addEventListener("click", (e) => {
				if (e.target.id === "new-room-holder") {
					roomCreator.parentNode.removeChild(roomCreator);
					return reject();
				}
			});
		});
	}

	$("#new-buffer").on("click", (e) => {

		showRoomMaker().then((res) => {

			let name = res.name;
			if (!name || !name.length) return;
			if (name[0] !== '#') name = "#" + name;

			const existingBuffer = _chat.buffers.find((it) => it.name === name);
			if (existingBuffer) {
				existingBuffer.draw();
				return;
			}

			const buffer = new ChatChannelBuffer(_chat, name);
			_chat.addBuffer(buffer);
	
			buffer.registerStatusChangeCallback("LOADED", () => {
				buffer.draw();
			}, true);
			buffer.join();

		}).finally((e) => {
			console.log(e);
		})
	});

	$("#chat-users-container").on("click", ".chat-user-link", (e) => {

		e.preventDefault();

		var $target = $(e.currentTarget);

		var bufferId = $($target).data("user-id");
		var bufferName = $target.text();

		openPrivateBuffer(chat, bufferId, bufferName);
	});

	$("#chat-body-container").on("click", ".chat-post-sender", (e) => {

		let $target = $(e.currentTarget);
		const userId = $target.data("userid");
		const userName = $target.text().trim();

		if (!userId || !userId.length) {
			return;
		}

		openPrivateBuffer(chat, userId, userName);
	});

	$("#user-profile-id").on("click", (e) => {
		e.preventDefault();
		openPrivateBuffer(chat, chat.me.id);
	});



});


function openPrivateBuffer(chat, user_id) {

	let buffer = chat.buffers.find((it) => it.friend_id === user_id);

	if (buffer) {
		buffer.draw();
		return;
	}

	buffer = new ChatPrivateBuffer(chat, user_id);
	chat.addBuffer(buffer);


	buffer.registerStatusChangeCallback("LOADED", (buf) => {
		buf.draw();
	}, true);

	buffer.join();
}
