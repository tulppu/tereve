
document.body.addEventListener("click", (e) => {
    if (!e.target.classList.contains("poster-icon")) return;
    
    const chatPostMessage = e.target.closest("article").querySelector(".chat-post-message");
    
    if (!chatPostMessage) return;
    //if (chatPostMessage.dataset.translated || chatPostMessage.dataset.translating) return;

    chatPostMessage.dataset.translating = true;

    const msg = chatPostMessage.innerText;
    if (!msg || !msg.trim().length) return;

    const lang = navigator.language || navigator.userLanguage; 

    translator.translate({
            target: lang,
            string: msg,
    }).then((res) => {
		chatPostMessage.dataset.translated = true;

        const el = document.createElement("span");
        el.classList.add("translation");

        const buffer = chatPostMessage.closest(".buffer-holder").buffer;

        if (!buffer) {
            console.log("Buf not found");
         }

        const msgFormatted = postBuilder.formatChatMessage(res.translation, buffer);
        el.innerHTML = "<br>" + msgFormatted;

        chatPostMessage.appendChild(el);

        messageExpander.toggleExpandButton(chatPostMessage.parentNode);
            
    }).catch((e) => {
        console.log("Failed to translate the message");
        console.log(e);
        chatFeeds.errorLog(e.toString() || e);
    }).finally(() => {
            chatPostMessage.dataset.translating = false;
 })

});
