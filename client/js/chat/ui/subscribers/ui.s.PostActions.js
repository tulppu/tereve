$("#chat-container").on("chatCreated", (e, _chat) => {

	const bodyContainer = document.getElementById("chat-body-container");
	bodyContainer.addEventListener("click", (e) => {
		
		if (!e.target.classList.contains("chat-post-toggle")) return;

		const postBody = e.target.closest("article").querySelector(".chat-post-body");

		const isHidden = postBody.classList.contains("hidden-post");
		const newIcon = (isHidden) ? "-" : "+";

		if (isHidden) {
			postBody.classList.remove("hidden-post");
		} else {
			postBody.classList.add("hidden-post");
		}
		e.target.innerText = newIcon;
	});

	bodyContainer.addEventListener("click", (e) => {
		if (!e.target.classList.contains("chat-post-delete")) return;

		const target = e.target;
		const buf = _chat.getActiveBufferItem();

		const contentWrapper = target.closest("article.chat-post-wrapper");
		const messageNum = contentWrapper.dataset.postNum;

		contentWrapper.classList.add("confirming-deletion");

		if (!confirm(`Vahvista #${messageNum}:n poisto`)) {
			contentWrapper.classList.remove("confirming-deletion")
			return;
		}

		buf.deleteMessage(messageNum);
	});

	bodyContainer.addEventListener("click",  (e) =>  {

		if (!e.target.classList.contains("chat-reply-backlink")) return;
		if ('ontouchstart' in window) return;

		const postNum = e.target.dataset.repliesTo;
		const postDocument = _chat.getActiveBufferItem().getHolderElement().querySelector("#p" + postNum);
		if (!postDocument) return;
		postDocument.scrollIntoView();
	});

	_chat.eventEmitter.addListener("s.buffer.channel.post.deleted", (event, self) => {

		const buf = _chat.getBufferById(event.buffer_id);
		if (!buf) {
			console.info("Message deleted but buffer not found");
			return;
		}

		const holder = buf.getHolderElement();
		if (!holder) {
			console.info("Message deleted but buffer has no holder");
			return;
		}

		const postArticle = holder.querySelector("article#p" + event.post_num);
		if (!postArticle) {
			console.info("Post deleted but post element was not found");
			return;
		}

		postArticle.classList.add("deleted-post");

		const postHeader = postArticle.querySelector("header.chat-post-header");

		const postBody = postArticle.querySelector(".chat-post-body");
		const thumb = postBody.querySelector("img.attachment-thumb");

		thumb?.parentNode.removeChild(thumb);
		postBody.parentNode.removeChild(postBody);


		removeNodes(postHeader.querySelectorAll(".default-right"));


		const deletionInfo = document.createElement("span");
		deletionInfo.classList.add("deletion-info");
		deletionInfo.innerText = "Poistettu";
		deletionInfo.style.float = "right";

		if (event.reason?.length) {
			deletionInfo.innerText += ("(" + event.reason + ")");
		}
		postHeader.appendChild(deletionInfo);

		//if (event.reason && event.reason.length) deletionMsg += ` (${postBuilder.escapeHtml(event.reason)})`;

		//headerRightItems.innerHTML = `<span class='deletion-info'>${deletionMsg}</span>`;
	});

});