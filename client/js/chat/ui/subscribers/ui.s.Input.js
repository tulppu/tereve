'use strict'

const input = {};

input.attachment = {};
input.attachment.name = null;
input.attachment.thumb = null;

input.attachment.removeThumb = function() {
	const thumb = input.form.thumb?.firstElementChild;
	if (thumb) {
		thumb.parentNode.removeChild(thumb);
		thumb.src = null;
	}
	input.form.thumbAlt.innerText = '';
	input.form.thumbAlt.hide();
}

input.attachment.thumbFile = function(file) {
	return new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.onload = (function () {
			const thumbFunc = (file.type.startsWith("image")) ? getImageThumbnail : getVideoThumbnail;
			thumbFunc(reader.result, (thumbBlob) => {
				const thumb = new Image();
				thumb.src = thumbBlob;
				return resolve(thumb);
			});
		});
		reader.readAsDataURL(file);
	});
}


input.attachment.reset = function() {
	input.attachment.removeThumb();

	input.form.file.value = null;
	input.attachment.thumb = null;
}
Object.seal(input.attachment);

input.elements = {};
input.form = {
	self		: null,
	txtarea		: null,
	submit		: null,
	clear		: null,
	file		: null,
	fileWrapper : null,
	thumb		: null,
	thumbAlt	: null,
	audio		: null,
	record		: null,
	stopRecord	: null,
};

input.form.isHovered = (() => input.form.self.matches(":hover"))

Object.seal(input.form);

input.init = function() { 
	input.form.self			= document.querySelector("form#input-stuff");
	input.form.txtarea 		= document.querySelector("textarea#chat-page-input");
	input.form.submit 		= document.querySelector("input#chat-submit");
	input.form.clear 		= document.querySelector("input#reset-media-inputs");
	input.form.file 		= document.querySelector("input#chat-file-input");
	input.form.fileWrapper	= document.querySelector("input#chat-file-input-wrapper");
	input.form.thumb 		= document.querySelector("div#chat-upload-thumbnail");
	input.form.thumbAlt 	= document.querySelector("div#chat-upload-thumbnail-alt");

	input.form.record 		= document.querySelector("input#record");
	input.form.stopRecord 	= document.querySelector("input#stop-record");
	input.form.audio		= document.querySelector("audio#chat-recording");

	Object.keys(input.form).forEach((key) => {
		const el = input.form[key];
		el.hide = (() => (el.classList.add("hidden")));
		el.show = (() => (el.classList.remove("hidden")));
	})

	if (input.form.file.files.length) {
		setTimeout(() => {	input.handleFileSelect(input.form.file.files[0]); });
	}
}

input.postingXhr = null;

input.recorder = {};
input.recorder.capturer = null;
input.recorder.blob = null;

input.recorder.start = async function() {

	if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
		console.error('getUserMedia not supported on your browser!');
		return;
	}

	try {
		const stream = await navigator.mediaDevices.getUserMedia({audio: true});
		const chunks = [];
		
		input.recorder.capturer = new MediaRecorder(stream, {
			mimeType: 'audio/ogg'
		});

		input.form.submit.setAttribute("disabled", "disabled");

		input.form.record.hide();
		input.form.stopRecord.show();

		input.attachment.reset();

		input.recorder.capturer.start();

		input.recorder.capturer.ondataavailable = function (e) {
			chunks.push(e.data);
		};

		input.recorder.capturer.onstop = function (_e) {

			const record = new File(chunks, "nauhoitus.mp3", {
				type: 'audio/ogg'
			});
			// https://stackoverflow.com/a/66466855
			const container = new DataTransfer();
			container.items.add(record);
			input.form.file.files = container.files;
			input.form.file.dispatchEvent(new Event("change"));

			//input.handleFileSelect(null, record);

			const audioUrl = URL.createObjectURL(record);
			input.form.audio.src = audioUrl;
			input.form.audio.classList.remove("hidden");

			input.form.submit.removeAttribute("disabled");

			//input.form.file.hide();
			input.form.record.hide();
			input.form.stopRecord.hide();
			input.form.fileWrapper.hide();
			input.form.clear.show();

			stream.getTracks().forEach((track) => track.stop());
		};

		input.form.stopRecord.addEventListener("click", () => input.recorder.capturer.stop(), {
			once: true
		});
		
	} catch (err) {
		console.error("Recording failed");
		console.error(err);
	}
}

input.recorder.reset = function() {
	if (input.recorder.capturer?.state !== "inactive") {
		input.recorder.capturer?.stop();
	}
	if (input.form.audio.src) {
		URL.revokeObjectURL(input.form.audio.src);
		input.form.audio.src = null;
		input.form.audio.classList.add("hidden");
	}
	input.recorder.blob = null;
}
Object.seal(input.recorder);


window.addEventListener('DOMContentLoaded', (_event) => {
	input.init();
});


Object.seal(input.form);

input.cancelPosting = function() {
	input.postingXhr?.abort();
	input.postingXhr = null;
}

input.reset = function(clearText = false) {

	input.attachment.reset();
	input.recorder.reset();

	input.form.submit.value = "Lähetä";
	input.form.clear.classList.add("hidden");

	input.form.submit.removeAttribute("disabled");

	const buffer = _chatGlobal.getActiveBufferItem();

	if (buffer && buffer.configs.maxFileSize >= 0) {
		input.form.file.classList.remove("hidden");
		input.form.fileWrapper.classList.remove("hidden");
		input.form.record.classList.remove("hidden");
	} else {
		input.form.file.classList.add("hidden");
		input.form.fileWrapper.classList.add("hidden");
		input.form.record.classList.add("hidden");
	}
	input.form.stopRecord.classList.add("hidden");
	input.form.submit.classList.remove("hidden");

	input.cancelPosting();

	if (clearText) {
		input.form.txtarea.value = '';
		input.form.txtarea.focus();
	}
}

input.handleFileSelect = function(file) {

	if (!treatedAsMobile()) input.form.txtarea.focus();
	
	input.attachment.removeThumb();

	const activeBuffer = _chatGlobal.getActiveBufferItem();
		
	if (activeBuffer?.configs.maxFileSize && file?.size > activeBuffer?.configs.maxFileSize * 1024 * 1024) {
		alert(`Maxium filesize for this channel is ${activeBuffer.configs.maxFileSize} MB`);
		input.attachment.reset();
		return;
	}

	input.form.thumbAlt.innerHTML = '';

	const thumbAltFilename = document.createElement("span");
	thumbAltFilename.id = "upload-filename";
	thumbAltFilename.innerText = file.name.substring(0, 64);
	thumbAltFilename.title = file.name;
	thumbAltFilename.style.cursor = "pointer";

	input.form.thumbAlt.append(thumbAltFilename);
	input.form.thumbAlt.filename = thumbAltFilename;

	input.form.thumbAlt.append(
		document.createRange().createContextualFragment(
			`(<span id='upload-type'>${file.type}</span>, <span id='upload-size'>${humanFileSize(file.size)}</span>)`
		)
	)
	input.form.clear.show();
	input.form.thumbAlt.show();

	thumbAltFilename.addEventListener("click", async () => {
		const _file = input.form.file.files[0];
		const newName = prompt("", _file.name);

		if (newName === null) return;

		const newFile = new File([_file], newName, {
			type: _file.type
		});

		const container = new DataTransfer();
		container.items.add(newFile);

		input.form.file.files = container.files;
		input.form.file.dispatchEvent(new Event("change"));
	});

	if (!file.type.startsWith("image") && !file.type.startsWith("video")) {
		input.form.thumbAlt.show();
		return;
	}

	if (file.size >= (1024 * 1024 * 40)) {
		input.form.thumbAlt.show();
		return;
	}

	input.attachment.thumbFile(file).then((thumb) => {
		if (thumb) {
			thumb.setAttribute("alt", file.name);
			input.form.thumb.appendChild(thumb);
			input.form.thumb.classList.remove("hidden");
		} else {
			input.form.thumbAlt.show();
		}
	});
}

$("#chat-container").on("chatCreated", (_e, chat) => {

	input.form.thumb.addEventListener("click", compressSelectedImage);
	input.form.thumbAlt.addEventListener("click", (e) => {
		if (e.target.id  === "upload-size") {
			compressSelectedImage();
		}
	});

	input.form.clear.addEventListener("click", () => {
		input.reset();
	});
	
		
	chat.eventEmitter.addListener("s.opened", () => input.form.txtarea.removeAttribute("disabled"));
	chat.eventEmitter.addListener("s.closed", () => input.form.txtarea.setAttribute("disabled", "disabled"));

	input.form.fileWrapper.addEventListener("click", () => input.form.file.click())
	
	input.form.record.addEventListener("click", () => {
		input.recorder.start();
	});


	// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
	window.addEventListener("dragover", (e) => {
		e.preventDefault();
	});


	// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
	window.addEventListener("drop", async (ev) => {
		ev.preventDefault();
		if (ev.dataTransfer.items) {
			const files = ev.dataTransfer.files;
			const file = files[0];
			input.form.file.files = files;

			input.handleFileSelect(file);
		}
		return false;
	});

	document.addEventListener("paste", async(pasteEvent) => {

		if (!pasteEvent.clipboardData.files.length) {
			return;
		}

		pasteEvent.preventDefault();

		const file = pasteEvent.clipboardData.files[0];

		const container = new DataTransfer();
		container.items.add(file);

		input.form.file.files = container.files;
		compressSelectedImage();

	});

	input.form.file.addEventListener('change', () => {
		const file = input.form.file.files[0]
		input.handleFileSelect(file);
	}, false);


	async function compressSelectedImage()
	{
		const container = new DataTransfer();
		const file = input.form.file.files[0];

		if (["image/jpeg", "image/png", "image/webp"].indexOf(file.type) === -1) {
			return;
		}

		const targetType = (file.type !== "image/jpeg") ? {
			type: "image/jpeg",
			ext: ".jpg",
		} : {
			type: "image/webp",
			ext: ".webp"
		};

		const blob = await readFileToBlob(file);
		const compressecPicture = await convertBlobTo(blob, {
			name: file.name,
			quality: 0.85,
			type: targetType.type,
			ext: targetType.ext
		});
		container.items.add(compressecPicture);

		input.form.file.files = container.files;
		input.form.file.dispatchEvent(new Event("change"));
	}

	async function submit() {

		const buf = _chatGlobal.getActiveBufferItem();

		const submitButton = input.form.submit;
		if (submitButton.getAttribute("disabled")) {
			return;
		}

		let message = input.form.txtarea.value;

		if (message && message[0] === '/') {
			await commands.executeIfExists(message, _chatGlobal)

			if (message[1] === '/') {;
				message = message.substring(1);
			} else {
				const success = await commands.executeIfExists(message, _chatGlobal)
				if (success) {
					input.form.txtarea.value = "";
				}
				return;
			}
		}

		if (input.postingXhr) {
			input.postingXhr.abort();
			input.postingXhr = null;
		}

		if (input.recorder.capturer && input.recorder.capturer.state !== "inactive") {
			alert("Finish the recording first.");
			return;
		}

		if (!message && !message.trim().replace(/\s+$/, '').length && !input.form.file.files.length) {
			return;
		}

		submitButton.setAttribute("disabled", "disabled");

		const submittedAt = Date.now()

		buf.sendMessage(message, input.form.file.files[0], function xhrHander(newXhr) {

			if (input.postingXhr) input.postingXhr.abort();
			
			input.postingXhr = newXhr;
			newXhr.upload.addEventListener("progress", function(event) {
				if (event.lengthComputable) {
					const postProgress = ((event.loaded / event.total) * 100).toFixed(1);
					submitButton.value = `Lähetä (${postProgress}%)`;
				}
			});
		}).then((post) => {
			input.reset(true);
			buf.chat.eventEmitter.emitEvent("postSent", [post]);
		}).finally(() => {
			const now = Date.now();
			const timeout = submittedAt - now + (buf.configs.cooldown * 1000);
			setTimeout(() => submitButton.removeAttribute("disabled"), timeout);
		})

	}

	input.form.txtarea.addEventListener("keydown", function(e) {

		if (e.which === 9 && !e.shiftKey) {
			const txtBox = input.form.txtarea
			txtBox.focus();
			document.execCommand('insertText', false /*no UI*/, '\t'); 
			return e.preventDefault();
		}


		//if (treatedAsMobile()) {
		//	input.form.txtarea.focus();
		//	document.execCommand('insertText', false /*no UI*/, '\n'); 
		//	return;
		//}

		if (e.which != 13 || e.shiftKey) {
			return;
		}

		if (treatedAsMobile()) {
			return;
		}

		e.preventDefault();

		submit();
	});

		
	input.form.submit.addEventListener("click", (e) => {
		e.preventDefault();
		submit();
	});

	document.getElementById("nick").addEventListener("nick", (_e) => {
		const newNick = prompt("Nimimerkki", "Anonyymi");
		if (!newNick) return;
		_chatGlobal.nick(newNick);
	});

	document.getElementById("chat-top-header").addEventListener("click", (e) => {
	//$("#chat-top-header").on("click", "#challenge-button", () => {
		if (!e.target.id !== "challenge-button") return;
		const buf = _chatGlobal.getActiveBufferItem();
		buf.sendChallengeRequest();
	});

	// TODO: Move post filtering to own file and do filtering on the server also?
	function filterPost(postArticle, q) {

		if (!q) { 
			postArticle.classList.remove("hidden");
			return;
		}

		const msg 				= postArticle.querySelector(".chat-post-message");
		const attachment 		= postArticle.querySelector("a.attachment-link");
		const name 				= postArticle.querySelector(".chat-post-sender");

		const matchesSearch 	= 	(msg?.textContent.toLowerCase().includes(q))
									|| (name?.textContent.toLowerCase().includes(q))
									|| (attachment?.textContent.toLowerCase().includes(q));

		if (!matchesSearch) postArticle.classList.add("hidden");
		else postArticle.classList.remove("hidden");
	}

	const filterInput = document.getElementById("post-search");
	filterInput.addEventListener("keyup", async function(e) {
		const q = e.target.value;

		const buf = chat.getActiveBufferItem();
		let articles = buf.getHolderElement().querySelectorAll("article");

		let changed = false;

		filterInput.addEventListener("keyup", () => changed = true, {once: true});

		for (var i = articles.length - 1; changed === false && i >= 0; i--) {
			const postArticle = articles[i];

			filterPost(postArticle, q);	

			if (i % 128 === 0) {
				await waiterFunc(8);
				scroller.scrollToEnd();
			}


			if (i === 0 && filterInput.value.length) {
				const buf = chat.getActiveBufferItem();
				const addedMore = await bufferLoader.loadMorePosts(buf, 512);
				if (!addedMore) return;
				articles = buf.getHolderElement().querySelectorAll("article");
				i =  articles.length;
			}

		}
	});

	chat.eventEmitter.addListener("postDrawn", (buf, addedPost, _data) => {
		if (!buf.isActive()) return;
		if (!filterInput.value) return;
		filterPost(addedPost[0], filterInput.value);
	});

	chat.eventEmitter.addListener("postsDrawn", (buf, addedPosts, _data) => {
		if (!buf.isActive()) return;
		if (!filterInput.value) return;

		for (var i = 0; i < addedPosts.length; i++) {
			filterPost(addedPosts[i], filterInput.value);
		}
	});

});
