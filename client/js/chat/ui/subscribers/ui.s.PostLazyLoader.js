
const lazyLoader = {};

lazyLoader.init = function(_chat)
{
    lazyLoader.observer = null;

    lazyLoader.syntaxWorker = new Worker('/js/chat/workers/w.HighLighter.js');
    lazyLoader.syntaxWorker.onmessage = (event) => {
        const el = document.querySelector(".code#" + event.data.id);
        if (!el) return;
        el.classList.add("highlighted");
        el.classList.add("lang-" + event.data.language)
        el.innerHTML = event.data.value;
    }

    lazyLoader.loadCodeSyntax = function(codeNode) {
        const id = Math.random().toString(36).replace(/[^a-z]+/g, '');
        codeNode.id = id;
        lazyLoader.syntaxWorker.postMessage({text: codeNode.textContent, id: id});
    }

    lazyLoader.loadThumb = function (thumb) {

        const thumbSrc = thumb.dataset.srcRoot || thumb.dataset.src;
        let thumbFile = thumb.dataset.thumbStatic;

        const mime = thumb.parentNode.dataset.mime;

        const animatedGifThumbs =  configs.getConfig("animated-gif-thumbs");
        const animatedVidThumb = configs.getConfig("animated-video-thumbs");

        const animatedThumb = animatedGifThumbs || animatedVidThumb;

        if (thumb.dataset.thumbAnimated && animatedThumb) {

            const isVideo = mime.startsWith("video/");
            const isPicture = mime.startsWith("image/");
    
            if (isVideo && animatedVidThumb) {
                thumbFile = thumb.dataset.thumbAnimated;
            }

            if (isPicture && animatedGifThumbs) {
                thumbFile = thumb.dataset.thumbAnimated;
            }
        }

        thumb.src = thumbSrc;

        if (thumbFile) {
            thumb.src +=  "/" + thumbFile;
        }

        thumb.classList.remove("thumb-lazy");

        delete thumb.dataset.srcRoot;
        delete thumb.dataset.src;
    }


    lazyLoader.loadSticker = async function(stickerWrapper, buffer)
    {
        const bufStickers = buffer.chat.getStickers(buffer);

        const stickersMap           =  bufStickers;
        const stickerKey            =  stickerWrapper.dataset.sticker

        if (!stickersMap) {
            return;
        }

        const stickerItem               = stickers.getStickerItem(stickerKey, stickersMap);
        const sticker                   = await stickers.buildSticker(stickerItem, stickersMap);

        if (sticker) {
            const clonedSticker = sticker.cloneNode(false);

            clonedSticker.width = stickerWrapper.clientWidth || 128;
            clonedSticker.height = stickerWrapper.clientHeight || 128;

            stickerWrapper.appendChild(clonedSticker);
        } else{
            console.info("No sticker " + stickerKey);
        }

    }

    lazyLoader.loadPost = function(postNode, buffer) {

        messageExpander.toggleExpandButton(postNode.querySelector("div.chat-post-body > div.chat-post-content"));
        if (!postNode.classList.contains("lazy")) {
            return;
        }

        if (!buffer) buffer = _chatGlobal.activeBuffer;

        postNode.classList.remove("lazy");
        lazyLoader.observer.unobserve(postNode);

        const contentWrapper = postNode.querySelector("div.chat-post-content");

        if (!contentWrapper) return;

        const t = contentWrapper.querySelector("img.thumb-lazy");
        const c = contentWrapper.querySelector(".code:not(.highligted)");

        if (t) lazyLoader.loadThumb(t);
        if (c) lazyLoader.loadCodeSyntax(c);

        const stickersWrappers = contentWrapper.querySelectorAll("span.sticker-wrapper");
        const count = stickersWrappers.length;
        for (var i = 0; i < count; i++) {
            lazyLoader.loadSticker(stickersWrappers[i], buffer);
        }

    }

    lazyLoader.subPostIfNeeded = function(postNode) {
        //const isLazy = postNode.classList.contains("lazy");
       // if (!isLazy) return;
        //lazyLoader.observer.unobserve(postNode);
        lazyLoader.observer.observe(postNode);
    }

    const lazyLoadOptions = {
        root: document.querySelector('#chat-page-posts'),
        //rootMargin: '-1500px',
        threshold: 0,
    };


    lazyLoader.observer = new IntersectionObserver((entries) => {
        // firefox instantly triggers event after posts are appended to document fragment in ui.Buffer.js
        // check is used to avoid loading all at once, instead "addingFragment" listener below loads last 20 posts before buffer is drawn.
        if (entries.length > 12) return;

        let postNode = entries[0].target;

        const buffer = postNode.closest(".buffer-holder")?.buffer || _chatGlobal.activeBuffer;

        entries.forEach(async (e) => {
            lazyLoader.loadPost(e.target, buffer);
        });
        
        // preload some posts above
        let loadCount = 0;
        for (var i = 0; loadCount < 15 && i < 35 && postNode; i++) {
            
            if (!postNode.classList.contains("lazy")) {
                postNode = postNode.previousElementSibling;
                continue;
            }
            lazyLoader.loadPost(postNode, buffer);
            // TODO: Decide whether to pick preivous or next based on scroll direction?
            postNode = postNode.previousElementSibling;
        }
    }, lazyLoadOptions);

    
	_chat.eventEmitter.addListener("postsDrawn", (buf, postNodes) => {
        postNodes.forEach((it) => {
            lazyLoader.subPostIfNeeded(it, buf);
        });
	});

	_chat.eventEmitter.addListener("postDrawn", (buf, postNodes, postData) => {
		if (!postNodes || !postNodes.length) return;
       //subPostIfNeeded(postNodes[0]);
       lazyLoader.loadPost(postNodes[0], buf);
	});

    
    // Insta load last 20 lazy posts.
	_chat.eventEmitter.addListener("bufferDrawn", (buf, postNodes, postData) => {
        const lastPosts = buf.getHolderElement().querySelectorAll("article.lazy:nth-last-of-type(-n+20)")
        const len = lastPosts.length;
        for (var i = len; i && i >= 0; i--) {
            lazyLoader.loadPost(lastPosts[i-1], buf);
        }
    });


    /*
    $("#chat-body-container").on("mouseover", "article.lazy", (e) => {
        //const a = e.target;
        lazyLoader.loadPost(e.target);
        //loadThumb(t, true);
    });*/


    //const postViewOrig = postsViewer.view;
    /*postsViewer.view = (posts, e, sourcePostId) => {
        const buffer = posts[0].closest(".buffer-holder").buffer;
        const viewer = postViewOrig(posts, e, sourcePostId);
        const postArticles = viewer.querySelectorAll("article.chat-post-wrapper.lazy");
        //if (viewer.firstElementChild.classList.contains("lazy")) {
            postArticles.forEach((it) => lazyLoader.loadPost(it, buffer));
        //}

        return viewer;
    };*/

}

$("#chat-container").on("chatCreated", (e, _chat) => {
    lazyLoader.init(_chat);
});
