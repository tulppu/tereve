$("#chat-container").on("chatCreated", (e, chat) => {


	const audioElementObserver = new IntersectionObserver((entries) => {
		const e = entries[0];
		if (e.intersectionRatio === 0) {
			const audio = e.target;
			const isPlaying = !audio.paused

			if (isPlaying && mediaEmbedder && !mediaEmbedder.isPlaying()) {
				const _closeLink = audio.closest("article").querySelector("a.closeAudio");
				const audioName = audio.closest("article").querySelector("a.attachment-link").title;

				mediaEmbedder.playMedia(audio, audioName);
				_closeLink.click();
			}
			audioElementObserver.unobserve(audio)
		}
	});

	$("body").on("click", ".audio-cover-art", (e) => {
	
		const audioLink = e.target.closest("a.chat-post-attachment");

		const artCoverHolder = e.target.closest("div.art-cover-holder");

		const src = audioLink.href
		let type = audioLink.dataset.mime;
		if (type === "audio/opus") type = "audio/ogg";

		const audioHtml = `
			<audio class='post-audio' controls>
				<source src='${src}' type='${type}'>
			</audio>`.trim();

		const audioTemplate = document.createElement('template');
		audioTemplate.innerHTML = audioHtml;

		const audio = audioTemplate.content.firstChild;

		const audioPlayerHolder = document.createElement("div");
		audioPlayerHolder.classList.add("audio-player-holder");
		audioPlayerHolder.appendChild(audio);

		audioLink.appendChild(audioPlayerHolder);

		artCoverHolder.classList.add("hidden");
		audio.play();

		const attachmentName = audioLink.closest(".chat-post-body").querySelector(".attachment-link");


		const closeLinkSpacer = document.createElement("span");
		closeLinkSpacer.classList.add("spacer");

		const closeLink = document.createElement("a");
		closeLink.classList.add("closeAudio")
		closeLink.style.color = "blue";
		closeLink.style.cursor = "pointer";
		closeLink.textContent = "[Sulje]";

		const closeLinkHolder =  document.createElement("span");

		closeLinkHolder.appendChild(closeLinkSpacer);
		closeLinkHolder.appendChild(closeLink);

		attachmentName.parentNode.insertBefore(closeLinkHolder, attachmentName.nextSibling)

		const imgHeight = artCoverHolder.clientHeight;
		const buf = chat.getActiveBufferItem();

		$("#chat-page-posts").get(0).scrollBy(0, imgHeight);


		function closeAudio(e) {

			const parentArticle = e.target.closest("article");

			const _audio = parentArticle.querySelector("audio");
			const _closeLinkHolder =  parentArticle.querySelector("a.closeAudio");
			const _artCoverHolder = parentArticle.querySelector("div.art-cover-holder");
			const _audioPlayerHolder = parentArticle.querySelector(".audio-player-holder");

			if (_audio) {
				_audio.src = '';
				_audio.load();
				_audio.play();
				_audio.parentElement.removeChild(_audio);
			}
			_closeLinkHolder.parentElement.removeChild(_closeLinkHolder);
			_audioPlayerHolder.parentElement.removeChild(_audioPlayerHolder);

			_artCoverHolder.classList.remove("hidden");


			if (buf.isActive() && scroller.isScrolledToBottom()) {
				$("#chat-page-posts").get(0).scrollBy(0, imgHeight);
			}

			closeLinkHolder.parentElement.removeChild(closeLinkHolder);
		}

		closeLink.addEventListener("click", closeAudio);

		const audioAudioEmbed = configs.getConfig("auto-audio-embed");
		if (audioAudioEmbed) {
			audioElementObserver.observe(audio);
		}

		e.preventDefault();
	});
});
