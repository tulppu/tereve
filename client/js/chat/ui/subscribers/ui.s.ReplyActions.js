$("#chat-container").on("chatCreated", (e, _chat) => {

	$("#chat-body-container")
		.on("mouseenter", ".chat-reply-backlink", async(e) => {

			//if (e.target)

			e.preventDefault();
			const target = e.target;

			const id = parseInt(target.closest("article.chat-post-wrapper").dataset.postNum);


			const repliesTo = parseInt(e.target.dataset.repliesTo);
			console.log(repliesTo);
			if (!repliesTo) {
				return;
			}

			const alreadyDisplayed = postsViewer.viewersHolder.querySelectorAll("article#p" + repliesTo);

			if (alreadyDisplayed.length) {
				alreadyDisplayed.forEach((post) => post.classList.add("post-on-highlight"));
				e.target.addEventListener("mouseleave", () => {
					alreadyDisplayed.forEach((post) => post.classList.remove("post-on-highlight"));
				}, {
					once: true
				});
				return;
			}

			const buf = _chat.getActiveBufferItem();
			let posts = buf.getHolder().querySelectorAll(`article#p${repliesTo}`);

			if (!posts?.length) {
				const postData = buf.getPostByNum(repliesTo) || await buf.fetchPostDataByNum(repliesTo);
				if (postData) {
					const postNode = postBuilder.buildPostNode(postData, buf);

					posts = [postNode]
				}
			}
			if (!posts.length) {
				$(e.target).css({'cursor': 'not-allowed'});
				return;
			}
			const postPopup = postsViewer.view(posts, e, id);
			const links = postPopup.querySelectorAll(`a.chat-reply-backlink[data-replies-to='${id}']`);
			links.forEach((l) => {
				l.style.textDecoration = 'underline';
				l.classList.add("popup-initiator")
			});
		})
		.on("mouseenter", ".chat-post-replies-count", (e) => {
			const $target = $(e.target);

			const buf = _chat.getActiveBufferItem();
			const id = e.target.closest("article").id;

			const replyIds = $target.get(0).dataset.replies
				.split(",")
				.map((it) => `article[id='${it}'],`)
				.join("")
				.slice(0, -1)
			const replies = buf.getHolder().querySelectorAll(replyIds);

			if (!replies.length) {
				$(e.target).css({'cursor': 'not-allowed'});
				return;
			}

			postsViewer.view(replies, e, id);
		});


	//const $inputEl = $(_chat.inputEl);
	const inputEl = document.getElementById("chat-page-input");


	//$("#chat-body-container").on("click", ".chat-post-num", (e) => {
	document.querySelector("#chat-page-posts").addEventListener("click", (e) => {

		if (!e.target.classList.contains("chat-post-num")) return;

		e.preventDefault();

		const postNum = e.target.closest("article").dataset.postNum;

		let postnumAndQuote = '';
		postnumAndQuote += ">>" + postNum + "\n";

		let selectedTest = getSelectedTextBeforeClick();
		if (selectedTest && selectedTest.length) {
			//selectedTest = selectedTest.replace(/^/gm, ">");
			postnumAndQuote += ">" + selectedTest + "\n";
		}
		setTimeout(() => {
			inputEl.focus();
			document.execCommand('insertText', false /*no UI*/, postnumAndQuote); 
		});
	});




	_chat.eventEmitter.addListener("postDrawn", (buf, postDocument, postData) => {

		if (!postData.replied_posts) {
			return;
		}

		const updateBacklinks = function(postRootNode, replied_post_num = null) {
			const postHeader = postRootNode.children[0];

			if (replied_post_num) {
				const backLink = postBuilder.buildBacklink(postData.post_num);
				const repliesHolder = postHeader.querySelector("div.replies");
				repliesHolder.appendChild(backLink);
			}

		}


		postData.replied_posts.forEach((replied_post_num) => {
			
			const selector = `article#p${replied_post_num}`;
			const repliedPost = buf.getHolder().querySelector(selector);
			const repliedPostsOnViewers = postsViewer.viewersHolder.querySelectorAll(selector)

			if (!repliedPost) {
				console.info(`Replied message ${replied_post_num} not found`);
				return;
			}
			updateBacklinks(repliedPost, replied_post_num);
			repliedPostsOnViewers.forEach((it) => updateBacklinks(it))
		});

	});
});
