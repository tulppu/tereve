$("#chat-container").on("chatCreated", (e, _chat) => {

	const $togglables = $("#side-bar-togglables");

	$("#side-bar-toggles > a").on("click", (e) => {


		const toBeDown = $(e.target).data("toggle");
		if (!toBeDown) return;

		$togglables.children().hide();

		$(".active-view").removeClass("active-view");

		$(toBeDown).show();
		$(e.target).addClass("active-view").show();

		e.preventDefault();
	});
});