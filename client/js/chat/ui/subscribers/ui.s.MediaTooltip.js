const mediaTooltip = {};

mediaTooltip.element = null;

mediaTooltip.getElement = (() => {
    return mediaTooltip.element;
})

mediaTooltip.isDisplayed = (() => {
    return mediaTooltip.element !== null;
})

mediaTooltip.isHovered = (() => {
    return !!(mediaTooltip.element && mediaTooltip.element.matches(":hover"));
})

$("#chat-container").on("chatCreated", (e, _chat) => {

    let timeout = null;


    $("#chat-page-posts").on("mouseenter", "a.attachment-link", (e) => {

        const attachmentLink    = e.target;
        const attachmentFooter  = attachmentLink.closest("footer.chat-attachment-footer-container");

        attachmentLink.addEventListener("mouseleave", (e) => {
            clearTimeout(timeout);
            const tooltip = e.target.querySelector("#media-tooltip");
            if (tooltip) {
                tooltip.parentNode.removeChild(tooltip);
            }
            mediaTooltip.element = null;
        }, {once: true});

        timeout = setTimeout(() => {
            let txt = '';

            const dataset = attachmentFooter.dataset;
            const fields = Object.keys(dataset);

            const padding = Math.max(8, ...fields.map((i) => i.length));

            const addLine = ((name, value) => {
                txt +=  "\n" + name + " ".repeat(padding - name.length) + ": " + value;
            });

            addLine("Filename", attachmentLink.innerText);
            fields.forEach((field) => {
                if (field === "size") return;
                let name = field.charAt(0).toUpperCase() + field.substring(1);
                addLine(name, dataset[field]);
            })
           addLine("Size", humanFileSize(dataset.size));

            if (!txt) return;

            const tooltip = document.createElement("div");
            tooltip.style.position      = "absolute";
            tooltip.innerText = txt.trim();

            attachmentLink.appendChild(tooltip);
            tooltip.id = "media-tooltip";

            const dict = attachmentLink.getBoundingClientRect();

            tooltip.style.display       = "none";
            tooltip.style.zIndex        = 15555555555;
            tooltip.style.left          = "0px";
            tooltip.style.top           = (dict.top + attachmentFooter.clientHeight)
            tooltip.style.display       = "block";
            tooltip.style.background    = "black";
            tooltip.style.color         = "white";
            tooltip.style.fontFamily    = "monospace";
            tooltip.style.fontSize      = "13px";
            tooltip.style.padding       = "2px 4px 2px 4px";
            tooltip.style.whiteSpace    = "pre";
            tooltip.style.minWidth      = attachmentLink.clientWidth + "px";
            tooltip.style.minHeight     = "5px";
            tooltip.style.visibility    = "hidden";

            setTimeout(() => {

                mediaTooltip.element = tooltip;

                const linkRects = attachmentLink.getBoundingClientRect();
                const bufferHeader          = document.querySelector("header#chat-top-header");
                const inputForm             = document.querySelector("form#input-stuff");

                const uiReversed            = configs.getConfig("reverse-ui-order");

                const fitsOnTop             = linkRects.top > tooltip.offsetHeight + bufferHeader.offsetHeight 
                                            + ((uiReversed) ? inputForm.offsetHeight : 0);

                const top                   = (fitsOnTop) 
                                                ? (tooltip.offsetHeight * -1) 
                                                : attachmentLink.offsetHeight;

                tooltip.style.top           = top + "px";
                tooltip.style.visibility    = "visible";

            })

            tooltip.innerText = txt.trim();

        }, 200)
    });

});