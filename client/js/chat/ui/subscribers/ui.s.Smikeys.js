
/*
 * Source:
 *  https://www.phpbb.com/customise/db/style/dark_style_phpbb3_smilies/
 *  https://www.phpbb.com/customise/db/style/10_smilies/
 *  https://www.phpbb.com/customise/db/style/475_narius_categorized_smilies_for_phpbb3/
 */

var phpbb_smileys = [
    // ['icon_e_biggrin.gif', ':D'],
    ['icon_e_biggrin.gif', ':-D'],
    ['icon_e_biggrin.gif', ':grin:'],
    ['icon_e_smile.gif', ':-)'],
    ['icon_e_smile.gif', ':smile:'],
    ['icon_e_wink.gif', ';)'],
    ['icon_e_wink.gif', ':wink:'],
    ['icon_e_sad.gif', ':-('],
    ['icon_e_sad.gif', ':sad:'],
    ['icon_e_surprised.gif', ':o'],
    ['icon_eek.gif', ':shock:'],
    ['icon_e_confused.gif', ':?'],
    ['icon_cool.gif', '8)'],
    ['icon_cool.gif', '8-)'],
    ['icon_lol.gif', ':lol:'],
    ['icon_mad.gif', ':x'],
    ['icon_mad.gif', ':mad:'],
    ['icon_razz.gif', ':P'],
    ['icon_razz.gif', ':-P'],
    ['icon_redface.gif', ':oops:'],
    ['icon_cry.gif', ':cry:'],
    ['icon_evil.gif', ':evil:'],
    ['icon_evil.gif', '&gt;:('],
    ['icon_twisted.gif', '&gt;:D'],
    ['icon_twisted.gif', ':twisted:'],
    ['icon_rolleyes.gif', ':roll:'],
    ['icon_exclaim.gif', ':!:'],
    ['icon_question.gif', ':?:'],
    ['icon_idea.gif', ':idea:'],
    ['icon_arrow.gif', ':arrow:'],
    ['icon_neutral.gif', ':|'],
    ['icon_neutral.gif', ':-|'],
    ['icon_mrgreen.gif', ':mrgreen:'],
    ['icon_e_geek.gif', ':geek:'],
    ['icon_e_ugeek.gif', ':ugeek:'],
    ['icon_alien.gif', ':ayy:'],
    ['icon_chicken.gif', ':chicken:'],
    ['icon_goofy.gif', ':goofy:'],
    ['icon_happy.gif', ':happy:'],
    ['icon_pig.gif', ':pig:'],
    ['icon_pig.gif', ':possu:'],
    ['martta.gif', ':martta:'],
    ['occasion14.gif', ':beer:'],
    ['drinkingbeer.gif', ':beer'],
    ['smokingred.gif', ':smoke:'],
    ['smokingred.gif', ':tupakki:'],
    ['cat.gif', ':kisu:'],
    ['cat.gif', ':cat:'],
    ['duel.gif', ':duel:'],
    ['naughty.gif', ':naughty:'],
    ['naughty.gif', ':soosoo:'],
    ['rolf.gif', ':rofl:'],
    ['scratchheadyellow.gif', ':HMM:'],
    ['waveyellow.gif', ':moikka:'],
    ['listening.gif', ':music:'],
    ['listening.gif', ':musa:'],
    ['roulette.gif', ':ruletti:'],
    ['roulette.gif', ':roulette:'],
    ['+1.gif', ':+1:'],
    ['imwithstupid.gif', ':stupid:'],

];


function formatSmileys(text) {
    phpbb_smileys.forEach((smiley) => {
        const key = smiley[1];
        const regex = new RegExp(key.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), "g");
        const url = `img/smileys/${regex}.png`;
        const el = `<img src='${url}'>`;
        text = text.replace(regex, el);
    });
    return text;
}

$("#chat-container").on("chatCreated", (e, chat) => {


});