
const bufferSwitcherQuick = {};


//const bufferSwitcher = document.createElement("div");

bufferSwitcherQuick.wrapper = document.createElement("div");;
bufferSwitcherQuick.wrapper.id = "bufferSwitherOverlay";
bufferSwitcherQuick.wrapper.classList.add("overlay");
bufferSwitcherQuick.wrapper.classList.add("hidden");
bufferSwitcherQuick.wrapper.style.display = "flex";
bufferSwitcherQuick.wrapper.style.flexDirection = "column";
bufferSwitcherQuick.wrapper.style.justifyContent = "center";
bufferSwitcherQuick.wrapper.style.alignItems	= "center";

bufferSwitcherQuick.wrapper.input = document.createElement("input");
bufferSwitcherQuick.wrapper.input.style.maxWidth = "30vw";
bufferSwitcherQuick.wrapper.input.style.marginTop = "auto";
bufferSwitcherQuick.wrapper.input.setAttribute("type", "text");

bufferSwitcherQuick.wrapper.appendChild(bufferSwitcherQuick.wrapper.input);

bufferSwitcherQuick.wrapper.buffers = document.createElement("div");
/*
bufferSwitcher.buffers.style.display = "flex";
bufferSwitcher.buffers.style.flexDirection = "column";
bufferSwitcher.buffers.style.justifyContent = "center";
bufferSwitcher.buffers.style.alignContent	= "center";
*/
bufferSwitcherQuick.wrapper.buffers.style.marginBottom = "auto";
bufferSwitcherQuick.wrapper.buffers.style.maxHeight = "80vh";
bufferSwitcherQuick.wrapper.buffers.style.overflow = "auto";
bufferSwitcherQuick.wrapper.buffers.style.background = "rgb(0,0,0,0.15)"
bufferSwitcherQuick.wrapper.buffers.style.fontSize = "1.2rem";
bufferSwitcherQuick.wrapper.buffers.style.padding = "1.2rem";

bufferSwitcherQuick.wrapper.appendChild(bufferSwitcherQuick.wrapper.buffers);
document.body.appendChild(bufferSwitcherQuick.wrapper);

bufferSwitcherQuick.isViewed = (() => bufferSwitcherQuick.wrapper.classList.contains("hidden") == false )
bufferSwitcherQuick.isHovered = (() => {
    return bufferSwitcherQuick.wrapper.buffers.matches(":hover") || bufferSwitcherQuick.wrapper.input.style.matches(":hover");
});


//	return postsViewer.viewersHolder.matches(":hover");

bufferSwitcherQuick.selectedClassname = "selectedBufferListWrapper";
bufferSwitcherQuick.getSelectedItem = (() => bufferSwitcherQuick.wrapper.querySelector("." + bufferSwitcherQuick.selectedClassname));

bufferSwitcherQuick.keyDownHandler = ((e) => {
    
    if (e.key === "Escape") {
        //closeSelection();
        bufferSwitcherQuick.close();
        e.preventDefault();
    }

    // arrow up
    if (e.keyCode === 38)
    {
        const current = bufferSwitcherQuick.getSelectedItem();
        let prev = current;

        while ((prev = prev.previousElementSibling) && prev && prev.classList.contains("hidden") && prev.previousElementSibling) {
            //prev = current.previousElementSibling;
        }

        if (!prev) {
            prev = bufferSwitcherQuick.wrapper.buffers.querySelector(".bufferListWrapper:not(.hidden):last-of-type");
        }

        if (prev && !prev.classList.contains("hidden")) {
            current.classList.remove(bufferSwitcherQuick.selectedClassname);
            prev.classList.add(bufferSwitcherQuick.selectedClassname);
        }
        e.preventDefault();
    }

    // arrow down
    if (e.keyCode === 40)
    {
        const current = bufferSwitcherQuick.getSelectedItem();
        let next = current;
        while ((next = next.nextElementSibling) && next && next.classList.contains("hidden") && next.nextElementSibling) {
            //next = next.nextElementSibling;
        }
        
        if (!next) {
            next = bufferSwitcherQuick.wrapper.buffers.querySelector(".bufferListWrapper:not(.hidden)");
        }

        if (next && !next.classList.contains("hidden")) {
            current.classList.remove(bufferSwitcherQuick.selectedClassname);
            next.classList.add(bufferSwitcherQuick.selectedClassname);
        }
        e.preventDefault();
    }
    

    if (e.key === "Enter") {
        e.preventDefault();

        const current = bufferSwitcherQuick.getSelectedItem();

        if (!current) {
            return;
        }

        const bufKey = parseInt(current.dataset.key);
        const nextActiveBuf = _chatGlobal.buffers.find((it) => it.key === bufKey);

        if (!nextActiveBuf) {
            return;
        }
        nextActiveBuf.draw();
    }

    bufferSwitcherQuick.wrapper.input.focus();
});

bufferSwitcherQuick.filteringHandler = ((e) => {
    bufferSwitcherQuick.wrapper.buffers.childNodes.forEach((it) => {
        const q = bufferSwitcherQuick.wrapper.input.value.toLowerCase().trim();

        if (q.length && !it.innerText.toLowerCase().includes(q))
        {
            it.classList.add("hidden");
        } else {
            it.classList.remove("hidden");
        }
    });
});

bufferSwitcherQuick.clickHandler = ((e) => {
	if (!bufferSwitcherQuick.isHovered()) {
        bufferSwitcherQuick.close();
    }
});

bufferSwitcherQuick.close = function()
{
    //bufferSwitcherQuick.wrapper.buffers.childNodes.forEach((it) => it.parentNode.removeChild(it));

    while (bufferSwitcherQuick.wrapper.buffers.lastElementChild) {
        bufferSwitcherQuick.wrapper.buffers.removeChild(bufferSwitcherQuick.wrapper.buffers.lastElementChild);
      }

    bufferSwitcherQuick.wrapper.classList.add("hidden");

    window.removeEventListener("keydown", bufferSwitcherQuick.keyDownHandler);
    window.removeEventListener("click", bufferSwitcherQuick.clickHandler);
    bufferSwitcherQuick.wrapper.input.removeEventListener("keyup", bufferSwitcherQuick.filteringHandler);
}

bufferSwitcherQuick.view = function()
{
    bufferSwitcherQuick.wrapper.classList.remove("hidden");
    bufferSwitcherQuick.wrapper.buffers.childNodes.forEach((it) => it.parentNode.removeChild(it));

    _chatGlobal.buffers.forEach((buf) => {
        const bufferListWrapper = document.createElement("div");
        bufferListWrapper.classList.add("bufferListWrapper");

        bufferListWrapper.innerText = buf.name;
        bufferListWrapper.dataset.key = buf.key;
        bufferListWrapper.style.maxWidth = "min(max(95vw, 300px), 20vw)";
        bufferListWrapper.style.overflow = "hidden";

        if (buf.isActive()) {
            bufferListWrapper.classList.add(bufferSwitcherQuick.selectedClassname);
        } 

        bufferSwitcherQuick.wrapper.buffers.appendChild(bufferListWrapper);
    });
    bufferSwitcherQuick.wrapper.input.focus();


    window.addEventListener("keydown", bufferSwitcherQuick.keyDownHandler);
    window.addEventListener("click", bufferSwitcherQuick.clickHandler);

    bufferSwitcherQuick.wrapper.input.addEventListener("keyup", bufferSwitcherQuick.filteringHandler);
}

document.addEventListener("keydown", (e) => {
    if (e.ctrlKey && e.key === "p") {

        e.preventDefault();
        if (bufferSwitcherQuick.isViewed())
        {
            bufferSwitcherQuick.close();
        } else {
            bufferSwitcherQuick.view();
        }
    }
})
