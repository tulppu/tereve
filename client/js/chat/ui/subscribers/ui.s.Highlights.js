"use strict";

const notifier = {};
notifier.interval = null;
notifier.i = 0;


notifier.stopIntervalling = (() => {

	if (!notifier.interval) return;

	clearInterval(notifier.interval);
	notifier.interval = null;
	notifier.i = 0;
	const buf = _chatGlobal.getActiveBufferItem();

	document.title = buf.getName();
	buf.unreadCount = 0;
})

$("#chat-container").on("chatCreated", (e, chat) => {

	$("#chat-page-posts").on("click", "div.chat-info-message-wrapper", (e) => {
		let wrapper = null;
		if (e.target.classList.contains(".chat-info-message-wrapper")) wrapper = e.target
		else wrapper = e.target.closest(".chat-info-message-wrapper")

		wrapper.remove();
	});

	chat.eventEmitter.addListener("postAdded", (buf, addedPosts, data) => {
		
		if (buf.isActive() && document.hasFocus()) {
			return;
		}

		if (buf.unreadCount && !notifier.interval && buf.isActive()) {
			notifier.interval = setInterval(() => {

				if (notifier.i % 2 !== 0) {
					document.title = "(" + buf.unreadCount + ") " + buf.getName()
				} else {
					document.title = buf.getName()
				}
				notifier.i++;
			}, 1000 * 1.5);

			window.addEventListener("click", notifier.stopIntervalling, { once: true });
			window.addEventListener("focus", notifier.stopIntervalling, { once: true });
			window.addEventListener("keydown", notifier.stopIntervalling, { once: true });

		}

		if (buf.unreadCount === 1)
		{
			const bufHolder = buf.getHolderElement();
			const newPostsMarker = bufHolder.data.newPostsMarker = bufHolder.data.newPostsMarker || document.createElement("hr");

			newPostsMarker.classList.add("new-posts-marker");
			newPostsMarker.style.cursor = "pointer";
			newPostsMarker.style.height = "20px";
			newPostsMarker.style.color = "transparent";
			newPostsMarker.style.margin = 0;

			bufHolder.insertBefore(newPostsMarker, addedPosts[0]);

			newPostsMarker.addEventListener("click", (e) => {
				const holder = e.target.closest(".buffer-holder");

				if (!holder) return;

				e.target.parentNode.removeChild(e.target);

				delete holder.data.newPostsMarker;

			}, { once: true });
		}
	});

	function _process_post_for_yous(post) {
		const replies = post.querySelectorAll(".chat-post-message > a.chat-reply-backlink.to-me");
		replies.forEach((r) => {

			const reply_wrapper = document.createElement("span");
			reply_wrapper.classList.add("you-highligter");
			reply_wrapper.style.background= "rgb(0,0,0,0.075)";
			let it = r.nextSibling;

			while (it &&
				(
						it.nodeType === 3 // Text node
					||	it.classList.contains("chat-reply-backlink") == false
				)
			) {
				const next = it.nextSibling;
				reply_wrapper.appendChild(it);
				it = next;
			}

			const you_wrapper = document.createElement("span");
			you_wrapper.classList.add("you-wrapper");
			r.parentNode.insertBefore(reply_wrapper, r.nextElementSibling);
			reply_wrapper.insertBefore(r, reply_wrapper.firstChild)
			r.parentNode.insertBefore(you_wrapper, r.nextSibling);

			you_wrapper.appendChild(r);

			const nbsp = document.createTextNode(String.fromCharCode(160))
			you_wrapper.appendChild(nbsp);

			const you = document.createElement("span");
			you.classList.add("you");
			you.innerText = "(You)";
			you_wrapper.appendChild(you);
		});
	}

	chat.eventEmitter.addListener("postDrawn", (buf, addedPost, data) => {
		_process_post_for_yous(addedPost[0]);
	});

	chat.eventEmitter.addListener("addingFragment", (buf, frag) => {
		_process_post_for_yous(frag);
	});
})
