return;

let closeTimeOut = null;
let videoPlayerCloseTimeout = null;

let attachmentDownloadXhr = null;

let $mediaPlayerEl = null;

$(document).ready(() => {
    $mediaPlayerEl = $(`<div id='media-viewer'></div>`);
    $("body").append($mediaPlayerEl);

    $("#chat-body-container").on("click", "a.attachment-link", async (e) => {

        const filename = $(e.target).attr("title").trim();

        if (filename.endsWith(".pdf")) {
            return;
        }
        e.preventDefault();


        const $noteEl = $(e.target).siblings(".attachment-note").find(".note-message");
        $noteEl.parent().show();

        const progressUpdater = (event) => {
            const inPersents = ((event.loaded / event.total) * 100).toFixed(1);
            $noteEl.text(inPersents + "%");
        };

        const postArticle               = e.target.closest("article");

        const attachmentElement         = postArticle.querySelector("a.chat-post-attachment");
        const attachmentFooter          = postArticle.querySelector("footer.chat-attachment-footer-container");
        const postSender                = postArticle.querySelector("a.chat-post-sender");
        const attachmentName            = attachmentFooter.querySelector("a.attachment-link");
        
        const params = {
            event: e,
            //$element: $el,
            encrypted: attachmentElement.dataset.encrypted,
            mime: attachmentElement.dataset.mime,
            url: attachmentElement.href,
            height: attachmentElement.dataset.height,
            width: attachmentElement.dataset.width,
            iv: attachmentElement.dataset.iv,
            signature: attachmentElement.dataset.signature,
            poster: postSender.dataset.id,
            name: (attachmentName) ? attachmentName.title : null,
            title: attachmentElement.dataset.title || attachmentElement.title,
            //name: attachmentFooter.querySelector("a.attachment-link").title,
        };




        try {
            const fileData = await downloadAttachment(params, progressUpdater);

            if (filename.endsWith(".pdf")) {
                window.open(fileData, "_blank");
                return;
            } else {
                const dlLink = document.createElement('a');
                dlLink.setAttribute('href', fileData);
                dlLink.setAttribute('download', filename);
                dlLink.setAttribute('target', '_blank');

                dlLink.style.display = 'none';
                document.body.appendChild(dlLink);

                dlLink.click();

                window.URL.revokeObjectURL(fileData);
                dlLink.parentNode.removeChild(dlLink);

                iv = null;
                signature = null;
            }

            $noteEl.parent().fadeOut(5000);
        } catch (err) {
            if (err instanceof Error) {
                //$noteEl.show();
                $noteEl.text("Error: " + err.message);
            }
        }
        return false;
    });
});



let mediaObject = null;


async function getMediaSource(params) {
    if (!params.encrypted) {
        return params.url;
    }
    try {
        return await downloadAttachment(params);
    } catch (e) { throw e; }
}

let onResizeListener = null;

async function displayMedia(params, rejectOnLoadFaill=true) {

    return new Promise(async(resolve, reject) => {

        clearTimeout(videoPlayerCloseTimeout);


        const buf = _chatGlobal.getActiveBufferItem();

        clearPlayer();
    
        let mediaPlayerFunc = mediaPlayerNextToThumb;

        if (getConfig("media-viewer-type") === "centered" || isMobile()) {
            mediaPlayerFunc = mediaPlayerCentered;
            $mediaPlayerEl.addClass("media-viewer-centered");
        } else {
            $mediaPlayerEl.removeClass("media-viewer-centered");
        }


        if (onResizeListener) onResizeListener.off();
        onResizeListener = $(window).resize(() => mediaPlayerFunc(params));

        const addBackgroundColor = !params.hasAnimation && params.size >= 1024 * 1024;
        if (addBackgroundColor) $mediaPlayerEl.addClass("media-loading");



        // video height and width can't be used
        const roratedVideo = params.mime.startsWith("video") && params.rotation;

        if (roratedVideo) {
            $mediaPlayerEl.css({
                "visibility": "hidden"
            });
        }

        //mediaPlayerFunc(params);

        try {
            return await openPlayer(mediaPlayerFunc, params,
                function mediaLoaded() {

                // dimensions not set by server or arent usable
                if (buf instanceof ChatE2eEncryptedBuffer || roratedVideo) {
                    params.width = mediaObject.naturalWidth || mediaObject.videoWidth;
                    params.height = mediaObject.naturalHeight || mediaObject.videoHeight;

                    mediaPlayerFunc(params);
                    $mediaPlayerEl.css({
                        "visibility": "visible"
                    });
                }

                $mediaPlayerEl.removeClass("media-loading");

                if (addBackgroundColor) {
                    $mediaPlayerEl.find("img").addClass("media-loaded");
                }
                
                return resolve(mediaObject);
            }, function loadError(err) {
                if (rejectOnLoadFaill) return reject(err);
                return resolve();
            });
        } catch (e) {
            console.error(e);
            return reject(e);
        }
    });
}



async function openPlayer(adjustPlayerFunc, params, mediaLoadedCallback, loadFaillCallback = null) {

    $mediaPlayerEl.show();

    // clear old one if exists
    
    if (mediaObject) {
        mediaObject.src = "";
        const parentNode = mediaObject.parentNode;
        if (parentNode) {
            mediaObject.parentNode.removeChild(mediaObject);
        }
        mediaObject = null;
    }

    const mime = params.mime;

    if (mime.startsWith("image")) {
        mediaObject = new Image();
    } else if (mime.startsWith("video") || mime.startsWith("audio")) {
        mediaObject = document.createElement('video');

        mediaObject.controls = true;
        mediaObject.volume = localStorage.getItem("volume") || 0.35;
        mediaObject.autoplay = true;
        mediaObject.disablePictureInPicture = true;
        mediaObject.controlslist = "nofullscreen disablePictureInPicture";
        //mediaObject.controlslist.add("nofullscreen");

        mediaObject.onvolumechange = (e) => localStorage.setItem("volume", e.target.volume);

    } else {
        console.info(`Passed mime '${mime}' which can't be viewed`);
        return;
    }
    // mediaObject.style.visibility = 'hidden';
    params.mediaObject = mediaObject;

    mediaObject.src = await getMediaSource(params);

    if (mime.startsWith("video")) {
        if (!mediaObject.canPlayType(mime)) {
            console.error("Cant play %s", mime);
            return;
        }

        const source = document.createElement("source");
        source.type = mime;

        mediaObject.appendChild(source);
        mediaObject.loop = true;
    }

    mediaObject.title = mediaObject.title = params.title || params.name;

    $mediaPlayerEl.append(mediaObject);

    adjustPlayerFunc(params);



    $media = $mediaPlayerEl.find("video,img");

    if (mediaObject.nodeName === "VIDEO") {
        mediaObject.addEventListener("loadedmetadata", () => mediaLoadedCallback(mediaObject));
    } else {
        $media.on("load", () => mediaLoadedCallback(mediaObject));
    }

    if (loadFaillCallback) mediaObject.addEventListener("error", (e) => loadFaillCallback(e));

}


function downloadAttachment(params, progressCallback) {

    const buf = params.buffer;
    const isEncrypted = params.encrypted;
    const iv =  hex2buf(params.iv);
    const mime = params.mime;

    return new Promise((baseResolve, baseReject) => {

        if (attachmentDownloadXhr) {
            attachmentDownloadXhr.abort();
            attachmentDownloadXhr = null;
        }

        attachmentDownloadXhr = new XMLHttpRequest();


        attachmentDownloadXhr.open("GET", params.url, true);

        if (isEncrypted) {
            attachmentDownloadXhr.overrideMimeType('text\/plain; charset=x-user-defined');
        } else {
            attachmentDownloadXhr.responseType = "arraybuffer";
        }

        attachmentDownloadXhr.onerror = () => baseReject(new Error("HTTP error"));

        if (progressCallback) {
            attachmentDownloadXhr.onprogress = progressCallback;
        }


        attachmentDownloadXhr.addEventListener("load", (async () => {

            if (attachmentDownloadXhr.status !== 200 && attachmentDownloadXhr.status !== 206) {
                if (attachmentDownloadXhr.status === 404) return baseReject(new Error("File not found"));
                else return baseReject(new Error("Failed to download file"));
            }

            if (!isEncrypted) {
                const baseStart = `data:${mime};base64,`;
                const base64 = arrayByfferToBase64(attachmentDownloadXhr.response);

                return baseResolve(baseStart + base64);
            }


            function validateSignature(chipperi, _signer, signature) {
                return new Promise((resolve, reject) => {
                    return verifyData(_signer, signature, chipperi)
                        .then((signatureMatches) => resolve(signatureMatches));
                });
            }

            function decryptFile(kikkeli) {
                return new Promise(async(resolve, reject) => {
                    _chatGlobal.crypto.profile.decryptSharedMessage(
                        await buf.contact.getSharedAES(buf.chat),
                        kikkeli,
                        iv,
                    ).then((attachment) => {
                        const dec = new TextDecoder();
                        const decoded = dec.decode(attachment);

                        return resolve(decoded);
                    }).catch((e) => reject(e));
                });
            }

            const httpResult = attachmentDownloadXhr.response;

            const signer = (params.poster === buf.chat.me.id)
                ? buf.chat.crypto.profile.signer.publicKey
                : buf.contact.keys.signer.key;

            if (!signer) return baseReject(new Error("Failed to fetch signer."));


            const attachmentChiper = b64ToArrayBuffer(httpResult);
            const signatureValid = await validateSignature(attachmentChiper, signer, hex2buf(params.signature));
            if (!signatureValid) {
                return baseReject(new Error("Invalid signature"));
            }

            const decryptedBase64 = await decryptFile(attachmentChiper);
            return baseResolve(decryptedBase64);
        }));

        attachmentDownloadXhr.send();

    });
}

function clearPlayer() {

    //$(event.target).off("mouseleave");
    $mediaPlayerEl.off("mouseleave");
    $mediaPlayerEl.off("click");


    if (closeTimeOut) {
        clearTimeout(closeTimeOut);
    }

    if (onResizeListener) onResizeListener.off();

    if (attachmentDownloadXhr) {
        attachmentDownloadXhr.abort();
        attachmentDownloadXhr = null;
    }


    if (mediaObject && mediaObject.nodeName === "VIDEO") {
        mediaObject.pause();
        mediaObject.removeAttribute("src");
        mediaObject.load();
    }

    if (mediaObject) {
        mediaObject.src = null;

        if (mediaObject.parentNode) {
            mediaObject.parentNode.removeChild(mediaObject);
        }
    }

    closeTimeOut = null;
    $mediaPlayerEl.empty();
}



function mediaPlayerNextToThumb(params) {

    const event = params.event;

    const thumb = params.event.target;

    //return mediaPlayerNextToThumbOrig(params);
}

function mediaPlayerNextToThumbOrig(params) {

    let dHeight = viewPort.height();
    let dWidth = viewPort.width();

    const event = params.event;

    let $target = $(params.event.target);
    let pos = $target.offset();

    let spaceInRight = dWidth - pos.left - 5 - $target.outerWidth();

    let mediaHeight = Math.min(params.height, dHeight);


    const left = pos.left + $target.outerWidth() + 3;


    let width = params.width;


    const needDownScale = params.width > spaceInRight || params.height > dHeight;

    if (needDownScale) {

        const downScaleRate = Math.min(spaceInRight / params.width, dHeight / params.height);

        console.info("Downscaling from %dx%d with rate %f", params.width, params.height, downScaleRate);

        mediaHeight = params.height * downScaleRate;
        width = params.width * downScaleRate;
    }


    const posTop = pos.top + $target.height() / 2;
    const spaceAbove = dHeight - pos.top + $target.height() / 2;

    const verticalMiddle = dHeight / 2;
    const toMiddle = (posTop <= verticalMiddle) ? verticalMiddle - posTop : posTop - verticalMiddle;

    const verticalSpaceRatio = toMiddle / verticalMiddle;


    let top = ((pos.top + $target.height() * 0.5)) - (mediaHeight * 0.5);

    if (posTop < spaceAbove) {
        top += mediaHeight * 0.5 * verticalSpaceRatio;
    } else {
        top -= mediaHeight * 0.5 * verticalSpaceRatio;
    }

    if (params.hasAlhpa) top += mediaHeight * 0.15;

    if (top + mediaHeight > dHeight) top -= (top + mediaHeight) - dHeight;
    if (top < 0) top = 0;

    $mediaPlayerEl.css({
        display: 'block',
        top: top,
        left: left,
        width: width,
        //height: mediaHeight,
        'visibility': 'visible',
    }).find("img,video").css({
        width: width,
        height: mediaHeight,
    });


    // Close viewer immediately if media is picture
    // If media is video then allow hovering over the player so controls can be accessed.
    function closePlayer() {

        if (getConfig("dev-mode")) return;

        if (params.mime.startsWith("image") || !getConfig("hoverable-videos")) {
            $(event.target).off("mouseleave");
            clearPlayer();
            return;
        }

        if (attachmentDownloadXhr) {
            attachmentDownloadXhr.abort();
        }

        videoPlayerCloseTimeout = setTimeout(() => {
            if (!$mediaPlayerEl.is(":hover")) {
                $(event.target).off("mouseleave");
                clearPlayer();
            }
        }, 100);
    }

    $(event.target).off("mouseleave").on("mouseleave", () => {
        if ($target.is(":hover")) {
            return;
        }
        closePlayer();
    });

    $mediaPlayerEl.off("mouseleave").on("mouseleave", () => {
        if ($target.is(":hover")) {
            return;
        }
        closePlayer();
    });
}



function mediaPlayerCentered(params) {
    $mediaPlayerEl.css({
        "left": 0,
        "top": 0,
        "display": "flex",
        "position": "absolute",
    });
   
    $mediaPlayerEl.one("click", () => {
        $mediaPlayerEl.hide();
        clearPlayer();
    });

    return;
}
