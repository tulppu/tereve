$("#chat-container").on("chatCreated", (e, _chat) => {


	return;

	if (!'speechSynthesis' in window) {
		return;
	}
 
	const voices = speechSynthesis.getVoices();
	const voice = voices.find((it) => it.lang === "ms");

	_chat.eventEmitter.addListener("postDrawn", async(buf, $addedMessage, data) => {

		if (!buf.isActive()) {
			return;
		}

		if (!data.message || !data.message.length) {
			return;
		}

		if (data.user_id === _chat.me.id) {
			return;
		}


		var msg = new SpeechSynthesisUtterance(data.message);

		msg.lang = "fi";

		if (voice) {
			msg.voice = voice;
		}

		//msg.pich = 0.5;
		msg.rate = 0.75;

		window.speechSynthesis.speak(msg);

	});
});