function getUserKeys(userId) {
	return new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		xhr.open('GET', `storage/profile/${userId}/keys.json`, true);

		xhr.onload = () => {
			if (xhr.status != 200) {
				return reject(new Error("Failed to download keys"));
			}
			const keys = JSON.parse(xhr.responseText);
			return resolve(keys);
		};

		xhr.onerror = (e) => {
			return reject(e);
		};
		xhr.send();
	});
}

function treatedAsMobile() {
	return getComputedStyle(document.body)
            .getPropertyValue("--mobile").trim() === "1";
	//return ("ontouchstart" in window);
}