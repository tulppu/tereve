let _chatGlobal = null;




console.dev = {};

const logOrig = console.log;
console.dev.log = function(arg) {
	if (configs.getConfig("dev-mode")) return logOrig(arg);
};

const timeOrig = console.time;
console.dev.time = function(arg) {
	if (configs.getConfig("dev-mode")) return timeOrig(arg);
};
const timeEndOrig = console.timeEnd;
console.dev.timeEnd = function(arg) {
	if (configs.getConfig("dev-mode")) return timeEndOrig(arg);
}



$(document).ready(function () {


	if (typeof window.crypto.subtle === "undefined") {
		alert("window.crypto.subtle missing, are you using https:// ?");
		return;
	}


	const chat = new Chat(defaultChatConfigs);
	_chatGlobal = chat;

	chat.uiFuncs.renderBufferUsers = renderBufferUsers;
	chat.uiFuncs.renderBufferItems = renderBufferItems;

	chat.inputEl = '#chat-page-input';

	const $chatHeader = $("#chat-top-header");

	chat.db = new DatabaseHelper();
	chat.db.open(() => {
		$("#chat-container").trigger("chatCreated", [ chat ]);
		chat.run();
	});

	renderBufferItems();

	function renderBufferItems() {

		chat.buffers.forEach((it) => {
			//it.renderBufferMenuElement();
			bufferList.drawBufferListItem(it);
		});
	}

	function renderBufferUsers(buffer) {
		$chatHeader.find("#buffer-users-count").find("#count").text(buffer.users.length);
	}

});
