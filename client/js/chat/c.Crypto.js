Chat.prototype.crypto = {};
Chat.prototype.crypto.profile = {};



$("#chat-container").on("chatCreated", (e, _chat) => {

	console.info("Getting profile");

	_chat.crypto.profile = new CryptoProfile(_chat.getPrefixedString("profile"));
	_chat.crypto.profile.loadKeys().then(() => {
		console.info("Loaded chat profile");
		getFingerprintFromPem(_chat.crypto.profile.signer.publicKeyPem).then((fingerprint) => {
			_chat.me.id = fingerprint;

			$("#user-profile-id").attr("href", "#@" + fingerprint).css("visibility", "visible");
			$("#user-profile-id").text(fingerprint);

		});
	});
	_chat.crypto.chat = _chat;


});


Chat.prototype.crypto.encryptSharedMessage = async function (shared_key, message) {

	let iv = window.crypto.getRandomValues(new Uint8Array(12));
	let enc = new TextEncoder();

	const encrypted = await window.crypto.subtle.encrypt(
		{
			name: "AES-GCM",
			iv: iv,
			tagLength: 128,
		},
		shared_key,
		enc.encode(message)
	);

	return {
		ciphertext: encrypted,
		iv: iv,
	};
};

Chat.prototype.crypto.makeCombinedFingerprint = function (pem1, pem2) {


	pem1 = getBase64ContentFromPublicPem(pem1);
	pem2 = getBase64ContentFromPublicPem(pem2);

	let key1 = new Uint8Array(base64ToArrayBuffer(pem1));
	let key2 = new Uint8Array(base64ToArrayBuffer(pem2));


	var startingKey = key1;
	var secondKey = key2;

	const maxLen = Math.min(key1.length, key2.length);


	for (var i = 0; i < maxLen; i++) {
		if (key1[i] === key2[i])
			continue;

		if (key1[i] > key2[i]) {
			startingKey = key1;
			secondKey = key2;
		} else {
			startingKey = key2;
			secondKey = key1;
		}

		break;
	}


	let combined = new Uint8Array(startingKey.length + secondKey.length);
	combined.set(startingKey);
	combined.set(secondKey, startingKey.length);

	let dec = new TextDecoder();

	return getFingerPrintFromString(dec.decode(combined));
};

async function loadSignerFromPEM(pem) {
	return await loadPublicKeyFromPEM(
		pem, {
			name: "RSASSA-PKCS1-v1_5",
			hash: "SHA-256",
			modulusLength: 2048,
		}, ["verify"]
	);
}


// Chrome fails to load ECDH keys from spki format which loadPublicKeyFromPEM() uses.
async function loadECDHFromPEM(pem) {
	return await loadPublicKeyFromRaw(
		P256EcdhPubPemToRaw(pem), {
			name: "ECDH",
			namedCurve: "P-256",
		},
		[],
	);
}

async function loadRSAFromPEM(pem) {
	return await loadPublicKeyFromPEM(
		pem, {
			name: "RSA-OAEP",
			hash: "SHA-256",
			modulusLength: 2048
		}, ["encrypt"]
	);
}
