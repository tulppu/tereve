
class ChatCommand {
    constructor(command, desc, usage, cb) {
        this.command = command;
        this.cb = cb;
        this.usage = usage;
        this.description = desc;
    }
}


const commands = {};
commands.list = [];

commands.executeIfExists = async function(input, chat) {
    const cmd = input.split(" ")[0];
    const command = commands.list.find((it) => '/' + it.command === cmd);

    if (!command) {
        chat.uiFuncs.displayMessage("Command was not found", "ERROR");
        return false;
    }

    input = input.substr(cmd.length).trim();

    if (!input.length && command.usage) {
        chat.uiFuncs.displayMessage(`Example usage: ${cmd} ${command.usage}`)
        return false;
    }

    command.cb(input, chat);

    return true;
}

commands.register = function(command, desc, usage, cb) {
    if (commands.list.find((it) => it.command === command)) throw new Error(`Command ${command} exists already`);
    commands.list.push(
        new ChatCommand(command, desc, usage, cb)
    );
}


commands.registerDefaultChatCommands = function(chat) {

    commands.register("help", "List all commands", null, (input, chat) => {
        const commandsStr = commands.list.map((it) => `
            <tr>
                <td class='help-command'>/${it.command}</td>
                <td class='help-command-description'>${it.description}</td>
            </tr>`)
            .join('');

        const helpTable = `
        <table class='helper'>
            <tbody>
                ${commandsStr}
            </tbody>
        </table>`;

        chat.uiFuncs.displayMessage(helpTable);
        
        return true;
    });

    commands.register("topic", "Sets topic for buffer", "", (input, chat) => {
        const buf = chat.getActiveBufferItem();
        if (!buf || buf.isPrivateConversation()) {
            return false;
        }
        buf.setTopic(input);
        return true;
    });

    commands.register("op", "Adds user as operator", null, (input, chat) => {
        const buf = chat.getActiveBufferItem();
        if (!buf || buf.isPrivateConversation()) {
            return false;
        }

        buf.addOp(input);
        return true;
    });

    commands.register("deop", "Removes user as operator", null, (input, chat) => {
        const buf = chat.getActiveBufferItem();
        if (!buf || buf.isPrivateConversation()) {
            return false;
        }

        buf.deOp(input);
        return true;
    });

    commands.register("nick", "Changes nick", "new_nick123", (input, chat) => {
        chat.nick(input);
        return true;
    });

    commands.register("join", "Joins to buffer or creates new one", "#buffer_name", async(input, chat) => {
        input = input.trim(input);

        let buf = chat.buffers.find((it) => it.name === input || it.friend_name === input);
        if (buf) {
            buf.draw();
            return true;
        }

        if (input[0] !== '#') {
            chat.uiFuncs.displayMessage(`Invalid channel name '${postBuilder.escapeHtml(input)}'`, "ERROR");
            return false;
        }

        buf = new ChatChannelBuffer(chat, input);
        buf.registerStatusChangeCallback("LOADED", (buf) => {
            buf.draw();
        }, true);

        try {
            chat.addBuffer(buf);
            await buf.join();
        } catch (err) { buf.destroy(); }
            
    });

    commands.register("close", "Closes buffer", null, (input, chat) => {
        const buf = chat.buffers.find((it) => it.name === input || it.friend_name === input) || chat.getActiveBufferItem();
        if (!buf) {
            return;
        }
        buf.destroy();
    });

    commands.register("configs", "Displays configs of buffer", null, (input, chat) => {

        const buf = chat.buffers.find((it) => it.name === input || it.friend_name === input)
            || chat.getActiveBufferItem();

        if (!buf) {
            chat.uiFuncs.displayMessage(`Buffer not found`);
            return false;
        }

        const $rows = $(`<tbody></tbody>`);
        const configKeys = Object.keys(buf.configs);

        configKeys.forEach((key) => {
            let val = buf.configs[key];
            const isString = typeof val === 'string';
            if (isString) {
                val = `"${val}"`;
            }


            $rows.append($(`
                <tr>
                    <td>${postBuilder.escapeHtml(key.toString())}</td>
                    <td class='buffer-config-value'>${postBuilder.escapeHtml(val.toString())}</td>
                </tr>
            `.trim()));
        });

        chat.uiFuncs.displayMessage(`
            <table class='buffer-configs'>
                ${$rows.html()}
            </table>
        `);

        return true;
    });

    commands.register("set", "Sets config of buffer ", "forcedPosterNick bernd", (input, chat) => {
        buf = chat.getActiveBufferItem();

        if (!buf) {
            chat.uiFuncs.displayMessage(`No active buffer`);
            return;
        }

        if (!buf instanceof ChatChannelBuffer) {
            chat.uiFuncs.displayMessage(`Config can only be set for channel`);
            return;
        }

        const configAndValue = input.split(" ");

        if (configAndValue.length === 0) {
            chat.uiFuncs.displayMessage(`Invalid input`);
            return;
        }

        const config = configAndValue[0];
        const val = input.substr(config.length + 1);

        buf.setConfig(config, val);

        return true;
    });
}


$("#chat-container").on("chatCreated", (e, chat) => {

    commands.registerDefaultChatCommands(chat);

    $("#chat-body-container").on("click", ".buffer-config-value", function setValue() {
        const key = $(this).prev().text();
        const currentVal = $(this).text();
        let newVal = prompt(`New value for config ${key}`, currentVal);

        currentBuffer = chat.getActiveBufferItem();

        if (newVal !== null) {
            currentBuffer.setConfig(key, newVal);
        }
    });

});
