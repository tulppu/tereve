function authenticate(_chat) {

	return new Promise(async(resolve, reject) => {

		const profile = _chat.crypto.profile;

		await profile.loadKeys();

		try {
			const challenge = await _chat.post("auth.challenge.request", {
				pem_signer: profile.signer.publicKeyPem,
				pem_ecdh: profile.ecdh.publicKeyPem,
				pem_rsa: profile.rsa.publicKeyPem,
			});

			const enc = new TextEncoder();
			const signature = await profile.signData(enc.encode(challenge.challenge));
			_chat.me.token = challenge.challenge;

			await _chat.post("auth.challenge.response", {
				signature: buf2hex(signature),
				challenge_string: challenge.challenge,
			});

			const nick = localStorage.getItem("nick");
			if (nick && nick.length) {
				await _chat.nick(nick);
			}

			chatFeeds.addItem({
				message: "Challenging done.",
				fadeoutAfter: 5,
			})
	
			console.info("Started sessions");
			return resolve(_chat.configs.server + challenge.challenge);
	
		} catch (err) {
			return reject(err);
		}

	});
}