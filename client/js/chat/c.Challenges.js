// Challenge responses between clients, no authentication with server done here.

const pendingChallengeRequest = new Map();


// encrypt random message with contact's RSA key and then use shared AES key to encrypt it
// -> send 2 times encrypted random message back to contact
// contact receives, decrypts it with shared AES key then with RSA and finally send back signature of original message
// <- when signature comes back check it matches to shared secret and set contact as verified if it does

Chat.prototype.crypto.sendChallengeRequest = async function (contact, successCb) {

	var array = new Uint16Array(32);
	window.crypto.getRandomValues(array);
	const secret_string = array.toString();

	pendingChallengeRequest.set(contact.id, {
		secret_string: secret_string,
		contact: contact,
		timestamp: Math.round((new Date()).getTime() / 1000),
		success_cb: successCb,
	});

	const enc = new TextEncoder();

	const sharedAES = await contact.getSharedAES(_chatGlobal);
	const RSA = contact.keys.rsa.key; 

	const privateString = await encryptUsingRSA(RSA, enc.encode(secret_string));
	const chiperAndIV = await this.profile.encryptSharedData(sharedAES, privateString);

	this.chat.post("crypto.challenge.request", {
		user_id: contact.id,
		secret_string: buf2hex(chiperAndIV.ciphertext),
		shared_iv: buf2hex(chiperAndIV.iv),
	});

};

Chat.prototype.crypto.verifyChallengeResponse = async function (response) {

	const responserId = response.responser_id;
	const request = pendingChallengeRequest.get(responserId);

	if (!request) {
		console.info("Received challenge response for unknown request from %s.", responserId);
		return false;
	}

	const contact = request.contact; 

	const success_cb = request.success_cb;

	pendingChallengeRequest.delete(responserId);

	
	const now = Math.round((new Date()).getTime() / 1000);
	if (request.timestamp < now - 15) {
		console.info("%s failed respnse to challenge request in time", responserId);
		const buf = _chatGlobal.buffers.find((it) => it.friend_id === responserId);
		if (buf) {
			buf.addInfoMessage(`${responserId} failed response to challenge request in time.`);
		}
		return false;
	}

	const enc = new TextEncoder();

	const validReponse = await this.profile.verifyMessage(
		contact.keys.signer.key,
		hex2buf(response.signature),
		enc.encode(request.secret_string)
	);

	if (validReponse) {
		console.info("%s was set was verified", responserId);

		contact.keys.signer.challenged = contact.keys.signer.pem;
		contact.keys.ecdh.challenged = contact.keys.ecdh.pem;
		contact.keys.rsa.challenged = contact.keys.rsa.pem;

		contact.save(_chatGlobal);

		if (success_cb) {
			success_cb(contact, request.secret_string, response.signature);
		}

	} else {
		if (buf) {
			buf.addInfoMessage(`<b>${responserId}</b> sent invalid challenge response.`);
		}
		console.info("%s sent invalid challenge response!", responserId);
	}
};


$("#chat-container").on("chatCreated", (e, _chat) => {

	_chat.eventEmitter.addListener("s.crypto.challenge.request", async (event, _chat) => {


		/*
			if (!getConfig("no-response-limit") && !_chat.buffers.find((it) => {
					return it instanceof ChatPrivateBuffer && it.friend_id === event.challenger_id;
				})) {
				console.info("Received challenge request from unknown users, ignoring.");
				return;
			}
		*/

			const sharedEcdh = await loadPublicKeyFromRaw(P256EcdhPubPemToRaw(event.challenger_ecdh), {
				name: "ECDH",
				namedCurve: "P-256",
			}, []);

			const shared_aes = await _chat.crypto.profile.deriveKey(sharedEcdh);

			let iv = hex2buf(event.shared_iv);

			const decyptedString = await _chat.crypto.profile.decryptSharedMessage(
				shared_aes,
				hex2buf(event.secret_string),
				iv,
			);


			// Decrypt result using own RSA key
			const originalString = await _chat.crypto.profile.decrypt(decyptedString);

			// Sign fully decrypted string
			const signature = await _chat.crypto.profile.signData(originalString);

			const dec = new TextDecoder();

			await _chat.post("crypto.challenge.response", {
				action: "crypto.challenge.response",
				responsing_to: event.challenger_id,
				signature: buf2hex(signature),
			});


		})
		.addListener("s.crypto.challenge.response", (event, _chat) => {
			console.info("got challenge response");
			_chat.crypto.verifyChallengeResponse(event);
		});



});