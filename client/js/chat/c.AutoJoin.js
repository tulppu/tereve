$("#chat-container").on("chatCreated", async (e, _chat) => {

	async function openBuffers(__chat) {
		__chat.buffers.forEach(async (buf) => {
			if (buf.isStatus("PRELOAD") || buf.isStatus("CLOSED")) {
				//chatFeeds.mobileLog("Autojoining " + buf.getName());
				await buf.join();
				if (isMobile()) {
					await waiter(50);
				}
			}
		});
	}

	_chat.eventEmitter.addListener("s.opened", async (event, self) => {
		setTimeout(() => {
			openBuffers(_chat);
		}, 100)
	});

	
	const hash = window.location.hash;
	let bufferToAutojoin = (hash && hash.length) ? postBuilder.escapeHtml(decodeURI(hash.trim())) : null;

	if (bufferToAutojoin && bufferToAutojoin.length > 2 && bufferToAutojoin[1] === '@') {
		bufferToAutojoin = bufferToAutojoin.substr(2);
	}

	if (bufferToAutojoin && bufferToAutojoin.length) {
		const bufferClass = (bufferToAutojoin[0] === '#') ? ChatChannelBuffer : ChatPrivateBuffer;
		const buf = new bufferClass(_chat, bufferToAutojoin);
		_chat.addBuffer(buf);

		_chat.setActiveBufferItem(buf);
	}

	const contacts = await Contact.getAll(_chat);
	contacts.forEach((it) => {
		// If buffer was autojoined
		if (_chat.buffers.find((it) => it.key === bufferToAutojoin)) return;
		_chat.addBuffer(new ChatPrivateBuffer(_chat, it.id));
	});



	/*	
	let autojoinBuffers = ("joinedBuffers" in localStorage) ? JSON.parse(localStorage.getItem("joinedBuffers")) : [];

	autojoinBuffers = Array.from(new Set(autojoinBuffers)); // remove duplicates

	autojoinBuffers.forEach((bufKey, i) => {
		let buf = null;
		if (bufKey[0] === '#') {
			buf = new ChatChannelBuffer(_chat, bufKey);
		} else {
			buf = new ChatPrivateBuffer(_chat, bufKey);
		}
		
		_chat.buffers.push(buf);

		if (i === 0) {
			_chat.setActiveBufferItem(buf);
		}
	});
	*/


});