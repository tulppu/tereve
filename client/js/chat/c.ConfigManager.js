const configs = {};

configs.nodeMap = new Map();


configs.readConfigNode = function (node) {
	const isCheckbox = node.getAttribute("type") == "checkbox";
	const res = (isCheckbox) ? node.checked : node.value;

	return res;
}

configs.getConfig = function(name) {

	let configNode = configs.nodeMap.get(name);
	if (configNode) {
		return configs.readConfigNode(configNode);
	} else {
		configNode = document.querySelector(`.chat-config[name='${name}']`);
		configs.nodeMap.set(name, configNode);
	}

	if (!configNode) throw new Error(`No such config as '${name}'`);

	return configs.readConfigNode(configNode);
}

configs.getConfigElement = function(name) {
	return document.getElementById("chat-settings").querySelector(`.chat-config[name='${name}']`);
}

configs.applyConfigs = function(root = document) {
	const blurThumbs = root.querySelector("input[name='thumbs-blur']");
	//$(".chat-config").on("change", (e) => {
	blurThumbs.addEventListener("apply", (e) => {
		const val = configs.readConfigNode(blurThumbs);
		if (val) {
			$("body").addClass("blurred-thumbs");
		} else {
			$("body").removeClass("blurred-thumbs");
		}
	});

	const thumbsLazyLoad = root.querySelector("input[name='lazyload']");
	thumbsLazyLoad.addEventListener("apply", (e) => {
	//$(".chat-config").on("change", (e) => {
		const val = configs.readConfigNode(thumbsLazyLoad);
		if (val) {
			$("img.attachment-thumb").attr("loading", "lazy");
		} else {
			$("img.attachment-thumb").attr("loading", "eager");
		}
	});



	const centeredUi = root.querySelector("input[name='centered-ui']");
	const centeredStyleCss = `
		body {
			display: flex !important;
			justify-content: center !important;
		}
		#chat-container {
			margin-left: 9.5vw;
			width: min(100vw, 1400px) !important;
		}
	`.trim();
	const ceteredStyleLink = document.createElement("style");
	ceteredStyleLink.innerText = centeredStyleCss;

	centeredUi.addEventListener("apply", (e) => {
		const enabled = configs.readConfigNode(centeredUi);

		if (enabled) {
			document.head.append(ceteredStyleLink);
			document.body.classList.add("centered-ui");
		} else {
			if (ceteredStyleLink.parentNode) {
				ceteredStyleLink.parentNode.removeChild(ceteredStyleLink);
			}
			document.body.classList.remove("centered-ui");

		}
	});

	
	const leftSideBarCSS = `
		#chat-side-bar {
			order: -1;
			margin-top: 50px;
			margin-left:
		}

		body.centered-ui #chat-side-bar {
			margin-left: -7vw;
		}

		body.centered-ui #chat-page-posts {
			width: 50vw !important;
		}
	`.trim();

	let leftSideBarStyleLink = null;
	
	const leftSideBar = root.querySelector("select[name='side-bar-position']");
	leftSideBar.addEventListener("apply", (e) => {
		const leftPositioned = configs.readConfigNode(leftSideBar) === "left";

		if (leftPositioned) {
			leftSideBarStyleLink = document.createElement("style");
			leftSideBarStyleLink.innerText = leftSideBarCSS;
			document.head.append(leftSideBarStyleLink);
			document.body.classList.add("left-sidebar");
		} else {
			if (leftSideBarStyleLink?.parentNode) {
				leftSideBarStyleLink.parentNode.removeChild(leftSideBarStyleLink);
				leftSideBarStyleLink.innerText = '';
			}
			document.body.classList.remove("left-sidebar");
		}
	})

	const reveserOrder = root.querySelector("input[name='reverse-ui-order']");
	reveserOrder.addEventListener("apply", (e) => {

		const linkEl = document.querySelector("link#reverse")
		const enabled = configs.readConfigNode(reveserOrder);

		if (enabled) {
			document.querySelector("body").classList.add("reversed");
			linkEl.href = linkEl.dataset.href;
		} else {
			document.querySelector("body").classList.remove("reversed");
			linkEl.href = "";
		}
	})

	const themeSelection = root.querySelector("select[name='theme']");

	function applyTheme()
	{
		const oldTheme = document.querySelector("link#selectedTheme");
		if (oldTheme) {
			oldTheme.href = null;
			oldTheme.parentElement.removeChild(oldTheme);
		}

		const defaultThemeNode = document.querySelector("#themes option[data-default='1']")
		const usedTheme =  localStorage[themeSelection.getAttribute("value")] || defaultThemeNode.value;

		const newTheme = document.createElement("link");
		newTheme.id = "selectedTheme";
		newTheme.href = "css/themes/" +  usedTheme;
		newTheme.setAttribute("type", "text/css");

		newTheme.addEventListener("error", (err) => {
			console.info("Failed to load theme  " + usedTheme, ", falling back to default.");
			newTheme.href = "css/themes/" + defaultThemeNode.value;

		}, {
			once: true
		});

		newTheme.setAttribute("rel", "stylesheet");

		document.head.appendChild(newTheme);

	}
	themeSelection.addEventListener("apply", applyTheme);
	applyTheme();


	function switchThumbFile(thumb, enabled) {
		const src = thumb.getAttribute("src");
		if (!src) return;

		const newFile = (enabled && thumb.dataset.thumbAnimated) ? thumb.dataset.thumbAnimated : thumb.dataset.thumbStatic;

		const oldFileStart = src.lastIndexOf("/");
		//const thumblessSrc = src.substr(0, oldExtStart);

		const newSrc = src.substring(0, oldFileStart + 1) + newFile;

		thumb.setAttribute("loading", "lazy");
		setTimeout(() => thumb.setAttribute("src", newSrc), 10);
	}


	const animatedGifThumbs = root.querySelector("input[name='animated-gif-thumbs']");
	animatedGifThumbs.addEventListener("apply", (e) => {
		const enabled = configs.readConfigNode(animatedGifThumbs);
		const thumbs = document.querySelectorAll("img.attachment-image.has-animation");
		for (var i = 0; i < thumbs.length; i++) {
			const t = thumbs[i];
			switchThumbFile(t, enabled);
		}
	})

	const animatedVidThumb = root.querySelector("input[name='animated-video-thumbs']");
	animatedVidThumb.addEventListener("apply", (e) => {
		const enabled = configs.readConfigNode(animatedVidThumb);
		const thumbs = document.querySelectorAll("img.attachment-video.has-animation");
		for (var i = 0; i < thumbs.length; i++) {
			const t = thumbs[i];
			switchThumbFile(t, enabled);
		}
	});

	root.querySelectorAll(".chat-config").forEach((it) => {
		it.addEventListener("change", (e) => {
			// Config elements are duplicated for mobile view. Make sure map has last changed node.
			configs.nodeMap.set(e.target.name, e.target);

			const val = configs.readConfigNode(e.target);
			localStorage.setItem(e.target.name, val);
		});
	})

}

$(document).ready(() => {
	configs.applyConfigs();
});

$(document).ready(() => {

	if (isMobile()) {
		if (!localStorage.getItem("media-viewer-type")) {
			$(".chat-config[name='media-viewer-type']").find("option[value='centered']").attr("selected", "selected");
		}
	}

	$(".chat-config").each(function(i) {
		const $el = $(this);

		const isCheckbox = $el.attr("type") === "checkbox";
		const isSelect = $el[0].nodeName === "SELECT";

		const configName = $el.attr("name");

		if (!localStorage.hasOwnProperty(configName)) return;

		const storedValue = localStorage.getItem(configName);

		if (isCheckbox) {
			if (storedValue && storedValue !== "false") {
				$el.attr("checked", "checked");
			} else {
				$el.removeAttr("checked");
			}
		} else if(isSelect) {
			$el.find(`option[value='${storedValue}']`).attr("selected", "selected");
		} else {
			$el.attr("value", storedValue);
		}

		const event = new Event('apply');
		$el.get(0).dispatchEvent(event);
	});

	$(".chat-config").on("change", (e) => {
		const event = new Event('apply');
		setTimeout(() => {
			e.target.dispatchEvent(event);
		}, 100);
	});


	if (configs.getConfig("thumbs-blur")) {
		$("body").addClass("blurred-thumbs")
	}


});
