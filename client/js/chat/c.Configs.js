const defaultChatConfigs = {
	server:
		(location.protocol === 'https:')
		? (`wss://${window.location.hostname}:${window.location.port}/socket/`)
		: (`ws://${window.location.hostname}:${window.location.port}/socket/`),
	serverPrefix: null,
	holderElement : '#chat-page-posts',
	inputElement : '#chat-page-input',
};