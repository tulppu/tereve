class Chat {
    constructor(configs) {
        this.buffers = []; // Change to map?

        this.socket = null;
        this.cbs = [];

        this.inputEl = '#chat-page-input';

        this.reopenInterval = null;

        this.configs = configs;
        this._activeBuffer = null;

        this.xhr = null;

        //this.authFunc = singInByPublicKeys;

        this.me = {
            id: null,
            nick: null,
            token: null,
        };

        this.eventEmitter = new EventEmitter();

        this.uiFuncs = {
            renderBuffer: null,
            renderBufferUsers: null,
            renderBufferHeader: null,

            renderBufferItems: null,

            displayMessage: (msg, type) => {
                const buf = this.getActiveBufferItem();
                const msgString = (type) ? `${type}: ${msg}` : msg;
                if (buf) {
                    buf.addInfoMessage(msgString, type);
                } else {
                    alert(msgString);
                }
            }
        };
    }

    getStickers(buffer) {
        return buffer.options.stickers || this.buffers.find((it) => it.options?.stickers)?.options.stickers;
    }

    getPrefixedString(str) {
        if (!this.configs.serverPrefix || !this.configs.serverPrefix.length) {
            return str;
        }
        return this.configs.serverPrefix + "-" + str;
    }

    get activeBuffer() {
        return this._activeBuffer;
    }

    set activeBuffer(activeBuffer) {
        throw new Error("use setActiveBufferItem()");
    }

    setActiveBufferItem(activeBuffer) {
        this._activeBuffer = activeBuffer;
        this.eventEmitter.emitEvent("bufferSwitched", [activeBuffer]);
        return this;
    }

    addBuffer(buffer) {
        const type = buffer.constructor.name;
        this.buffers.forEach((it) => {
            if (it.constructor.name === type && it.key === buffer.key) {
                throw new Error(`Buffer '${buffer.key}' already added.`);
            }
        });
        this.buffers.push(buffer);
        this.eventEmitter.emitEvent("bufferAdded", [buffer]);
    }

    getActiveBufferItem() {
        return this.activeBuffer;
    }

    getBufferById(id) {
        return this.buffers.find((it) => it.id === id);
    }

    getWebSocket() {
        return this.socket;
    }

    close() {
        if (this.socket) {
            this.socket.close();
        }
    }

    send(data) {

        if (!this.socket && this.socket.readyState !== WebSocket.OPEN) {
            return false;
        }

        let msg = '';
        let keys = Object.keys(data);

        keys.forEach((key) => {
            let val = data[key];
            msg += `${key}: ${ (val) ? encodeURI(data[key]) : ''}\n`
        });

        this.socket.send(msg);
        return true;
    }


    async run() {

        const self = this;

        function waitHelper(time=300) {
            return new Promise((resolve) => {
                setTimeout(() => resolve(), time);
            })
        }

        while(true) {

            while (self.socket) {
                await waitHelper(500);
                continue;
            }
            
            let socketAddr = null;
            try {
                socketAddr = await authenticate(self);
            } catch (err) {
                console.info(err);
                await waitHelper(3500);
                continue
            }
            console.log("Opening sock from addr " + socketAddr);

            try {
                self.socket = new WebSocket(socketAddr, '6455');
                self.socket.self = self;
                self.socket.binaryType = "arraybuffer"

                window.addEventListener("onunload", () => self.socket?.close());
                window.addEventListener("beforeunload", () => self.socket?.close());

            } catch (e) {
                console.error("Failed to open socket");
                console.error(e);

                chatFeeds.addItem({
                    message: "Failed to open socket.",
                    fadeoutAfter: 2,
                 });
                continue;
            }

            self.socket.onopen = function (event) {
                console.info("Opened connection");

                chatFeeds.addItem({
                    message: "Socket open.",
                    fadeoutAfter: 2,
                })

                self.eventEmitter.emit("s.opened", [event, self]);
            }
            self.socket.onmessage = function (evt) {
                if (evt.data instanceof ArrayBuffer) {
                    self.eventEmitter.emitEvent(`stream`, [evt.data, self]);
                    return;
                }
                try {
                    if (!evt || !evt.data) {
                        return;
                    }

                    const eventObj = JSON.parse(evt.data);
                    if (eventObj.event) {
                        self.eventEmitter.emitEvent(`s.${eventObj.event}`, [eventObj, self]);
                    } else {
                        console.info("Received event without event name");
                        console.info(eventObj);
                    }
                } catch (e) {
                    console.error("Failed to parse event");
                    console.error(evt);
                    console.error(e);
                    return;
                }
            }
            self.socket.onclose = function (e) {
                console.info("Socket closed");
                chatFeeds.addItem({
                    message: "Connection lost, trying to reopen.",
                    fadeoutAfter: 5,
                })

                self.buffers.forEach((buf) => buf.setStatus("CLOSED"));
                self.eventEmitter.emitEvent(`s.closed`, [e, self]);

                self.socket.onopen = null;
                self.socket.onmessage = null;
                self.socket.onclose = null;
                self.socket = null;
            }
        }
    }

    fetchAction(action, params) {
        if (!params.headers) params.headers = {};
        if (!params.headers.token) {
            params.headers.token = this.me.token;
        }
        return fetch("api/" + action + "?" + new URLSearchParams(params.qsData).toString(), {
            method: "GET",
            headers: params.headers,
            //signal: params.signal || null,
        });
    }

    post(action, body, file, requestStartingCallback = null) {

        const self = this;

        return new Promise((resolve, reject) => {
            const formData = new FormData();

            if (file) {
                formData.append("file", file, file.name);
            }

            Object.keys(body).forEach((key) => {
                const val = body[key];
                if (typeof val === "undefined") return;
                formData.set(key, val);
            });


            const xhr = new XMLHttpRequest();
            xhr.open("POST", "api/" + action, true);
            xhr.setRequestHeader("token", self.me.token);

            if (requestStartingCallback) {
                requestStartingCallback(xhr);
            }

            xhr.send(formData);

            const onError = (err) => {
                console.error(err);
                const buffer = self.getActiveBufferItem();
                if (!buffer) {
                    alert(err);
                    return;
                }

                chatFeeds.addItem({
                    source: buffer.getName(),
                    message: err,
                    fadeoutAfter: 2,
                })


               // buffer.addInfoMessage(err);
                return reject(err);
            };

            xhr.addEventListener("load", () => {
                let response = null;
                if (xhr.getResponseHeader("content-type") === "application/json") {
                    try {
                        response = JSON.parse(xhr.response);
                    } catch (e) {
                        console.error(e);
                        console.info(xhr);
                    }
                }

                if (xhr.status === 200 && response?.success) {
                    chatFeeds.addItem({
                        message: response.message,
                        fadeoutAfter: 2,
                    })
                }
                if (xhr.status === 400 && response?.success == false) {
                    chatFeeds.addItem({
                        message: response.message,
                        fadeoutAfter: 2,
                    })
                }


                if (xhr.status !== 200) {
                    const errMsg = (response) ? (response.desc || response.error) : `${xhr.statusText}`;
                    return onError(errMsg);
                }
                return resolve(response);

            });

            xhr.onerror = (e) => onError(xhr.statusText);
        });
    }

    requestChannelInfo(buffer, postsLimit=-1) {
        return this.post("buffer.channel.load", {
            buffer_name: buffer,
            posts_limit: postsLimit,
        });
    }

    chatBan(userId) {
        this.post("user.ban", {
            user_id: userId,
        });
    }

    async nick(nick) {
        const res = await this.post("user.nick", {
            nick: nick,
        });
        localStorage.setItem("nick", nick);
        //$("#nick").get(0).innerText = nick;
        document.getElementById("nick").innerText = nick;
        return res;
    }

    deleteChannelMessage(buffer, post_num, reason) {
        return this.post("buffer.channel.post.delete", {
            buffer_name: buffer,
            post_num: post_num,
            reason: reason || '',
        });
    }

    deletePrivateMessage(buffer, post_num, reason) {
        return this.post("buffer.private.post.delete", {
            buffer_id: buffer,
            post_num: post_num,
        });
    }

    join(buffer, pass) {

        if (buffer[0] !== '#') {
            throw new InvalidBufferNameException();
        }

        return this.post("buffer.channel.join", {
            buffer_name: buffer,
            pass: (pass && pass.lenght) ? pass : '',
        });
    }

    part(buffer, reason) {
        if (buffer[0] === '#') {
            return this.partChannel(buffer, reason);
        } else {
            this.closePrivate(buffer);
        }
    }

    partChannel(buffer_name, reason = null) {
        return this.post("buffer.channel.part", {
            buffer_name: buffer_name,
            reason: reason,
        });
    }

    openPrivate(friend_id) {
        return this.post("buffer.private.open", {
            friend_id: friend_id,
        });
    }

    closePrivate(buffer_id) {
        return this.post("buffer.private.close", {
            buffer_id: buffer_id,
        });
    }

    topic(buffer, topic) {
        return this.post("buffer.topic", {
            buffer: buffer,
            topic: topic,
        });
    }

    addOp(buffer, user_nick) {
        return this.post("buffer.user.op", {
            buffer: buffer,
            user_nick: user_nick,
        });
    }

    deOp(buffer, user_nick) {
        return this.post("buffer.user.deop", {
            buffer: buffer,
            user_nick: user_nick,
        });
    }

    kick(buffer, user_nick) {

    }

    setConfig(buffer, config, value) {
        return this.post("buffer.channel.config.set", {
            buffer: buffer,
            config: config,
            value: value,
        });
    }

    whisper(msg, postNum, buffer, bufferType, isEncrypted) {
        return this.post("user.whisper", {
            buffer: buffer,
            buffer_type: bufferType,
            message: msg,
            post_num: postNum,
            is_encrypted: isEncrypted,
        });
    }


}
