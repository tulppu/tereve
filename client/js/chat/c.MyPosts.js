// TODO: switch to indexdb?

const myPosts = {};
myPosts.store = {};
try {
	myPosts.posts = new Set(JSON.parse(localStorage.getItem("myPosts")) || []);
} catch (e) {
	myPosts.posts = new Set();
}

myPosts.getPostId = (postNum, bufferId) => {
	if (typeof postNum  === 'undefined' || typeof bufferId === 'undefined') throw new Error("Invalid argument")
	return bufferId + "_" + postNum;
};

myPosts.addPost = (postNum, bufferId) => {
	const key = myPosts.getPostId(postNum, bufferId);
	myPosts.posts.add(key);
	//localStorage.setItem("myPosts", JSON.stringify(myPosts.posts))
	localStorage.setItem("myPosts", JSON.stringify(Array.from(myPosts.posts)))

};

myPosts.hasPost = (postNum, bufferId) => {
	const key = myPosts.getPostId(postNum, bufferId);
	const res =  myPosts.posts.has(key);
	return res;
}

$("#chat-container").on("chatCreated", (e, _chat) => {
	_chat.eventEmitter.addListener("postSent", (post) => {
		myPosts.addPost(post.post_num, post.buffer_id);
	});
});