/*
Chat.prototype.db = {};
Chat.prototype.db.stores = {};
Chat.prototype.db.instance = null;
*/

//Chat.prototype.db = {};
//window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.OIndexedDB || window.msIndexedDB,
//IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.OIDBTransaction || window.msIDBTransaction,

class DatabaseHelper {

	constructor(name) {
		this.name = name;
		this.stores = {};
		this.instance = null;
	}

	open(readyCallback) {

		var req = indexedDB.open(name, 8);
		let self = this;

		req.onsuccess = function(event) {
			self.instance = event.target.result;
			readyCallback(event.target.result);
		};

		req.onupgradeneeded = function (event) {

			let stores = event.target.result.objectStoreNames;

			self.instance = event.target.result;

			if (!stores.contains("contacts")) {
				self.createStore("contacts", "id", true);
			}

			if (!stores.contains("myPosts")) {
				self.createStore("myPosts", "id", true);
			}

			if (!stores.contains("stickers")) {
				self.createStore("stickers", "id", true);
			}


		};

		req.onerror = function(evt) {
			self.instance = this.result;
		};

	}

	isLoaded() {
		return !!this.instance;
	}
	
	createStore(name, keypath, isUnique) {		
		let store = this.instance.createObjectStore(name, {keyPath: keypath});
		store.createIndex("id", "id", { unique: isUnique });
		
		return store;
	}
	

	getStore(name) {
		let transaction = this.instance.transaction([name], "readwrite");

		transaction.onerror = (err) => console.error(err);

		return transaction.objectStore(name);
	}

	getFromStore(storeName, id) {
		const self = this;
		return new Promise((resolve, reject) => {
			const store = self.getStore(storeName);

			const req = store.get(id);

			req.onsuccess = (event) => resolve(req.result);
			req.onerror = (err) => {
				console.error(err);
				console.error(transaction.error);
				return reject(err);
			};
		});
	}

	addToStore(storeName, object) {
		return new Promise((resolve, reject) => {
			let store = this.getStore(storeName);
			let r = store.put(object);
			r.onsuccess = () => {
				resolve(object);
			};
			r.onerror = () => {
				reject(e);
				console.error("Failed to add object!");
			};
		});
	}

	removeFromStore(storeName, object) {
		return new Promise((resolve, reject) => {
			let store = this.getStore(storeName);
			let r = store.delete(object);
			r.onsuccess = () => {
				resolve(object);
			};
			r.onerror = () => {
				reject(e);
				console.error("Failed to remove object!");
			};
		});

	}
}
