class Contact {

	constructor(id) {
		this.id = id;

		this.alias = null;
		this.added_at = new Date().getTime();

		this.keys = {
			signer: {
				key: null,
				pem: null,
				fingerprint: null,
				challenged: null,
			},
			ecdh: {
				key: null,
				pem: null,
				fingerprint: null,
				challenged: null,
			},
			rsa: {
				key: null,
				pem: null,
				fingerprint: null,
				challenged: null,
			},
		};
		this.loadKeys();
	}

	static getAll(chat) {
		return new Promise((resove, reject) => {
			const store = chat.db.getStore("contacts");
			store.onerror = (e) => reject(e);

			const all = store.getAll();
		
			all.onsuccess = () => resove(all.result);
		});
	}

	get fingerprint() {
		return this.keys.fingerprints.signer;
	}

	get keysOk() {
		return this.keys.signer.pem && this.keys.signer.pem === this.keys.signer.challenged
				&& this.keys.ecdh.pem && this.keys.ecdh.pem === this.keys.ecdh.challenged
				&& this.keys.rsa.pem && this.keys.rsa.pem === this.keys.rsa.challenged;

	}

	getSharedAES(_chat) {
		return _chat.crypto.profile.ecdh.deriveSharedKey(this.keys.ecdh.key);
	}

	async load(chat) {

		const stored = await chat.db.getFromStore("contacts", this.id);

		if (!stored) return;

		this.alias = stored.alias;
		
		this.keys = stored.keys;
		this.added_at = stored.added_at;

		if (!this.keys.signer.key) {
			this.loadKeys();
			this.save(chat);
		}
	}

	save(chat) {
		return chat.db.addToStore("contacts", this);
	}

	remove(chat) {
		return chat.db.removeFromStore("contacts", this.id);
	}

	async loadKeys() {

		const url = `storage/profile/${this.id}/keys.json`;

		const res = await fetch(url);
		const keys = await res.json();

		this.keys.signer.key = await loadSignerFromPEM(keys.signer);
		this.keys.signer.pem = keys.signer;
		this.keys.signer.fingerprint = await getFingerprintFromPem(keys.signer);

		this.keys.ecdh.key = await loadECDHFromPEM(keys.ecdh);
		this.keys.ecdh.pem = keys.ecdh;
		this.keys.ecdh.fingerprint = await getFingerprintFromPem(keys.ecdh);

		this.keys.rsa.key = await loadRSAFromPEM(keys.rsa);
		this.keys.rsa.pem = keys.rsa;
		this.keys.rsa.fingerprint = await getFingerprintFromPem(keys.rsa);

		return keys;
	}

	static async load(chat, id) {
		try {
			const res = await chat.db.getFromStore("contacts", id);
			return res;
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	static async exists(chat, id) {
		return !!await Contact.load(chat, id);
	}

	static async addNew(chat, id, alias) {
		let contact = new Contact(id);
		contact.id = id;
		contact.alias = alias;

		return await chat.db.addToStore("contacts", contact);
	}


}