importScripts('/js/highlight.min.js');

onmessage = (event) => {
    const result = self.hljs.highlightAuto(event.data.text);
    postMessage({value: result.value, language: result.language, id: event.data.id});
};