
class ChatE2eEncryptedBuffer extends BaseBuffer {

	constructor(chat, id) {

		super(chat, id);

		this.keys_loaded = false;
		this.keys_awaiter_cbs = [];
	}


	async sendMessage(message, attachment = null, reqStartedCb = null) {

		//if (!this.keys.shared_aes) {
			//return reject(new Error("Shared key missing"));
		//	throw new Error("Shared key missing");
		//}

		let obj = {};
		//obj.action = "buffer.private.message";
		obj.friend_id = this.friend_id;
		obj.encrypted = true;

		let enc = new TextEncoder();

		const hasMessage = message && message.length;
		const hasAttachment = attachment && attachment.file;

		const encryptSharedData = async (data) => {
			return await this.chat.crypto.profile.encryptSharedData(await this.contact.getSharedAES(this.chat), data);
		};

		const signData = async (data) => {
			return await this.chat.crypto.profile.signData(data);
		};

		if (hasMessage) {
			// Encrypt message and sign chiper
			const encodedMessage = enc.encode(message);

			const encryptedMessage = await encryptSharedData(encodedMessage);

			obj.message = buf2hex(encryptedMessage.ciphertext);
			obj.message_iv = buf2hex(encryptedMessage.iv);


			obj.signature_message = buf2hex(await signData(encryptedMessage.ciphertext));
		}

		let file = null;
		if (hasAttachment) {
			// Encrypt file data and sign chiper

			const encodedAttachment = enc.encode(await fileToBase64(attachment.file));
			const encryptedAttachment = await encryptSharedData(encodedAttachment);

			//obj.attachment_data = arrayByfferToBase64(encryptedAttachment.ciphertext);
			obj.attachment_data_iv = buf2hex(encryptedAttachment.iv);

			obj.signature_attachment = buf2hex(await signData(encryptedAttachment.ciphertext));

			//const url = `data:blob;base64:${}`
			//const blob = new Blob([encryptedAttachment.ciphertext], {type: 'text/plain'}),
			file = new File([arrayByfferToBase64(encryptedAttachment.ciphertext)], "decrypted.dat", {
				type: 'text/plain'
			});


			// Encrypt file meta.
			const attachmentMeta = JSON.stringify({
				original_filename: attachment.file.name,
				mime: attachment.file.type,
				thumb_url_default: attachment.thumbnail,
			});
			const encodedAttachmentMeta = enc.encode(attachmentMeta);

			const encrypedAttachmentMeta = await encryptSharedData(encodedAttachmentMeta);

			obj.attachment_meta = buf2hex(encrypedAttachmentMeta.ciphertext);
			obj.attachment_meta_iv = buf2hex(encrypedAttachmentMeta.iv);

			// TODO: handle by server and relay to receivers
			obj.signature_attachment_meta = buf2hex(await signData(encrypedAttachmentMeta.ciphertext));
		}

		return this.chat.post("buffer.private.post", obj, file, reqStartedCb);
	}

	async openMessage(postData) {

		const self = this;

		if (postData.decrypted) {
			return postData;
		}

		if (!postData.options) {
			postData.options = {};
			return postData;
		}

		const signer = (postData.user_id === this.chat.me.id)
			? this.chat.crypto.profile.signer.publicKey
			: this.contact.keys.signer.key;

		const decryptSharedData = async (data, iv) => {

			if (!iv || !iv.length) {
				throw new Error("IV missing");
			}

			return await self.chat.crypto.profile.decryptSharedMessage(
				await self.contact.getSharedAES(self.chat),
				hex2buf(data),
				hex2buf(iv),
			).catch((e) => {
				throw e;
			});
		};

		const verifySignature = async (data, signature) => {
			return await verifyData(
				signer,
				hex2buf(signature),
				hex2buf(data)
			).catch((e) => console.error(e));
		};

		const hasMessage = postData.message && postData.message.length;
		const hasAttachment = postData.attachment && postData.attachment.meta;

		let dec = new TextDecoder();

		if (hasMessage) {

			const chipertext = postData.message;

			try {
				const decryptedMessage = await decryptSharedData(chipertext, postData.options.message_iv);
				postData.message = dec.decode(decryptedMessage);

				if (!postData.options.message_iv) {
					console.error("Missing iv!");
					return postData;
				}

				let signature = postData.options.signature_message;
				postData.signature = signature;

				const signatureMatched = await verifySignature(chipertext, signature);
				postData.options.signature_verified = signatureMatched;

				if (!signatureMatched) {
					console.error("Signature mismatch");
					return postData;
				}

			} catch (e) {
				console.error(e);
				return postData;
			}

		} else {
			postData.options.signature_verified = true;
		}

		if (hasAttachment) {
			try {

				const decryptedAttachmentMeta = await decryptSharedData(
					postData.attachment.meta,
					postData.options.attachment_meta_iv
				);

				postData.attachment.meta = JSON.parse(dec.decode(decryptedAttachmentMeta));
			} catch (e) {
				console.error("Failed to open attachment meta");
				return;
			}
		}

		return postData;
	}
}