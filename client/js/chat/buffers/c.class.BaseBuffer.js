class BaseBuffer {
    constructor(chat, name) {

        if (!chat) throw new Error("Params chat is unset.");
        if (!name || !name.length) throw new Error("Param name is unset.");

        this.s = {};
        this.s.name = name;
        this.chat = chat;
        this.holderElement = null;
        this.postCache = new Map();
        /*
            "PRELOAD" - buffer was only created in client side and haven't asked any data from server.
            "LOADING" - buffer is waiting data from server
            "DRAWING" - received posts are being built and added to be displayed.
            "LOADED" - data was received and server will keep sending it.
            "CLOSED" - buffer was open but it was closed because of disconnect.
         */
        this.status = "PRELOAD";

        // true if buffer has messages that replies to user or if there are unread messages in private conversation
        this.highlighted = false;
        this._isLoading = false

        // this.s = Object.assign(this.s, data);

        //this.config = {};
        this.clientConfig = {};
       // this.lastDrawnPostNum = 0;
        this.unreadCount = 0;
        this.cbs = [];
    }

    get key() {
        throw "Ilmementation missing!";
    }

    get id() {
        return this.s?.id;
    }

    set id(id) {
        this.s.id = id;
    }

    get lastBump() {
        return this.s.last_bump;
    }

    get name() {
        return this.s.name;
    }

    set name(name) {
        this.s.name = name;
    }

    get users() {
        return this.s.users;
    }

    set users(users) {
        this.s.users = users;
    }

    get posts() {
        return (this.s.posts) ? this.s.posts : [];
    }

    set posts(posts) {
        this.s.posts = posts;
    }

    set isLoading(isLoading) {
        this._isLoading = isLoading;
    }

    get isLoading() {
        return this._isLoading;
    }


    get isDrawing() {
        return this.holderElement.classList.contains("buffer-drawing");
    }

    set isDrawing(isDrawing) {
        if (isDrawing == false) {
            this.holderElement.classList.remove("buffer-drawing");
        } else {
            this.holderElement.classList.add("buffer-drawing");
        }
    }

    bsGetPostIndexByNum(postNum)
	{
        if (!this.posts.length) return -1;

		let left = 0;
		let right = Math.max(this.posts.length - 1, 0);

		if (postNum < this.posts[left].post_num) return -1;
		if (postNum > this.posts[right].post_num) return -1;

		while (left <= right) {
            const middle = Math.floor(left + ((right - left) / 2));
			if (this.posts[middle].post_num === postNum) {
				return middle;
			}
			if (postNum < this.posts[middle].post_num) {
				right = middle-1;
			} else {
				left = middle+1;
			}
		}
		return -1;
	}

    getPostByNum(postNum)
	{
		const index = this.bsGetPostIndexByNum(postNum);
		if (index === -1) return null;
		return this.posts[index];
	}

    async fetchPostDataByNum(postNum)
    {
        const self = this;
        if (self.postCache.has(postNum)) {
            return this.postCache.get(postNum);
        }

        const fetchRes = await this.chat.post("buffer.posts.load", {
            buffer_name: self.getName(),
            posts_start: postNum,
            posts_limit: 1,
        });
        const postData = fetchRes.at(0);
        this.postCache.set(postNum, postData);
        return postData;
    }

    async fetchPosts(start, limit) {
        if (this.isLoading) return [];
        const self = this;
        this.isLoading = true;
        const posts = await this.chat.post("buffer.posts.load", {
            buffer_name: self.getName(),
            posts_start: start,
            posts_limit: limit,
        });
        this.isLoading = false;
        return posts.reverse();
    }

    addPost(postData) {
        // TODO: secure ordering by postnum and check isn't already added.
        this.posts.push(postData);

        this.chat.eventEmitter.emitEvent("postAdded", [
            this,
            postData,
        ]);
    }

    addPosts(posts) {
        this.posts = this.posts.concat(posts);
		this.posts = this.posts.sort((a, b) => {
			if (a.post_num < b.post_num) return -1;
				if (a.post_num > b.post_num) return 1;
				return 0;
		});
    }

    get post_count() {
        return (this.s.post_count);
    }

    set post_count(post_count) {
        this.s.post_count = post_count;
    }


    get options() {
		return (this.s.options) ? this.s.options : {};
	}


    get configs() {
        return (this.s.config) ? this.s.config : {};
    }

    set configs(config) {
        this.s.config = config;
    }

    highlighted() {
        return this.highlighted;
    }


    getFirstMessage() {
        return this.firstPost;
    }

    setFirstMessage(firstPost) {
        this.firstPost = firstPost;
    }


    addInfoMessage(message, type) {
        const infoMessage = buildDocumentFromHtmlString(
        `<div class='chat-info-message-wrapper'>
            <div class='chat-info-message-holder'>
                <span class='chat-info-message' data-type='${type}'>${message}</span>
                <!-- <span class='chat-info-close'>x</span> <--!>
            </div>
        </div>`);
        this.getHolderElement().appendChild(infoMessage);

        scroller.autoScrollToEnd({buffer: this});
    }

    getUrlHash() {
        throw new Error("Impelention missing!");
    }

    getJoiningId() {
        throw new Error("Impelention missing!");
    }

    // Overrided
    draw() {
        throw new Error("Missing implementation!");
    }

    async sendMessage(message, to, attachment = {}, repliesTo = null) {
        throw "Implement sendMessage";
    }

    getStatus() {
        return this.status;
    }

    validateStatus(status) {
        if (["PRELOAD", "LOADING", "LOADED", "CLOSED"].indexOf(status) === -1) {
            throw new Error(`Invalid buffer status '${status}'`);
        }
    }

    isStatus(status) {
        this.validateStatus(status);
        return this.getStatus() === status;
    }

    setStatus(status) {
        this.status = status;
        if (["PRELOAD", "LOADING", "DRAWING", "LOADED", "CLOSED"].indexOf(status) === -1) {
            throw `Invalid buffer status '${status}'`;
        }
        this.getHolderElement().setAttribute("data-status", status);
        this.executeCallback(status);
    }

    registerStatusChangeCallback(status, cb, once = false) {
        if (["PRELOAD", "LOADING", "LOADED", "CLOSED"].indexOf(status) === -1) {
            throw `Invalid buffer status '${status}'`;
        }
        this.registerCallback(status, cb, once);
    }

    registerCallback(event, cb, once) {
        this.cbs.push({
            event: event,
            cb: cb,
            once: once
        });
    }

    executeCallback(event) {
        if (!this.cbs.length) {
            return;
        }
        this.cbs.forEach((it) => {
            if (it.event === event) {
                it.cb(this);
                it.fired = true;
            }
        });
        // remove callbacks that should be fired only once
        this.cbs = this.cbs.filter((it) => {
            return !it.once || !it.hasOwnProperty("fired")
        });
    }

    get bufHolderName() {
        return "buffer-" + this.id;
    }

    hasHolderElement() {
        return this.holderElement !== null && typeof this.holderElement !== "undefined";
        //return !!$(this.chat.configs.holderElement).children("#" + this.bufHolderName).length;
    }

    createHolderElement() {

        if (!this.id && !this.name) {
            throw "Can't add holder element because either id or name is unset";
        }

        this.holderElement = document.createElement("div");
        this.holderElement.buffer = this;

        this.holderElement.data = {
            buffer: this,
            last: null,
        };

        this.holderElement.id = this.name || this.id;
        this.holderElement.buffer = this;
        this.holderElement.classList.add("buffer-holder");
        this.holderElement.classList.add("hidden");

        document.querySelector(this.chat.configs.holderElement).appendChild(this.holderElement);

        //Object.seal(this.holderElement);

        //$(this.chat.configs.holderElement).append("<div id='" + this.bufHolderName + "' style='' class='buffer-holder hidden'></div>");
    }

    getHolderElement() {


        if (!this.hasHolderElement()) {
            this.createHolderElement();
        }


        return this.holderElement;
    }

    getHolder() {
        return this.getHolderElement();
        /*
        if (this.holderElement) return this.holderElement;
        this.holderElement = document.getElementById(this.bufHolderName);
        return this.holderElement;*/
    }

    isMod(user_id) {
        return !!this.ops.find((it) => it === user_id);
    }

    setTopic(topic) {
        this.chat.topic(this.name, topic);
    }

    isPrivateConversation() {
        return false;
    }

    isActive() {
        return this === this.chat.getActiveBufferItem();
    }


    saveAutojoins(self = null) {
        self = (self) ? self : this;
        var storageKey = self.chat.getPrefixedString("joinedBuffers");
        var bufsToSave = self.chat.buffers.map((it) => it.getJoiningId());

        localStorage.setItem(storageKey, JSON.stringify(bufsToSave));
    }

    async join() {
        var self = this;
        //this.setStatus("LOADING");
        this.registerStatusChangeCallback("LOADED", () => {
            self.saveAutojoins(self);
        }, true);
    }

    part() {
        var index = this.chat.buffers.indexOf(this);
        if (index >= 0) {
            this.chat.buffers.splice(index, 1);
        }
        this.saveAutojoins();
    }


    deleteMessage(message_id, reason) {
        throw new Error("Missing implementation");
    }

    amModerator() {
        return false;
    }

    destroy() {
        this.part();
        this.getHolderElement().remove();
        bufferList.removeItemByBuffer(this);

        const index = this.chat.buffers.indexOf(this);
        if (index >= 0) {
            this.chat.buffers.splice(index, 1);
            this.saveAutojoins();
        }
    }

    addOp(user_nick) {
        this.chat.addOp(this.name, user_nick);
    }

    deOp(user_nick) {
        this.chat.deOp(this.name, user_nick);
    }

    requestInfo() {
        if (this.status !== "LOADED") {
            throw "Buffer needs to be loaded before info can be requested!";
        }
        if (this.isPrivateConversation()) {
            throw "Buffer info can't be requested for private buffer!";
        }
        //this.setStatus("LOADING");

        this.chat.requestChannelInfo(this.name, (isMobile) ? 512 : -1);
    }

}
