class ChatPrivateBuffer extends ChatE2eEncryptedBuffer {

	constructor(chat, friend_id) {

		super(chat, friend_id);
		
		this.contact = new Contact(friend_id);
		this.contact.load(chat);
	}


	get friend_id() {
		return this.contact.id;
	}

	async getFingerprint() {
		const contactPEM = this.contact.keys.signer.pem;
		const myPEM = this.chat.crypto.profile.signer.publicKeyPem;

		return this.chat.crypto.makeCombinedFingerprint(contactPEM, myPEM);
	}

	getName() {
		return "@" + ((this.alias) ? `${this.alias} (${this.name})` : this.friend_id);
	}

	getUrlHash() {
		return "@" + this.friend_id;
	}

	getJoiningId() {
		return this.friend_id;
	}

	get alias() {
		return this.contact.alias;
	}

	get key() {
		return this.friend_id.replace(/:/g, "_");
	}

	get particiants() {
		return this.s.particiants;
	}

	deleteMessage(message_id) {
        this.chat.deletePrivateMessage(this.id, message_id);
	}
	
	whisper(message, postNum) {
		return this.chat.whisper(message, postNum, this.id, "private", true);
	}

	isPrivateConversation() {
		return true;
	}


	get bufHolderName() {
		return "buffer-" + this.key;
	}

	async join() {
		BaseBuffer.prototype.join.call(this);
		return await this.chat.openPrivate(this.friend_id);
	}

	part() {
		this.chat.closePrivate(this.id);
		return BaseBuffer.prototype.part.call(this);
	}

}