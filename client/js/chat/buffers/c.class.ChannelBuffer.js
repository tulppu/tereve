class ChatChannelBuffer extends BaseBuffer {

	constructor(chat, name) {
		super(chat, name);
	}

	getUrlHash() {
		return this.name.substr(1, this.name.length - 1);
	}

	getJoiningId() {
		return this.name;
	}

	getName() {
		return this.name;
	}


	// constant unique string that can be used as attribute of html element
	get key() {
		if (this._key) {
			return this._key;
		}
		// https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
		this._key = this.name.toLowerCase().split('').reduce((prevHash, currVal) =>
			(((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
		return this._key;
	}

	get topic() {
		return this.s.topic;
	}

	set topic(topic) {
		this.s.topic = topic;
	}

	get users() {
		return (this.s.users) ? this.s.users : [];
	}

	set users(users) {
		this.s.users = users;
	}

	get ops() {
		return (this.s.ops) ? this.s.ops : [];
	}

	set ops(ops) {
		this.s.ops = ops;
	}


	amModerator() {
		return this.ops.find((it) => it.id === this.chat.me.id);
	}

	join() {
		BaseBuffer.prototype.join.call(this);
		return this.chat.join(this.getName());
	}

	part() {
		this.chat.partChannel(this.name);
		BaseBuffer.prototype.part.call(this);
	}

	deleteMessage(post_num, reason = null) {
		return this.chat.deleteChannelMessage(this.name, post_num, reason);
	}

	async sendMessage(message, file = null, reqStartedCb = null) {
		let obj = {};
		//obj.action = "buffer.channel.post";
		obj.buffer = this.name;
		obj.message = message;

		return this.chat.post("buffer.channel.post", obj, file, reqStartedCb);
		//return (res) ? resolve() : reject();
	}
	
	whisper(message, postNum, isEncrypted) {
		return this.chat.whisper(message, postNum, this.name, "channel", isEncrypted);
	}


	setConfig(config, value) {
		return this.chat.setConfig(this.name, config, value);
	}
}
