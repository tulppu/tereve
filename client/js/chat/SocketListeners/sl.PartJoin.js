$("#chat-container").on("chatCreated", (e, _chat) => {

	_chat.eventEmitter.addListener("s.buffer.channel.join", (event, chat) => {
		const buf = chat.getBufferById(event.buffer_id);
		if (buf && buf.users) {
			if (buf.users.find((it) => it === event.user_id)) return;
			buf.users.push(event.user_id);

			if (buf.isActive()) {
				chat.uiFuncs.renderBufferUsers(buf);
			}
		}
	})
	.addListener("s.buffer.channel.part", (event, chat) => {
		const buf = chat.getBufferById(event.buffer_id);
		if (buf && buf.users) {

			const index = buf.users.findIndex((it) => it === event.user_id);
			if (index >= 0) {
				buf.users.splice(index, 1);
				if (buf.isActive()) chat.uiFuncs.renderBufferUsers(buf);
			}
		}
	});

});