
"use strict";

const bufferLoader = {};

$("#chat-container").on("chatCreated", (e, _chat) => {


    const eventEmitter = _chat.eventEmitter;
    const chat = _chat;

    const initialFetchedPosts = (isMobile()) ? 32 : 128;

    async function loadBuffer(event, buffer) {

        if (buffer.isStatus("LOADING")) {
            console.info("Loading already loading buffer %s", buffer.getName());
            chatFeeds.mobileLog("Already loading buffer");
            return;
        }

        if (!buffer.chat.getActiveBufferItem()) {
            buffer.chat.setActiveBufferItem(buffer);
        }

        const holder = buffer.getHolderElement();
        const wasPreloaded = buffer.isStatus("PRELOAD") || !buffer.posts?.length;

        buffer.setStatus("LOADING");

        if (buffer.isActive()) {
        //    await buffer.draw()
        }


        //const lastDrawnPost = holder.data.last;
        //const lastDrawnPostNum = parseInt(lastDrawnPost?.dataset.postNum || 0);
        const lastDrawnPostNum = parseInt(holder.querySelector("article:last-of-type")?.dataset.postNum) || 0

        // If already loaded and not refilling.
        const lastGivenPostNum = event.buffer.posts.at(-1)?.post_num || 0;
        const firstGivenPostNum = event.buffer.posts.at(0)?.post_num || 0;

        const lastKnownPostNum = Math.max(lastDrawnPostNum, buffer.posts.at(-1)?.post_num  || 0, 0);

        event.buffer.posts = event.buffer.posts.filter((it) => it.post_num > lastKnownPostNum);

        if (!wasPreloaded) {
            event.buffer.posts = [];
        }

        buffer.s = event.buffer;

        if (!buffer.hasHolderElement()) {
            buffer.createHolderElement();
        }
        buffer.renderBufferMenuElement();


        if (wasPreloaded)
        {
            if (buffer.isActive()) {
                await buffer.draw();
                window.requestAnimationFrame(async() => {
                    await bufferLoader.loadMorePosts(buffer, initialFetchedPosts);
                    buffer.setStatus("LOADED");
                    _chat.eventEmitter.emitEvent("bufferLoaded", [buffer]);    
                });
            } else {
                buffer.setStatus("LOADED");
                _chat.eventEmitter.emitEvent("bufferLoaded", [buffer]);
            }
        }
        else
        {
            if (buffer.isActive()) {
                chatFeeds.mobileLog(lastGivenPostNum + " asd " + lastKnownPostNum);
            }

            // Already have posts displayed but connection went down so check if new posts were added.
            if (lastGivenPostNum > lastKnownPostNum) {

                const maxPostsToLoad = lastGivenPostNum - lastKnownPostNum;

                // Instead of loading new posts for all buffers on
                // reconnection mark buffer as dirty or something and fetch posts when switched to?
                buffer.fetchPosts(lastGivenPostNum, maxPostsToLoad).then(async(loadedPosts) => {
                    console.log("Fetched moar posts");
                    chatFeeds.mobileLog("Loaded new posts for " + buffer.getName());

                    loadedPosts = loadedPosts.filter((it) => it.post_num > lastKnownPostNum);

                    chatFeeds.mobileLog("Adding  " + loadedPosts.length + " posts");
                    chatFeeds.mobileLog(initialFetchedPosts + ", " + maxPostsToLoad + ", " + lastKnownPostNum);

                    if (buffer.isActive()) {
                        const builtPosts = await buffer.drawPosts(loadedPosts, false);
                        chatFeeds.mobileLog("Added  " + builtPosts.length + " posts");
                        builtPosts.slice(-32).forEach((it) => {
                            lazyLoader.loadPost(it, buffer);
                        });
                    } else {
                        buffer.addPosts(loadedPosts);
                        /*
                        loadedPosts.forEach((it) => {
                            buffer.addPost(it);
                        });*/    
                    }
                    buffer.setStatus("LOADED");
                    _chat.eventEmitter.emitEvent("bufferLoaded", [buffer]);
                }).catch((err) => {
                    chatFeeds.mobileLog(err.toString());
                })
            }

        }

            
    }

    eventEmitter.addListener("s.buffer.private.loaded", (event, chat) => {

        let buffer = chat.buffers.find((it) => it.friend_id && it.friend_id === event.buffer.friend_id);
        if (!buffer) {
            buffer = new ChatPrivateBuffer(chat, event.buffer.friend_id);
            //buffer.posts; // Load posts.
            chat.addBuffer(buffer);
        }

        loadBuffer(event, buffer);
    });

    eventEmitter.addListener("s.buffer.channel.loaded", (event, chat) => {

        let buffer = chat.buffers.find((it) => it.name && it.name.toLowerCase() === event.buffer.name.toLowerCase());
        if (!buffer) {
            buffer = new ChatChannelBuffer(chat, event.buffer.name);
             /*
                Load posts, eases or fixes race condition? that makes determining
                last post to fail when buffer has tons of posts and when it's being loaded two times simultaneously.
                eg. client has set it to be autojoined and server auto sends pinned channel on join.
            */
            buffer.posts;
            chat.addBuffer(buffer);
        }

        loadBuffer(event, buffer);
    });

});

addEventListener('DOMContentLoaded', (event) => {
//$("#chat-container").on("chatCreated", (e, _chat) => {

    bufferLoader.lastFetch = 0;

    const msgsHolder = document.querySelector("#chat-page-posts");
    msgsHolder.addEventListener("scroll", async() => {

        const chat = _chatGlobal;

        
        const currBuf = chat.getActiveBufferItem();
        //const buffHolder =  currBuf.getHolderElement();


        //if (buffHolder.dataset.status !== "READY") return;
        if (!currBuf.isStatus("LOADED")) {
            return;
        }

        const reversedOrder = configs.getConfig("reverse-ui-order");

        const sHeight = msgsHolder.scrollHeight;

        const loadTriggered = (reversedOrder)
            ? (msgsHolder.scrollTop + (msgsHolder.clientHeight * 2)  >= sHeight - msgsHolder.clientHeight)
            : (msgsHolder.scrollTop < 150);


        if (loadTriggered && currBuf.isLoading == false && bufferLoader.lastFetch + 250 <= Date.now() && currBuf.isDrawing === false) {
            bufferLoader.loadMorePosts(chat.getActiveBufferItem());
          }
    });

    bufferLoader.loadMorePosts = async function(buffer, amount = (isMobile()) ? 32 : 128)
    {
        const buffHolder =  buffer.getHolderElement();


        const reversedOrder = configs.getConfig("reverse-ui-order");
        const currBuf = buffer;
        bufferLoader.lastFetch = Date.now();


        if (!currBuf.posts.length) {
            
            const firstKnownPost = buffHolder.querySelector("article:first-of-type");
            if (!firstKnownPost) return false;

            const postNum = parseInt(firstKnownPost.dataset.postNum);
            const fetchedPosts = await currBuf.fetchPosts(postNum, amount);
            fetchedPosts.pop()
            
            // In case load was triggered multiple times and posts already filled.
            if (!buffer.posts.length) {
                //buffer.posts = fetchedPosts;
            }
            buffer.addPosts(fetchedPosts);
            //if (currBuf.posts.length === 0) return false;
            bufferLoader.lastFetch = Date.now();
        }

        if (currBuf.posts.length) {
            const preScrollHeight = buffHolder.clientHeight;

            const postsToAdd = currBuf.posts.splice(-254);
            const builtPosts = await buffer.drawPosts(postsToAdd, true);

            builtPosts.slice(-16).forEach((it) => {
                lazyLoader.loadPost(it, buffer);
            });

            if (!reversedOrder) {
                const postScrollHeight = buffHolder.clientHeight;
                msgsHolder.scrollTo(0, postScrollHeight - preScrollHeight );
            }
            return true;
        }
        return false;

    }

//});

});
