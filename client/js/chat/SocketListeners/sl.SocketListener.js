//function registerDefaultChatEventHandlers($el) {
$("#chat-container").on("chatCreated", (e, chat) => {
    const eventEmitter = chat.eventEmitter;
    eventEmitter. addListener("s.response.error", (event, self) => {
		const msg = `
			Virhe: <b>${event.error}</b><br>
			Kuvaus: <b>${event.desc}</b><br>
			Koodi: <b>${event.code}</b><br>
			Toimi: <b>${event.failed_action}</b><br>
		`.trim();

		self.uiFuncs.displayMessage(msg);
    })
	.addListener("s.user.nickchange", (event) => {
        chat.buffers.forEach((buffer) => {
            if (buffer && buffer.users) {
                buffer.users.forEach((usr) => {
                    if (usr.id === event.user_id) {
                        usr.nick = event.new_nick;
                    }
                });
            }
        });
    })

    .addListener("s.user.auth.status", (event, self) => {
        if (event.authed !== true) {
            return;
        }

        const nick = localStorage.getItem(self.getPrefixedString("chat-nick"));
        if (nick) {
            self.nick(nick);
        }

    })
    // TODO: Ask user if privatechat should be opened?
    .addListener("s.buffer.private.posts", function (event, self) {
        if (!buffer) {
            const buf = new ChatPrivateBuffer(self, event.user_id);
            buf.highLighted = true;
            buf.join();
            chat.addBuffer(buf);
        }
    })

    .addListener("s.buffer.channel.configchange", (event, self) => {
        const buffer = self.getBufferById(event.buffer_id);
        let configs = buffer.configs;
        const oldVal = configs[event.config];
        
        if (oldVal === event.new_value) {
            return;
        }

        configs[event.config] = event.new_value;

        buffer.addInfoMessage(`<b>${event.config}</b> was changed from <b>${oldVal}</b> to <b>${event.new_value}</b>.`);
    })

    .addListener("s.buffer.channel.topic", (event, self) => {
        const buf = self.getBufferById(event.buffer_id);
        if (buf) {
            buf.topic = event.topic;
            self.uiFuncs.renderBufferHeader(buf);
            buf.renderBufferMenuElement();
        }
    })

    .addListener("s.buffer.channel.opped", (event, self) => {
        const buf = self.getBufferById(event.buffer_id);
        if (!buf) {
            return;
        }

        buf.ops.push(event.opped_id);
        self.uiFuncs.renderBufferUsers(buf);
    })

    .addListener("s.buffer.channel.deopped", (event, self) => {
        const buf = self.getBufferById(event.buffer_id);
        if (!buf) {
            return;
        }

        const index = buf.ops.indexOf(event.deopped_id);
        if (index > -1) {
            buf.ops.splice(index, 1);
            self.uiFuncs.renderBufferUsers(buf);
        }
    })

});
