$("#chat-container").on("chatCreated", (e, _chat) => {


	function addPost(buffer, post) {
		buffer.addPost(post)
		if (buffer.getHolderElement().data.last) {
			buffer.drawPost(post);
		}

	}

	_chat.eventEmitter.addListener("s.buffer.channel.post", (postData, chat) => {

		const buffer = _chat.getBufferById(postData.buffer_id);

		if (!buffer) {
			return;
		}

		addPost(buffer, postData);
	});

	_chat.eventEmitter.addListener("s.buffer.private.post", async (postData, chat) => {

		const buffer = chat.getBufferById(postData.buffer_id);

		if (!buffer) {
			return;
		}

		const openedPost = await buffer.openMessage(postData);
		//buffer.openedPost = postData.post_nu;
		buffer.drawPost(openedPost);
	});


	_chat.eventEmitter.addListener("s.buffer.channel.post.update", (postData, chat) => {

		const buffer = _chat.getBufferById(postData.buffer_id);
		if (!buffer) {
			return;
		}

		const bHolder = buffer.getHolderElement();
		
		const oldPostArticle = bHolder.querySelector("#p" + postData.post_num);
		if (oldPostArticle) {
			const newArticle = postBuilder.buildPostNode(postData, buffer);
			lazyLoader.loadPost(newArticle, buffer);
			bHolder.insertBefore(newArticle, oldPostArticle);
			bHolder.removeChild(oldPostArticle);

			console.info("Updated post");
			return;
		}


		const lastAddedPostNum = parseInt(buffer.getHolderElement().data.last?.dataset.postNum || 0);

		const highestKnownPost = Math.max(
			lastAddedPostNum,
			buffer.posts.at(-1)?.post_num,
		);


		if (postData.post_num > highestKnownPost) {
			addPost(buffer, postData);
			return;
		}

		let i = -1;
		if ((i = buffer.posts.findIndex((it) => it.post_num === postData.post_num)) >= 0) {
			buffer.posts[i] = postData;
		}

	});

});
