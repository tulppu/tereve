'use strict';

class CryptoBaseClass {

	constructor(name = null, ALGORITM = null, uses = []) {
		this.name = name;

		this.privateKey = null;
		this.publicKey = null;

		this.publicKeyPem = null;

		this.algorithm = ALGORITM;
		this.uses = uses;
	}

	get privKeyName() {
		return this.name + ".priv";
	}

	get pubKeyName() {
		return this.name + ".pub";
	}

	getUsesByName() {
		throw "Missing implemenation"
	}

	async generateKeys() {
		return await window.crypto.subtle.generateKey(
			this.algorithm,
			true,
			this.uses
		);
		//throw "Implemetation missing";
	}

	async saveKeys() {
		async function saveKey(name, key) {
			return new Promise((resolve, reject) => {
				return window.crypto.subtle.exportKey(
					"jwk",
					key
				).then((exported) => {
					localStorage.setItem(name, JSON.stringify(exported));
					resolve();
				}).catch((e) => reject(e))
			});
		}

		await saveKey(this.privKeyName, this.privateKey);
		await saveKey(this.pubKeyName, this.publicKey);
	}

	hasKeySaved(name) {
		var key = localStorage.getItem(name);
		return key && key.length;
	}

	// https://github.com/mdn/dom-examples/blob/master/web-crypto/import-key/pkcs8.js
	async importPublicPem(pem) {


		// fetch the part of the PEM string between header and footer
		const pemHeader = "-----BEGIN PUBLIC KEY-----";
		const pemFooter = "-----END PUBLIC KEY-----";
		const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);
		// base64 decode the string to get the binary data
		const binaryDerString = window.atob(pemContents);
		// convert from a binary string to an ArrayBuffer
		const binaryDer = str2ab(binaryDerString);


		var self = this;

		//return new Promise((resolve, reject) => {
			return await window.crypto.subtle.importKey(
				"spki",
				binaryDer,
				self.algorithm,
				true,
				["encrypt"]
			)

	}


	loadKeys() {

		var self = this;

		function loadKey(name) {

			var loadedKey = localStorage.getItem(name);
			if (!loadedKey || !loadedKey.length) {
				throw `No key '${name}' in localStorage`;
			}

			return window.crypto.subtle.importKey(
				"jwk",
				JSON.parse(loadedKey),
				self.algorithm,
				true,
				self.getUsesByName(name),
			)
		}

		var loadPrivPromise = loadKey(self.privKeyName);
		var loadPubPromise = loadKey(self.pubKeyName);

		return new Promise((resolve, reject) => {
			Promise.all([loadPrivPromise, loadPubPromise]).then((keys) => {
				self.privateKey = keys[0];
				self.publicKey = keys[1];
				resolve(true);
			}).catch((e) => reject(e));
		});
	};

	loadKeysOrGenerateAndSave() {

		let self = this;

		return new Promise((resolve, reject) => {

			if (!self.hasKeySaved(self.privKeyName) || !self.hasKeySaved(self.pubKeyName)) {

				return self.generateKeys().then((keyPair) => {

					self.privateKey = keyPair.privateKey;
					self.publicKey = keyPair.publicKey;

					return self.saveKeys().then(() => {
						return publicKeyToPEM(self.publicKey).then((pem) => {
							self.publicKeyPem = pem;
							resolve()
						});
					})

				})
			} else {

				return self.loadKeys().then(() => {
					return publicKeyToPEM(self.publicKey).then((pem) => {
						self.publicKeyPem = pem;
						resolve()
					});
				})
			}
		});
	}
}

class SignerClass extends CryptoBaseClass {

	constructor(name) {
		super(name + ".signer", {
			name: "RSASSA-PKCS1-v1_5",
			hash: "SHA-256",
			modulusLength: 2048,
		}, ["sign", "verify"]);
	}

	generateKeys() {
		return window.crypto.subtle.generateKey(
			Object.assign(this.algorithm, {
				publicExponent: new Uint8Array([1, 0, 1])
			}),
			true,
			["sign", "verify"]
		)
	}

	getUsesByName(name) {
		return (name.endsWith(".priv")) ? ["sign"] : ["verify"];
	}

	signData(data) {

		if (!this.privateKey) {
			throw "Can't sign because private key is not loaded";
		}

		//let enc = new TextEncoder();
		//let encoded = enc.encode(message);

		return window.crypto.subtle.sign(
			this.algorithm,
			this.privateKey,
			data,
		);
	}

	verifyMessage(publicKey, signature, message) {
		return crypto.subtle.verify(
			this.algorithm,
			publicKey,
			signature,
			message,
		)
	}

}

class RSAEncrypterClass extends CryptoBaseClass {
	constructor(name) {
		super(name + ".encrypter", {
			name: "RSA-OAEP",
			hash: "SHA-256",
			modulusLength: 2048
		}, ["encrypt", "decrypt"]);
	}

	getUsesByName(name) {
		return (name.endsWith(".priv")) ? ["decrypt"] : ["encrypt"];
	}

	generateKeys() {
		return window.crypto.subtle.generateKey({
				name: "RSA-OAEP",
				// Consider using a 4096-bit key for systems that require long-term security
				modulusLength: 2048,
				publicExponent: new Uint8Array([1, 0, 1]),
				hash: "SHA-256",
			},
			true,
			["encrypt", "decrypt"]
		)
	}

	encryptMessage(message) {

		let enc = new TextEncoder();
		let encoded = enc.encode(message);

		return window.crypto.subtle.encrypt({
				name: "RSA-OAEP"
			},
			this.publicKey,
			encoded,
		);
	}

	decryptMessage(ciphertext) {
		let self = this;

		return window.crypto.subtle.decrypt({
				name: "RSA-OAEP"
			},
			self.privateKey,
			ciphertext,
		);
	}
}


class EcdhCryptoClass extends CryptoBaseClass {
	constructor(name) {
		super(
			name + ".ECDH", {
				name: "ECDH",
				namedCurve: "P-256",
			},
			["deriveKey", "deriveBits"]
		);
		this.encryptionAlgorithm = {
			name: 'AES-GCM',
			length: 256,
		}
		this.bits = null;
	}

	getUsesByName(name) {
		return (name.endsWith(".priv")) ? ["deriveKey", "deriveBits"] : [];
	}

	deriveBits(forgeinPublicKey) {

		if (!forgeinPublicKey) {
			throw "Cant derive bits because public key is not set";
		}

		let self = this;


		return new Promise((resolve, reject) => {
			return window.crypto.subtle.deriveBits(

				Object.assign(this.algorithm, {
					public: forgeinPublicKey
				}),
				self.privateKey,
				256,
			).then((bits) => {
				self.bits = bits;
				resolve(bits);
			})
		});
	}

	deriveSharedKey(forgeinPublicKey) {
		var self = this;
		return new Promise((resolve, reject) => {
			return window.crypto.subtle.deriveKey(
				Object.assign(this.algorithm, {
					public: forgeinPublicKey
				}),
				self.privateKey,
				self.encryptionAlgorithm,
				false,
				["encrypt", "decrypt"]
			).then((sharedKey) => {
				resolve(sharedKey);
			});
		});
	}
}

class CryptoProfile {

	constructor(name) {
		this.name = name;
		this.rsa = new RSAEncrypterClass(name);
		this.ecdh = new EcdhCryptoClass(name);
		this.signer = new SignerClass(name);

		this.id = null;
		this.created_at = null;
	}

	loadKeys() {
		return Promise.all([
			this.rsa.loadKeysOrGenerateAndSave(),
			this.ecdh.loadKeysOrGenerateAndSave(),
			this.signer.loadKeysOrGenerateAndSave(),
		]);
	}

	static import()
	{
		return new Promise((resolve, reject) => {
			const fileSelect = document.createElement("input");
			fileSelect.type = "file";
			fileSelect.accept = "text/json";
			fileSelect.onchange = ((e) => {
				const file = e.target.files[0];

				if (!file) {
					return resolve(null);
				}

				const reader = new FileReader();
				reader.readAsText(file);
				reader.onload = () => {
					const obj = JSON.parse(reader.result);
					const profile = Object.assign(new CryptoProfile(), obj);
					return resolve(profile);
				}
				reader.onerror = (e) => {
					console.error("Failed to read imported profile");
					console.error(e);
					return resolve(null);
				}
			});
			fileSelect.click();
		})
	}

	static async export(profile)
	{
		//const jsoned = JSON.stringify(profile);

	
		//const keys = profile;
		const keys = {
			rsa: await window.crypto.subtle.exportKey("spki", profile.rsa.privateKey),
			ecdh: await window.crypto.subtle.exportKey("spki", profile.ecdh.privateKey),
			signer: await window.crypto.subtle.exportKey("spki", profile.signer.privateKey),
		}

		const blob = new Blob([JSON.stringify(keys)], {
			type: "octet/stream"
		});
		const url = window.URL.createObjectURL(blob);

		const link = document.createElement("a"); // Or maybe get it from the current document
		link.href = url;
		link.download = "profile.json";
		link.click();

		window.URL.revokeObjectURL(url);
	}

	get publicRSA() {
		return this.rsa.publicKey;
	}

	get publicEcdh() {
		return this.ecdh.publicKey;
	}

	get publicSigner() {
		return this.signer.publicKey;
	}

	getPEMs() {
		return {
			rsa: this.rsa.publicKeyPem,
			ecdh: this.ecdh.publicKeyPem,
			signer: this.signer.publicKeyPem,
		}
	}

	exportAsChip() {

	}

	deriveKey(publicKey) {
		return this.ecdh.deriveSharedKey(publicKey);
	}

	async encryptSharedData(sharedKey, data) {

		let iv = window.crypto.getRandomValues(new Uint8Array(12));

		/*
		const chiper = await window.crypto.subtle.encrypt({
				name: "AES-GCM",
				iv: iv,
				tagLength: 128
			},
			sharedKey,
			data,
		);

		return {
			chipertext: chiper,
			iv: iv,
		};*/



		return new Promise((resolve, reject) => {
			return window.crypto.subtle.encrypt({
						name: "AES-GCM",
						iv: iv,
						tagLength: 128
					},
					sharedKey,
					data,
				)
				.then((encrypted) => {
					resolve({
						ciphertext: encrypted,
						iv: iv,
					});
				}).catch((e) => reject(e));
		});
	}

	decryptSharedMessage(sharedKey, chipertext, iv) {
		return new Promise((resolve, reject) => {
			return window.crypto.subtle.decrypt({
					name: "AES-GCM",
					iv: iv,
					tagLength: 128
				},
				sharedKey,
				chipertext,
			).then((res) => resolve(res)
			).catch((e) => reject(e));
		});
	}

	decrypt(chipertext) {
		return this.rsa.decryptMessage(chipertext);
	}

	signData(data) {
		return this.signer.signData(data);
	}

	verifyMessage(publicKey, signature, message) {
		return this.signer.verifyMessage(publicKey, signature, message);
	}
}

async function verifyData(publicKey, signature, chiper) {
	return crypto.subtle.verify({
			name: "RSASSA-PKCS1-v1_5",
			hash: "SHA-256",
			modulusLength: 2048,
		},
		publicKey,
		signature,
		chiper,
	);
}



function P256EcdhPubPemToRaw(pem) {

	let pemContent = getBase64ContentFromPublicPem(pem);
	let arr = Uint8Array.from(atob(pemContent), c => c.charCodeAt(0));

	/*
		Depending on browser(?) on which pem was generated on it seems
		to have some meta(?) data before raw key which seems always to be 65 bytes.
	*/
	return arr.slice(arr.length - 65);
}

function loadPublicKeyFromRaw(raw, algorithm, uses) {
	return window.crypto.subtle.importKey(
		"raw",
		raw,
		algorithm,
		true,
		uses
	);
}


function loadPublicKeyFromPEM(pem, algorithm, uses) {

	const pemHeader = "-----BEGIN PUBLIC KEY-----";
	const pemFooter = "-----END PUBLIC KEY-----";
	const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);
	// base64 decode the string to get the binary data
	const binaryDerString = window.atob(pemContents);
	// convert from a binary string to an ArrayBuffer
	const binaryDer = str2ab(binaryDerString);

	return window.crypto.subtle.importKey(
		"spki",
		binaryDer,
		algorithm,
		true,
		uses
	);
}

function publicKeyToRaw(key) {
	return new Promise((resolve, reject) => {
		return window.crypto.subtle.exportKey(
			"raw",
			key,
		).then((rawd) => {
			resolve(rawd);
		})
	});
}

function publicKeyToPEM(key) {
	return new Promise((resolve, reject) => {
		return window.crypto.subtle.exportKey(
			"spki",
			key,
		).then((exported) => {
			const exportedAsString = ab2str(exported);
			const exportedAsBase64 = window.btoa(exportedAsString);
			const pem = `-----BEGIN PUBLIC KEY-----\n${exportedAsBase64}\n-----END PUBLIC KEY-----`;

			resolve(pem);
		})
	});
}


function getBase64ContentFromPublicPem(pem) {
	return pem
		.replace("-----BEGIN PUBLIC KEY-----", "")
		.replace("-----END PUBLIC KEY-----", "")
		.replace(/\s/g, '')
	//.trim();
}

async function getFingerPrintFromString(str) {

	const encoder = new TextEncoder();
	const data = encoder.encode(str);


	const digestValue = await window.crypto.subtle.digest('SHA-256', data);
	return hex2string(digestValue).replace(/(.{2})(?=.)/g, '$1:');
}

function getFingerprintFromPem(pem) {

	pem = getBase64ContentFromPublicPem(pem);
	return getFingerPrintFromString(pem);
}


async function encryptUsingRSA(key, data) {
	return await window.crypto.subtle.encrypt({
			name: "RSA-OAEP",
		},
		key,
		data,
	);
}
