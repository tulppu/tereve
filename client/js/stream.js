
const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
return;

const frameCount = audioCtx.sampleRate * 2.0;
const sampleRate = audioCtx.sampleRate


//window.addEventListener('load', () => {
$("#chat-container").on("chatCreated", async (e, _chat) => {

	const audioMime = 'audio/webm;codecs=opus';

	const streamSettings = {
		audioBitsPerSecond: 16000,
		mimeType: audioMime,
		sampleRate: sampleRate,
		frameCount: frameCount,
};

	let  mediaRecorder = null;

	//window.AudioContext = window.AudioContext || window.webkitAudioContext;
	//var context = new AudioContext();

	async function startAudioRecord(onDataAvaiable) {

		if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
			alert('getUserMedia not supported on your browser!');
			return;
		}

		try {
			const stream = await navigator.mediaDevices.getUserMedia({
				audio: true,
				video: false,
			});

			mediaRecorder = new MediaRecorder(stream, streamSettings);
			mediaRecorder.start(254);

			mediaRecorder.ondataavailable = onDataAvaiable;

			mediaRecorder.addEventListener("stop", function (e) {
				stream.getTracks().forEach((track) => track.stop());
			});

			const track = stream.getAudioTracks()[0];
			track.sampleRate = sampleRate;
			track.frameCount = frameCount;


			track.applyConstraints({
				sampleRate: sampleRate,
				frameRate: frameCount,
				//sampleSize: 8,
				channelCount: 1,
				//latency: 0.1,

				autoGainControl: true,
				noiseSuppression: true,
				echoCancellation: true,
			});

			return mediaRecorder;
		} catch (err) {
			console.error("Recording failed");
			console.error(err);
		}
	}

	//const audioElement = document.getElementById("stream");

	const audioElement = document.createElement("audio");
	audioElement.id = "stream";
	audioElement.style.position = "absolute";
	audioElement.style.top = "5px";
	audioElement.style.right = "5px";
	audioElement.autoplay = true;

	document.getElementById("chat-page-posts").appendChild(audioElement);

	audioElement.addEventListener("canplay", () =>  {
		audioElement.play();
	}, {once: true});

	//audioElement.src = "http://localhost/api/stream";
	
	const mediaSource = new MediaSource();
	let sourceBuffer = null;

	audioElement.src = URL.createObjectURL(mediaSource);
	URL.revokeObjectURL(mediaSource.src);

	mediaSource.addEventListener("sourceopen", function (e) {


		const mediaSource = e.target;

		sourceBuffer = mediaSource.addSourceBuffer(audioMime);
		if (sourceBuffer.mode == "segments") {
			sourceBuffer.mode = "sequence";
		}
	});

	_chat.eventEmitter.addListener("stream", (chunk, __chat) => {

		if (!sourceBuffer) return;

		if (sourceBuffer.updating) {
			return;
		}

		sourceBuffer.appendBuffer((chunk));

	})

	const syncDelay = 0.05;
	setInterval(() => {
		if (!audioElement) return;

		const timeRangesObject = audioElement.seekable;
		if (!timeRangesObject.length) return;

		const end = timeRangesObject.end(0);

		if (end && end - syncDelay> 0) audioElement.currentTime = end - syncDelay
	}, 2 * 1250)




	const nappulaNauhoita = document.createElement("button");
	nappulaNauhoita.innerText = "Nauhoita"
	nappulaNauhoita.style.position = "absolute";
	nappulaNauhoita.style.top = "25px";
	nappulaNauhoita.style.right = "5px";
	
	document.getElementById("chat-page-posts").appendChild(nappulaNauhoita);

	nappulaNauhoita.addEventListener("click", () => {
		if (mediaRecorder && mediaRecorder.state === "recording") {
			mediaRecorder.stop();
			nappulaNauhoita.innerText = "Nauhoita"
		} else {
			nauhoita();
		}
	})


	
	async function nauhoita() {

		nappulaNauhoita.innerText = "Lopeta"

		const buffer = new AudioBuffer({
			numberOfChannels: 1,
			length: frameCount,
			sampleRate: sampleRate,
		});

		//const source = audioCtx.createBufferSource();
		mediaRecorder = await startAudioRecord(async function onDataAvaiable(recordEvent) {

			if (!recordEvent.data.size) return;
	
			const chunk = await recordEvent.data.arrayBuffer();


			if (!chunk.byteLength) {
				return;
			}

			var audioBuffer = audioCtx.createBuffer(1, chunk.byteLength, sampleRate);
			audioBuffer.getChannelData(0).set(chunk);

			var source = audioCtx.createBufferSource();
			source.buffer = audioBuffer;
			source.connect(audioCtx.destination);
			source.start();
			return source;
		}, true);

		return;

	}


});
