const viewPort = {};
viewPort._width = document.documentElement.clientWidth, window.innerWidth || 0;
viewPort._height = document.documentElement.clientHeight, window.innerHeight || 0;

viewPort.width = () => viewPort._width;
viewPort.height = () => viewPort._height;

window.addEventListener("resize", () => {
    viewPort._width = document.documentElement.clientWidth, window.innerWidth || 0;
    viewPort._height = document.documentElement.clientHeight, window.innerHeight || 0;
});


/*
Convert a string into an ArrayBuffer
from https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
*/
function str2ab(str) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function buf2hex(buffer) {
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

function hex2buf(str) {
    if (!str) {
        return new Uint8Array();
    }

    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }

    return new Uint8Array(a);
}

function hex2arrbuf(hex) {
    return new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
    }))
}

function hex_to_ascii(str1) {
    var hex = str1.toString();
    var str = '';
    for (var n = 0; n < hex.length; n += 2) {
        str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
}

function hex2string(buffer) {
    const byteArray = new Uint8Array(buffer);

    const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16);
        const paddedHexCode = hexCode.padStart(2, '0');
        return paddedHexCode;
    });

    return hexCodes.join('');
}


function arrayByfferToBase64(bufferArray) {
    return btoa([].reduce.call(new Uint8Array(bufferArray), function (p, c) {
        return p + String.fromCharCode(c)
    }, ''));
}

function base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

var BASE64_MARKER = ';base64,';

function convertDataURIToBinary(dataURI) {

    var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    var base64 = dataURI.substring(base64Index);
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for (i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
}


let _selectedMessageText = null;

function getSelectedTextBeforeClick() {
    return _selectedMessageText;
}

/*
$(document).on("mousedown", (e) => {
    _selectedMessageText = document.getSelection().toString();
});
*/
document.addEventListener("mousedown", () => {
    _selectedMessageText = document.getSelection().toString();
});

function humanFileSize(bytes, si=true) {
    var thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(2) + ' ' + units[u];
}


function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.lengthclearGarbage;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
}


function clearGarbage() {
    var s = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";
    for (var i = 0; i < 100; i++) {
        var img = document.createElement("img");
        img.src = "dot.png"; // or the other, both mean the same here.
        img.src = s;
        document.getElementById("media-viewer").appendChild(img);
        setTimeout(function () {
            img = null;
        }, 1000);

        //var buf = new Uint8Array(254);

    }
}


// https://github.com/niklasvh/base64-arraybuffer/blob/master/lib/base64-arraybuffer.js
function b64ToArrayBuffer(base64) {

    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    // Use a lookup table to find the index.
    var lookup = new Uint8Array(256);
    for (var i = 0; i < chars.length; i++) {
        lookup[chars.charCodeAt(i)] = i;
    }


    var bufferLength = base64.length * 0.75,
        len = base64.length,
        i, p = 0,
        encoded1, encoded2, encoded3, encoded4;

    if (base64[base64.length - 1] === "=") {
        bufferLength--;
        if (base64[base64.length - 2] === "=") {
            bufferLength--;
        }
    }

    var arraybuffer = new ArrayBuffer(bufferLength),
        bytes = new Uint8Array(arraybuffer);

    for (i = 0; i < len; i += 4) {
        encoded1 = lookup[base64.charCodeAt(i)];
        encoded2 = lookup[base64.charCodeAt(i + 1)];
        encoded3 = lookup[base64.charCodeAt(i + 2)];
        encoded4 = lookup[base64.charCodeAt(i + 3)];

        bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
        bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
        bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
    }

    return arraybuffer;
}


function hasMouse()
{
    return matchMedia('(pointer:fine)').matches
}


function isMobile()
{
    const cssIsMobile = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--mobile'));
    return cssIsMobile === 1 || (/Mobi|Android/i.test(navigator.userAgent));
}



// https://stackoverflow.com/a/29387295
const typeSizes = {
    "undefined": () => 0,
    "boolean": () => 4,
    "number": () => 8,
    "string": item => 2 * item.length,
    "object": item => !item ? 0 : Object
        .keys(item)
        .reduce((total, key) => sizeOf(key) + sizeOf(item[key]) + total, 0)
};


// https://stackoverflow.com/a/38935990
function dataURLtoU8Arr(dataurl) {
    var arr = dataurl.split(','),
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return u8arr;
}


// https://stackoverflow.com/a/57272491
const fileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});


function buildDocumentFromHtmlString(htmlString) {
    const fragment = document.createRange().createContextualFragment(htmlString.trim());
    return fragment.firstChild;
}

async function waiterFunc(waitTime = 128) {
    return new Promise((resolve, reject) => {
        window.requestAnimationFrame(() => {
            setTimeout(() => resolve(), waitTime);
        });
    })
}



function convertBlobTo(imageBlob, params = {})
{

    const destType = params.type || 'image/jpeg';

    return new Promise((resolve, reject) => {
        let canvas = document.createElement("canvas");

        const context = canvas.getContext("2d"); 
        context.globalAlpha = params.aplha || 1;

        const img = new Image();

        img.addEventListener("error", (err) => reject(err));
        img.src = imageBlob;


        img.addEventListener("load", (e) => {
            
            canvas.width = params.width || img.naturalWidth;
            canvas.height = params.height || img.naturalHeight;

            context.drawImage(img, 0, 0);

            /*
            const blob = dataURItoBlob(
                canvas.toDataURL(destType),
                params.quality || 0.75
            );*/
            canvas.toBlob(((blob) => {
                canvas = null;

                let destFileName = params.name || "compressed.jpg";

                if (params.ext && !destFileName.endsWith(params.ext)) {
                    destFileName += params.ext;
                }

                const destImg = new File([blob], destFileName, {
                    type: destType,
                });
        
                return resolve(destImg);
    
            }), destType, params.quality);

            
        });
    });
}

function getScadelWidthAndHeight(width, height) {

    const MAX_WIDTH = 125;
    const MAX_HEIGHT = 100;

    const wRatio = MAX_WIDTH / width;
    const hRatio = MAX_HEIGHT / height;

    const scaleRatio = Math.min(hRatio, wRatio);

    return {
        width: Math.floor(width * scaleRatio),
        height: Math.floor(height * scaleRatio),
    };
}

const readFileToBlob = (async(file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = (function () {
            return resolve(reader.result);
        });
        reader.readAsDataURL(file);
    });
});

function getImageThumbnail(imageSource, callback, blur = false) {

    let img = new Image();

    img.onload = () => {
        const scaledSizes = getScadelWidthAndHeight(img.width, img.height);

        const width = scaledSizes.width;
        const height = scaledSizes.height;

        const canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext('2d');

        if (blur) ctx.filter = 'blur(10px)';

        ctx.drawImage(img, 0, 0, width, height);

        canvas.toBlob(((blob) => {
            return callback(URL.createObjectURL(blob));
        }), "image/webp", 0.65);

    };
    img.src = imageSource;
}



// https://codepen.io/aertmann/pen/mAVaPx
function getVideoThumbnail(videoSource, callback, blur = 0) {
    let video = document.createElement('video');

    function clearVideo() {
        video.removeEventListener('timeupdate', timeupdate);
        video.pause();
        video.src = '';
        video.load();
        video.remove();
        video = null;
    }

    var timeupdate = function () {
        if (snapImage()) {
            clearVideo();
        }
    };

    video.addEventListener('loadeddata', function () {
        if (snapImage()) {
            clearVideo();
        }
    });

    var snapImage = function () {
        var canvas = document.createElement('canvas');

        const scaledSizes = getScadelWidthAndHeight(video.videoWidth, video.videoHeight);

        canvas.width = scaledSizes.width;
        canvas.height = scaledSizes.height;

        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
        if (blur) canvas.filter = `blur(${blur}px)`;

        var image = canvas.toDataURL("image/jpeg", 0.7);
        var success = image.length > 1000 && image.length < 10000;

        if (success) {
            callback(image);
            /*
              var img = document.createElement('img');
              img.src = image;
              document.getElementsByTagName('div')[0].appendChild(img);
            */
            //URL.revokeObjectURL(url);
        }

        return success;
    };

    video.addEventListener('timeupdate', timeupdate);
    video.preload = 'metadata';
    video.src = videoSource;
    // Load video in Safari / IE11
    video.muted = true;
    video.playsInline = true;
    video.play();
}




function removeNodes(nodes) {
    nodes.forEach((n) => {
        n.parentNode.removeChild(n);
    });
}
