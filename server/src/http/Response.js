class Response {
	
	constructor(conn) {
		this.reponseCode = 200;
		this.responseContent = null;
		this.conn = conn;
		this.sent = false;
		this.contentType = "application/json";
	}

	setResponse(obj) {
		this.responseContent = obj;
	}

	setResponseCode(code) {
		this.reponseCode = code;
	}

	setContentType(type) {
		this.contentType = type;
	}

	getContentType() {
		return this.contentType;
	}

	send() {

		if (this.sent) {
			console.info("Already sent response");
			return;
		}

		/*
		this.conn.writeHead(this.reponseCode, {'content-type': 'application/json'});
		this.conn.end((this.responseContent instanceof String)
				? this.responseContent
				: JSON.stringify(this.responseContent));
		*/
		
		const responseIsString = this.responseContent instanceof String;
		//const contentType = (responseIsString) ? "application/json";

		//if (this.conn.writableFinished) {
			this.conn.writeHead(this.reponseCode, { 'content-type': this.contentType });
			this.conn.end(
				(responseIsString) ? this.responseContent: JSON.stringify(this.responseContent)
			);
		//} else {
		//	console.info("Response can't be written anymore!");
		//}
		

		this.sent = true;
	}

	senfIfNotSent() {
		if (!this.sent) {
			return this.send();
		}
	}

	responesSuccess(successMsg) {
		this.setResponseCode(200);
		this.setResponse({
			success: true,
			message: successMsg,
		});
		console.log(successMsg);
		this.send();
	}

	
	responseFailure(failureMsg) {
		this.setResponseCode(400);
		this.setResponse({
			success: false,
			message: failureMsg,
		});
		console.log(failureMsg);
		this.send();
	}
}

module.exports = Response;