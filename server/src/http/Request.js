const {
	InvalidRequestException,
	TereveBaseException
} = require(global.srcRoot + "/main.js").exceptions;

const queryString = require("querystring");

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}


class InvalidTypeException extends TereveBaseException {
	constructor(gotType, requiredType, field) {
		const desc = `Got type '${gotType}' when required '${requiredType}' on field '${field}'`;
		super("Invalid field type.", desc, 'ERR_INVALID_FIELD_TYPE');
	}
}

class MissingFieldException extends TereveBaseException {
	constructor(missingField) {
		const desc = `Field '${missingField}' is missing`;
		super("Missing field.", desc, 'ERR_MISSING_FIELD');
	}
}



class Request {

	constructor(fields, files, ip_address, rawRequest) {
		this.fields = fields;
		this.files = files;
		this.ip_address = ip_address;
		this.rawRequest = rawRequest;
		this.qs = null;
	}

	getQueryParams() {
		if (this.qs) {
			return this.qs;
		}
		const qsStart = this.rawRequest.url.indexOf("?");
		if (qsStart < 0) return null;
		this.qs = queryString.parse(this.rawRequest.url.substring(qsStart + 1));
		return this.qs;
	}

	getAQueryParam(parameter) {
		const qsData = this.getQueryParams();
		return (qsData) ? qsData[parameter] : null;
	}

	hasFile() {
		return !!this.files.file;
	}

	getFile() {
		return this.files.file;
	}

	getFileType() {
		return this.files.file.type;
	}

	getClientAddress() {
		return this.ip_address;
	}

	has(key) {
		return !!this.fields[key];
	}

	get(key, defaultValue = null, maxLenght = 0) {

		const res = this.fields[key] || defaultValue;

		return res;
	}

	thowIfHasnt(key) {
		if (
			!this.has(key) ||
			!this.get(key)
		) {
			throw new MissingFieldException(key);
		}
	}
	

	getOrThrow(key, maxLenght = 0) {
		this.thowIfHasnt(key);
		let res = this.get(key, null, maxLenght);
		return res;
	}

	getInt(key, _default=null) {
		let res = this.get(key, _default);
		if (!res) {
			return _default;
		}
		const type = typeof res;

		if (!isNumeric(res)) {
			throw new InvalidTypeException(type, "integer", key);
		}

		res = Number(res);
		
		if (!Number.isInteger(res)) {
			throw new InvalidTypeException(type, "integer", key);
		}
		// Remove possible decimal
		return parseInt(res);
	}

	getIntOrThrow(key) {
		this.thowIfHasnt(key);
		return this.getInt(key);
	}

	getNumber(key, _default = null) {
		const res = this.get(key, _default);
		if (!res) {
			return _default;
		}
		const type = typeof res;

		if (!isNumeric(res)) {
			throw new InvalidTypeException(type, "number", key);
		}
		return Number(res);
	}

	getNumberOrThrow(key) {
		this.thowIfHasnt(key);
		return this.getNumber(key);
	}

	getString(key, _default = null, maxLenght=0) {

		const res = this.get(key, _default);

		if (res === null || typeof res === "undefined") {
			return _default;
		}

		const type = typeof res;
		if (type !== "string") {
			throw new InvalidTypeException(type, "string", key);
		}
		if (maxLenght && key.length > maxLenght) {
			throw new InvalidRequestException
			(`Length of field '${key}' is too big (${res.length}), max ${maxLenght} allowed.`);
		}

		return res;
	}

	getStringOrThrow(key, maxLenght=0) {
		this.thowIfHasnt(key);
		return this.getString(key, maxLenght);
	}

	getBool(key, _default=null) {
		const res = this.get(key, 5);
		if (res === null) {
			return null;
		}
		if (res !== "true" && res !== "false") {
			throw new InvalidTypeException(typeof res, "bool", key);
		}
		return (res === "true") ? true : false;
	}

	getBoolOrThrow(key) {
		this.thowIfHasnt(key);
		return this.getBool(key);
	}

	getJson(key, defaultValue = null, maxLenght=10000) {

		let record = this.get(key, defaultValue);

		if (!record) {
			return null;
		}

		if (record.length > maxLenght) {
			throw new InvalidRequestException
			(`Length of field '${key}' is too big (${record.length}), max ${maxLenght} allowed.`);
		}

		if (record) {
			try {
				return JSON.parse(record);
			} catch(e) {
				throw new InvalidRequestException(`Unable to parse JSON from field '${key}'`);
			}
		}
	}
}


module.exports = Request;
