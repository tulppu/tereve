
class MongoBaseExcpection extends Error {
	constructor(message = "Database error", desc=null, code=null) {
		super(message);
		this.name = this.constructor.name;
		Error.captureStackTrace(this, this.constructor);
		this.failed_action = null;
		this.message = message;
		this.desc = desc;
		this.action = null;
		this.code = code;
	}
}

class RecordNotFoundExpection extends MongoBaseExcpection {
	constructor(id, collectionString, code = null) {
		super(`Record ${id} was not found from collection '${collectionString}'`, code);
	}
}
module.exports.RecordNotFoundExpection = RecordNotFoundExpection;

class DuplicateValueExpection extends MongoBaseExcpection {
	constructor(field) {
		super("Duplicate value", field, 'DUPLICATE_FIELD');
	}
}
module.exports.DuplicateValueExpection = DuplicateValueExpection;
