const EventEmitter = require('events');
const MongoClient = require("./MongoClient.js");
const assert = require('assert').strict;
const db = global.app.DB;

const {
	RecordNotFoundExpection
} = require("./MongoExpections.js");


class MongoModel {
	constructor(id) {
		if (!id) {
			throw "Missing id for record!";
		}
		this.record = {};
		this.record.id = id;
	}

	get id() {
		return this.record.id;
	}

	set id(id) {
		this.record.id = id;
	}

	/*
	static async existsOne(id) {

	}*/
	static async queryOne(query) {
		return new Promise((resolve, reject) => {

			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.findOne((query), (err, res) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	static async exists(id) {
		return new Promise((resolve, reject) => {

			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.findOne({
				id: id
			}, (err, result) => {
				if (err) return reject(err);
				const res = !!result;
				return resolve(res);
			});
		});
	}


	static async getOne(id) {
		let self = this;

		return new Promise((resolve, reject) => {

			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.findOne({
				id: id
			}, (err, record) => {
				if (err) return reject(e);
				if (record) {
					const res =  new self(id);
					res.record = record;
					return resolve(res);
				}
				return resolve(null);
			});
		});

	}

	static async getOneOrThrow(id) {
		const res = await this.getOne(id);
		if (!res) throw new RecordNotFoundExpection(id);
		return res;
	}

	async get() {
		let self = this;
		return new Promise((resolve, reject) => {
			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());
			collection.findOne({
				id: self.id
			}, async(err, result) => {
				if (err) return reject(e);
				if (!result) return reject(new RecordNotFoundExpection(self.id, collectionString()));
				self.record = result;
				//this.eventEmitter.emit("loaded");
				return resolve(true);
			});
		});
	}

	async load() {
		return this.get();
	}

	async save() {

		if (!this.id) {
			throw new Error("Missing id!");
		}

		let self = this;
		return new Promise((resolve, reject) => {

			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.updateOne({
					id: self.id
				}, {
					$set: self.record
				}, {
					upsert: true
				},
				(err, result) => {
					if (err) {
						if (
							err.name === 'MongoError' &&
							err.code === 11000) {
							return reject(new DuplicateValueExpection());
						}
						console.error("Failed to save object");
						console.error(err);
						return reject(err);
					}
					return resolve(true);
				});
		});
	}

	async remove() {
		let self = this;
		const collection = db.collection(this.constructor.collection());
			assert.notStrictEqual(collection, null);

		return new Promise((resolve, reject) => {
			collection.removeOne({
				id: self.id
			}, (err, result) => {
				assert.equal(err, null);
				assert.equal(result.result.n, 1);
				return resolve(true);
			});
		});
	}

	static async RemoveOne(params) {
		const collection = db.collection(params.collection);
		assert.notStrictEqual(collection, null);

		return new Promise((resolve, reject) => {
			collection.removeOne(params.filter, (err, result) => {
				assert.equal(err, null);
				assert.equal(result.result.n, 1);
				return resolve(true);
			});
		});

	}

	static async insertRecord(params) {
		const collectionString = params.collection || this.constructor.collection || this.collection
		const collection = db.collection(collectionString);
		assert.notStrictEqual(collection, null);
		try {
			const res = await collection.insertOne(params.record);
			return res;
		} catch (err) {
			console.log("Failed to insert document");

			if (err) {
				console.error(err);
				if (
					err.name === 'MongoError' &&
					err.code === 11000) {
					throw new Error(new DuplicateValueExpection());
				}
				return false;
			}
		}
	}

	static async updateRecord(params) {
		const collectionString = params.collection || this.collection();
		const collection = db.collection(collectionString);
		assert.notStrictEqual(collection, null);

		//assert.equal(typeof params.record, Object);
		
		const res = await collection.updateOne(params.filter,
			{
				$set: params.record
			},
			{
				upsert: false,
			}
		);
		assert.equal(res.result.n, 1);
	}

	static getCollection() {
		const collectionString = this.constructor.collection || this.collection;
		const collection = db.collection(collectionString());

		return collection;
	}

	static async insertManyRecords(records) {

		const collectionString = this.constructor.collection || this.collection;
		const collection = db.collection(collectionString());

		if (!Array.isArray(records)) {
			throw new Error("Records is not array");
		}

		try {
			const res = await collection.insertMany(records);
			return res;
		} catch (err) {
			console.log("Failed to insert document");

			if (err) {
				console.error(err);
				if (
					err.name === 'MongoError' &&
					err.code === 11000) {
					throw new Error(new DuplicateValueExpection());
				}
				return false;
			}

		}
	}

		
	async exists() {
		const self = this;
		return new Promise((resolve, reject) => {

			const collectionString = self.constructor.collection || self.collection;

			const collection = db.collection(collectionString());

			collection.findOne({
				id: self.id
			}, (err, result) => {
				if (err) return reject(err);
				const res = !!result;
				return resolve(res);
			});
		});
	}

	static async count(query) {
		return new Promise((resolve, reject) => {

			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.count(query)
		});
	}

	static async query(params) {
		const self = this;

		return new Promise((resolve, reject) => {
			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.find(params.query).sort(params.sort || {}).toArray((err, records) => {
				if (err) return reject(err);
				
				return resolve((records.map((rec) => {
					const res = new self(rec.id);
					res.record = rec;

					//res.eventEmitter.emit("loaded");

					return res;
				})));
			});
		});
	}


	static async queryRaw(params) {
		return new Promise((resolve, reject) => {
			const collectionString = params.collection;
			const collection = db.collection(collectionString);

			collection.find(params.query).sort(params.sort || {}).toArray((err, records) => {
				if (err) return reject(err);
				return resolve(records);
			});
		});
	}

	static async has(params) {
		
		const self = this;
		const collectionString = params.collection || this.collection() || this.constructor.collection()
		const collection = db.collection(collectionString);

		console.log(collectionString);
		
		assert.notStrictEqual(collection, null);

		const res = collection.findOne(params.filter)//.limit(1).explain();
		return !!res;

	}


	static async getAll() {
		const self = this;

		return new Promise((resolve, reject) => {
			const collectionString = this.constructor.collection || this.collection;
			const collection = db.collection(collectionString());

			collection.find({}).toArray((err, records) => {
				if (err) return reject(e);
				
				return resolve((records.map((rec) => {
					const res = new self(rec.id);
					res.record = rec;

					//res.eventEmitter.emit("loaded");

					return res;
				})));
			});
		});
	}


	async getJson() {
		throw "Missing implemantation";
		/*
		return `{
			id: ${self.id}
		}`*/
	}
}
module.exports = MongoModel;