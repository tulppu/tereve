

const BufferModel = require("../MongoModel.js");
//const ChatPost = require("../../classes/ChatPost");

/*
const ChatPostBase = {
	id: String,
	post_num: Number,
	message: String,
	user: String, // Display name
	user_id: String,
	buffer_id: String,
	ip_hash: String,
	date: Date,

	replid_posts: Array, // a List of post numbers this replies to.
	replied_by: Array, // a List of post numbers that replies to this.

	geo: Object,
}*/

class ChatPostModel extends BufferModel {

	constructor(id) {
		super(id);
	}

	static collection() {
		return "Posts";
	}

}


module.exports = ChatPostModel;