

const BufferModel = require("../../database/models/BufferModel.js");
const ChatPostModel = require("../../database/models/ChatPostModel.js");
const MongoModel = require("../../database/MongoModel");
const ChatPost = require("../../classes/ChatPost");

class ChatBuffer extends BufferModel {
	constructor(id) {

		super(id);
		this.sessions = [];
		this.bans = [];
		this.posts = [];
	}

	// Directory name used mainly for attachments.
	getStorageName() {
		return (this.name || this.id).replaceAll("#", "")//.replace("")//.replaceAll("%", "");
	}

	getClientObject()
	{
		return {
			id: this.id,
			last_bump: this.lastBump,
			name: this.name,
			topic: this.topic,
			post_count: this.post_count,
			users: (this.configs.hideUsers) ? [] : this.getJoinedProfileIds(),
			posts: this.posts.slice(-16).map((it) => it.getClientObject()),
			last_post: this.posts.slice(-1).post_num,
			ops: this.ops,
			bans: this.bans,
			config: this.configs.simpleObjected,
			options: this.options,
		};
	}


	async loadPosts() {
		this.posts = (await (MongoModel.queryRaw(
		{
			query: {
				buffer_id: this.id,
			},
			sort: {
				post_num: 1,
				//date: 1,
			},
			collection: ChatPostModel.collection(),
		})) || []).map((it) => ChatPost.buildFromPlainObject(it));
	}

	async sortPosts() {
		this.posts = this.posts.sort((a, b) => {
			/*if (a.date < b.date) return -1;
			if (a.date < b.date) return -1;*/
			if (a.post_num > b.post_num) return 1;
			if (a.post_num < b.post_num) return -1;
			return 0;
		});
	}

	async addPost(post) {

		let i = 0;
		const totalPosts = this.posts.length;
		while (totalPosts - i > this.configs.backlogSize && this.configs.backlogSize > 1) { 
			const post = this.posts[1]; // take first after op

			if (!post) break;
			
			//post.deleteAttachment(this);
			//this.posts.splice(1, 1);
			await this.removePost(post);
			i++;
		}

		this.posts.push(post);
		this.sortPosts();
		//const chatPostsCollection = ChatPostModel.getCollection();
		//chatPostsCollection.update

		MongoModel.insertRecord({
			record: post,
			collection: ChatPostModel.collection(),
		})

		this.lastBump = Math.floor(Date.now() / 1000);
		await this.save();

		await global.app.variables.eventEmitter.emit("post.sent", {buffer: this, post: post})
	}

	async removePost(post, reason) {
		const num = post.post_num;
		const self = this;

		return post.deleteAttachment(this).then(async() => {
			const index = this.posts.findIndex((it) => it.post_num === num);
			if (index >= 0) {
				self.sendEvent("post.deleted", {
					post_num: num,
					reason: reason,
				});
				this.posts.splice(index, 1);
				//await this.save();
			}

			await MongoModel.RemoveOne({
				filter: {
					post_num: num,
					buffer_id: this.id,
				},
				collection: ChatPostModel.collection(),
			});
			console.info("Post deleted");

		});
	}

	getName() {
		throw "Missing implementation!";
	}


	saveHistory() {
		this.save();
	}

	loadHistory() {
		var rec = this.getRecord();
		return (rec) ? rec.posts : [];
	}

	isChannelOperator(user) {
		return this.ops.indexOf(user.id) >= 0;
	}

	hasJoined(session) {
		return !!this.sessions.find((it) => it === session);
	}

	setTopic(newTopic, setBy) {
		this.topic = newTopic;
		this.sendEvent("topic", {
			topic: this.topic,
			set_by_id: (setBy) ? setBy.id : null,
		}, setBy);
		this.save();
	}

	addOp(user_id, oppedBy = null) {

		var usr = this.sessions.find((it) => it.profile.id === user_id);
		if (!usr) {
			throw new global.app.exceptions.InvalidRequestException("User not in channel.");
		}
		this.ops.push(user_id);

		this.sendEvent("opped", {
			opped_id: user_id,
			opped_by_id: (oppedBy) ? oppedBy.id : null,
		}, oppedBy);
		this.save();
	}

	deOp(user_id, deoppedBy = null) {

		var index = this.ops.indexOf(user_id);
		if (index === -1) {
			throw new global.app.exceptions.InvalidRequestException("User is not OP");
		}
		this.ops.splice(index, 1);

		this.sendEvent("deopped", {
			deopped_id: user_id,
			deopped_by_id: (deoppedBy) ? deoppedBy.id : null,
		}, deoppedBy);
		this.save();
	}

	kick(kicked_id, kickedBy = null, reason = null) {
		const filtered = this.sessions.filter((it) => it.prpfile.id === user_id);
		if (filtered.length === this.sessions.length) {
			throw `Käyttäjä ei ole kanavalla.`;
		}
		this.sendEvent("kicked", {
			kicked_id: kicked_id,
			kicked_by_id: (kickedBy) ? kickedBy.id : null,
			reason: reason
		}, kickedBy);
		this.sessions = filtered;
		this.save();
	}

	ban(banned_id, bannedBy = null, reason = null) {
		this.sendEvent("banned", {
			banned_id: banned_id,
			banned_by_id: (bannedBy) ? bannedBy.id : null,
			reason: reason
		}, bannedBy);

		this.sessions = this.sessions.filter((it) => it.id === user_id);
		this.ops = this.ops.filter((it) => it === user_id);

		this.bans.push({
			userId: user_id,
			banned_by: banned_by,
			reason: reason,
			date: Date.now(),
		})
		return this.save();
	}


	unban(unbanned_id, unbannedBy) {
		var filtered = this.bans.filter((it) => it.user_id !== user_id);

		if (filtered.length === this.bans.length) {
			throw `Käyttäjä ${user_id} ei ole bannattu`;
		}
		this.bans = filtered;

		this.sendEvent("unbanned", {
			unbanned_id: unbanned_id,
			unbanned_by_id: (unbannedBy) ? unbannedBy.id : null
		}, unbannedBy);
		return this.save();
	}


	join(session) {
		if (this.sessions.find((it) => it.socket.id === session.socket.id)) {
			return 0;
			//throw new global.app.exceptions.ActionUnavailableException("Already joined to buffer");
		}

		if (this.bans.find((it) => it.userId === session.profile.id)) {
			throw new global.app.exceptions.UnauthorisedActionException("Banned from buffer");
		}

		// Already joined once using another session
		const hasProfileJoinedAlready = !!this.sessions.find((it) => it.profile.id  ===  session.profile.id);

		session.joinedBuffers.push(this);
		this.sessions.push(session);

		// No need to inform other clients about join
		if (hasProfileJoinedAlready) {
			return 1;
		}

		this.sendEvent("join", {
			user_id: session.profile.id,
		}, session);

		return 1;
	}


	part(session, reason = null) {

		const event = this.getEventBaseObj("part", {
			user_id: session.profile.id,
			reason: reason,
		});

		const self = this;

		this.sessions = this.sessions.filter((it) => it !== session);
		session.joinedBuffers = session.joinedBuffers.filter((buf) => buf !== self);

		// don't send part message if another session with same profile is still in channel
		if (!this.sessions.find((usr) => usr.profile.id === session.profile.id)) {
			this.sendEvent('part', event, session);
		}
	}

	sendPost(message) {
		throw "Implemention for sendPost missing!";
	}

	async sendUpdatedPost(post) {
		console.info("sendUpdatedPost impelemantation missing");
	}

	getMessage(message_id) {
		return this.posts.find((it) => it.id === message_id);
	}


    getPostIndexByNum(postNum)
	{
		if (!this.posts.length) return -1;

		let left = 0;
		let right = Math.max(this.posts.length - 1, 0);

		if (postNum < this.posts[left].post_num) return -1;
		if (postNum > this.posts[right].post_num) return -1;

		while (left <= right) {
            const middle = Math.floor(left + ((right - left) / 2));
			if (this.posts[middle].post_num === postNum) {
				return middle;
			}
			if (postNum < this.posts[middle].post_num) {
				right = middle-1;
			} else {
				left = middle+1;
			}
		}
		return -1;
	}

	getPostByNum(postNum)
	{
		const index = this.getPostIndexByNum(postNum);
		if (index === -1) return null;
		return this.posts[index];
	}


	getPostIndexFromTailByNum(postNum, maxIteration=128) {
		const len = this.posts.length;

		if (maxIteration === null || maxIteration <= 0) {
			maxIteration = len;
		}

		for (var i = len-1; i > len - maxIteration && i >= 0; i--) {
			const p = this.posts[i];
			if (p.post_num === postNum) {
				return i;
			}
		}
		return -1;
	}

	getPostFromTailByNum(postNum, maxIteration=128) {
		const i = this.getPostIndexFromTailByNum(postNum, maxIteration);

		if (i === -1) {
			return null;
		}

		return this.posts.at(i);

		/*
		const len = this.posts.length;
		for (var i = len-1; i > len - maxIteration && i >= 0; i--)
		{
			const p = this.posts[i];
			if (p.post_num === postNum) return p;
		}
		return null;
		*/
	}

	getEventBaseObj(eventName, properties = {}) {

		const event = Object.assign({}, properties);
		event.event = "buffer.channel." + eventName;
		event.buffer_id = this.id;
		event.timestamp = Date.now();

		return event;
	}

	sendEvent(eventName, obj) {

		const event = Object.assign(obj, this.getEventBaseObj(eventName));
		//event.event = "buffer.channel." + eventName;

		this.sessions.forEach((sesIter) => {
			sesIter.send(event);
		});
	}

	getJoinedProfileIds() {
		// Map to .profile.id and remove duplicates
		const sessions = [... new Set(this.sessions.map((it) => it.profile.id))];

		return sessions;
	}


	sendToSession(session, postsLimit=null) {
		const event = `{
			"id": "${this.id}",
			"event": "buffer.channel.loaded",
			"buffer": ${JSON.stringify(this.getClientObject(postsLimit))}
		}`;

		session.send(event);
	}
}


module.exports.ChatBuffer = ChatBuffer;
