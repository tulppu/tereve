const MongoModel = require("../MongoModel.js");
const ChatPost = require(global.srcRoot + "/classes/ChatPost.js");

//const BufferConfigs = require("BufferConfigs.js")
const { BufferConfigs } = require(global.srcRoot + "/database/models/BufferConfigs.js");



class BufferModel extends MongoModel {

	constructor(id) {
		super(id);
		this.configs = new BufferConfigs(id);
	}

	static async getAndLoad(id) {
		const buf = new this(id);
		if (await buf.exists(id)) {
			await buf.load();
			if (await buf.configs.exists()) {
				await buf.configs.load();
			}
			await buf.loadPosts();
		}
		return buf;
	}

	async loadConfigs() {
		if (await this.configs.exists()) {
			await this.configs.load();
		}
	}

	remove() {
		const self = this;
		this.posts.forEach((post) => self.removePost(post))
		super.remove();
	}

	get id() {
		return this.record.id;
	}

	set id(id) {
		if (!this.record) {
			this.record = {};
		}
		this.record.id = id;
	}

	get pinned() {
		return this.record.pinned;
	}

	set pinned(pinned) {
		this.record.pinned = pinned;
	}

	get hidden() {
		return this.record.hidden;
	}

	set hidden(hidden) {
		this.record.hidden = hidden;
	}

	get lastBump() {
		return this.record.lastBump || 0;
	}

	set lastBump(lastBump) {
		this.record.lastBump = lastBump;
	}

	get name() {
		return this.record.name;
	}

	set name(name) {
		this.record.name = name.toLowerCase();
	}

	get topic() {
		return this.record.topic;
	}

	set topic(topic) {
		this.record.topic = topic;
	}


	get options() {
		if (!this.record.options) {
			this.record.options = {};
		}
		return this.record.options;
	}

	set options(options) {
		this.record.options = options;
	}

	get post_count() {
		return this.record.post_count || 0;
	}

	set post_count(post_count) {
		this.record.post_count = post_count;
	}

}

module.exports = BufferModel;