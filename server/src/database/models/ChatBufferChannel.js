const ChatBuffer = require('./ChatBuffer.js').ChatBuffer;



class ChatBufferChannel extends ChatBuffer
{
	constructor(name) {

		name = name.toLowerCase();

		//const id = require('crypto').createHash('md5').update(name).digest("hex");
		const id = ChatBufferChannel.genIdFromName(name);
		super(id);

		this.record.name = name;
	}

	static collection() {
		return "ChannelBuffers";
	}

	static genIdFromName(name) {
		return require('crypto').createHash('md5').update(name).digest("hex");
	}

	get bans() {
		return this.record.bans || [];
	}

	set bans(bans) {
		this.record.bans = bans;
	}

	get ops() {
		return this.record.ops || [];
	}

	set ops(ops) {
		this.record.ops = ops;
	}

	get allowedusers() {
		return this.record.allowedUsers || [];
	}

	set allowedusers(allowedUsers) {
		this.record.allowedUsers = allowedUsers;
	}

	async sendPost(post) {
		post.sent = true;

		const clientObject = JSON.parse(JSON.stringify(post));

		clientObject.buffer_id = this.id;
		clientObject.event = "buffer.channel.post";

		this.sessions.forEach((it) => it.send(clientObject));
	}

	async sendUpdatedPost(post) {
		const clientObject = JSON.parse(JSON.stringify(post));

		clientObject.buffer_id = this.id;
		clientObject.event = "buffer.channel.post.update";

		this.sessions.forEach((it) => it.send(clientObject));
	}

	getName() {
		return this.name;
	}


}

module.exports = ChatBufferChannel;
