const getFingerPrintFromPublicPEM = require("../../utils.js").getFingerPrintFromPublicPEM;

const MongoModel = require(global.srcRoot + "/database/MongoModel.js");


const {
	promises: fs
} = require("fs");
const path = require('path');

const {
	PublicKeySigner,
	PublicKeyECDH,
	PublicKeyRSA,
} = require(global.srcRoot + "/classes/PublicKey.js");

class Login {
	constructor(success, ip, geodata) {
		this.succes = success;
		this.ip = ip;
		this.time_ISO = new Date().toISOString();
		this.geodata = geodata;
	}
}


class UserProfile extends MongoModel {

	constructor(id) {
		super(id);
	}

	static collection() {
		return "users";
	}

	addLogin(success, ip, geodata) {
		if (!this.logins) {
			this.logins = [];
		}
		if (this.logins.length > 10) {
			this.logins.shift();
		}
		this.logins.push(new Login(success, ip, geodata));
		this.save();
	}

	get logins() {
		if (!this.record.logins) {
			this.record.logins = [];
		}
		return this.record.logins;
	}

	set logins(logins) {
		this.record.logins = logins;
	}

	get id() {
		return this.record.id;
	}

	set id(id) {
		if (!this.record) {
			this.record = {};
		}
		this.record.id = id;
	}

	get alias() {
		return (this.record.alias) ? this.record.alias : "";
	}

	set alias(alias) {
		this.record.alias = alias;
	}

	get keys() {

		const keys = this.record.keys;

		return {
			signer: new PublicKeySigner(keys.signer_pem),
			rsa: new PublicKeyRSA(keys.rsa_pem),
			ecdh: new PublicKeyECDH(keys.ecdh_pem),
		};
	}

	set keys(keys) {
		this.record.keys = keys;
	}

	setAlias(alias) {
		this.alias = alias;
	}

	getClientObject() {
		return {
			id: this.id,
			alias: this.alias,
			signer_pem: this.keys.signer.pem,
			rsa_pem: this.keys.rsa.pem,
			ecdh_pem: this.keys.ecdh.pem,
		};
	}
}


async function getProfileOrCreateNew(keys) {

	if (!keys || !keys.signer.pem || !keys.ecdh.pem || !keys.rsa.pem) {
		throw new Error("Key(s) missing!");
	}

	const id = getFingerPrintFromPublicPEM(keys.signer.pem);
	let profile = new UserProfile(id);

	profile.keys = {
		signer_pem: keys.signer.pem,
		ecdh_pem: keys.ecdh.pem,
		rsa_pem: keys.rsa.pem,
	};

	if (!await profile.exists()) {
		console.info(`New profile '${id}' created`);
		profile.save();
	}

	const profilePath = `${global.app.configs.server.storage.directory}profile/${id}`;

	try {
		await fs.access(profilePath);
	} catch (e) {
		console.info("Making user directory");
		await fs.mkdir(profilePath, 0755);
	}


	const pemsJson = JSON.stringify({
		signer: keys.signer.pem,
		ecdh: keys.ecdh.pem,
		rsa: keys.rsa.pem
	}, null, '\t');

	await fs.writeFile(`${profilePath}/keys.json`, pemsJson);

	await fs.writeFile(`${profilePath}/signer.pem`, keys.signer.pem);
	await fs.writeFile(`${profilePath}/ecdh.pem`, keys.ecdh.pem);
	await fs.writeFile(`${profilePath}/rsa.pem`, keys.rsa.pem);


	return profile;
}


module.exports.UserProfile = UserProfile;
module.exports.Login = Login;
module.exports.getProfileOrCreateNew = getProfileOrCreateNew;