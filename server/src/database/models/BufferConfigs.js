const MongoModel = require(global.srcRoot + "/database/MongoModel.js");


const exceptions = global.app.exceptions;
const InvalidConfigException = exceptions.InvalidConfigException;

const globals = global.app.configs.bufferGlobals;


const validators = {
	backlogSize: (newValue, user) => {
		if (newValue <= 1) {
			throw new exceptions.InvalidConfigException(`Minium value is 1.`);
		}
		if (newValue > globals.backlogSize && !user.isAdmin()) {
			throw new exceptions.InsufficientPermissionsException(`Maxium allowed is ${globals.backlogSize}`);
		}
	}
}

module.exports.validators = validators;

class BufferConfigItem {
	constructor(name, value, type, validator = null, serverModOnly = true) {

		this.name = name;
		if (["boolean", "number", "string"].indexOf(type) === -1) {
			throw `Invalid type '${type}' for config '${name}'!`;
		}

		this.type = type;
		this.value = null;
		this.serverModOnly = serverModOnly;
		this.validator = validator;
		this.dirty = false;

		this.setValue(value);
	}

	setValue(val, dirty=false) {
		let type = typeof val;

		if (this.type === "boolean") {
			if (val === "true") {
				val = true;
			}
			if (val === "false") {
				val = false;
			}
			type = typeof val;
		}

		if (type !== this.type) {
			throw new InvalidConfigException(`Config value '${val}' has invalid type '${type}' for config ${this.name} (${this.type} required)!`);
		}

		this.value = val;
		this.dirty = dirty;
	}
}

class BufferConfigs extends MongoModel {

	constructor(id) {
		super(id);

		const dMaxFileSize = global.app.configs.attachments.maxFileSize;

		this.configs = [
			new BufferConfigItem("maxFileSize", dMaxFileSize, "number"),
			// name, value, type, validator = null, serverModOnly = true
			new BufferConfigItem("backlogSize", globals.backlogSize, "number", validators.backlogSize, false),
			new BufferConfigItem("maxMessageLenght", globals.maxMessageLenght, "number"),
			new BufferConfigItem("cooldown", globals.cooldown || 0.75, "number"),
			new BufferConfigItem("hidePosterId", globals.hidePosterId, "boolean", null, false),
			new BufferConfigItem("hidePosterNick", globals.hidePosterNick, "boolean", null, false),
			new BufferConfigItem("showGeodata", globals.showGeodata, "boolean", null, false),
			new BufferConfigItem("hideUsers", globals.hideUsers, "boolean", null, false),
			new BufferConfigItem("forcedPosterNick", globals.forcedPosterNick, "string", null, false),
		];
		this.simpleObjectise();

		return new Proxy(this, {
			get: (obj, prop) => {

				if (typeof prop !== "string") {
					return;
				}

				if (typeof this[prop] === "undefined") {
					let config = obj.configs.find((it) => it.name === prop);
		
					if (config) {
						return config.value;
					}
				}
				return obj[prop];
			},
			set: (obj, prop, value) => {
				if (typeof obj[prop] === 'undefined') {
					obj.set(prop, value);
				}
				obj[prop] = value;
				return true;
			}
		});
	}

	static collection() {
		return "BufferConfigs";
	}


	getConfig(key) {
		return this.configs.find((it) => it.name === key);
	}

	getConfigValue(name) {
		//console.log(this.configs);
		const config = this.configs.find((it) => it.name === name);
		if (!config) {
			throw new InvalidConfigException(`No such config as '${name}'!`);
		}

		this.simpleObjectise();

		return config.value;
	}

	setDefault(name, value) {
		const config = this.configs.find((it) => it.name === name);

		if (!config) {
			//return;
			throw new InvalidConfigException(`No such such config as '${name}'`)
		}
		const oldVal = config.value;
		config.setValue(value, false);


		this.simpleObjectise();

		return oldVal;
	}

	setConfig(name, value, user) {
		const config = this.configs.find((it) => it.name === name);

		if (!config) {
			//return;
			throw new InvalidConfigException(`No such such config as '${name}'`)
		}

		if (config.serverModOnly && user && !user.isAdmin()) {
			throw new global.app.exceptions.UnauthorisedActionException
				(`Only server moderators are allowed to change config '${name}'`);
		}

		if (config.validator && user) {
			config.validator(value, user);
		}

		let oldVal = config.value;

		config.setValue(value, true);
		this.simpleObjectise();

		return oldVal;
	}


	async save() {
		const id = this.record.id;
		this.record = {};
		this.record.id = id;
		let isDirty = false;
		this.configs.forEach((config) => {
			if (config.dirty) {
				this.record[config.name] = config.value;
				isDirty = true;
			}
		});

		if (!isDirty) return;

		return super.save();
	}

	async load() {
		const self = this;

		await super.load();
		const keys = Object.keys(self.record);

		keys.forEach((k) => {
			if (k === "id" || k === "_id") return;
			self.setDefault(k, self.record[k]);
		});
	}

	simpleObjectise() {
		let obj = {};
		this.configs.forEach((config) => {
			obj[config.name] = config.value;
		})
		this.simpleObjected = obj;
	}

	getSimpleObjected() {
		if (this.simpleObjected) {
			return this.simpleObjected;
		}
		this.simpleObjectise();
		return this.simpleObjected;
	}

}


//module.exports.ConfigValueValidator = ConfigValueValidator;
module.exports.BufferConfigItem = BufferConfigItem;
module.exports.BufferConfigs = BufferConfigs;
