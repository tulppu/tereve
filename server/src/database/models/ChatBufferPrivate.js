const ChatBuffer = require('./ChatBuffer.js').ChatBuffer;

class ChatPrivateBuffer extends ChatBuffer
{
	
	constructor(id) {
		super(id);
		//this.configs.set("hidePosterId", false);
		this.configs.setConfig("hidePosterId", false);

	}

	static collection() {
		return "PrivateBuffers";
	}


	get participants() {
		return this.record.participants || []
	}

	set participants(participants) {
		this.record.participants = participants;
	}

	getName() {
		return this.companion; // TODO: check this
	}



	sendPost(post) {

		post.event = "buffer.private.post";
		post.buffer_id = this.id;

		let receivers = [];
		this.participants.forEach((it) => {
			
			const usrSessions = global.app.variables.userSessions.get(it.id) || [];
			receivers = receivers.concat(usrSessions, receivers);
		});
		
		// Remove duplicates
		receivers =  [...new Set(receivers)];

		post.send(receivers);
		this.addPost(post);
	}

	getClientObject() {
		return {
			id: this.id,
			config: this.configs.simpleObjected,
			posts: this.posts,
			post_count: this.post_count,
			participants: this.participants,
		};
	}
}

module.exports = ChatPrivateBuffer;
