const MongoClient = require('mongodb').MongoClient;

const dbName = global.app.configs.server.database.name;
const url = 'mongodb://' + global.app.configs.server.database.address;


let db = null;

async function initCollections(db) {

	const channels = await db.createCollection("ChannelBuffers", {
		validator: {
			$jsonSchema: {
				required: [
					"id",
					"name"
				],
				properties: {
					name: {
						bsonType: "string",
					},
					post_num: {
						bsonType: "int",
					}
				}
			}
		}
	});
	await channels.removeMany();
	await channels.createIndex("id", {
		unique: true,
	});
	await channels.createIndex("name", {
		unique: true,
	});


	const privates = await db.createCollection("PrivateBuffers", {
		validator: {
			$jsonSchema: {
				required: [
					"id",
					"participants"
				]
			}
		}
	});
	await privates.removeMany();
	await privates.createIndex("id", {
		unique: true
	});

	const users = await db.createCollection("users", {
		validator: {
			$jsonSchema: {
				required: [
					"id",
					"keys",
					"keys.signer_pem",
					"keys.ecdh_pem",
					"keys.rsa_pem",
				],
				properties: {
					alias: {
						bsonType: "string",
					}
				}
			}
		}
	});
	await users.removeMany();
	await users.createIndex("id", {
		unique: true,
	});
	await users.createIndex("alias", {
		unique: true,
		sparse: true,
	});
	await users.createIndex("keys.signer_pem", {
		unique: true
	});
	await users.createIndex("keys.ecdh_pem", {
		unique: true
	});
	await users.createIndex("keys.rsa_pem", {
		unique: true
	});


	const bufferConfigs = await db.createCollection("bufferConfigs", {
		validator: {
			$jsonSchema: {
				required: [
					"id",
				]
			}
		}
	});
	await bufferConfigs.removeMany();



	const Posts = await db.createCollection("Posts", {
		validator: {
			$jsonSchema: {
				required: [
					"buffer_id",
					"post_num"
				]
			}
		}
	});
	await Posts.removeMany();
	await Posts.createIndex("buffer_id");
	await Posts.createIndex( { "buffer_id": 1, "post_num": 1 }, { unique: true } )
}

async function initDatabaseConnection() {
	//return new Promise((resolve, reject) => {

	const client = await MongoClient.connect(url);
	db = client.db(dbName);

	const readline = require('readline');
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});

	if (process.argv.indexOf("--reset-db") !== -1) {
		const resetDatabase = await new Promise((resolve, reject) => {
			rl.question("Reset database?.\n", (answer) => {
				return resolve(answer === "yes" || answer === 'y');
			});
		});
		if (resetDatabase) {
			await initCollections(db);
			console.info("Database initalised");
		}
	}
}

async function getDatabaseConnection() {
	if (!db) {
		await initDatabaseConnection();
	}
	return db;
}

module.exports.getDatabase = getDatabaseConnection;
module.exports.initCollections = initCollections;