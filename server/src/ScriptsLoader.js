let scripts = [];

const {
	promises: fs
} = require("fs");

async function loadScripts(path) {

	let loadedScripts =  [];

	console.log("Loading scripts from " + path);

	const files = (await fs.readdir(path)).sort();

	for (let i = 0; i < files.length; i++) {
		const file = files[i];

		const stats = await fs.lstat(path + file);
		if (stats.isDirectory()) {
			const dir = path + file + "/";
			await loadScripts(dir);
		} else if (file.endsWith(".js")) {
			const script = require("../" + path + file);
			if (typeof script.init === "function") {
				const res = await script.init();
				if (!res) {
					throw new Error(file + " didn't return 1");
				}
				loadedScripts.push(script);
			} else {
				loadedScripts.push(script);
			}
			console.log("..loaded " + file + ".");
		}

	}

	scripts = scripts.concat(loadedScripts);

	return loadedScripts;
}

module.exports.loadScripts = loadScripts;
