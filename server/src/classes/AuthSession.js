const crypto = require('crypto');

class AuthSession {
	constructor(ip_address, profile) {
		this.ipAddress = ip_address;
		this.ipHash = crypto.createHash('md5').update(ip_address).digest('hex');
		this.ipHash = 
		this.profile = profile;
		this.nick = null;
		this.socket = null;
		this.startedAt = global.currentTimestamp();
		this.joinedBuffers = [];
		this.geoData = {};
	}

	get id() {
		return (this.profile) ? this.profile.id : null;
	}

	setNick(newNick) {
		const oldNick = this.nick;
		this.nick = newNick;

		const self = this;

		const usersToBroadcast = [];

		this.joinedBuffers.forEach((buf) => {
			buf.sessions.forEach((ses) => {
				//if (usersToBroadcast.indexOf((ses)) === -1) {
					usersToBroadcast.push(ses);
				//}
			});
		});

		usersToBroadcast.forEach((usr) => {
			usr.send({
				event: "user.nickchange",
				user_id: self.id,
				session_id: self.socket.id,
				old_nick: oldNick,
				new_nick: self.nick,
			});
		});
	}

	send(buf) {

		if (!this.socket) {
			console.error("Socket not opened");
			return;
			//throw new Error("Socket not opened");
		}

		try {
			if (this.socket.readyState == this.socket.OPEN) {
				var buf = (typeof buf !== "string") ? JSON.stringify(buf) : buf;
				this.socket.send(buf);
			} else {
				this.socket.close();
			}
		} catch (e) {
			console.error("Failed to send: " + e);
		}
	}

	isAdmin() {
		return global.app.configs.admins.indexOf(this.id) !== -1;
	}
}

module.exports = AuthSession;
