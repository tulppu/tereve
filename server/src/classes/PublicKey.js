const crypto = require("crypto");


class PublicKey {
	constructor(pem) {
		this.pem = pem;
		this.key = Buffer.from(pem, 'ascii');
	}
}


module.exports = {

	PublicKeySigner: class PublicKeySigner extends PublicKey {
	
		verify(signature, toBeVerified) {
	
			const verifier =  crypto.createVerify('SHA256', toBeVerified);
			verifier.update(toBeVerified);
		
			return verifier.verify(this.key, signature);
		
		}
	},
	PublicKeyECDH: class PublicKeyECDH extends PublicKey {},
	PublicKeyRSA: class PublicKeyRSA extends PublicKey {},
}

