
const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

const { spawn } = require('child_process');
const ChatPostModel = require("../database/models/ChatPostModel");
const MongoModel = require('../database/MongoModel');

const clientFields = new Set([
    "id",
    "post_num",
    "message",
    "buffer_id",
    "date",
    "replied_posts",
    "replied_by",
    "post_num",
    "user_id",
    "user",
    "attachment",
    "options",
]);



class ChatPost {

    static staticField;

    constructor(message, session) {
        
        this.id = Math.random().toString(26).slice(2);

        // if encrypted true this should be in hex format sent by client
        this.message = message;
        //this.sent = false;

        if (session) {
            this.user = session.nick;
            this.user_id = session.profile.id;
        }

        this.date = new Date().toISOString();

        this.replied_posts = [];
        //this.replied_users = [];

        this.replied_by = [];

        this.post_num = 0;
        this.buffer_id = null;

        this.attachment = null;
        this.options = {
            //encrypted: false,
            //message_iv: null, // iv in hex format sent by client

            //attachment_data_iv: null,
            //attachment_name_iv: null,

            //signature: null
        };
        //Object.seal(this);
        this.clientJson = null;
    }

    getClientJsonString() {
        if (this.clientJson) {
            return this.clientJson;
        }
        const obj = this.getClientObject();
        this.clientJson = JSON.stringify(obj);
        return this.clientJson;
    }

    invalidateClietJsonString() {
        this.clientJson = null;
    }


    getClientObject() {
        //let res = {}
        const res = Object.assign({}, this);
        const keys = Object.keys(res);

        for (var i = 0; i < keys.length; i++) {
            const key = keys[i];
            if (!clientFields.has(key) || !res[key]) {
                delete res[key];
            }
        }
        return res;
    }


    static buildFromPlainObject(obj) {
        const post = new ChatPost();
        return Object.assign(post, obj);
    }

    async save() {
        this.invalidateClietJsonString();
        return ChatPostModel.updateRecord({
            filter: {
                id: this.id
            },
            record: this,
            //collection: ChatPostModel.collection(),
        })
    }

    send(receivers) {
        receivers.forEach((usr) => {
            usr.send(this);
        });
    }

    async deleteAttachment(buffer) { 

        const self = this;
        /*
        const index = buffer.posts.findIndex((it) => it.id === self.id);
        if (index > -1) {
            buffer.posts.splice(index, 1);
            buffer.save();
        }*/

        if (!this.attachment) {
            return;
        }

        const dir = this.attachment.dir_path.trim();
        if (!dir || !dir.trim().length) {
            console.error("No directory set for attachment in message.");
            return;
        }

        if (!dir.endsWith("/")) {
            console.error(`Invalid dir name '${dir}', wont execute rm`);
            return;
        }

        if (!this.id || !this.id.trim().length) {
            console.error("No id set for message, can't remove attachment");
            return;
        }

        const bufId = buffer.name || buffer.id;
        if (!bufId || !bufId.length) {
            console.error("No id set for buffer, can't remove attachment");
            return;
        }

        // carefull
        //const baseFileName = bufId.replace("#", "").trim() + "." + this.post_num;
        const attachmentPath = `${dir}${this.attachment.real_filename}`;

        //const cmdRmFile = `rm ${attachmentPath}`;
        //const cmdRmThumbs = `rm ${dir}thumbs/${this.id}.*`;

        //console.info(`Executing '${cmdRmFile}' and ${cmdRmThumbs}`);

        //const rmAttachment = spawn('rm', [attachmentPath]);
        //const rmThumbs = spawn('rm', [`${dir}thumbs/${this.id}.*`]);

        // https://github.com/xxorax/node-shell-escape/blob/master/shell-escape.js
        function shellescape(a) {
            var ret = [];
          
            a.forEach(function(s) {
              if (/[^A-Za-z0-9_\/:=-]/.test(s)) {
                s = "'"+s.replace(/'/g,"'\\''")+"'";
                s = s.replace(/^(?:'')+/g, '') // unduplicate single-quote at the beginning
                  .replace(/\\'''/g, "\\'" ); // remove non-escaped single-quote if there are enclosed between 2 escaped
              }
              ret.push(s);
            });
          
            return ret.join(' ');
          }

        const executeRm = ((path) => {

            path = shellescape([path]);

            console.log("Removing " + path);

            const cmdRm =  spawn('rm', [path], {shell: true});

            cmdRm.stdout.on('data', (data) => {
                console.log(`stdout: ${data}`);
              });

            cmdRm.on("close", (code) => {
                console.log(`child process exited with code ${code}`);
            });
    
            cmdRm.stderr.on('data', (data) => {
                console.log(`Removing ${path} failed: ${data}`);
            });
        });

        try {
            executeRm(attachmentPath);
            executeRm(`${dir}thumbs/${this.id}.*`);
    
            //await execPromisified(cmdRmFile);
            //await execPromisified(cmdRmThumbs);


        } catch (e) {
            console.error(e);
        }
    }
}



module.exports = ChatPost;