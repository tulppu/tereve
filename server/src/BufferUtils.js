
const ChatPrivateBuffer = require(global.srcRoot + "/database/models/ChatBufferPrivate.js");
const ChatChannelBuffer = require(global.srcRoot + "/database/models/ChatBufferChannel.js");

const ChatUserProfile = require(global.srcRoot + "/database/models/ChatUserProfile.js").UserProfile;

const { RecordNotFoundExpection } = require(global.srcRoot + "/database/MongoExpections.js");

const assert = require('assert').strict;


module.exports.executeBufferOpAction = async function (buf, func, user, args, errorCode) {

	if (!buf) {
		throw new global.app.exceptions.InvalidRequestException("No such buffer!");
	}

	if (!buf.isChannelOperator(user)) {
		throw new global.app.exceptions.UnauthorisedActionException("You are not channel moderator.");
	}

	if (!buf.hasJoined(user)) {
		throw new global.app.exceptions.UnauthorisedActionException("You haven't joined to this channel.");
	}

	return func.apply(buf, args);
};

module.exports.throwIfNotMod = function(buf, user) {
	if (!buf.isChannelOperator(user)) {
		throw new global.app.exceptions.UnauthorisedActionException("You are not channel moderator.");
	}
};

async function getPrivateBufferById(bufferId) {
	if (!bufferId || !bufferId.length) {
		throw new Error("Unset id");
	}

	const buf = global.app.variables.privateBuffers.get(bufferId) || await ChatPrivateBuffer.getOne(bufferId);
	if (!buf) return null;

	//buf.posts = buf.posts.map((it) => ChatPost.buildFromPlainObject(it));

	global.app.variables.privateBuffers.set(bufferId, buf);

	return buf;
}
module.exports.getPrivateBufferById = getPrivateBufferById;

async function getPrivateBufferByIdOrThrow(bufferId) {
	const buf = await getPrivateBufferById(bufferId);
	if (!buf) {
		throw new global.app.exceptions.ResourceNotFoundException(`Private buffer '${bufferId}' was not found.`);
	}
	return buf;
}
module.exports.getPrivateBufferByIdOrThrow = getPrivateBufferByIdOrThrow;

function getPrivateConversationId(id1, id2,) {

	const len = id1.length;

	for (let i = 0; i < len; i++) {
		if (id1.charCodeAt(i) > id2.charCodeAt(i)) {
			return `${id1}_${id2}`;
		} else if (id1.charCodeAt(i) < id2.charCodeAt(i)) {
			return `${id2}_${id1}`;
		}
	}

	return `${id1}_${id2}`;
}
module.exports.getPrivateConversationId = getPrivateConversationId;

async function getPrivateBuffer(currentUser, friendId) {

	if (!currentUser.profile) {
		throw new global.app.exceptions.InvalidRequestException("Authentication is required");
	}

	if (!friendId) {
		throw new Error("Missing friend id");
	}
	
	let friendProfile = null;
	try {
		friendProfile = await ChatUserProfile.getOneOrThrow(friendId);
	} catch (e) {
		if (e instanceof RecordNotFoundExpection) {
			throw new global.app.exceptions.InvalidRequestException(`User '${friendId}' does not exists in server.`);
		} else {
			throw e;
		}
	}

	const bufId = getPrivateConversationId(friendId, currentUser.id);
	const buf = await getPrivateBufferById(bufId) || new ChatPrivateBuffer(bufId);

	buf.participants = [
		friendProfile.getClientObject(),
		currentUser.profile.getClientObject(),
	];


	return buf;
}
module.exports.getPrivateBuffer = getPrivateBuffer;



const bufferMap = new Map();

module.exports.hasBuffer = function(buffer) {
	return !!bufferMap.has(buffer.getName());
}

module.exports.registerBuffer = async function(buffer) {
	if (module.exports.hasBuffer(buffer)) {
		throw new Error(`Buffer '${buffer.getName()}' already registered.`);
	}
	bufferMap.set(buffer.getName(), buffer);
	return true;
}

module.exports.getBufferByName = async function(name) {
	//if (module.exports.hasBuffer(buffer)) {
	//	throw new Error(`Buffer '${buffer.getName()}' already registered.`);
	//}

	//const mappedBuf = bufferMap.get(name);
	if (bufferMap.has(name)) {
		return bufferMap.get(name);
	}

	if (await ChatChannelBuffer.has({
		filter: { name: name }
	})) {
		const buf = new ChatChannelBuffer(name);
		await buf.load()
		assert.equal(name, buf.getName());
		//bufferMap.set(name, buf);

		await module.exports.registerBuffer(buf);

		return buf;
	}
}

/*
async function getBufferByName (name, session) {
	if (!name || !name.length) {
		throw new global.app.exceptions.InvalidRequestException("Buffer name is not set");
	}

	let buf = global.app.variables.channelBuffers.get(name.toLowerCase());
	if (buf) {
		return buf;
	}

	buf = new ChatChannelBuffer(name);
	if (!await buf.exists()) {
		return null;
	}

	await buf.load();
	return buf;
}
module.exports.getBufferByName = getBufferByName;
*/

async function getBufferByNameOrThrow(name) {
	const buf = await module.exports.getBufferByName(name);
	if (!buf) {
		throw new global.app.exceptions.ResourceNotFoundException(`Buffer '${name}' was not found.`);
	}

	return buf;
}
module.exports.getBufferByNameOrThrow = getBufferByNameOrThrow;

