const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);


module.exports.getPictureDimensions = async function(picturePath, animated=false)
{
    let dimensions = null;

    if (!dimensions) {
        try {
            let cmd = `identify -format "%[fx:w]x%[fx:h]" ${picturePath}`;
            if (animated) {
                cmd += "[0]";
            }

            dimensions = (await execPromisified(cmd)).stdout.split("x");
        } catch (err) { console.error(err) }
    }

    if (!dimensions) {
        try {
            const cmd = `ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 -i ${picturePath}`;
            dimensions = (await execPromisified(cmd)).stdout.split("x");
        } catch (err) { console.error(err) }
    }

    if (!dimensions) {
        return null;
    }

    return {
        w: parseInt(dimensions[0]),
        h: parseInt(dimensions[1]),
    };

}

module.exports.getVideoInfo = async function(videoPath)
{
    // newer ffmepg might fail find to rotation, for attachments rotation may be set in SetMediaInfo.js
	const cmd = `ffprobe -v error -select_streams v:0 -show_entries stream=width,height:stream_tags=rotate -of csv=p=0 -i ${videoPath}`;
    const stdout = (await execPromisified(cmd)).stdout.trim().replace("\n", ",");

    const videoInfo = stdout.split(",");

    const res = {
        w: parseInt(videoInfo[0]),
        h: parseInt(videoInfo[1]),
    };

    if (videoInfo.length > 2) {
        res["r"] = videoInfo[2];
    }

    Object.freeze(res);


    return res;
}

module.exports.mimeExtOverrides = [
    // Videos
	{
		mime:	"video/mp4",
		ext:	"mp4",
	},
	{
		mime:	"video/mpeg",
		ext:	"mpeg"
	},
    // Audios
	{
		mime:	"audio/mpeg",
		ext:	"mp3",
	},
    {
        mime:   "audio/ogg",
        ext:    "ogg",
    },
    // Images
    {
        mime:   "image/webp",
        ext:    "webp"
    },
    {
		mime: 	"image/jpeg",
		ext:	"jpg"
	},
    // Text
    {
        mime:   "text/x-c++src",
        ext:    "cc",
    },
    {
        mime:   "text/x-c++hdr",
        ext:    "hh",
    }
];


module.exports.getExtFromMime = async function(mime)
{

    const extOverride = module.exports.mimeExtOverrides.find((it) => it.mime === mime);
	if (extOverride) {
		return extOverride.ext;
	}

	// image/gif					gif
	const cmdExt 			= `cat /etc/mime.types | grep '${mime}'`;
    let extTypeResult       = null;

    try {
        extTypeResult		= (await execPromisified(cmdExt));
    } catch (e) {
        console.error("Ext dig failed");
        console.error(e);
    }


    if (!extTypeResult || extTypeResult.code === 1) {
        console.log(`No ext found for mime ${mime}`)
        return null;
    }

    const stdout = extTypeResult.stdout;
	if (!stdout || !stdout.length) {
        return null;
	}

    let ext = null;
    const splitstdOut = stdout.replaceAll("\n", "").replaceAll("\t", ' ').split(" ");

    if (splitstdOut.length > 1) {
        ext = splitstdOut.at(-1);
    } else {
        ext = mime.split("/").at(-1);
    }

	return ext;
}

module.exports.getMimeFromFile = async function(path)
{
	const cmdMime 		= `file -b --mime-type '${path}'`

    const stdoutCleaned = (await execPromisified(cmdMime)).stdout.replaceAll("\n", "");

    /*
    if (stdoutCleaned === "application/octet-stream")
    {
        const mediaInfoCmd = `ffprobe -v error -print_format json -show_format -show_streams '${path}'`;
        const ffpromeOut = await execPromisified(mediaInfoCmd);

        if (ffpromeOut && ffpromeOut.stdout && ffpromeOut.stdout) 
        {
            try {
                const jsoned = JSON.parse(omeOut.stdot);
                
            } catch (e) {
                console.error(e);
            }
        }
    }*/

	return stdoutCleaned;
}