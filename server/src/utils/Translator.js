
// https://github.com/thedaviddelta/lingva-translate


const translator = {};
translator.lingvaInstances = [
    "https://translate.jae.fi",
    "https://lingva.ml",
    "https://translate.igna.wtf",
    "https://translate.plausibility.cloud",
    "https://lingva.lunar.icu",
    "https://translate.projectsegfau.lt",
    "https://translate.dr460nf1r3.org",
    "https://lingva.garudalinux.org",
];

translator.returnModel = 
{
    translation : String,
    sourceLang  : String
}

translator.translate = async function(params = {})
{
    const source = params.source || "auto";
    const target = params.target;
    const q = params.q;
    const instance = params.instance || translator.lingvaInstances[0];


    return new Promise((resolve, reject) => {
        const url = instance + `/api/v1/${source}/${target}/${encodeURIComponent(q)}`
        //const url ="https://asdadasd";
        console.log("Using instance " + instance);
        fetch(url)
            .then((res) => {
                console.log(res);
                if (res.ok) {
                    return res.json()
                }
                return null;
            }).then((data) => {
                if (!data) return;
                const result = Object.assign(translator.returnModel, {
                    translation: data.translation,
                    sourceLang: data.detectedSource,
                    instance: instance,
                });
                console.log(result);
                return resolve(result);
            }).catch((e) => {
                console.log("err")
                console.log(e);
                //console.log(e instance Netw)
                
                const instanceIndex = translator.lingvaInstances.findIndex((it) => it === instance);
                if (instanceIndex >= 0 && !!translator.lingvaInstances[instanceIndex+1])
                {
                    console.info("Translation failed, trying next instance");
                    params.instance = translator.lingvaInstances[instanceIndex+1];
                    return translator.translate(params);
                }

                return reject(new Error("Translation error"));
            })
            
    }).then(() => console.log("Translated"));
};


module.exports.translator = translator;