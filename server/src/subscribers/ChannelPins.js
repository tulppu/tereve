const ChatBufferChannel = require(global.srcRoot + '/database/models/ChatBufferChannel.js');
const eventEmitter = global.app.variables.eventEmitter;


let pinnedChannels = [];


const bufferUtls = require("../BufferUtils");

module.exports.init = async function () {

	for (const it of global.app.configs.buffers) {

		//const buffer = new ChatBufferChannel(it.name.toLowerCase());
		const buffer = await ChatBufferChannel.getAndLoad(it.name.toLowerCase());
		//global.app.variables.channelBuffers.set(buffer.name, buffer);

		await bufferUtls.registerBuffer(buffer);

		/*
		if (await buffer.exists()) {
			await buffer.load();
		}*/

		if (it.configs) {
			Object.keys(it.configs).forEach((key) => {
				buffer.configs.setDefault(key, it.configs[key]);
			});
		}

		if (it.hidden) {
			buffer.hidden = true;
		}

		if (it.pinned && !it.hidden) {
			pinnedChannels.push(buffer);
			global.app.variables.channelBuffers.set(buffer.name, buffer);
			buffer.pinned = true;
		}


		await buffer.save();
	}


	if (global.app.configs.cyclicBuffers) {
		const buffers = (await ChatBufferChannel.query({
			hidden: {
				$ne: true,
			}
		})).sort((a, b) => {
			const stampA = a.lastBump;
			const stampB = b.lastBump;
	
			return ((stampA < stampB) ? -1 : ((stampA > stampB) ? 1 : 0));
		})
		buffers.forEach(async(it) => {
			global.app.variables.channelBuffers.set(it.name, it);
		});
	}

	return true;
};

eventEmitter.on("post.sent", async (data, next) => {

	if (!global.app.configs.cyclicBuffers) {
		return next();
	}

	const post = data.post;
	const buffer = data.buffer;

	if (post.post_num !== 1) {
		return next();
	}

	if (buffer.hidden) {
		return next();
	}

	// broadcast new buffer to all sessions
	global.app.variables.sessions.forEach((ses) => {
		buffer.join(ses);
		buffer.sendToSession(ses);
	});


	const buffers = (await ChatBufferChannel.query({
		pinned: {
			$ne: true
		}
	})).sort((a, b) => {
		const stampA = a.lastBump;
		const stampB = b.lastBump;

		return ((stampA < stampB) ? -1 : ((stampA > stampB) ? 1 : 0));
	});

	while (buffers.length > global.app.configs.maxPublicBuffers) {
		const lastBuf = buffers.shift();
		pinnedChannels = pinnedChannels.filter((it) => it.id !== lastBuf.id);
		global.app.variables.channelBuffers.delete(lastBuf.name.toLowerCase());
		await lastBuf.remove();
	}

	return next();
});

eventEmitter.on("user.authed", (data, next) => {

	const session = data.session;

	if (global.app.configs.cyclicBuffers) {
		/*global.app.variables.channelBuffers.forEach((buffer) => {
			console.log(buffer);
			if (buffer?.hidden) return;
			if (!buffer.posts.length) return;
			try {
				buffer.join(session);
			} catch (e) {
				buffer.delete();
				return;
			}
			buffer.sendToSession(session);
		})*/
	}

	pinnedChannels.forEach((buf) => {
		try {
			buf.join(session);
		} catch (e) {
			buf.delete();
			return;
		}
		buf.sendToSession(session);
	});


	return next();
});