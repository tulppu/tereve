const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

const fs = require("fs");
module.exports.validatePost = async function(params) {
	
	const post 	= params.post;
	const buffer 		= params.buffer;
	const session 		= params.session;
	const request 		= params.request;

	if (!post.attachment &&
		!post.message)
	{
		throw new global.app.exceptions.InvalidRequestException("Empty post!");
	}

	if (!post.post_num) {
		throw new global.app.exceptions.ServerErrorException("Failed to build post.");
	}

	if (post.attachment) {
		const attachment = post.attachment;
		const fPath = attachment.dir_path + attachment.real_filename;
		post.attachment.size = (await fs.promises.stat(fPath)).size
	}

	return post;
}

module.exports.init = async function()
{
	console.log("initing validator");
	global.app.variables.eventEmitter.on("post.received", (params, next) => {
		console.log("validating..");
		(async () => {
			//await module.exports.validatePost(data)
			const postValidated = await module.exports.validatePost(params);
			console.log(postValidated);
			//Object.assign(params.post, postValidated);
			console.log(params.post);
			params.post = postValidated;
			//Object.assign(params.post, postValidated);
			//console.log(data.post);
			//console.log("Post " + data.post?.post_num + " validated.", postValidated?.post?.post_num);
			console.log("..validated ", postValidated.post_num);
			return next();
		})().catch((e) => {
			console.error(e);
			return next(e);
		});
	});
	return true;
}
