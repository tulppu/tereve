const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;
	const path = attachment.dir_path + attachment.real_filename;
	const tmpPath = attachment.dir_path + "tmp." + attachment.real_filename;

	return next();

	if (!attachment.meta.mime.startsWith("image/") || attachment.meta.is_animated) {
		return next();
	}

	if (attachment.meta.mime === "image/apng") {
		return next();
	}

	try {
		await execPromisified(`convert ${path} -strip -auto-orient ${path}`);
	
		//await execPromisified(`ffmpeg -y -i ${path} ${tmpPath}`);
		//await execPromisified(`rm ${path}`);
		//await execPromisified(`mv ${tmpPath} ${path}`);

	} catch (e) {
		console.error("Failed to strip exif from image");
		console.error(e);
	}
	return next();
});