
const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;

	if (!attachment.meta.mime.startsWith("audio") && !attachment.meta.mime.startsWith("video") && !attachment.meta.is_animated) {
		return next();
	}


	const filePath = attachment.dir_path + attachment.real_filename;
	const cmd = `ffprobe -i ${filePath} -show_entries format=duration -v quiet -of csv="p=0"`;

	let duration = null;

	try {
		duration = (await execPromisified(cmd)).stdout;
	} catch (err) {
		if (err) {
			console.error("Failed to fetch duration");
			console.error(err);
			return next();
		}
	}
	try {
		if (isNaN(duration.replace(".", ","))) attachment.meta.duration = parseFloat(duration.toString()).toFixed(2);
	} catch (e) {
		console.error("Failed to parse video duration");
		console.error(e);
	}

	return next();
});