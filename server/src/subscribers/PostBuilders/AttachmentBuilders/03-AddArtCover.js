const { promisify } = require('util');
const execPromisified = promisify(require('child_process').exec);
const {
	promises: fs
} = require("fs");

const { getPictureDimensions } = require("../../../utils/Media");

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;
	const path = attachment.dir_path + attachment.real_filename;
	const message = data.message;
	const buffer = data.buffer;

	const attachmentConfigs = global.app.configs.attachments;


	if (["audio/mp3", "audio/mpeg", "audio/x-flac", "audio/ogg"].indexOf(attachment.meta.mime) === -1) {
		return next();
	}

	const coverFileName = `${message.id}.cover.jpg`;

	const endPath = "uploads/" + buffer.getStorageName() + "/thumbs/" + coverFileName;

	const writePath = global.app.configs.server.storage.directory + endPath;
	const wwwPath = global.app.configs.server.storage.http_address + endPath;

	const maxArtCoverWidth = attachmentConfigs.maxArtCoverWidth || attachmentConfigs.maxThumbWidth;
    const maxArtCoverHeight = attachmentConfigs.maxArtCoverHeight || attachmentConfigs.maxThumbHeight;


	const extractCover = `ffmpeg -i ${path} -pix_fmt rgb8  -vf "scale=220:-1" -an  ${writePath}`;

	try {
		await execPromisified(extractCover);
		attachment.meta.coverart = wwwPath;

		const coverDimensions = await getPictureDimensions(writePath);
		const downScaleRate = Math.max(1,coverDimensions.w / maxArtCoverWidth, coverDimensions.h / maxArtCoverHeight);

		if (downScaleRate > 1) {
			coverDimensions.w /= downScaleRate;
			coverDimensions.h /= downScaleRate;

			await execPromisified(`mogrify -resize ${coverDimensions.w}x${coverDimensions.h} ${writePath}`);
		}

		attachment.meta.thumbWidth = coverDimensions.w;
		attachment.meta.thumbHeight = coverDimensions.h;

	} catch (e) {
		if (e.message && e.message.includes("Output file #0 does not contain any stream")) return next();
		console.error("Failed to extract art cover from audio file");
		console.error(e);
		try {
			await fs.unlink(writePath);
		} catch (e) {
			console.error(e)
		}
	}
	return next();
});