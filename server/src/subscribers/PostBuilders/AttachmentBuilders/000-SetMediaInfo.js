const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);


const seekeds = [
    {
        name:       "filename",
        regex:      /^Filename\s+:{0,128}: (.{1,254}$)/m,
    },
    {
        name:       "title",
        regex:      /^Title\s+:{0,128}: (.{1,254}$)/m,
    },
    {
        name:       "band",
        regex:      /^Band\s+:{0,128}: (.{1,254}$)/m,
    },
    {
        name:       "artist",
        regex:       /^Artist\s+:{0,128}: (.{1,128}$)/m,
    },
    {
        name:       "album",
        regex:      /^Album\s+:{0,128}: (.{1,128}$)/m
    },
    {
        name:       "genre",
        regex:      /^Genre\s+:{0,128}:\s(.{1,128}$)/m
    },
    {
        name:       "bitrate",
        regex:      /^Audio Bitrate\s{0,128}:\s(.{1,16}$)/m,
    },
    {
        name:       "samplerate",
        regex:      /^Sample Rate\s{0,128}:\s(.{1,16}$)/m,
    },
    {
        name:       "comment",
        regex:      /^Comment\s{0,128}:\s(.{1,128}$)/m,
    },
    {
        name:       "date",
        regex:      /^Date\s{0,128}:\s(.{1,32}$)/gm,
    },
    {
        name:       "date",
        regex:       /^Recorded date\s{0,128}:\s(.{1,32}$)/m,
    },
    {
        name:       "Released",
        regex:       /^DISCOGS_RELEASED\s{0,128}:\s(.{1,32}$)/m,
    },
    {
        name:       "date",
        regex:       /^Year\s{0,128}:\s(.{1,32}$)/m,
    },
    {
        name:       "Recording_time",
        regex:      /^Recording Time\s{0,128}:\s(.{1,32}$)/m
    },
    {
        name:       "duration",
        regex:      /^Duration\s+:{0,128}: (.{1,32}$)/m,
    },
    {
        name:       "rotation",
        regex:      /^Rotation\s+:{0,128}: (.{1,8}$)/m,
    }
];

async function getExifInfo(pathInDisk) {
  const cmd = `exiftool '${pathInDisk}'`;
  try {
    const exifInfo = await execPromisified(cmd);

    const stderr = exifInfo.stderr;
    if (stderr) {
        console.error(stderr);
        return next();
    }

    const stdout = exifInfo.stdout;
    const res = {};

    for (var i = 0; i < seekeds.length; i++)
    {
        const s = seekeds[i];
        const match =  stdout.match(s.regex);

        if (match && match[1]) {
            const str = match[1].substr(0, 64).trim();
            if (!str.length) continue;
            if (parseInt(str) === 0) continue;
            res[s.name] = str;
        }
    }

    return res;

  } catch (e) {
    console.error(e);
    return false;
  }
}


global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;

    const mime = attachment.meta.mime;

	//if (!mime.startsWith("video/") && !mime.startsWith("audio/") /*&& !attachment.meta.is_animated)*/ ) {
	//	return next();
	//}

	const path = attachment.dir_path + attachment.real_filename;
    const mediaInfo = await getExifInfo(path);

    if (mediaInfo) {
       attachment.meta.mediaInfo = mediaInfo;
       if (mediaInfo.rotation && !attachment.meta.rotation)
       {
            attachment.meta.rotation = mediaInfo.rotation;
       }
    }

    return next();
});


const pdfInfoRegexes = {
    title:      /Title:\s{0,128}(.{1,512}$)/m,
    subject:    /Subject:\s{0,128}(.{1,512}$)/m,
    author:     /Author:\s{0,128}(.{1,512}$)/m,
}

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;

    const mime = attachment.meta.mime;
    if (mime !== "application/pdf") return next();

    const path = attachment.dir_path + attachment.real_filename;
    const cmd = `pdfinfo ${path}`;

    const res = await execPromisified(cmd);

    if (res.stderr) {
        console.error(res.stderr);
        return next();
    }
    const keys = Object.keys(pdfInfoRegexes);
    keys.forEach((key) => {
        const r = res.stdout.match(pdfInfoRegexes[key]);
        //console.log(r);
        //console.log(r[1]);
        if (r && r.length >= 1) {
            attachment.meta[key] = r[1].trim();
        }
    });

    /*
    attachment.meta["rotation"] && !attachment.meta.rotation {
        attachment.meta.rotation = attachment.mediaInfo
    }*/


    return next();
});