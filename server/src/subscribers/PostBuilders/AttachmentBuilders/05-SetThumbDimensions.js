
global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;


	if (!attachment.meta.width || !attachment.meta.height) {
		return next();
	}

    const attachmentConfigs = global.app.configs.attachments;
    
	const maxThumbWidth = attachmentConfigs.maxThumbWidth;
    const maxThumbHeight = attachmentConfigs.maxThumbHeight;

    const fWidth  = attachment.meta.thumbWidth = attachment.meta.width;
    const fHeight = attachment.meta.thumbHeight = attachment.meta.height;


    const downScaleRate = Math.max(fWidth / maxThumbWidth, fHeight / maxThumbHeight);
    if (downScaleRate < 1) return next();


    attachment.meta.thumbWidth /= downScaleRate;
    attachment.meta.thumbHeight /= downScaleRate;

    return next();
});