const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);



global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachmentConfigs = global.app.configs.attachments;

	const attachment = data.attachment;
	const input_path = attachment.dir_path  + attachment.real_filename;
	const message = data.message;
	const buffer = data.buffer;

	if (!attachment.meta.mime.startsWith("video")) {
		return next();
	}

	const thumbDir = `${attachment.dir_path + "thumbs"}`;

	const thumbFileBase = `${message.id}.thumb`;

	const thumbFileStatic 	= `${thumbFileBase}.static.webp`;
	const thumbFileAnimated = `${thumbFileBase}.animated.webp`;

	const wwwBase = global.app.configs.server.storage.http_address + "uploads/"
		+ buffer.getStorageName() + "/thumbs/";

	const dDimenstions = `${attachment.meta.thumbWidth}:${attachment.meta.thumbHeight}`;
	const cmdThumbStatic = `ffmpeg  -i ${input_path} -y -vframes 1 -vf scale="${dDimenstions}" ${thumbDir}/${thumbFileStatic}`;

	const cmdThumbAnimated = attachmentConfigs.videoThumbCmdAnimated
		.replace("{in}", input_path)
		.replace("{dimensions}", dDimenstions)
		.replace("{out}", `${thumbDir}/${thumbFileAnimated}`);

	try {
		await execPromisified(cmdThumbStatic);
		attachment.meta.thumb_url_static = wwwBase  + thumbFileStatic

		// big videos can use use too much ram
		if (attachment.size < 1024 * 1024 * 20) {
			try {
				await execPromisified(cmdThumbAnimated);
				attachment.meta.thumb_url_animated = wwwBase + thumbFileAnimated;
			} catch (e) {
				console.log("Failed to generate animated thubmail for " + input_path);
			}
			//attachment.meta.animated_thumb = true;
		}
	} catch(e) {
		console.error("Failed to thumbnail video");
		console.error(e);
		return next(e);
	}

	return next();
});