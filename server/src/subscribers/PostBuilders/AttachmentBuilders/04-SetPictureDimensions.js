//const util = require('util');
//const execPromisified = util.promisify(require('child_process').exec);


const { getPictureDimensions } = require("../../../utils/Media");

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;

	if (!attachment.meta.mime.startsWith("image")) {
		return next();
	}

	const attachmentFileName = attachment.real_filename;
	const path = attachment.dir_path + attachmentFileName;

	try {
		const dimensions = await getPictureDimensions(path, attachment.meta.is_animated);

		attachment.meta.width = dimensions.w;
		attachment.meta.height = dimensions.h;

	} catch (e) {
		console.error("Failed to get picture dimensions");
		console.error(e);
	}
	return next();
});