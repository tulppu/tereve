const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);


const { getPictureDimensions } = require("../../../utils/Media");

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;

	if (!attachment.meta.mime.startsWith("image")) {
		return next();
	}

    const filePath = attachment.dir_path + attachment.real_filename;
	const cmd = `mogrify -auto-orient ${filePath}`;


	try {
        await execPromisified(cmd)
	} catch (e) {
		console.error("Failed to get picture dimensions");
		console.error(e);
	}
	return next();
});