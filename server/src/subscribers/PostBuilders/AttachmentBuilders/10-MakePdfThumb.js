const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

const { getPictureDimensions } = require("../../../utils/Media");


global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachmentConfigs = global.app.configs.attachments;

	const attachment = data.attachment;
	const input_path = attachment.dir_path + attachment.real_filename;
	const message = data.message;
	const buffer = data.buffer;

    if (attachment.meta.mime !== "application/pdf") {
		return next();
	}
    
    const thumbFileName = `${message.id}.thumb.jpg`;

    const endPath =  "uploads/" + buffer.getStorageName() + "/thumbs/" + thumbFileName;

	const writePath = global.app.configs.server.storage.directory + endPath;
    const wwwPath = global.app.configs.server.storage.http_address + endPath;

    const cmdThumbPdf = `convert -colorspace rgb -strip ${input_path}[0] ${writePath}`

	const maxThumbWidth = attachmentConfigs.maxPdfThumbWidth || attachmentConfigs.maxThumbWidth;
    const maxThumbHeight = attachmentConfigs.maxPdfThumbHeight || attachmentConfigs.maxThumbHeight;

    try {
        await execPromisified(cmdThumbPdf);

        const coverDimensions = await getPictureDimensions(writePath);
		attachment.meta.thumbWidth = coverDimensions.w;
		attachment.meta.thumbHeight = coverDimensions.h;

        const downScaleRate = Math.max(attachment.meta.thumbWidth / maxThumbWidth, attachment.meta.thumbHeight / maxThumbHeight);

        if (downScaleRate > 1) {
            attachment.meta.thumbWidth /= downScaleRate;
            attachment.meta.thumbHeight /= downScaleRate;
    
            await execPromisified(`mogrify -resize ${attachment.meta.thumbWidth}x${attachment.meta.thumbHeight} ${writePath}`);
        }

        attachment.meta["thumb_url_default"] = wwwPath;
    } catch (e) {
        console.error("Failed to create thumbnail for PDF");
        console.log(e);
    }

    return next();
});