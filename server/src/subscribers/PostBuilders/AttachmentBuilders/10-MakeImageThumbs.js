const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);


global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachmentConfigs = global.app.configs.attachments;
	const thumbSizeSmall = attachmentConfigs.thumbSizeSmall;

	const attachment = data.attachment;
	const input_path = attachment.dir_path + attachment.real_filename;
	const message = data.message;
	const buffer = data.buffer;

	if (!attachment.meta.mime.startsWith("image")) {
		return next();
	}

	const baseThumbName = message.id;
	const thumbDir = attachment.dir_path + "thumbs/";

	const dimensions = `${attachment.meta.thumbWidth}:${attachment.meta.thumbHeight}`;

	const makeStaticThumb = (async (thumb_name, ext = "webp") => {

		const fileName = `${baseThumbName}.static.${ext}`;

		const outPath = thumbDir + "/" + fileName;
		const cmd = `ffmpeg -i ${input_path} -frames:v 1 -vcodec webp -vf 'scale=${dimensions}:force_original_aspect_ratio=decrease' ${outPath}`;

		await execPromisified(cmd);

		const wwwPath = global.app.configs.server.storage.http_address +
			"uploads/" + buffer.getStorageName() + "/thumbs/" + fileName;

		attachment.meta["thumb_url_" + thumb_name] = wwwPath;
	});

	const makeAnimatedThumb = (async (thumb_name) => {

		const fileName = `${baseThumbName}.animated.webp`;

		const cmd = attachmentConfigs.imageThumbCmdAnimated
			.replace("{in}", input_path)
			.replace("{dimensions}", dimensions)
			.replace("{out}", thumbDir + "/" +  fileName);

		await execPromisified(cmd);

		const www_path = global.app.configs.server.storage.http_address +
			"uploads/" + buffer.getStorageName() + "/thumbs/" + fileName;

		attachment.meta["thumb_url_" + thumb_name] = www_path;
	});

	//const isGoodGif = attachment.meta.animated_thumb;

	try {
		if (attachment.meta.is_animated /*&& attachment.meta.mime !== "image/webp"*/ ) {
			await makeAnimatedThumb("animated");
			await makeStaticThumb("static");
			attachment.meta.animated_thumb = true;
		} else {
			await makeStaticThumb("static");
		}
		await makeStaticThumb("small", ".webp", thumbSizeSmall);
	} catch (e) {
		console.error("Thumbing problem!");
		console.error(e);
	}

	return next();
});