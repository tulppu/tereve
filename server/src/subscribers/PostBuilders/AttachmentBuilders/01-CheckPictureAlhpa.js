const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);


global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {


	const attachment = data.attachment;
	const path = attachment.dir_path + attachment.real_filename;

	attachment.meta.has_alpha = false;

	const isAPNG 	=  attachment.meta.mime === "image/apng";
	const isWEBP 	= attachment.meta.mime === "image/webp";
	const isGif 	= attachment.meta.mime === "image/gif";
	const isPNG 	= attachment.meta.mime === "image/png";

	if (
		!isPNG && !isGif && !isAPNG && !isWEBP
	) { 
		return next();
	}

	if (isAPNG) {
		const cmdIsApngAnimated = `pngcheck -v ${path} |  grep -E "acTL|fcTL"`;
		const res = (await execPromisified(cmdIsApngAnimated)).stdout
		attachment.meta.is_animated = !!res.length;
	}



	if (isWEBP) {
		const cmdIsWebpAnimated = `echo $(grep -c "ANMF" ${path})`;
		const res = parseInt((await execPromisified(cmdIsWebpAnimated)).stdout)
		attachment.meta.is_animated = res !== 0;
	}

	try {
		const cmdHasAlhpa = `identify -format '%A'  ${path}`;
		const alphaOutput = (await execPromisified(cmdHasAlhpa)).stdout;

		attachment.meta.has_alpha = alphaOutput.indexOf("True") > -1 ||  alphaOutput.indexOf("Blend") > -1;

		if (!isAPNG && !isWEBP) {
			const cmdIsAnimated = `identify -format '%n' ${path}`;	
			attachment.meta.is_animated = parseInt((await execPromisified(cmdIsAnimated)).stdout) > 1;
		}
	} catch (err) {
		console.error("alpha check failed");
		console.error(err);
		return next();
	}


	attachment.meta.is_good_gif = attachment.meta.has_alpha &&
		attachment.meta.is_animated &&
		attachment.size <= 1024 * 1024;

	return next();
});