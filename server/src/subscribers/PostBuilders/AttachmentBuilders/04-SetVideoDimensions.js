/*
const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);
*/

const { getVideoInfo } = require("../../../utils/Media");

global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;
	const path = attachment.dir_path + attachment.real_filename;

	if (!attachment.meta.mime.startsWith("video")) {
		return next();
	}

	try {
		const videoInfo = await getVideoInfo(path);

		attachment.meta.width = videoInfo.w;
		attachment.meta.height = videoInfo.h;

		if (videoInfo.r) {
			attachment.meta.rotation = videoInfo.r;
		}
	} catch (e) {
		console.error("Failed to get video dimensions");
		console.error(e);
	}
	return next();
});