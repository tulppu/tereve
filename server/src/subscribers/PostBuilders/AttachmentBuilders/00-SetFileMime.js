const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

const {
	promises: fs
} = require("fs");

const mediaUtils = require("../../../utils/Media");


global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;
	const path = attachment.dir_path + attachment.real_filename;

	const request = data.request;

	const mime = attachment.meta.mime || request?.getFileType() || await mediaUtils.getMimeFromFile(path)

	if (!mime || !mime.length) {
		return next("Failed to dig mime with command " + cmdMime);
	}

	let ext = attachment.meta.ext || await mediaUtils.getExtFromMime(mime);

	if (!ext || !ext.length) {
		ext = "dat";
	}

	if (ext.includes("html")) {
		ext = "txt";
	}

	attachment.meta.ext 		= ext;
	attachment.meta.mime 		= mime;
	attachment.real_filename	+= "." + ext;
	attachment.url 				+= "." + ext;

	console.log(attachment.meta.mime, attachment.meta.ext);


	await fs.rename(path, attachment.dir_path + attachment.real_filename);

	return next();
});


// Replace video/ with audio/ if mp4 or webm media has no video track.
global.app.variables.eventEmitter.on("buildAttachment", async (data, next) => {

	const attachment = data.attachment;
	const mime = attachment.meta.mime;

	//if (mime !== "video/mp4" && mime !== "video/webm") {
	if (!mime.startsWith("video")) {
		return next();
	}

	const filePath = attachment.dir_path + attachment.real_filename;

	const cmd = `ffprobe -loglevel error -show_entries stream=codec_type -of default=nw=1 '${filePath}'`;

	const codecs = (await execPromisified(cmd)).stdout;
	if (codecs.indexOf("video") === -1) {
		attachment.meta.mime = "audio/" + mime.substr(6);
	}

	return next();
});