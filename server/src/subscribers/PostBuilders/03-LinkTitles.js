
module.exports.MAX_TITLE_LEN = 128;

module.exports.providers = {};

module.exports.providers.generic = async function(link, postFix = null)
{
	let httpRes = null;
	try {
		httpRes = await fetch(link);
	} catch (e) {
		console.error("Failed to fetch '" + link + "' (" + e + ")");
		return;
	}

	if (!httpRes.ok) return;
	if (httpRes.status !== 200) return;

	const bodyText = await httpRes.text();

	try {
		let title = (bodyText.split('<title>')[1]?.split('</title>')[0]).replaceAll(/\n/g, "");
		if (title.length > module.exports.MAX_TITLE_LEN) title = title.substring(0, module.exports.MAX_TITLE_LEN) + "...";

		if (postFix) {
			title += postFix;
		}

		return title;
	} catch (err) {
		console.error(err);
	}
}



module.exports.providers.noembed = async function(link)
{
	if (link.startsWith("https://old.reddit.com")) {
		link = link.replace("old.", "");
	}

	try {
		const httpRes = await fetch("https://noembed.com/embed?url=" + link);
		if (!httpRes.ok) return;
		if (httpRes.status !== 200) return;

		try {
			const json = await httpRes.json();
			return json.title;
		} catch (err) {
			console.error("noembed error");
			console.log(err);
		}

	} catch (err) {
		console.error(err);
		return;
	}

}


module.exports.regexes = {
	//youtube	: /\bhttps\:\/\/m?(?:www\.)?\.?youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})(?:[A-Za-z0-9_\-&;=\.\?]{0,255})?/m,
	youtube	: /\bhttps\:\/\/m?(?:www\.)?\.?youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})(?:[A-Za-z0-9_\-&;=\.\?]{0,255})?/m,
	any	:/(?:\s|^)((http[s]{0,1}\:\/\/\S{4,}))\s{0}/gi,
}


module.exports.supportedLinks = [
	/*
	{
		name: "YouTube",
		regex: module.exports.regexes.youtube,
		provider: module.exports.noembedProvidor,
	},
	{
		name: "noembed",
		regex: [
			/(?:\s|^)((http[s]{0,1}\:\/\/\S{4,}))\s{0}/gi
		],
		//regex: regexes,
		provider: module.exports.noembedProvidor 
	},*/
	{
		name: "Generic",
		regex: module.exports.regexes.any,
		provider: async function(link) {

			if (
					link.startsWith("https://twitter.com/"
				||	link.startsWith("https://x.com/"))
			) {
				return module.exports.providers.noembed(link);
			}

			if (
					link.startsWith("https://youtube.com/")
				||	link.startsWith("https://www.youtube.com/")
				||	link.startsWith("https://youtu.be/")
				||	link.startsWith("https://soundcloud.com/")
				|| 	link.startsWith("https://github.com/")
				||	link.startsWith("https://gitlab.com/")
				|| 	link.startsWith("https://4chan.org/")
				|| 	link.startsWith("https://kohlchan.net/")
				||	link.startsWith("https://gitgud.io/")
				//||	link.startsWith("https://www.youtube.com/")
				//||	link.startsWith("https://youtu.be/")
				||	link.startsWith("https://imgur.com/")
				||	link.startsWith("https://yewtu.be/")
				//||	link.startsWith("https://twitter.com/")
				|| link.startsWith("https://myanimelist.net/anime/")

			) {
				return module.exports.providers.generic(link);
			}
		},
	}
];

module.exports.getTitles = async function(text, post=null, buffer=null) {

	const res = [];
	for (let i = 0; i < module.exports.supportedLinks.length; i++) {
		const it = module.exports.supportedLinks[i];

		const match = (!Array.isArray(it.regex))
			? text.match(it.regex)
			: it.regex.find((it) => text.match(it));

		if (!match) continue;

		const ignore = it.matches_not && it.matches_not.find((not) => not.test(text));
		if (ignore) continue;
		const title = await it.provider(match[0]);

		if (!title || !title.length) continue;

		//   "asdsa asd https://www.youtube.com/watch?v=baKA1B5Ymuo dsadasd ".match(module.exports.supportedLinks[0].regex) 
		// -> Array [ "https://www.youtube.com/watch?v=baKA1B5Ymuo", "baKA1B5Ymuo" ]

		console.log(".. -> " + match[0] || match[1]);

		res.push({
			url: match[0] || match[1],
			id: match[1],
			title: title.substring(0, module.exports.MAX_TITLE_LEN)
		});
	}


	if (post && res.length) {

		const postIndex = buffer.getPostIndexFromTailByNum(post.post_num);
		//const post = buffer.posts[postIndex];

		if (postIndex < 0 || !post) {
			console.info("Post links set but post not found?");
			return;
		}

		if (!post.options) {
			post.options = {};
		}

		post.options.links = res;

		if (buffer && post.sent) {
			buffer.sendUpdatedPost(post);
			await post.save();
		}
	}

	return res;
}



module.exports.init = async function()
{
	global.app.variables.eventEmitter.on("post.received", (params, next) => {
		(async () => {
			const post = params.post;
			const buffer = params.buffer;

			if (post.message) {
				module.exports.getTitles(post.message, post, buffer).then(() => {
						//console.log("Titles set");
				});
			}

			return next();
		})().catch((e) => {
			console.error(e);
			return next(e);
		});
		
	});


	return true;
}