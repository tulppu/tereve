
const {
	promises: fs
} = require("fs");
//const { fs } = require("fs");



global.app.variables.eventEmitter.on("post.received", async (data, next) => {


	const message = data.post;
	const buffer = data.buffer;
	const session = data.session;
	const request = data.request;

	if (!request.hasFile()) {
		return next();
	}

	(async () => {
		const isEncrypted = request.get("encrypted");
		const fileData = request.getFile();

		let attachmentMeta = (isEncrypted)
			? request.get("attachment_meta", null, 1024 * 100)
			: request.getJson("attachment_meta", null, 5000);

		if (request.get("encrypted")) {

			message.options.encrypted = true;

			if (request.hasFile()) {

				message.options.attachment_data_iv = request.getOrThrow("attachment_data_iv");
				message.options.attachment_meta_iv = request.getOrThrow("attachment_meta_iv");

				message.options.signature_attachment = request.getOrThrow("signature_attachment");
			}

		} else if (request.hasFile()) {
			message.attachment_meta = attachmentMeta;
			message.options.signature_message = request.get("signature_message");
			message.options.signature_attachment = request.get("signature_attachment");
		}

		try {
			const attachment = await module.exports.initAttachment(fileData, message, buffer, isEncrypted);

			if (isEncrypted) {
				attachment.meta = request.getOrThrow("attachment_meta");
				message.attachment = attachment;

				return next();
			}

			try {
				await global.app.variables.eventEmitter.emit("buildAttachment", {
					attachment: attachment,
					path: 		attachment.dir_path + attachment.real_filename,
					message: 	message,
					buffer: 	buffer,
					request: 	request,
				});
			} catch (err) {
				return next(err);
			}
			message.attachment = attachment;
		} catch (e) {
			return next(e)
		}
		return next();
	})().catch((e) => next(e));
});

module.exports.initAttachment = async function(fileData, message, buffer, isEncrypted = false) {
//async function initAttachment(fileData, message, buffer, isEncrypted = false) {

	let attachment = {};


	const dirName = "uploads/" + buffer.getStorageName() + "/";

	attachment.dir_path = global.app.configs.server.storage.directory + dirName 
	attachment.real_filename =+ message.post_num;


	if (isEncrypted) {
		attachment.real_filename += ".dat";
	}

	attachment.url = `${global.app.configs.server.storage.http_address}${dirName}${attachment.real_filename}`;

	try {
		await fs.access(attachment.dir_path);
	} catch (e) {
		await fs.mkdir(attachment.dir_path);
		await fs.mkdir(attachment.dir_path + "thumbs/");
	}

	const finalDestinationPath = attachment.dir_path + attachment.real_filename;
	await fs.rename(fileData.path, finalDestinationPath);
	attachment.size = fileData.size;

	if (!attachment.size) {
		const fileStats = await fs.stat(finalDestinationPath);
		attachment.size = fileStats.size
	}

	if (isEncrypted) {
		return attachment;
	}

	
	const ext = fileData.name.substr(fileData.name.lastIndexOf("."));

	if (!ext) {
		throw new global.app.exceptions.InvalidRequestException("Invallid filename");
	}

	const original_filename = (fileData.name.length > 254) ? (fileData.name.substr(0, 254) + ".." + ext) : fileData.name;

	attachment.meta = {
		original_filename: original_filename,
		thumb_url_small: null,
		thumb_url_default: null,
		thumb_url_static: null,
		mime: null,
		ext: null,
	};

	return attachment;
}
