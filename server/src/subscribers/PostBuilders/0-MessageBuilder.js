function applyChannelConfigs(post, buffer, session) {

	if (!buffer.configs) {
		return;
	}

	if (buffer.configs.getConfigValue("hidePosterNick")) {
		delete post.user;
	}

	//post.configs = buffer.configs;

	const forcedNick = buffer.configs.getConfigValue("forcedPosterNick");
	if (forcedNick && forcedNick.length) {
		post.user = forcedNick;
	}

	if (buffer.configs.getConfigValue("showGeodata")) {
		post.geo = session.geoData;
	}
}

const repliesRegex = />>(\d+)/gm;

module.exports.parseReplies = function(post, buffer)
{
	let m = null;
	const repliedPosts = [];


	do {
		m = repliesRegex.exec(post.message);
		if (m) {
			const postNum = parseInt(m[1]);
			if (repliedPosts.find((it) => it === postNum)) continue;
			repliedPosts.push(postNum);
		}
		if (repliedPosts.length > 15)
			break;
	} while (m);

	repliedPosts.forEach(async(replied_post_num) => {
		const repliedPost  = buffer.getPostByNum(replied_post_num);
		if (repliedPost) {
			repliedPost.replied_by.push(post.post_num);
			post.replied_posts.push(replied_post_num);

			//repliedPost.invalidateClietJsonString();
			await repliedPost.save();
		}
	});
}


module.exports.init = async function() {
	global.app.variables.eventEmitter.on("post.received", async (data, next) => {

		const post = data.post;
		const buffer = data.buffer;
		const session = data.session;
		//const request = data.request;

		post.ip_hash = session.ipHash;

		console.log("..building..");

		await (async () => {

			applyChannelConfigs(post, buffer, session);
			post.buffer_id = buffer.id;

			const unix_now = new Date().getTime() / 1000;
			const postsTail = buffer.posts.slice(-128);

			const postingCooldown = buffer.configs.getConfigValue("cooldown");
			if (postingCooldown)
			{
				const isCooldownViolation = !!postsTail.find((it) => {		
					const date_unix = Date.parse(it.date) / 1000;
					return (date_unix + postingCooldown) > unix_now;
				});
				if (isCooldownViolation)
				{
					throw new global.app.exceptions.InvalidRequestException("Cooldown violation!");
				}
			}
		
			const isFlooding = postsTail.filter((it) => {
				const date_unix = Date.parse(it.date) / 1000;
				return session.ipHash == post.ip_hash && date_unix > (unix_now - 8);
			}).length > 7;

			if (isFlooding) {
				throw new global.app.exceptions.InvalidRequestException("Flood detected!");
			}

			buffer.post_count++;
			post.post_num = buffer.post_count;
			//post.post_num = buffer.posts.pop()?.post_num + 1;

			module.exports.parseReplies(post, buffer);
			console.log("..built..?");

			next();
		})().catch((e) => next(e));
	});

	return 1;
}