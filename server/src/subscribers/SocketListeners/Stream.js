


return;
let audioHeader = null;
const fs = require("fs");

const activeStreamingClients = new Map();
let streamingClient = null;

const streamWaiters = [];

function getStreamingClient(cb) {
	if (streamingClient) {
		return cb(streamingClient);
	}
	streamWaiters.push(cb);
}

function broadCastStream(client) {
	streamWaiters.forEach((cb) => cb(client));
	streamWaiters.length = 0;
}

global.app.variables.eventEmitter.on("user.authed", (data, next) => {
	return;
	const session = data.session;
	const socketClient = session.socket;


	global.app.variables.eventEmitter.on("streamRequest", (data, next) => {

		const response = data.response;
		const request = data.request;

		const streamingClient = activeStreamingClients.get(data.id);

		if (!streamingClient) {			
			response.writeHead(404);
			response.end();
			return;
		}

		response.writeHead(200, {
			"Content-Type": "audio/webm;codecs=opus",
			"Transfer-Encoding": "chunked"
		});
		//response.write(streamingClient.mediaHeaders);

		const stream = fs.createReadStream("/tmp/" + data.id + ".ogg", {autoClose: false, emitClose: false})//.pipe(response, {});
		//stream.on("open", () => stream.pipe(response, {}));
		stream.on("end", () => console.log("loppu"));

		stream.on("data", (chunk) => {
			response.write(chunk);
			//response.end();
		})

		//stream.on("data", (chunk) =>  {
		//	response.write(chunk)
		//});

		streamingClient.streamSubs.push(response);
		//request.listenedStream = streamingClient;

		//request.on("close", () => {
		//	request.listenedStream.streamSubs = request.listenedStream.streamSubs.filter((it) => it
		//})

		console.log("..serving");

		//return next();
	});


	socketClient.on("message", (chunk) => {

		if (!socketClient.streamPipe) {
			//socketClient.streamSubs = [];
			socketClient.mediaHeaders = chunk;

			socketClient.subs = [];

			socketClient.sub = ((cb) => {
				cb(socketClient.mediaHeaders);
				socketClient.subs.push(cb);
			});

			
			//const streamId = session.profile.id;
			//activeStreamingClients.set(session.profile.id, socketClient);

			//const streamPath = "/tmp/" + streamId + ".ogg";
			const streamPath = "/tmp/stream.ogg";
			fs.writeFileSync(streamPath, "");

			socketClient.streamPipe = fs.createWriteStream(streamPath, {overwrite: true});

			console.log("piping to " + streamPath);
			console.log("töh");

			socketClient.on("close", () => {
				//socketClient.streamPipe.end();
				//fs.unlinkSync(streamPath);
				socketClient.subs.forEach((cb) => cb(null));
			});
			broadCastStream(socketClient);

			//streamingClient = socketClient;
		}
		socketClient.subs.forEach((cb) => cb(chunk));
		//socketClient.streamPipe.write(message);

		//socketClient.streamSubs.forEach((sub) => sub.write(message));
		//socketClient.streamPipe.write(message);

		//console.log(message.length);
		//global.app.variables.wss.clients.forEach((it) => it.send(message, {binary: true}));

		//socketClient.streamPipe.write(message)
		//socketClient.send(message, {binary: true});
	});

	return next();
});

global.app.variables.eventEmitter.on("s.stream", async (parameters, next) =>
{
	return;
	const response = parameters.response.conn;
	const request = parameters.request;


	response.writeHead(200, {
		"Content-Type": "audio/webm;codecs=opus",
		"Transfer-Encoding": "chunked"
	});
	//response.write(streamingClient.mediaHeaders);

	//while(!streamingClient) {};

	console.log("Haetaan striimiä");
	getStreamingClient((streamingClient) => {

		console.log("Tarjotaan striimi");

		streamingClient.sub(function gotChunk(chunk)  {

			if (chunk === null) {
				return;
			}

			response.write(chunk);
			console.log("..saatiin");
		});
	});

	/*
	const stream = fs.createReadStream("/tmp/stream.ogg", {autoClose: false, emitClose: false})//.pipe(response, {});
	//stream.on("open", () => stream.pipe(response, {}));
	stream.on("end", () => {
		console.log("loppu");
		response.end();
	});

	stream.on("data", (chunk) => {
		console.log("lähetty roskaa");
		response.write(chunk);
		//response.end();
	})*/

	console.log("..serving");

	//response.writeHead(404, {'content-type': 'text/plain'});
	//response.end();

	//return next();
});

/*
function receiveStreamChunk(parameters) {

	const request = parameters.request;

	let chunkContainer = chunksMap.get(chunkKey);
	if (!chunkContainer) {
		chunkContainer = {
			ipAddress: parameters.ipAddress,
			clearingTimeout: null
		}
	}

	chunkContainer.body = null;

	request.on("data", function (streamChunk) {
		if (!chunkContainer.body) {
			chunkContainer.body = streamChunk;
		} else {
			chunkContainer.body = Buffer.concat([chunkContainer.body, streamChunk]);
		}
		chunksMap.set(chunkKey, chunkContainer);
	
	});

	request.on("end", function() {
		return serveLatestChunk(parameters);
	});

	//Buffer.concat([request.body, chunkContainer.body]);

	clearTimeout(chunkContainer.clearingTimeout);
	chunkContainer.clearingTimeout = setTimeout(() => {
		chunksMap.delete(parameters.request.ipAddress);
	}, 50)

}
*/
