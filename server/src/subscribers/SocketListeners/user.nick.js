const { DuplicateValueExpection } = require(global.srcRoot + "/database/MongoExpections.js");



global.app.variables.eventEmitter.on("s.user.nick", async (data, next) => {

	(async() => {
		const session = data.session;
		const request = data.request;

		if (!session) {
			return next()
		}

		const newNick = request.getOrThrow("nick").trim().replace(/\s/g, '');

		if (!newNick.length > 25) {
			throw new global.app.exceptions.InvalidRequestException("Invalid nickname");
		}

		await session.setNick(newNick);
		return next();
	})().catch((e) => {
		if (e instanceof DuplicateValueExpection) {
			next(new global.app.exceptions.InvalidRequestException("Nickname already in use"))
		} else {
			next(e);
		}
	});
});