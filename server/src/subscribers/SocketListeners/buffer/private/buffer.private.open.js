const Bufferutils = require(global.srcRoot + "/BufferUtils.js");

const getPrivateBuffer = Bufferutils.getPrivateBuffer;

const {
	UserProfile
} = require(global.srcRoot + "/database/models/ChatUserProfile.js");

global.app.variables.eventEmitter.on("s.buffer.private.open", async (data, next) => {

	const session = data.session;
	const request = data.request;

	(async() => {
		if (!session.profile.id) {
			throw new global.app.exceptions.InvalidRequestException("Missing authentication");
		}

		const friend_id = request.getOrThrow("friend_id");

		if (session.joinedBuffers.find((it) => it.id === friend_id)) {
			throw new global.app.exceptions.ActionUnavailableException(`Buffer already opened`);
		}

		const friend = await UserProfile.getOne(friend_id);

		if (!friend) {
			throw new global.app.exceptions.ActionUnavailableException(`User '${friend_id}' does not exist in server`);
		}

		const buffer = await getPrivateBuffer(session, friend_id);

		if (!buffer.sessions.find((it) => it === session)) {
			buffer.sessions.push(session);
		}

		if (session.joinedBuffers.find((it) => it.id === buffer.id)) {
			throw new global.app.exceptions.InvalidRequestException("Buffer already opened");
		}

		session.joinedBuffers.push(buffer);

		const clientObject = buffer.getClientObject();

		clientObject.name = (friend && friend.name) ? friend.alias : friend_id;
		clientObject.friend_id = friend_id;

		const res = `{
						"event": "buffer.private.loaded",
						"buffer": ${JSON.stringify(clientObject)}
					}`;

		session.send(res);

		next();
	})().catch((e) => next(e));

});