const Bufferutils = require(global.srcRoot + "/BufferUtils.js");
const getPrivateBuffer = Bufferutils.getPrivateBuffer;
const ChatPost = require(global.srcRoot + "/classes/ChatPost.js");

global.app.variables.eventEmitter.on("s.buffer.private.post", async (data, next) => {

	const session = data.session;
	const request = data.request;
	const response = data.response;

	(async () => {
		let friendId = request.getOrThrow("friend_id");

		const msg = request.getString("message", "").trim();

		if ((!msg || !msg.length) && !request.hasFile()) {
			throw new global.app.exceptions.InvalidRequestException("Post data missing");
		}

		const buffer = await getPrivateBuffer(session, friendId);
		const post = new ChatPost(msg, session);

		//message.encrypted = !!request.get("encrypted");
		post.options.encrypted = !!request.get("encrypted");

		if (msg && msg.length) {
			post.options.signature_message = request.getOrThrow("signature_message");
			post.options.message_iv = request.getOrThrow("message_iv", null);
		}

		const params = { 
			post: post,
			buffer: buffer,
			session: session,
			request: request,
		};

		try {
			await global.app.variables.eventEmitter.emit("post.received", params);
		} catch (_err) {
			return next(_err);
		}

		buffer.sendPost(post);
		
		/*if (await !buffer.exists()) {
			await buffer.save();
		}*/

		response.setResponse(post);

		return next();

	})().catch((e) => next(e));

});