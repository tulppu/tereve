const {
	getPrivateBufferByIdOrThrow
} = require(global.srcRoot + "/BufferUtils.js");



global.app.variables.eventEmitter.on("s.buffer.private.message.delete", async(data, next) => {

	const session = data.session;
	const request = data.request;

	(async () => {

		const messageNum = request.getIntOrThrow("message_num");
		const bufferId = request.getStringOrThrow("buffer_id");

		const buffer = await getPrivateBufferByIdOrThrow(bufferId);
		const message = buffer.getPostByNum(messageNum);

		if (!message) {
			throw new global.app.exceptions.ResourceNotFoundException(`Message '#${messageNum}' was not found.`);
		}

		const canDelete = message.user_id === session.id || session.isAdmin();

		if (!canDelete) {
			throw new global.app.exceptions.UnauthorisedActionException(`Deletion of message '#${messageNum}' is not allowed`);
		}

		buffer.removePost(message);

		next();
	})().catch((e) => next(e));
});