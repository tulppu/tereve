
global.app.variables.eventEmitter.on("s.buffer.private.close", (data, next) => {

	const session = data.session;
	const request = data.request;

	(async () => {

		const priv_id = request.getOrThrow("buffer_id");
		const buf = session.joinedBuffers.find((it) => it.id === priv_id);

		if (!buf) {
			//throw new global.app.Exceptions.ActionUnavailableException("Buffer not joined");
			throw new global.app.exceptions.ActionUnavailableException("Buffer not joined");

		}

		session.joinedBuffers = session.joinedBuffers.filter((it) => it !== buf);
		buf.sessions = buf.sessions.filter((it) => it !== session); 

		if (!buf.sessions.length) {
			global.app.variables.privateBuffers.delete(priv_id);
		}

		return next();
	})().catch((e) => next(e));
});