const {
	getBufferByNameOrThrow,
	throwIfNotMod
} = require(global.srcRoot + "/BufferUtils.js");

global.app.variables.eventEmitter.on("s.buffer.topic", async (data, next) => {

	const user = data.user;
	const request = data.request;
	const session = data.session;

	(async () => {
		const buf = await getBufferByNameOrThrow(request.getOrThrow("buffer"), session);
		const newTopic = request.getOrThrow("topic");

		if (!buf.isChannelOperator(session.profile)) {
			throw new global.app.exceptions.UnauthorisedActionException("You are not channel moderator.");
		}

		throwIfNotMod(buf, session.profile);
		buf.setTopic(newTopic, session.profile);

		next();
	})().catch((e) => next(e));

});