
const ChatBufferChannel = require(global.srcRoot + '/database/models/ChatBufferChannel.js');

const BuffeUtils = require("../../../../BufferUtils");

global.app.variables.eventEmitter.on("s.buffer.channel.join", async (data, next) => {

	const session = data.session;
	const request = data.request;

	(async() => {

		if (!session || !session.profile.id) {
			return next(new global.app.exceptions.AuthMissingException());
		}

		const bufferName = request.getStringOrThrow("buffer_name", 32).trim();
		if (bufferName.length <= 1) throw new global.app.exceptions.InvalidRequestException("Invalid buffer name");

		// TODO: Read max buffer limit from configs.
		if (session.joinedBuffers.length > 64) {
			throw new global.app.exceptions.UnauthorisedActionException("Max buffer limit reached (64)");
		}


		let buffer = await BuffeUtils.getBufferByName(bufferName);

		if (buffer && buffer.sessions.find((it) => it.client === session.client)) {
			throw new global.app.exceptions.InvalidRequestException("Already joined a buffer.");
		}

		if (buffer) {
			if (session.joinedBuffers.find((it) => it === buffer)) {
				throw new global.app.exceptions.InvalidRequestException("Already joined a buffer.");
			}
	
			buffer.join(session);
			buffer.sendToSession(session);
			return next();
		}


		if (bufferName.substr(1).indexOf("#") >= 0) throw new global.app.exceptions.InvalidRequestException("Invalid buffer name");
		if (bufferName[0] !== '#') {
			throw new global.app.exceptions.InvalidRequestException("Invalid buffer name"); 
		}

		const unallovedCharacters = [
			"\"",
			"\\",
			"\"",
			"'",
			"`",
			".",
			"/",
			";",
			"*",
			">",
			"<",
			"|",
			"&",
			"?",
			"$",
			"%",
			" ",
		];
		unallovedCharacters.forEach((char) => {
			if (bufferName.indexOf(char) >= 0) {
				throw new global.app.exceptions.InvalidRequestException(`Illegal character '${char}' in buffer name '${bufferName}'`);
			}
		})


		buffer = await BuffeUtils.getBufferByName(bufferName) || await ChatBufferChannel.getAndLoad(bufferName)
		if (!buffer.ops || !buffer.ops.length) {
			buffer.ops = [ session.profile.id ];
		}

		if (!BuffeUtils.hasBuffer(buffer)) {
			await BuffeUtils.registerBuffer(buffer);
		}

		global.app.variables.channelBuffers.set(buffer.name, buffer);

		buffer.join(session);
		buffer.sendToSession(session, 16);

		return next();
	})().catch((e) => next(e));
});