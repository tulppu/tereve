//import { JsonStreamStringify } from 'json-stream-stringify';

global.app.variables.eventEmitter.on("s.buffer.posts.load", async (params, next) => {

	const session = params.session;
	const request = params.request;
	const response = params.response;

	(async () => {
		const bufName = request.getOrThrow("buffer_name");

		//const buffer = getBufferByNameOrThrow(bufName);
		const buffer = session.joinedBuffers.find((it) => it.name === bufName);
		if (!buffer) {
			throw new global.app.exceptions.ActionUnavailableException("Not joined to this buffer");
		}

		//buffer.sendToSession(session);
		const postsLimit = Math.max(1, request.getInt("posts_limit", 254));
		const postsStart = request.getInt("posts_start", buffer.posts.at(-1)?.post_num, 0);

		if (postsLimit < 0 || postsLimit >= 1024 * 1) {
			throw new global.app.exceptions.ActionUnavailableException("Invalid post limit " + postsLimit);
		}

		const postsStartIndex = buffer.getPostIndexByNum(postsStart);
		const lastIndex = Math.max(buffer.posts.length - 1, 0);

		/*
		console.log(postsStart);
		console.log(postsStartIndex);
		console.log(buffer.posts.at(postsStartIndex));
		*/

		response.conn.setHeader('Content-Type', 'application/json')
		//response.conn.setHeader(this.reponseCode, { 'content-type': this.contentType });

		response.conn.write("[\n");
		let first = true;
		for (
			let i = postsStartIndex;
				i >= 0
				&& i <= lastIndex
				&& i >= postsStartIndex - postsLimit - 1;
				//&& buffer.posts[i].post_num >= buffer.posts[postsStartIndex].post_num - postsLimit;
				//&& i < lastIndex;
			i--
		) {
			if (!first) {
				response.conn.write(",\n");
			}
			response.conn.write(buffer.posts[i].getClientJsonString());
			first = false;
		}
		response.conn.write("\n]");
		
		response.conn.end();
		response.sent = true;

		next();
	})().catch((e) => next(e));
});