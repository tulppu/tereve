module.exports.init = async function() 
{
	const {
		getBufferByNameOrThrow,
	} = require("../../../../BufferUtils");

	//buffer.channel.posts.delete
	global.app.variables.eventEmitter.on("s.buffer.channel.post.delete", async(data, next) => {

		const session = data.session;
		const request = data.request;

		(async () => {

			const postNum = request.getIntOrThrow("post_num");
			const bufferName = request.getStringOrThrow("buffer_name");
			const reason = request.getString("reason", null, 32);

			const buffer = await getBufferByNameOrThrow(bufferName, session);
			const post = buffer.getPostByNum(postNum);

			if (!post) {
				throw new global.app.exceptions.ResourceNotFoundException(`Post '#${postNum}' was not found.`);
			}

			const canDelete = post.user_id === session.profile.id
								|| buffer.isChannelOperator(session)
								|| session.isAdmin();

			if (!canDelete) {
				throw new global.app.exceptions.UnauthorisedActionException(`Deletion of post '#${postNum}' not allowed`);
			}


			/*
			if (post.attachment) {
				await post.deleteAttachment(buffer);
				delete post.attachment;
			}
			post.message = "Poistettu " + Math.random();
			buffer.updatePost(post);
			*/
			buffer.removePost(post, reason);

			return next();
		})().catch((e) => next(e));
	});

	return 1;
}