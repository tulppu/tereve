const ChatPost = require(global.srcRoot + "/classes/ChatPost.js");


global.app.variables.eventEmitter.on("s.buffer.channel.post", async (data, next) => {

	const session = data.session;
	const request = data.request;
	const response = data.response;

	let called = false;
	const callNext = (e) => {
		if (e) {
			console.error("e:");
			console.error(e);
		}
		if (!called) return next(e);
		called = true;
	};

	

	(async () => {

		if (!session) {
			throw new Error("Session unset");
		}

		if (!session.profile) {
			throw new global.app.exceptions.ActionUnavailableException("Profile unset.");
		}
		//if (!session.profile.alias.length) {
		//	throw new global.app.exceptions.ActionUnavailableException("Unset nickname.");
		//}

		const bufferName = request.getOrThrow("buffer");
		const msg = (request.getString("message") || "").trim();

		if ((!msg || !msg.length) && !request.hasFile()) {
			throw new global.app.exceptions.InvalidRequestException("Post content is missing.");
		}

		//const buffer = await getBufferByNameOrThrow(bufferName, session);
		const buffer = session.joinedBuffers.find((it) => it.name === bufferName);
		if (!buffer) {
			throw new global.app.exceptions.UnauthorisedActionException("Channel not joined.");
		}

		if (msg && msg.length > buffer.configs.maxMessageLenght) {
			throw new global.app.exceptions.InvalidRequestException(
				`Max lenght of message is ${buffer.configs.maxMessageLenght} characters (${msg.length} got).`
			);
		}

		let post = new ChatPost(msg, session);
		let _params = {
			post: post,
			buffer: buffer,
			session: session,
			request: request,
		};

		try {
			await global.app.variables.eventEmitter.emit("post.received", _params);
			setTimeout(() => { 
				console.log("emitted?", _params.post.post_num, _params.post.message)
			}, 100);
		} catch (_err) {
			console.error(_err);
			return callNext(_err);
		}
		console.log("p", _params.post.post_num);
		response.setResponse(_params.post);
		buffer.sendPost(_params.post);
		await buffer.addPost(_params.post);
		await buffer.save();
		return next();

	})().catch((e) => callNext(e));
});
