
global.app.variables.eventEmitter.on("s.buffer.channel.part", async (data, next) => {

	const session = data.session;
	const request = data.request;

	if (!session || !session.id) {
		return next(new global.app.exceptions.AuthMissingException());
	}

	(async () => {
		const bufferName = request.getOrThrow("buffer_name");
		const buffer = session.joinedBuffers.find((it) => it.name === bufferName);
		const partReason = request.getString("reason", "Leaving");

		if (!buffer) {
			throw new global.app.exceptions.ActionUnavailableException("Not joined to this buffer");
		}

		buffer.part(session, partReason);
		if (!buffer.sessions.length) {
			global.app.variables.channelBuffers.delete(buffer.name);
		}

		next();
	})().catch((e) => next(e));
});