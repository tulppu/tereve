"use strickt;"

const {
	getBufferByNameOrThrow,
} = require("../../../../BufferUtils");


class BufferInfoUser
{
    constructor(params)
    {
        this.id = params.id;
        this.name = params.name;
        this.desc = params.title;
        this.iconSrc = params.iconSrc;
        this.source =  params.source | "";
        this.pmAble = params.pmAble || true;
        this.country = params.country;
    }
}
module.exports.BufferInfoUser = BufferInfoUser;

module.exports.getBufferInfo = async function(params)
{

    const buffer = params.buffer;
    const res = {
        id: buffer.id,
        topic: buffer.topic,
        users: [],
    };


    for (var i = 0; i < buffer.sessions.length; i++) {
        const ses = buffer.sessions[i];
        res.users.push(new BufferInfoUser({
                id: ses.id,
                name: ses.nick,
                title: '',
                iconSrc: '',
                source: 'local',
                pmAble: true,
                country: (ses.geo) ? ses.geo.country : '',
        }));
    }

    return res;
}

Object.seal(module.exports);

global.app.variables.eventEmitter.on("s.buffer.channel.info", async (params, next) => {

	const request = params.request;
    const session = params.session;

	(async () => {
        
        if (!session) {
            throw new global.app.exceptions.ActionUnavailableException("Invalid session.");
        }
    

        let bufName = request.getAQueryParam("buffer_name") || request.getOrThrow("buffer_name");
        if (bufName[0] !== "#") {
            bufName = "#" + bufName;
        }

		//const buffer = await getBufferByNameOrThrow(bufName);
		const buffer = session.joinedBuffers.find((it) => it.name === bufName);
		if (!buffer) {
			throw new global.app.exceptions.ActionUnavailableException("Buffer not found or joined.");
		}

        const info = await module.exports.getBufferInfo({
            buffer: buffer,
        });

        const response = params.response;

        response.setResponse(info);
        //if (response.)0wi
        response.send();

		next();
	})().catch((e) => next(e));
});