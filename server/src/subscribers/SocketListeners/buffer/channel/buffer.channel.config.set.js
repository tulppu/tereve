
global.app.variables.eventEmitter.on("s.buffer.channel.config.set", (data, next) => {

	const session = data.session;
	const request = data.request;

	(async () => {

		//const buf = await getBufferByNameOrThrow(request.getOrThrow("buffer"), session);
		const bufName = request.getOrThrow("buffer");

		const buf = session.joinedBuffers.find((it) => it.name === bufName);
		if (!buf) {
			return next(new global.app.exceptions.UnauthorisedActionException("Channel needs to be joined first."));
		}

		if (!buf.isChannelOperator(session.profile) && !session.isAdmin()) {
			throw new global.app.exceptions.UnauthorisedActionException("Only channel operators may change this.");
		}
		

		const configName = request.getOrThrow("config");
		const conf = buf.configs.getConfig(configName);
		
		let typedGetter = "getOrThrow";

		switch (conf.type) {
			case 'number':
				typedGetter = "getNumberOrThrow";
				break;
			case 'string':
				typedGetter = "getStringOrThrow";
				break;
			case 'boolean':
				typedGetter = "getBoolOrThrow";
				break;
		}
		
		const configValue = request[typedGetter]("value");

		const oldVal = buf.configs.setConfig(configName, configValue, session);

		if (oldVal === configValue) {
			return next();
		}

		buf.configs.save();
		buf.sendEvent("configchange", {
			config: configName,
			old_value: oldVal,
			new_value: buf.configs[configName],
		});
		
		next();
	})().catch((e) => next(e));
});