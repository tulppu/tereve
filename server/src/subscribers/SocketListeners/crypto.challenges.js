const challengeRequests = new Map();

class ChallengeRequest {
	constructor(requester, next) {
		this.requester = requester;
		this.responser = null;
		this.next = next;
		this.nextCalled = false;

		this.timeout1 = null;
		this.timeout2 = null;
	}
}

const errMsgTimeourOrOffline = "User is offline or not responding.";

const callNextIfNotCalled = (challengeReq, err) => {

	if (!challengeReq.nextCalled) challengeReq.next(err);
	challengeReq.nextCalled = true;

	clearTimeout(challengeReq.timeout1);
	clearTimeout(challengeReq.timeout2);

	challengeRequests.delete(challengeReq.requester.profile.id);
};


global.app.variables.eventEmitter.on("s.crypto.challenge.request", (data, next) => {

	const session = data.session;
	const request = data.request;

	if (!session || !session.id) {
		return next(new global.app.exceptions.AuthMissingException());
	}

	if (challengeRequests.get(session.profile.id)) {
		return next(new global.app.exceptions.UnauthorisedActionException("Wait previous challenge request to timeout."));
	}

	const challengeRequest = new ChallengeRequest(session, next);

	(async () => {

		const receiver_id = request.getOrThrow("user_id");

		const receiver_sessions = global.app.variables.userSessions.get(receiver_id);
		const receiver = (receiver_sessions && receiver_sessions.length) ? receiver_sessions[0] : null;

		// TODO: Send request to all sessions?
		// TODO: Dont allow challenging if receiver hasn't added requester as contact and receiver dont have conversation open.
		//const challengeRequest = new ChallengeRequest(session, receiver, next);
		challengeRequest.responser = receiver;
		if (!receiver) {
			challengeRequest.timeout1 = setTimeout(() => {
				callNextIfNotCalled(challengeRequest, new global.app.exceptions.ActionUnavailableException(errMsgTimeourOrOffline));
			}, 10 * 1000);
		}

		challengeRequests.set(session.profile.id, challengeRequest);

		challengeRequest.timeout2 = setTimeout(() => {
			callNextIfNotCalled(challengeRequest, new global.app.exceptions.ActionUnavailableException(errMsgTimeourOrOffline));
		}, 10 * 1000);

		if (!receiver) return;

		receiver.send(JSON.stringify({
			event: "crypto.challenge.request",
			challenger_id: session.profile.id,
			secret_string: request.getOrThrow("secret_string", 1024 * 8),
			shared_iv: request.getOrThrow("shared_iv", 1024 * 8),
			challenger_ecdh: session.profile.keys.ecdh.pem,
		}));

		console.info("Relayed challenge request");
	})().catch((e) => next(challengeRequest, e));

//	return next();
});


global.app.variables.eventEmitter.on("s.crypto.challenge.response", (data, next) => {

	const session = data.session;
	const request = data.request;

	const responsingTo = request.get("responsing_to");

	const challengeRequest = challengeRequests.get(responsingTo);
	if (!challengeRequest) {
		return next();
	}

	if (!challengeRequest.requester) {
		return next();
	}


	challengeRequest.requester.send(JSON.stringify({
		event: "crypto.challenge.response",
		responser_id: session.profile.id,
		signature: request.get("signature"),
	}));

	callNextIfNotCalled(challengeRequest);

	return next();
});