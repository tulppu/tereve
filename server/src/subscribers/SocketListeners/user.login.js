const crypto = require("crypto");
const geoip = require("geoip-lite");
const AuthSession = require(global.srcRoot + "/classes/AuthSession.js");

const {
	getProfileOrCreateNew,
} = require(global.srcRoot + "/database/models/ChatUserProfile.js");

const FailedToAuthException = global.app.exceptions.FailedToAuthException;

const {
	PublicKeySigner,
	PublicKeyECDH,
	PublicKeyRSA,
} = require(global.srcRoot + "/classes/PublicKey.js");


/*
	client requests challenge
		-> server sends
			-> client signs it
				-> client sends signature
					-> server verifies and responses token back
						 - websocket connection must be opened withing 10s
						 - token must be attached in headers when posting action
						-> clients open websocket connection via /socket/<token>
						 -> server validates it matches to ip of challenger
						 - token is invalided when websocket connection is opened
*/


class PendingChallenge {
	constructor(keys, ip_address) {
		this.keys = keys;
		this.challengeString = global.currentTimestamp() + "_" + crypto.randomBytes(25).toString('hex');
		this.ipAddress = ip_address;
		this.sentAt = global.currentTimestamp();
	}
}


const pendingChallenges = new Map();
//const authorisedUsers = new Map();


global.app.variables.eventEmitter.on("s.auth.challenge.request", async (data, next) => {

	const request = data.request;
	const reponse = data.response;

	(async () => {

		try {
			const keys = {
				signer: new PublicKeySigner(request.getStringOrThrow("pem_signer", 512)),
				ecdh: new PublicKeyECDH(request.getStringOrThrow("pem_ecdh", 512)),
				rsa: new PublicKeyRSA(request.getStringOrThrow("pem_rsa", 512)),
			};

			const challenge = new PendingChallenge(keys, request.getClientAddress());
			pendingChallenges.set(challenge.challengeString, challenge);

			reponse.setResponse({
				"challenge": challenge.challengeString,
			});

			setTimeout(() => {
				pendingChallenges.delete(challenge.challengeString);
			}, 10 * 1000);

		} catch (e) {
			console.error(e);
			throw new FailedToAuthException("Invalid public keys provided");
		}

		return next();

	})().catch((e) => next(e));
});

global.app.variables.eventEmitter.on("s.auth.challenge.response", (data, next) => {

	(async () => {

		const request = data.request;
		//const response = data.response;

		const ip_address = request.getClientAddress();

		let signature = null;
		const challengeString = request.getStringOrThrow("challenge_string");

		const challenge = pendingChallenges.get(challengeString);

		if (!challenge || challenge.ipAddress !== ip_address) {
			throw new global.app.exceptions.ResourceNotFoundException("Challenge expired or never existed.");
		}
		const keys = challenge.keys;

		pendingChallenges.delete(challengeString);

		try {
			signature = Buffer.from(request.getStringOrThrow("signature", 1024), 'hex');
		} catch (e) {
			return next(new FailedToAuthException("Bad signature."));
		}

		const isVerified = keys.signer.verify(signature, challengeString);



		if (!isVerified) {
			return next(new FailedToAuthException("Invalid signature"));
		}

		const profile = await getProfileOrCreateNew(keys);
		const session = new AuthSession(ip_address, profile);

		global.app.variables.sessions.set(challengeString, session);


		// Delete session if socket isn't opened withing 10s
		setTimeout(() => {
			if (!session.socket) {
				global.app.variables.sessions.delete(challengeString);
				console.info("socket wasnt opened in time");
			}
		}, 10 * 1000);

		let geodata = geoip.lookup(session.ipAddress);

		session.geoData = {
			country: (geodata && geodata.country) ? geodata.country.toLowerCase() : 'unknown',
			region: (geodata) ? geodata.region : 'unknown',
			city: (geodata) ? geodata.city : 'unkown' ,
		};
	
		return next();

	})().catch((e) => next(e));
});



global.app.variables.eventEmitter.on("s.opened", async (data, next) => {


	const socket = data.socket;

	const endpointRoot = `${global.app.configs.server.websocket.endpoint_root}socket/`;
	const token = socket.upgradeReq.url.substr(endpointRoot.length);


	if (!token || !token.length) {
		console.info("No token provided");
		socket.close();
		return next();
	}

	const session = global.app.variables.sessions.get(token);

	if (!session) {
		console.info("Unauthorized user opened socket.");
		socket.close();
		return next();
	}


	if (session.ipAddress !== socket.address) {
		console.info("Unauthorised token use");
		socket.close();
		return next();
	}

	session.socket = socket;
	socket.session = session;

	if (global.app.configs.defaultNick) {
		session.nick = global.app.configs.defaultNick;
	}

	if (!global.app.variables.userSessions.get(session.profile.id)) {
		global.app.variables.userSessions.set(session.profile.id, []);
	}

	global.app.variables.userSessions.get(session.profile.id).push(session);


	await global.app.variables.eventEmitter.emit("user.authed", {
		session: session
	});


	return next();
});