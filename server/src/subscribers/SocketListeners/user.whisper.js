const {
	getPrivateBufferByIdOrThrow,
	getBufferByNameOrThrow,
} = require(global.srcRoot + "/BufferUtils.js");

const VALID_BUFFER_TYPES = ["private", "channel"];

module.exports.whisper = async function(params)
{
	const request = params.request;
	const response = params.response;

	const postNum = request.getIntOrThrow("post_num");
	const sourcerBuffer = request.getOrThrow("buffer");
	const bufferType = request.getOrThrow("buffer_type");
	const isEncrypted = request.getBoolOrThrow("is_encrypted");

	const maxLength = (isEncrypted) ? 512 : 200;

	const message = request.getOrThrow("message", maxLength);


	if (VALID_BUFFER_TYPES.indexOf(bufferType) === -1) {
		const errorMsg = `Invalid buffer type ${bufferType}. Can be either ${VALID_BUFFER_TYPES.join(' or ')}.`;
		throw new global.app.exceptions.InvalidRequestException(errorMsg);
	}

	const buffer = await (
		(bufferType === "private") ?
			getPrivateBufferByIdOrThrow(sourcerBuffer) :
			getBufferByNameOrThrow(sourcerBuffer)
	);

	const whisperSourceMessage = buffer.getPostByNum(postNum);
	if (!whisperSourceMessage) {
		throw new global.app.exceptions.ResourceNotFoundException(`Post '#${postNum}' was not found.`);
	}

	const receivers = buffer.sessions.filter((it) => it.profile.id === whisperSourceMessage.user_id);
	if (!receivers.length) {
		throw new global.app.exceptions.ActionUnavailableException(`Post sender don't seem to be present.`);
	}

	const whisperEventObject = {
		event: `user.whisper`,
		source_buffer: sourcerBuffer,
		source_buffer_type: bufferType,
		source_post_num: postNum,
		is_encrypted: isEncrypted,
		message: message,
	};

	receivers.forEach((usr) => usr.send(whisperEventObject));
	
	response.responesSuccess("Whisper sent.");

	return true;
}

global.app.variables.eventEmitter.on("s.user.whisper", async (params, next) => {

	(async () => {
		await module.exports.whisper(params);
		next();
	})().catch((e) => {
		return next(e);
	});
});