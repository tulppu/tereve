const crypto = require("crypto");
const { Readable } = require('stream');
const { finished } = require('stream/promises');
const fs = require("fs");


module.exports.striptHeadersFromPublicPEM = function(pem) {
	return pem
		.replace("-----BEGIN PUBLIC KEY-----", "")
		.replace("-----END PUBLIC KEY-----", "")
		.replace(/\?s/g, '')
		.trim();
}

module.exports.getFingerPrintFromPublicPEM = function(pem) {

	pem = module.exports.striptHeadersFromPublicPEM(pem);

	let res = crypto.createHash('sha256')
                   .update((pem))
				   .digest('hex')
				   .replace(/(.{2})(?=.)/g, '$1:')
	return res;
}



// https://stackoverflow.com/a/74722818
module.exports.downloadFile = async function(url, path)
{
    const stream = fs.createWriteStream(path);
    const { body } = await fetch(url);
    return finished(Readable.fromWeb(body).pipe(stream));
}
