"use strict";



/*
const serveNonApiRequestOrig = httpListener.serveNonApiRequest;
const querystring = require('node:querystring');
*/


module.exports.start = async function ()
{
    const channelInfoHandler = require("../../../subscribers/SocketListeners/buffer/channel/buffer.channel.info");
    const { kotchan } = require("./Kotchan");

    const kotchanBuffer = kotchan.getBuffer();


    let cachedLurkers = null;

    const getBufferInfoOrig = channelInfoHandler.getBufferInfo;

    async function fetchLurkers() {
        const lurkersReq = await fetch(kotchan.endpoint + "/lurkers");
        console.log(lurkersReq);
        //if (!lurkersReq.ok) return resultOrig;
        if (!lurkersReq.ok) return; 


        const lurkersBody = await lurkersReq.text();
        const lurkers = lurkersBody.split("\n");

        cachedLurkers = lurkers.map((it) => {
            return new channelInfoHandler.BufferInfoUser({
                    id: '',
                    name: it,
                    desc: '',
                    title: 'Kotchan user',
                    iconSrc: kotchan.endpoint + 'favicon.png',
                    source: 'kotchan',
                    pmAble: false,   
            });
        });
        setTimeout(() => cachedLurkers = null, 3 * 1000)
        return cachedLurkers;
    }



    channelInfoHandler.getBufferInfo = async function(params)
    {
        const resultOrig = await getBufferInfoOrig(params);
        if (params.buffer !== kotchanBuffer) {
            return resultOrig;
        }

        try {
            resultOrig.users = resultOrig.users.concat(cachedLurkers || await fetchLurkers());
        } catch (e) {
            console.error("Kotlurkers error");
            console.error(e);
        }
        return resultOrig;
    };


    return 1;
}