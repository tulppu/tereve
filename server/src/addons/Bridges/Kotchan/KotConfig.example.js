module.exports = {
    buffer: {
        topic: "a Bridge to https://kotchan.fun/chat/int#General",
        backlogSize: 1024 * 32,
        lastPostsToLoad: 64,
    },
    setXForwardedTo: true,
}
