
const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

const ChatPost = require("../../../classes/ChatPost");

const Tripflags  = require("./Tripflags");
const { kotchan } = require("./Kotchan"); 
const regionCodes = require("./RegionCodes");

const postValidator = require("../../../subscribers/PostBuilders/10-Validator");
const messageBuilder = require("../../../subscribers/PostBuilders/0-MessageBuilder");
const attachmentBuilder = require("../../../subscribers/PostBuilders/01-AttachmentBuilder");

const linkTitles = require("../../../subscribers/PostBuilders/03-LinkTitles");

kotchan.buildPost = async function(data, buildAttachment=true)
{
    const post = new ChatPost(data.body, null);

    post.user = data.name;
    post.user_id = data.identifier;

    if (!post.options) post.options = {};

	const icons = getIcons(data, post);
	if (icons) {
        post.options.icons = icons;
	}
	post.options.source = "kotchan";

    post.geo = {
        country     : data.country_name,
        region      : 'Kotchan',
        // await execPromisified(`wget ${kotchan.endpoint}js/sticker_list.js -O ${listPath}`);
		//
        city        : 'unkown',
        // await execPromisified(`wget ${kotchan.endpoint}js/sticker_list.js -O ${listPath}`);
        //flagSrc     : `https://kotchan.fun/icons/countries2/${flagFile}.png`
    };


    post.buffer_id = kotchan.buffer.id;
    post.post_num = data.count;
    

    post.date = Date.parse(data.date);

	if (data.image && buildAttachment) {

        const filename = data.image.split("/").slice(-1);

        const outDir = global.app.configs.server.storage.directory + "tmp/";
        const outFile = outDir + filename;

        const cmd = `wget '${kotchan.endpoint}tmp/uploads/${filename}' -O ${outFile}`;

        await execPromisified(cmd);

        try {
            const attachment = await attachmentBuilder.initAttachment({
                path: outFile,
                name: data.image_filename,
            }, post, kotchan.buffer, false);

            await global.app.variables.eventEmitter.emit("buildAttachment", {
                attachment: attachment,
                path: attachment.dir_path + attachment.real_filename,
                message: post,
                buffer: kotchan.buffer,
            });
            post.attachment = attachment;
        } catch (e) {
            console.error("Failed to attach attachment.")
            console.error(e);
        }
    }

    if (post && post.message && post.message.length) {
        linkTitles.getTitles(post.message, post, kotchan.buffer).then(() => {});
    }


    return post;
}



function getIcons(data, post)
{
    const icons = [];

    const trip = Tripflags.trips.get(data.trip);
    const isBotTrip = data.trip === "!!dhKR/S5BfA";
    const hasGeoInfo = data.country || data.country_name;

    if (!trip && data.trip) {
        if (isBotTrip) {
            icons.push({
                name        : "Bot",
                title       : "Bot",
                src         : 'https://kotchan.fun/icons/bot.png',
                width       : 18,
                height      : 20,
                type        : "icon"
            });        
        } else {
            post.user += data.trip;
        }
    }

    if (trip) {
        icons.push({
            name: trip.string,
            title: trip.string,
            src: `https://kotchan.fun/icons/tripflags/${trip.image}`,
            type: "trip",
        });
    } else if (!isBotTrip && (hasGeoInfo)) {

        // Can be DE or DE-02
        const countryCode = (data.country_name && data.country_name.includes("-")) ? data.country_name : data.country;
        const isRegional = countryCode && countryCode.includes("-");

        const countryCodeRegional   = (isRegional) ? countryCode : null;
        const countryCodeRegionless = (isRegional) ? countryCode.split("-").at(0) : countryCode;

        //console.log(countryCodeRegional, countryCodeRegionless);

        const getCountryNameByCode = ((country_code) => {
            try {
                return kotchan.regionNames.of(country_code);
            } catch (e) {
                console.info(`No country name found for '${country_code}'`);
            }
            return null;
        })

        const countryName = getCountryNameByCode(countryCodeRegionless) || data.country_name;

        // Push regionless flag first
        if (countryCodeRegional) {
            icons.push({
                name        : countryName,
                title       : countryName,
                src         : `https://kotchan.fun/icons/countries2/${countryCodeRegionless}.png`,
                width       : 20,
                height      : 20,
                type        : "flag-country",
            });
        }

        icons.push({
            name        : (isRegional) ?  regionCodes[countryCode] : countryCode,
            title       : (isRegional) ?  regionCodes[countryCode] : countryName,
            src         : `https://kotchan.fun/icons/countries2/${countryCode}.png`,
            width       : 20,
            height      : 20,
            type        : (isRegional) ? "flag-country" :"flag-region",
        });
    }

    icons.push({
        name        : "Kotchan",
        title       : data.convo,
        src         : 'https://kotchan.fun/favicon.png',
        width       : 16,
        height      : 16,
        type        : "icon"
    });

	return icons;
}
