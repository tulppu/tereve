
//"use strict;"

const request = require("request");
const path = require('path');
const fs = require("fs");


const postValidator = require("../../../subscribers/PostBuilders/10-Validator");
const messageBuilder = require("../../../subscribers/PostBuilders/0-MessageBuilder");
const ChatChannelBuffer = require("../../../database/models/ChatBufferChannel.js");
const  { registerBuffer }  = require("../../../BufferUtils");

const eventEmitter = global.app.variables.eventEmitter;

const kotchan = {};

kotchan.config = {};
kotchan.endpoint = "https://kotchan.fun/";
kotchan.stickerDir = global.app.configs.server.storage.directory + "stickers/";
kotchan.lastBridgeDelivery = Date.now();
kotchan.regionNames = new Intl.DisplayNames(['en'], {type: 'region'});
kotchan.pendingPostCallbacks = new Map();

kotchan.buffer = null;
kotchan.getBuffer = (() => kotchan.buffer);

module.exports.kotchan = kotchan;

module.exports.init = async function () {

    console.log("Loading Kotchan");

    kotchan.buffer = await ChatChannelBuffer.getAndLoad("#kotchan");

    kotchan.config = require("./KotConfig");

    kotchan.buffer.configs.setConfig("showGeodata", true, null);
    kotchan.buffer.configs.setConfig("backlogSize", kotchan.config.buffer.backlogSize, null);
    kotchan.buffer.configs.setConfig("maxFileSize", 40, null);
    kotchan.buffer.configs.setConfig("cooldown", 3.5, null);

    kotchan.buffer.topic = kotchan.config.topic

    await registerBuffer(kotchan.buffer);

    await kotchan.buffer.configs.save();

	//global.app.variables.channelBuffers.set("#kotchan", kotchan.config.buffer.buffer);


	module.exports.kotchan = kotchan;

	const { stickers } = require("./Stickers");
	const { postBuilder } = require("./BuildPost");
	const { startIO } = require("./KotIO.js");
	const  lurkers = require("./Lurkers");

    await kotchan.loadStickers(kotchan.buffer);
    await lurkers.start()

	setInterval(() => kotchan.loadStickers(kotchan.buffer), 60 * 1000 * 5);

	startIO();
    //sockio(kotchan.buffer);

    const whispers = require("../../../subscribers/SocketListeners/user.whisper");
    const whipserOrig = whispers.whisper;
    whispers.whisper = async function(params) {

        const request = params.request;
        const response = params.response;

        const buffer = request.getOrThrow("buffer");
        if (buffer !== kotchan.buffer.name) {
            return whipserOrig(params);
        }

        const postNum = request.getIntOrThrow("post_num");
        const post = kotchan.buffer.getPostFromTailByNum(postNum, 512);

        if (!post) {
            throw new global.app.exceptions.ResourceNotFoundException(`Post '#${postNum}' was not found or it's too old.`);
        }

        if (post.options.source === "local") {
            return whipserOrig(params);
        } else {
            const message = request.getStringOrThrow("message");

            const formData = new FormData();
            formData.append("id", postNum);
            formData.append("text", message);
            formData.append("name", "Kot");

            const pmRes = await fetch(kotchan.endpoint + "priv", {
                "credentials": "include",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                    "X-Requested-With": "XMLHttpRequest",
                    "Sec-Fetch-Site": "same-origin"
                },
                "referrer": kotchan.endpoint + "chat/int",
                "body": new URLSearchParams(formData).toString(),
                "method": "POST",
                "mode": "cors"
            });

            if (pmRes.ok) {
                response.responesSuccess("PM sent.");
            } else {
                response.responseFailure("Failed to bridge PM.");
            }
        }
        return true;
    }



    const validatePostOrig = postValidator.validatePost;
    postValidator.validatePost = async function(params) {

        const resOrig = await validatePostOrig(params);

        if (params.buffer !== kotchan.buffer) {
            return resOrig;
        }        

        Object.keys(require.cache).forEach(function(key) { delete require.cache[key] });
        kotchan.config = require("./KotConfig");

        const postObject = params.post;
        const msg = postObject.message || '';


        let formData = {};
        formData.body = msg;

        if (postObject.attachment) {
            
            const filePath  = postObject.attachment.dir_path + postObject.attachment.real_filename;
            const filename  = postObject.attachment.meta.original_filename;

            const pathAbsolute = path.resolve(filePath);
            
            formData.image = {
                value: fs.createReadStream(pathAbsolute),
                options: {
                    filename:       filename,
                    contentType:    postObject.attachment.meta.mime,    
                }
            }
        }

        if (!postObject.options) postObject.options = {};
        postObject.options.source = "local";
        
        formData.name = (params.session.nick) ? params.session.nick : '';

        if (!postObject.attachment || postObject.attachment.size <= 1024 * 64) {
            kotchan.lastBridgeDelivery = Date.now();
        }

        return new Promise((resolve, reject) => {

            if (params.session.kotAborter) {
                params.session.kotAborter.abort();
            }
            params.session.kotRequest?.abort();

            params.session.kotRequest = request.post({
                    url: `${kotchan.endpoint}chat/int`,
                    formData: formData,
                }, function(error, httpResponse, body) {

                params.session.kotRequest = null;
                kotchan.lastBridgeDelivery = Date.now();

                if (error) {
                    console.log("Kotchan post error");
                    console.error(error);
                    return reject(error);
                }

                let jsoned = null;
                try {
                    jsoned = JSON.parse(body);
                    console.log(jsoned);
                } catch (e) {
                    console.log("Failed to parse post result as json");
                    console.log(body);
                    return reject();
                }

                let linkingTimeout = null;

                if (jsoned?.failure) {
                   return reject(new global.app.exceptions.InvalidRequestException(`Bridging error: ${jsoned.failure}.`))
                }

                if (httpResponse.statusCode !== 200) {
                    return reject(new global.app.exceptions.InvalidRequestException(`Undefined bridging error (reponse ${httpResponse.statusCode}).`));
                }


                if (jsoned?.id) {
                    console.log("Posting to kotchan success, got id " + jsoned.id+ " for post.");
                    
                    // Wait for post to bounce back from kotchan.
                    kotchan.pendingPostCallbacks.set(jsoned.id,
                        async function postCameBack(kotchanPost) {
                            console.log("Merging bridged post " + kotchanPost.post_num)
                            const mergedPost = await mergePosts(kotchanPost, postObject);
                            console.log("..merged?", mergePosts.post_num);
                            //params.post = mergePosts;
                            resolve(mergedPost);
                            clearTimeout(linkingTimeout);
                            return mergedPost;
                        }
                    );

                    linkingTimeout = setTimeout(() => {
                        console.log("Briding timeout received");
                        //const mergedPost = await mergePosts(null, postObject);
                        mergePosts(null, postObject).then((mergedPost) => {
                            //resolve(mergedPost);
                            //params.post = mergedPost;
                            clearTimeout(linkingTimeout);
                            //return mergedPost;
                            return reject(new global.app.exceptions.ServerErrorException(`Failed (timeout) to bridge post to kotchan, discarding.`));
                        });
                    }, 1000 * 90);

                } else {
                    postObject.post_num = "L" + postObject.post_num;
                }

            });

            if (kotchan.config.setXForwardedTo) {
                params.session.kotRequest.setHeader("X-Forwarded-For",    params.session.ipAddress);
                //params.session.kotRequest.setHeader("X-Bridged-From",     global.app.configs?.bridgeToken);
            }

            // Doesn't need to be accurate
            const approxReqSize = (postObject.attachment) ? postObject.attachment.size : 254;

            params.session.kotRequest.on("drain", (e) => {
                const kotReq = params.session.kotRequest;
                console.log( "~" + ((kotReq.req.connection.bytesWritten / approxReqSize) * 100).toFixed(2) + "% sent.")
                if (kotReq.req.connection.bytesWritten >= approxReqSize - 1024 * 64) {
                    console.log("About to finish POST.");
                    kotchan.lastBridgeDelivery = Date.now();
                }
            });
        });

        async function mergePosts(kotchPost, postObject)
        {
            const origPostNum = postObject.post_num;
            const finalPost = postObject;
            
            if (kotchPost) {
                postObject.post_num = kotchPost.post_num;
                const i = kotchan.buffer.posts.findIndex((it) => it.post_num == origPostNum);

                kotchan.pendingPostCallbacks.delete(kotchPost.id);
                finalPost.post_num = kotchPost.post_num;
                finalPost.options.icons = kotchPost.options.icons;

                //const i = kotchan.buffer.posts.findIndex((it) => it.post_num === finalPost.post_num);
                //const i = kotchan.buffer.getPostIndexByNum(origPostNum);
                if (i >= 0) {
                    console.log("updating old post")
                    kotchan.buffer.posts[i] = finalPost;
                } else {
                    console.log("huh?", origPostNum, kotchPost.post_num, postObject.post_num);
                }
            }
            // Discard replies with original post_num and replace with post_num received from kotchan.
            /* finalPost.replied_posts.forEach((it) => {
                if (!Array.isArray(it.replied_by)) return;
                const repliedPost = kotchan.buffer.getPostByNum(it.post_num);
                if (!repliedPost) return;
                console.log("replied post found");
                repliedPost.replied_posts = repliedPost.replied_posts.filter((it) => it != origPostNum);
            });*/
            finalPost.replied_posts = [];
            finalPost.invalidateClietJsonString();
            messageBuilder.parseReplies(finalPost, kotchan.buffer);

            //kotchan.buffer.sortPosts();

            await kotchan.buffer.save();

            return finalPost
        }
    }


    eventEmitter.on("user.authed", (data, next) => {

        const session = data.session;
    
        kotchan.buffer.join(session);
        kotchan.buffer.sendToSession(session);
        //kotchan.buffer.sessions.push(session);

        return next();
    });
    
    return 1;
}
