const path = require("path");
const fs = require("fs");
const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);

const { kotchan } = require("./Kotchan"); 
const utils = require("../../../utils");
const MediaUtils = require("../../../utils/Media");


kotchan.loadStickers = async function()
{
	console.log("Loading kotch stickers");
    const stickersMap = {
        storages: {
            1: {
                local_path: kotchan.stickerDir,
                www_path: "storage/stickers/",
            }
        },
        items: [],
        categories: [],
    };

    if (!kotchan.buffer.options) {
        kotchan.buffer.options = {};
    }

    if (!kotchan.buffer.options.stickers) {
        kotchan.buffer.options.stickers = stickersMap;
    }

    const currentPath = __dirname + "/"
    const listPath = currentPath + "StickerList.js";

    const stickerPathAbsolute = path.join(kotchan.stickerDir);

    try {
        if (!fs.existsSync(stickerPathAbsolute)){
            fs.mkdirSync(stickerPathAbsolute);
        }
    
        await utils.downloadFile(`${kotchan.endpoint}js/sticker_list.js`, listPath);
        fs.appendFileSync(listPath, "\nmodule.exports = stickers;");

		Object.keys(require.cache).forEach(function(key) { delete require.cache[key] })
        const stickers = require("./StickerList");

		// First check already saved ones and see if some were deleted.
		const items = kotchan.buffer.options.stickers.items;
		for (let i = 0; i < items.length; i++) {
			const it = items[i];
			if (!stickers.includes(it.key)) {
				console.log(`${it.key} was not found from sticker list, it might be deleted? (unregistering)`);
				kotchan.buffer.options.stickers.items = items.slice(i, 1);
            	const srcPath = stickerPathAbsolute + "" + it.key + ".webp";
                if (fs.existsSync(srcPath)) {
					console.log(`Unlinking ${srcPath}`);
					fs.unlinkSync(srcPath);
				}
			}
		}

        // Check for new ones.
		for (let i = 0; i < stickers.length; i++) {
            const it = stickers[i];
            const category = it.split("-")[0];
            if (!category) return;

            if (!kotchan.buffer.options.stickers.categories.find((it) => it === category)) {
                kotchan.buffer.options.stickers.categories.push(category)
            }

            const strickerUrl = kotchan.endpoint + "images/stickers/" + it + ".png";
            const tmpPath = "/tmp/" + it + ".png";
            const destPath = stickerPathAbsolute + "/" + it + ".webp";

            const index = kotchan.buffer.options.stickers.items.findIndex((s) => s.key == it);
            if (index !== -1) {
                if (!fs.existsSync(destPath)) {
                    console.info("Sticker " + it + " already imported.");
                    console.log("..but it's missing the file!");
                    kotchan.buffer.options.stickers.items.splice(index, 1)
                } else {
                    continue;
                }
            }
        
            console.info("Importing sticker " + it);


            if (!fs.existsSync(destPath)) {

                console.log("Downloading " + strickerUrl + " to " + tmpPath);
                await utils.downloadFile(strickerUrl, tmpPath);
    
                const dimensionsTmp = await MediaUtils.getPictureDimensions(tmpPath);
                const scaleRate = Math.min(1, 138 / dimensionsTmp.w, 254 / dimensionsTmp.h);
                const scales = `${dimensionsTmp.w * scaleRate}:${dimensionsTmp.h * scaleRate}`;
                // force_original_aspect_ratio=decrease
                try {
                    const ffmpegCmd = `ffmpeg -y -i ${tmpPath} -vf scale='${scales}:force_original_aspect_ratio=decrease' ${destPath}`;
                    //console.log(ffmpegCmd);
                    console.log("ffmpeg'd");
                    await execPromisified(ffmpegCmd);
                } catch (err) {
                    console.info(err);
                    try {
                        fs.unlinkSync(destPath);
                        continue;
                    } catch(err2) {
                        console.info(err2);
                        continue;
                    }
                }
                fs.unlinkSync(tmpPath);
            }

            try {
                const dimensions = await MediaUtils.getPictureDimensions(destPath);

                if (!dimensions) {
                    console.info("Failed to get dimensions from file " + destPath, ", probably invalid/corrupt sticker. Unlinking.");
                    fs.unlinkSync(destPath);
                    continue;
                }

                kotchan.buffer.options.stickers.items.push({
                    key: it,
                    filename: it + ".webp",
                    storage: 1,
                    width: dimensions.w,
                    height: dimensions.h,
                    category: category
                });
            } catch (err) {
                console.error(err);
                fs.unlinkSync(destPath);
            }

            if (i % 32 == 0) {
                console.log("Saving imports");
                await kotchan.buffer.save();
            }

        }

        kotchan.buffer.options.stickers.items = kotchan.buffer.options.stickers.items.sort((a, b) => a.key.localeCompare(b.key));

        await kotchan.buffer.save();

        console.info("Loaded kotchan stickers.");
        
    } catch (e) {
        console.info("Failed to set kotchan stickers.");
        console.error(e);
        return null;
    }
}


