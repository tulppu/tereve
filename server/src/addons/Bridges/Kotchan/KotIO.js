const { kotchan } = require("./Kotchan");
const messageBuilder = require("../../../subscribers/PostBuilders/0-MessageBuilder");

const linkTitles = require("../../../subscribers/PostBuilders/03-LinkTitles");

module.exports.startIO = (() => {

	kotchan.io = require('socket.io-client');
	kotchan.socket = kotchan.io.connect(kotchan.endpoint, {
		secure: true,
		reconnect: true,
	});

	kotchan.socket.on('connect', function (_sock) {
		console.log('Connected to kot!');
		kotchan.socket.emit('subscribe', "int");


		fetch(kotchan.endpoint + 'data/int?limit=' + kotchan.config.lastPostsToLoad || 1500)
			.then((data) => data.json())
			.then(async(posts) => {
				posts = posts.slice(0, kotchan.config.lastPostsToLoad || 1500).reverse()

				for (let i = 0; i < posts.length; i++)
				{
					const p = posts[i];

					if (p.convo !== "General") continue

					if (kotchan.buffer.posts.find((it) => it.post_num === p.count)) {
						//console.log("Post " + p.count + " is already added");
						continue;
					}

					const addedPost = await kotchan.buildPost(p);
					messageBuilder.parseReplies(addedPost, kotchan.buffer);

					if (addedPost && addedPost.message && addedPost.message.length) {
						await linkTitles.getTitles(addedPost.message, addedPost, kotchan.buffer);
					}

					if (addedPost) {
						await kotchan.buffer.addPost(addedPost);
						await kotchan.buffer.sendPost(addedPost);
						console.log("Post " + addedPost.post_num +  " sent and added");
					}

					//if (i % 32 === 0) await kotchan.buffer.save();
				}
				console.log("Posts added");
				/*kotchan.buffer.posts = kotchan.buffer.posts.sort((a, b) => {
					if (a.post_num < b.post_num) return -1;
					if (a.post_num > b.post_num) return 1;
					return 0;
				});*/
				kotchan.buffer.sortPosts();
				//await kotchan.buffer.save();
				console.log("Posts sorted");
		}).catch((e) => {
			console.error("Failed to fetch kotposts!");
			console.error(e);
		})
	});

	kotchan.socket.on('chat', async function (data) {
		const excpectingDuplicate = (kotchan.lastBridgeDelivery + (5 * 1000)) >= Date.now();

		if (excpectingDuplicate) {
			console.info("Expecting own post to bounce back.");
		}

		setTimeout(async() => {
			const pendingCallback = kotchan.pendingPostCallbacks.get(data.count);

			if (pendingCallback)
			{
				console.log("Found callack, mergings posts");
				console.log(pendingCallback);
				const kotchPost = await kotchan.buildPost(data, false);
				const post = await pendingCallback(kotchPost);
				console.log("..done?", post?.post_num);
				console.log(post);
				console.log(post.post_num);
				return post;
			}  {
				const kotchPost = await kotchan.buildPost(data, true);
				messageBuilder.parseReplies(kotchPost, kotchan.buffer);
				kotchan.buffer.sendPost(kotchPost);
				await kotchan.buffer.addPost(kotchPost);
				kotchan.buffer.sortPosts();
				await kotchan.buffer.save();
			}
		// Timeout only if expecting own post to be coming coming.
		}, (excpectingDuplicate) ? 100 : 0);

	});

	kotchan.socket.on('user_count', function (data) { 
		console.log(data);
	});

	kotchan.socket.on("connect_error", (err) => {
		console.info("connect_error");
		console.error(err);
	});

	kotchan.socket.io.on("error", (err) => {
		console.info("sockio_error");
		console.error(err);
	});

	kotchan.socket.connect();

});
