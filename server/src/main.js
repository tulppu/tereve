const AsyncEventEmitter = require('async-eventemitter');


class ChatEmitter extends AsyncEventEmitter {

	emit(action, args) {
		return new Promise((resolve, reject) => {
			super.emit(action, args, (err) => {
				if (err) {
					if (err instanceof global.app.exceptions.TereveBaseException === false) {
						console.error("Tuntematon virhe!");
						console.error(err);
					}
					return reject(err);
				}
				return resolve(true);
			});
		});
	}
}



class TereveBaseException extends Error {
	
	constructor(error = "Something went wrong!", desc=null, code=null) {
		super(error);
		this.name = this.constructor.name;
		Error.captureStackTrace(this, this.constructor);
		this.event = "response.error";

		this.failed_action = null;

		this.error = error;
		this.desc = desc;
		this.code = code;

		this.http_code = 500;
	}

	sendToClient(client_session, request) {
		this.failed_action = (request) ? request.get("action") : null;

		if (global.app.configs.server.debug) {
			Object.defineProperty(this, "request", request);
		}

		client_session.send(this);
	}
}

module.exports.exceptions = {
	TereveBaseException: TereveBaseException,

	InvalidRequestException: class InvalidRequestException extends TereveBaseException {
			constructor(desc=null) {
					super("Invalid request!", desc, 'ERR_INVALID_REQUEST');
					this.http_code = 400;
			}
	},
	LimitReachedException: class LimitReachedException extends TereveBaseException {
		constructor(desc=null) {
				super("Limit reached!", desc, 'TOO_MANY_REQUESTS');
				this.http_code = 429;
		}
	},
	AuthMissingException: class AuthMissingException extends TereveBaseException {
			constructor(desc=null) {
					super("Authentication missing", desc, 'ERR_AUTHENTICATION_MISSING');
					this.http_code = 401;
			}
	},
	InsufficientPermissionsException: class InsufficientPermissionsException extends TereveBaseException {
		constructor(desc=null) {
				super("Insufficient permissions.", desc, 'ERR_INSUFFICIENT_PERMISSIONS');
				this.http_code = 401;
		}
	},
	UnauthorisedActionException: class UnauthorisedActionException extends TereveBaseException {
		constructor(desc=null) {
				super("Action is not authorised", desc, 'ERR_ACTION_NOT_AUTHHORISED');
				this.http_code = 403;
		}
	},
	ActionUnavailableException: class ActionUnavailableException extends TereveBaseException {
			constructor(desc) {
					super("Action is not available", desc, 'ERR_ACTION_UNAVAILABLE');
					this.http_code = 405;
			}
	},
	ResourceNotFoundException: class ResourceNotFoundException extends TereveBaseException {
		constructor(resource) {
				super("Resource was not found from server", resource, 'ERR_RESOURCE_NOT_FOUND');
				this.http_code = 404;
		}
	},
	ServerErrorException: class ServerErrorException extends TereveBaseException {
			constructor(desc=null) {
					super("Server error!", desc, 'ERR_INTERNAL_ERROR');
					this.http_code = 500;
			}
	},
	FieldRequiredException: class FieldRequiredException extends TereveBaseException {
			constructor(field) {
					super(`Field '${field}' is not set`, null, 'ERR_FIELD_REQUIRED');
					this.http_code = 400;
			}
	},
	InvalidConfigException: class InvalidConfigException extends TereveBaseException {
			constructor(message) {
					super(message, null, 'ERR_INVALID_CONFIG');
					this.http_code = 400;
			}
	}
};


const chatEventEmitter = new ChatEmitter();
chatEventEmitter.setMaxListeners(20);

// Global global.app.variables.
module.exports.variables = {
	// connected clients
	//client_sessions: new Map(),
	sessions: new Map(),
	userSessions: new Map(), // Userid [sessions..]
	channelBuffers: new Map(),
	privateBuffers: new Map(),
	ws: null,
	eventEmitter: chatEventEmitter,
};
