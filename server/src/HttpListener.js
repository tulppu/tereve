
const Request = require(global.srcRoot + "/http/Request.js");
const Response = require(global.srcRoot + "/http/Response.js");

const https = require('https');
const http = require('http');

const Formidable = require('formidable');

const WebSocket = require('ws');
const fs = require('fs');

const { randomUUID } = require('crypto');

//const mime = require("mime-types");
const mediaUtils = require("./utils/Media");

// Can be used by addons.
module.exports.serveNonApiRequest = (async (params) => {
	return null;
});


module.exports.openHttpListener = (started) => {

	let wsOptions = {
		maxPayload: 1024,
		/*
		perMessageDeflate: {
			zlibDeflateOptions: {
				// See zlib defaults.
				chunkSize: 1024,
				memLevel: 7,
				level: 3
			},
			zlibInflateOptions: {
				chunkSize: 10 * 1024
			},
			// Other options settable:
			clientNoContextTakeover: true, // Defaults to negotiated value.
			serverNoContextTakeover: true, // Defaults to negotiated value.
			serverMaxWindowBits: 10, // Defaults to negotiated value.
			// Below options specified as default values.
			concurrencyLimit: 10, // Limits zlib concurrency for perf.
			
			// Size (in bytes) below which messages should not be compressed.
			threshold: 1024 
		},*/
		perMessageDeflate: false,
	};
	var httpOptions = {};

	var wss = null;

	const SSLEnabled = global.app.configs.server.websocket.ssl_cert && global.app.configs.server.websocket.ssl_key;

	if (SSLEnabled) {
		try {
			httpOptions.cert = fs.readFileSync(global.app.configs.server.websocket.ssl_cert);
			httpOptions.key = fs.readFileSync(global.app.configs.server.websocket.ssl_key);
		} catch (e) {
			console.error("\n\nFailed to read certificate\n");
			console.error(e + "\n\n");
			const process = require('process');
			process.exit(1);
		}
	}


	const server = (SSLEnabled) ? https.createServer(httpOptions) : http.createServer(httpOptions);

	server.listen(global.app.configs.server.websocket.port, global.app.configs.server.websocket.address, function listening() {
		const addr = server.address();
		console.info(`Listening for requests at ${addr.address}:${addr.port}..`);
	});

	server.on("error", (err) => console.error(err)); 
	server.on("clientError", (err) => console.info(err));

	server.on("request", async (rawRequest, rawResponse) => {

		const ip_address = (global.app.configs.server.websocket.reverse_proxied)
			? rawRequest.headers['x-forwarded-for'] 
			: rawRequest.connection.remoteAddress;

		const session = global.app.variables.sessions.get(rawRequest.headers.token);


		await global.app.variables.eventEmitter.emit("httpRequest", {
			request: rawRequest,
			ipAddress: ip_address,
			session_token: rawRequest.headers.token,
			session: session
		});


		if (rawRequest.url.length > 1024) {
			rawResponse.writeHead(400, {'content-type': 'text/plain'});
			rawResponse.end();
			return;
		}

		const attachmentConfigs = global.app.configs.attachments;
		const acceptedExtentions = attachmentConfigs.acceptedExtentions;


		/*
		if (rawRequest.url.startsWith(global.app.configs.server.websocket.endpoint_root + "stream")) {
			global.app.variables.eventEmitter.emit("s.stream", {
				session: session,
				ipAddress: ip_address,
				request: rawRequest,
				response: rawResponse,
				session_token: rawRequest.headers.token,
			}).then(() => {
				return;
			})
			return;
		}*/

		const endpointRoot =  `${global.app.configs.server.websocket.endpoint_root}`;

		const apiEndpoint = `${endpointRoot}api/`;
		const streamEndpoint = `${endpointRoot}stream/`;

		let urlWitoutQs = rawRequest.url;
		//let qsData = {};

		if ((qsStart = rawRequest.url.indexOf("?")) > 0) {
			urlWitoutQs = rawRequest.url.substring(0, qsStart);
		}


		console.log(rawRequest.url, urlWitoutQs);

		if (urlWitoutQs.startsWith(streamEndpoint)) {
			//const id = rawRequest.url.substr(rawRequest.url.length - streamEndpoint.length);
			const id = rawRequest.url.substr(streamEndpoint.length);
			global.app.variables.eventEmitter.emit("streamRequest", {
				request: rawRequest,
				response: rawResponse,
				ipAddress: ip_address,
				session_token: rawRequest.headers.token,
				id: id,
				session: session
			});
	
			return;
		}
		

		const action = urlWitoutQs.substring(apiEndpoint.length);
		const eventName = `s.${action}`;

		if (!global.app.variables.eventEmitter.eventNames().find((it) => it === eventName)) {
			rawResponse.writeHead(404, {'content-type': 'text/plain'});
			rawResponse.end();
			return;
		}


		if (!urlWitoutQs.startsWith(apiEndpoint)) {
			const served = await module.exports.serveNonApiRequest({
				request: rawRequest,
				response: rawResponse,
				ipAddress: ip_address,
				session_token: rawRequest.headers.token,
				session: session
			});
			
			if (!served) {
				rawResponse.writeHead(404, {'content-type': 'text/plain'});
				rawResponse.end();
			}

			return;
		}

		const form = new Formidable();
		form.uploadDir = global.app.configs.server.storage.directory + "tmp/";

		form.maxFields = 5;
		form.multiples = false;
		form.hash = 10;

		form.maxFileSize = attachmentConfigs.maxFieldsSize;
		form.maxFieldsSize = attachmentConfigs.maxFieldsSize;


		form.on ('fileBegin', function(name, file){ 

			//let ext = "." + mime.extension(file.type);
			mediaUtils.getExtFromMime(file.type).then((ext) => {
				ext = "." + ext;

				if (file.type === "application/vnd.adobe.flash.movie") {
					ext = ".sfw";
				}

				const isSupported = acceptedExtentions.find((it) => it === ext);
				if (isSupported) return;


				file.open = () => {};
				file.write = () => {};
				file.end = () => {};

				const errMsg = `Unaccepted filetype '${file.type}/.${ext}'`;
				this.emit('error', new global.app.exceptions.TereveBaseException(errMsg));
			});
		});


		form.parse(rawRequest, async(err, fields, files) => {

			const httpRequest = new Request(fields, files, ip_address, rawRequest);
			const httpResponse = new Response(rawResponse);

			if (err) {
				if (err instanceof global.app.exceptions.TereveBaseException) {
					httpResponse.setResponse({error: err.toString()});
					httpResponse.setResponseCode(400);
					httpResponse.send();
				} else {
					console.error("Form parse error");	
					console.error(err);

					httpResponse.setResponseCode(500);
					httpResponse.send();

				}
				return;
			}

	

			if (session && session.ipAddress && session.ipAddress !== ip_address) {
				console.info("Ip address doesn't match");
				// TODO: log and stuff
				return;
			}

			// TODO: Do not allow to commit other than authentication actions if session is not set.
			if (global.app.variables.eventEmitter.listenerCount("s." + action).length === 0) {
				httpResponse.setResponse({message: `No such action as '${action}'`});
				httpResponse.setResponseCode(404);
				httpResponse.senfIfNotSent();
				return;
			} 

			try {
				await global.app.variables.eventEmitter.emit("s." + action, {
					request			: httpRequest,
					response		: httpResponse,
					session_token	: rawRequest.headers.token,
					session			: session
				});

				httpResponse.senfIfNotSent();
	
			} catch (error) {
				console.error("got err on action " + action);
				if (error instanceof global.app.exceptions.TereveBaseException) {
					httpResponse.setResponse(error);
					httpResponse.setResponseCode(error.http_code || 500);
				} else {
					console.log("??");
					httpResponse.setResponse({message: 'Server error'});
					httpResponse.setResponseCode(500);

					console.error(error);
				}
				httpResponse.senfIfNotSent();
			}
		});
	});

	wsOptions.server = server;
	wss = new WebSocket.Server(
		wsOptions
	);

	started();


	function exitSignalHandler(code) {
		console.info(`\n${code} received\n`);
		
		if (code === "SIGINT") {
			console.info(`Closing ${wss.clients.length} connections..`);
			wss.clients.forEach((it) => it.close());
			console.info("..connections closed.\n");
		}

		console.info("Closing server..")
		server.close((e) => {
			if (e) {
				console.error("Server close error");
				console.error(e);
				return;
			}
			console.info("..server closed.\n\nTerminating.");
			process.exit(0);
		});
	}

	process.once('SIGTERM', function (code) {
		exitSignalHandler(code);
	});
	process.once('SIGINT', function (code) {
		exitSignalHandler(code);
	});


	global.app.variables.wss = wss;


	wss.on("error", function (err) {
		console.error("Server error");
		console.error(err);
	});


	wss.on("connection", async function (client) {

		//client.id = uuidv4();
		client.id = randomUUID();

		// PINGs without PONGs
		client.pignsSent = 0;

		client.pingInterval = setInterval(() => {

			client.pignSent++;

			if (client.readyState !== WebSocket.OPEN) {
				console.log("Found bad client, closing?");
				closeClient(client);
				return;
			}

			if (client.pignSent >= 3) {
				console.log("Timeouting.");
				//client.terminate();
				client.close();
				closeClient(client);
			} else {
				client.ping(null,null, true);
			}
		}, 15 * 1000)
		

		client.on("pong", () => client.pignSent = 0);

		client.address = (global.app.configs.server.websocket.reverse_proxied) ?
			client.upgradeReq.headers['x-forwarded-for'] :
			client._socket.remoteAddress;
		
		await global.app.variables.eventEmitter.emit("s.opened", {
			socket: client,
		});


		client.on("error", function (err) {
			console.info("Client error");
			console.info(err.message);
		});

		async function closeClient()
		{
			clearInterval(client.pingInterval);

			const endpoint = `${global.app.configs.server.websocket.endpoint_root}/socket`;
			const sessionToken = client.upgradeReq.url.substr(endpoint.length);
			const session = client.session;

			console.log("täh suljetaan?");

			if (!session) {
				console.info("\n\nNo session?\n\n");
				return;
			}

			session.joinedBuffers.forEach((buf) => buf.part(session));

			await global.app.variables.eventEmitter.emit("s.closed", {
				session: session,
			}, null);


			//const leftUserSessions =
			//	global.app.variables.userSessions.get(session.profile.id).filter((it) => it.socket.id !== client.id);
			const clientSessions = global.app.variables.userSessions.get(session.profile.id);

			const sessionIndex = clientSessions?.findIndex((it) => it.socket.id === client.id);
			if (sessionIndex >= 0) {
				clientSessions.splice(sessionIndex, 1);
			}
			//const leftUserSessions = sessionClients.filter((it) => it.socket.id !== client.id);

			if (clientSessions?.length) {
				global.app.variables.userSessions.set(session.profile.id, clientSessions);
			} else {
				global.app.variables.userSessions.delete(session.profile.id);
			}

			//const sessionToken = socket.upgradeReq.url.substr("/socket/".length);
			global.app.variables.sessions.delete(sessionToken);

			client.close();
			process.nextTick(() => {
				if ([client.OPEN, client.CLOSING].includes(client.readyState)) {
					// Socket still hangs, hard close
					client.terminate();
				}
			});
		}

		client.addEventListener("close", async function (e) {
			console.log("Close detected");
			closeClient()
		});
	});
};

