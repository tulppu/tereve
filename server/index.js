#!/bin/node

const configPath = "./configs.js";
const path = require('path');

global.srcRoot = path.resolve(__dirname + "/src/");
global.app = require("./src/main.js");


function loadConfigs() {
	try {
		const configs = require(configPath);
		global.app.configs = configs;
	} catch(e) {
		console.error(e);
		console.error("Failed to read config file. You might want to rename or copy config.js.example to config.js.");
	}
}

loadConfigs();

const fs = require('fs');

fs.watch(configPath, (curr, prev) => {
	console.log(`Reloading ${configPath}`);
	Object.keys(require.cache).forEach(function(key) { delete require.cache[key] })
	loadConfigs();
});


// https://stackoverflow.com/a/64550489

setInterval(() => {
	const formatMemoryUsage = (data) => `${Math.round(data / 1024 / 1024 * 100) / 100} MB`;
	const memoryData = process.memoryUsage();

	const memoryUsage = {
		rss: `${formatMemoryUsage(memoryData.rss)} -> Resident Set Size - total memory allocated for the process execution`,
		heapTotal: `${formatMemoryUsage(memoryData.heapTotal)} -> total size of the allocated heap`,
		heapUsed: `${formatMemoryUsage(memoryData.heapUsed)} -> actual memory used during the execution`,
		external: `${formatMemoryUsage(memoryData.external)} -> V8 external memory`,
	};

	console.log(memoryUsage);
}, 30 * 1000);



global.currentTimestamp = () => Math.floor(Date.now() / 1000);

const { loadScripts} = require('./src/ScriptsLoader.js');

const MongoClient = require("./src/database/MongoClient.js");


const { openHttpListener } = require("./src/HttpListener.js");
const rl = require('readline');




(async() => {
	
	global.app.DB = await MongoClient.getDatabase();
	console.info("Database up!");

	if (process.argv.indexOf("--init-collection") !== -1) {
		const resetDatabase = await new Promise((resolve) => {
			rl.question("Reset & initalise database collections?.\n", (answer) => {
				return resolve(answer === "yes" || answer === 'y');
			});
		});

		if (resetDatabase) {
			await MongoClient.initCollections();
			console.info("Database initalised");
		}
	}


	console.info("Loading scripts..");
	const scripts = await loadScripts("./src/subscribers/");
	console.info(scripts.length +  " scritps loaded");

	console.info("Loading addons..");
	const addons = await loadScripts("./src/addons/");
	console.info(addons.length + " addons loaded");


	openHttpListener(async() => {
		console.info("Opened HTTP listener");
	});
})();
