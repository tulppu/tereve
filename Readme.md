### PDF:n pikkukuvitus

Lisää tiedoston /etc/ImageMagick-6/policy.xml \<policymap>:n sisälle:

\<policy domain="coder" rights="read | write" pattern="PDF" />


### Kansikuvallisen mp3:n MIME-tunnistuksen korjaus

/etc/magic -tiedostoon:

#MPEG Layer 3 sound files
0       beshort     &0xffe0     audio/mpeg    
!:mime  audio/mpeg                 
#MP3 with ID3 tag                             
0       string      ID3     audio/mpeg        
!:mime  audio/mpeg                        


